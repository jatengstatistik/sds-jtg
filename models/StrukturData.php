<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "struktur_data".
 *
 * @property int $id
 * @property int $parent_id
 * @property int $data_id
 * @property string $nama_struktur Nama yang akan di tampilkan
 * @property string $parameter Field yang akan dibaca APInya
 * @property string $tipe_data
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Data $data
 * @property StrukturData $parent
 * @property StrukturData[] $strukturDatas
 */
class StrukturData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'struktur_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parameter_api_id', 'created_at', 'updated_at'], 'integer'],
            [['nama_struktur', 'parameter', 'relasi', 'grafik'], 'string', 'max' => 50],
            // [['data_id'], 'exist', 'skipOnError' => true, 'targetClass' => Data::className(), 'targetAttribute' => ['data_id' => 'id']],
            // [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => StrukturData::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'parameter_api_id' => Yii::t('app', 'Data ID'),
            'nama_struktur' => Yii::t('app', 'Nama Kolom'),
            'parameter' => Yii::t('app', 'Field Kolom'),
            'relasi' => Yii::t('app', 'Relasi'),
            'grafik' => Yii::t('app', 'Grafik'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameterApi()
    {
        return $this->hasOne(ParameterApi::className(), ['id' => 'parameter_api_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    // public function getParent()
    // {
    //     return $this->hasOne(StrukturData::className(), ['id' => 'parent_id']);
    // }

    /**
     * @return \yii\db\ActiveQuery
     */
    // public function getStrukturDatas()
    // {
    //     return $this->hasMany(StrukturData::className(), ['parent_id' => 'id']);
    // }
}
