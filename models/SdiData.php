<?php

namespace app\models;

use Yii;
use app\components\UploadBehavior;
use app\models\FileModel;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\httpclient\Client;

/**
 * This is the model class for table "sdi_data".
 *
 * @property int $id
 * @property int $instansi_id
 * @property int $api_client_id
 * @property int $uploaded_file_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * @property int $lock
 * @property string $title
 * @property int $year
 *
 * @property ApiClient $apiClient
 * @property Instansi $instansi
 * @property UploadedFile $uploadedFile
 */
class SdiData extends \yii\db\ActiveRecord
{
    // public $file;
    const STATUS_PRIVATE = 1;
    const STATUS_PUBLIC = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sdi_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['instansi_id',  'created_at', 'updated_at', 'created_by', 'updated_by', 'deleted_by', 'lock', 'sdi_category_id','public'], 'integer'],
            // 'api_client_id', 'uploaded_file_id','year',
            [['title','name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            // [['api_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApiClient::className(), 'targetAttribute' => ['api_client_id' => 'id']],
            [['instansi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Instansi::className(), 'targetAttribute' => ['instansi_id' => 'id']],
            // [['file'], 'file','extensions' => 'xlsx' ,'maxSize'=>1024 * 1024 * 3],
            // [['uploaded_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => UploadedFile::className(), 'targetAttribute' => ['uploaded_file_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sdi_category_id' => Yii::t('app', 'Categories'),
            'instansi_id' => Yii::t('app', 'Instansi'),
            'public' => Yii::t('app', 'Status Data'),
            // 'uploaded_file_id' => Yii::t('app', 'Uploaded File'),
            'created_at' => Yii::t('app', 'Dibuat Pada'),
            'updated_at' => Yii::t('app', 'Diupdate Pada'),
            'created_by' => Yii::t('app', 'Dibuat Oleh'),
            'updated_by' => Yii::t('app', 'Diupdate Oleh'),
            'deleted_by' => Yii::t('app', 'Dihapus Oleh'),
            'lock' => Yii::t('app', 'Lock'),
            'title' => Yii::t('app', 'Judul'),
            // 'year' => Yii::t('app', 'Tahun'),
        ];
    }

    public function behaviors()
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'name',
                'ensureUnique'=>true,
            ],
            // 'upload' => [
            //     'class' => UploadBehavior::className(),
            //     'attribute' => 'file', // required, use to receive input file
            //     'savedAttribute' => 'uploaded_file_id', // optional, use to link model with saved file.
            //     'uploadPath' => '@runtime/upload/sdi-files', // saved directory. default to '@runtime/upload'
            //     'autoSave' => true, // when true then uploaded file will be save before ActiveRecord::save()
            //     'autoDelete' => true, // when true then uploaded file will deleted before ActiveRecord::delete()
            // ]
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord)
        {           
            $this->public = $this->public==1?1:0;
        }else{
            $this->public = $this->public==1?1:0;
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResources()
    {
        return $this->hasMany(SdiResource::className(), ['sdi_data_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApiClient()
    {
        return $this->hasOne(ApiClient::className(), ['id' => 'api_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstansi()
    {
        return $this->hasOne(Instansi::className(), ['id' => 'instansi_id']);
    }

    public function getUserCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUserUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function getListInstansi()
    {
        $merge = [];
        $list = Instansi::find()->Where('jenis_instansi_id != 3')->asArray()->all();
        // $list = Instansi::find()->asArray()->all();
        foreach ($list as $value) {
            $merge[] = [
                'id' => $value['id'],
                // 'nama' => $value['nama'].' - '.strtoupper(substr($value['nama'], 0,3)),
                'nama' => $value['nama_instansi'].' - '.$value['singkatan_instansi'],
            ];
        }
        return ArrayHelper::map($merge, 'id', 'nama');
    }

}