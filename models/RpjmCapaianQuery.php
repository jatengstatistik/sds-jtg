<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RpjmCapaian]].
 *
 * @see RpjmCapaian
 */
class RpjmCapaianQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return RpjmCapaian[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return RpjmCapaian|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
