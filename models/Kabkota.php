<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "kab_kota".
 *
 * @property int $id Id Kabupaten/Kota
 * @property string $kode_kab Kode Kab/Kota Sesuai Data BPS
 * @property string $tipe_kab
 * @property string $nama_kab
 * @property string $detail_kab
 * @property string $ibu_kota
 * @property string $kepala_kab
 * @property int $updated_at
 * @property int $created_at
 */
class Kabkota extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kab_kota';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_kab', 'nama_kab', 'updated_at', 'created_at'], 'required'],
            [['updated_at', 'created_at'], 'integer'],
            [['kode_kab'], 'string', 'max' => 2],
            [['tipe_kab'], 'string', 'max' => 15],
            [['nama_kab', 'detail_kab', 'ibu_kota'], 'string', 'max' => 60],
            [['kepala_kab'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Id Kabupaten/Kota'),
            'kode_kab' => Yii::t('app', 'Kode Kab/Kota Sesuai Data BPS'),
            'tipe_kab' => Yii::t('app', 'Tipe Kab'),
            'nama_kab' => Yii::t('app', 'Nama Kab'),
            'detail_kab' => Yii::t('app', 'Detail Kab'),
            'ibu_kota' => Yii::t('app', 'Ibu Kota'),
            'kepala_kab' => Yii::t('app', 'Kepala Kab'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
}
