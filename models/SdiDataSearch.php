<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SdiData;

/**
 * SdiDataSearch represents the model behind the search form of `app\models\SdiData`.
 */
class SdiDataSearch extends SdiData
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['instansi_id', 'title'], 'safe'],
            // 'api_client_id', 'uploaded_file_id', 'year'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SdiData::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // if (empty($this->created_by || $this->updated_by)) {
            $query->joinWith('instansi');   
        // }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'instansi_id' => $this->instansi_id,
            // 'api_client_id' => $this->api_client_id,
            // 'uploaded_file_id' => $this->uploaded_file_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
            'lock' => $this->lock,
            // 'year' => $this->year,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
        ->orFilterWhere(['like', 'instansi.nama_instansi', $this->instansi_id]);

        return $dataProvider;
    }
}
