<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "api_body".
 *
 * @property int $id
 * @property int $api_client_id
 * @property string $label
 * @property string $attribute
 * @property string $value
 *
 * @property ApiClient $apiClient
 */
class ApiBody extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'api_body';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // [['label', 'attribute'], 'required'],
            [['api_client_id'], 'integer'],
            [['label', 'attribute', 'value'], 'string', 'max' => 100],
            [['api_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApiClient::className(), 'targetAttribute' => ['api_client_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'api_client_id' => 'Api Client ID',
            'label' => 'Label',
            'attribute' => 'Attribute',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApiClient()
    {
        return $this->hasOne(ApiClient::className(), ['id' => 'api_client_id']);
    }
}
