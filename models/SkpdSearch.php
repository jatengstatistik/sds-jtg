<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Skpd;

/**
 * SkpdSearch represents the model behind the search form of `app\models\Skpd`.
 */
class SkpdSearch extends Skpd
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            // [['created_at', 'updated_at'], 'integer'],
            [['kode_skpd', 'nama_skpd', 'akronim_skpd', 'alamat_skpd', 'telpon_skpd', 'nama_pic_skpd', 'telepon_pic_skpd'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Skpd::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'kode_skpd', $this->kode_skpd])
            ->andFilterWhere(['like', 'nama_skpd', $this->nama_skpd])
            ->andFilterWhere(['like', 'akronim_skpd', $this->akronim_skpd])
            ->andFilterWhere(['like', 'alamat_skpd', $this->alamat_skpd])
            ->andFilterWhere(['like', 'telpon_skpd', $this->telpon_skpd])
            ->andFilterWhere(['like', 'nama_pic_skpd', $this->nama_pic_skpd])
            ->andFilterWhere(['like', 'telepon_pic_skpd', $this->telepon_pic_skpd]);

        return $dataProvider;
    }
}
