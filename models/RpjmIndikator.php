<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "rpjm_indikator".
 *
 * @property int $id
 * @property int $organisasi_id
 * @property int $urusan_id
 * @property string $indikator
 * @property string $slug_indikator
 * @property string $satuan
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property RpjmCapaian[] $rpjmCapaians
 * @property RpjmOrganisasi $organisasi
 * @property RpjmUrusan $urusan
 */
class RpjmIndikator extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rpjm_indikator';
    }

    public function behaviors()
    {
       return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            // 'sluggable' => [
            //     'class' => SluggableBehavior::className(),
            //     'attribute' => 'name',
            //     'slugAttribute' => 'slug',
            // ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['organisasi_id', 'urusan_id'], 'required'],
            [['organisasi_id', 'urusan_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['indikator', 'slug_indikator'], 'string', 'max' => 255],
            [['indikator'], 'unique'],
            [['satuan'], 'string', 'max' => 100],
            [['organisasi_id'], 'exist', 'skipOnError' => true, 'targetClass' => RpjmOrganisasi::className(), 'targetAttribute' => ['organisasi_id' => 'id']],
            [['urusan_id'], 'exist', 'skipOnError' => true, 'targetClass' => RpjmUrusan::className(), 'targetAttribute' => ['urusan_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'organisasi_id' => Yii::t('app', 'Organisasi'),
            'urusan_id' => Yii::t('app', 'Urusan'),
            'indikator' => Yii::t('app', 'Indikator'),
            'slug_indikator' => Yii::t('app', 'Slug Indikator'),
            'satuan' => Yii::t('app', 'Satuan'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRpjmCapaians()
    {
        return $this->hasMany(RpjmCapaian::className(), ['indikator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganisasi()
    {
        return $this->hasOne(RpjmOrganisasi::className(), ['id' => 'organisasi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUrusan()
    {
        return $this->hasOne(RpjmUrusan::className(), ['id' => 'urusan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * {@inheritdoc}
     * @return RpjmIndikatorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RpjmIndikatorQuery(get_called_class());
    }
}
