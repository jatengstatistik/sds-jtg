<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RpjmCapaian;

/**
 * RpjmCapaianSearch represents the model behind the search form of `app\models\RpjmCapaian`.
 */
class RpjmCapaianSearch extends RpjmCapaian
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'indikator_id', 'tahun_data', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['target_rkpd', 'target_dppa', 'capaian'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RpjmCapaian::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'indikator_id' => $this->indikator_id,
            'tahun_data' => $this->tahun_data,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'target_rkpd', $this->target_rkpd])
            ->andFilterWhere(['like', 'target_dppa', $this->target_dppa])
            ->andFilterWhere(['like', 'capaian', $this->capaian]);

        return $dataProvider;
    }
}
