<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\data\ArrayDataProvider;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "data_dasar".
 *
 * @property int $id
 * @property int $files
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class DataDasar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rpjm_data_dasar';
    }

    public function behaviors()
    {
       return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            // 'sluggable' => [
            //     'class' => SluggableBehavior::className(),
            //     'attribute' => 'name',
            //     'slugAttribute' => 'slug',
            // ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['organisasi_id', 'tahun_data', 'urusan_id'], 'required'],
            [['organisasi_id', 'tahun_data', 'urusan_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            // [['organisasi_id', 'urusan_id'],'safe'],
            [['file_name'], 'string'],
            [['files'],'file','skipOnEmpty' => true, 'extensions' => 'xlsx, csv, json, geojson'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'organisasi_id' => Yii::t('app', 'Instansi'),
            'urusan_id' => Yii::t('app', 'Urusan'),
            'tahun_data' => Yii::t('app', 'Tahun Data'),
            'files' => Yii::t('app', 'Files'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return DataDasarQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DataDasarQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganisasi()
    {
        return $this->hasOne(RpjmOrganisasi::className(), ['id' => 'organisasi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUrusan()
    {
        return $this->hasOne(RpjmUrusan::className(), ['id' => 'urusan_id']);
    }

    public function getDataRpjm()
    {
        $inputFileName = $this->files;

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($inputFileName);
        $spreadsheetMetaData = $spreadsheet->getProperties();
        $data = $spreadsheet->getActiveSheet()->toArray(null,true,true,true);
        $cleanData = $this->cleanDataIndikator($data);
        return $cleanData;
    }

    public function cleanDataIndikator($data=array())
    {
        // unset($data[0]);
        //  && isset($value['F']) && isset($value['H'])
        foreach ($data as $value) {
            if (!empty($value['B']) && !empty($value['C']) && is_numeric($value['D'])) {
                $clean[] = [
                    'indikator' => $value['B'],
                    // 'slug_indikator' => preg_replace('/\s+/', ' ',$value[1]), 
                    // str_replace(" ","-",$value[1]),
                    'satuan' => $value['C'],
                    'rkpd' => $value['D'],
                    'dppa' => $value['F'],
                    'realisasi' => $value['H'],
                ];    
            }
        }
        // Delete First Row
        array_shift($clean);
        return $clean;
    }

    public function getFileAttributeGridView($sheetData='')
    {
        
        $changeKeyArray[] = ['class' => 'yii\grid\SerialColumn'];
        foreach ($sheetData[0] as $key => $value) {
            $changeKeyArray[] = [
                'attribute' => $key,
                'label' => ucwords($key),
            ];
        }

        return $changeKeyArray;
    }


    public function getDataProvider($dataset)
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => $dataset,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => $this->getFileAttributeGridView($dataset),
            ],
        ]);

        return $dataProvider;
    }
}
