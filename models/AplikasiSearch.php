<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Aplikasi;

/**
 * AplikasiSearch represents the model behind the search form of `app\models\Aplikasi`.
 */
class AplikasiSearch extends Aplikasi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nama_aplikasi','url_aplikasi', 'slug_nama_aplikasi', 'username', 'password', 'token', 'created_at', 'updated_at','instansi_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Aplikasi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('instansi');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'tree_id' => $this->tree_id,
            'instansi_id' => $this->instansi_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'nama_aplikasi', $this->nama_aplikasi])
            ->andFilterWhere(['like', 'url_aplikasi', $this->url_aplikasi])
            ->andFilterWhere(['like', 'slug_nama_aplikasi', $this->slug_nama_aplikasi])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->orFilterWhere(['like', 'instansi.nama_instansi', $this->instansi_id])
            ->andFilterWhere(['like', 'token', $this->token]);

        return $dataProvider;
    }
}
