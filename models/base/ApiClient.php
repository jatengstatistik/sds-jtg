<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "api_client".
 *
 * @property integer $id
 * @property integer $instansi_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property integer $lock
 * @property string $title
 * @property string $notes
 * @property string $method
 * @property string $url
 * @property string $segment
 *
 * @property \app\models\ApiBody[] $apiBodies
 * @property \app\models\Instansi $instansi
 * @property \app\models\ApiHeader[] $apiHeaders
 * @property \app\models\ApiQuery[] $apiQueries
 */
class ApiClient extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    private $_rt_softdelete;
    private $_rt_softrestore;

    public function __construct(){
        parent::__construct();
        $this->_rt_softdelete = [
            'deleted_by' => \Yii::$app->user->id,
            'deleted_at' => date('d-m-Y H:i:s'),
        ];
        $this->_rt_softrestore = [
            'deleted_by' => 0,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
    }

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'apiBodies',
            'instansi',
            'apiHeaders',
            'apiQueries',
            'apiDatas'

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['instansi_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['title', 'notes', 'url', 'segment'], 'string', 'max' => 255],
            [['method'], 'string', 'max' => 20],
            // [['lock'], 'default', 'value' => '0'],
            // [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'api_client';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    // public function optimisticLock() {
    //     return 'lock';
    // }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'instansi_id' => Yii::t('app', 'Instansi ID'),
            'lock' => Yii::t('app', 'Lock'),
            'title' => Yii::t('app', 'Title'),
            'notes' => Yii::t('app', 'Notes'),
            'method' => Yii::t('app', 'Method'),
            'url' => Yii::t('app', 'Url'),
            'segment' => Yii::t('app', 'Segment'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApiBodies()
    {
        return $this->hasMany(\app\models\ApiBody::className(), ['api_client_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstansi()
    {
        return $this->hasOne(\app\models\Instansi::className(), ['id' => 'instansi_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApiHeaders()
    {
        return $this->hasMany(\app\models\ApiHeader::className(), ['api_client_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApiQueries()
    {
        return $this->hasMany(\app\models\ApiQuery::className(), ['api_client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApiDatas()
    {
        return $this->hasMany(\app\models\ApiData::className(), ['api_client_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            // 'uuid' => [
            //     'class' => UUIDBehavior::className(),
            //     'column' => 'id',
            // ],
        ];
    }

    /**
     * The following code shows how to apply a default condition for all queries:
     *
     * ```php
     * class Customer extends ActiveRecord
     * {
     *     public static function find()
     *     {
     *         return parent::find()->where(['deleted' => false]);
     *     }
     * }
     *
     * // Use andWhere()/orWhere() to apply the default condition
     * // SELECT FROM customer WHERE `deleted`=:deleted AND age>30
     * $customers = Customer::find()->andWhere('age>30')->all();
     *
     * // Use where() to ignore the default condition
     * // SELECT FROM customer WHERE age>30
     * $customers = Customer::find()->where('age>30')->all();
     * ```
     */

    /**
     * @inheritdoc
     * @return \app\models\ApiClientQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new \app\models\ApiClientQuery(get_called_class());
        return $query->where(['api_client.deleted_by' => 0]);
    }
}
