<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "aplikasi".
 *
 * @property int $id
 * @property int $tree_id
 * @property string $nama_aplikasi
 * @property string $slug_nama_aplikasi
 * @property string $username
 * @property string $password
 * @property string $token
 * @property int $instansi_id
 * @property string $created_at
 * @property string $updated_at
 */
class Aplikasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aplikasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['instansi_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nama_aplikasi','url_aplikasi'], 'string', 'max' => 150],
            [['slug_nama_aplikasi'], 'string', 'max' => 155],
            [['username'], 'string', 'max' => 50],
            [['password', 'token'], 'string', 'max' => 255],
            // [[]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            // 'tree_id' => Yii::t('app', 'Tree ID'),
            'nama_aplikasi' => Yii::t('app', 'Sumber Data'),
            'slug_nama_aplikasi' => Yii::t('app', 'Slug Nama Aplikasi'),
            'url_aplikasi' => Yii::t('app', 'Alamat Aplikasi'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'token' => Yii::t('app', 'Token'),
            'instansi_id' => Yii::t('app', 'Instansi'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'nama_aplikasi',
                'slugAttribute' => 'slug_nama_aplikasi',
                'ensureUnique'=>true,
            ],
        ];
    }

    public function getInstansi()
    {
        return $this->hasOne(Instansi::className(), ['id' => 'instansi_id']);
    }

    // public function getCategory()
    // {
    //     return $this->hasOne(Category::className, ['tree_id' => 'id']);
    // }
}
