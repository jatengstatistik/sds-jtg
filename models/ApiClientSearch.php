<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApiClient;

/**
 * app\models\ApiClientSearch represents the model behind the search form about `app\models\ApiClient`.
 */
 class ApiClientSearch extends ApiClient
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'instansi_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['title', 'notes', 'method', 'url', 'segment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApiClient::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'instansi_id' => $this->instansi_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
            'lock' => $this->lock,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'notes', $this->notes])
            ->andFilterWhere(['like', 'method', $this->method])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'segment', $this->segment]);

        return $dataProvider;
    }
}
