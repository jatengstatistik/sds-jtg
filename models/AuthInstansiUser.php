<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auth_instansi_user".
 *
 * @property int $id
 * @property int $user_id
 * @property int $instansi_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Instansi $instansi
 * @property User $user
 */
class AuthInstansiUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auth_instansi_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['instansi_id', 'user_id'], 'required'],
            [['user_id', 'instansi_id', 'created_at'], 'integer'],
            [['user_id', 'instansi_id'], 'unique', 'targetAttribute' => ['user_id', 'instansi_id']],
            [['instansi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Instansi::className(), 'targetAttribute' => ['instansi_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'instansi_id' => Yii::t('app', 'Instansi ID'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstansi()
    {
        return $this->hasOne(Instansi::className(), ['id' => 'instansi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
