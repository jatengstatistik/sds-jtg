<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "status_sds".
 *
 * @property int $id
 * @property int $instansi_id
 * @property string $alamat_situs
 * @property string $host
 * @property string $port
 * @property string $username
 * @property string $password
 * @property string $keterangan
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Instansi $instansi
 */
class StatusSds extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'status_sds';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['instansi_id', 'created_at', 'updated_at'], 'integer'],
            [['keterangan'], 'string'],
            [['alamat_situs'], 'string', 'max' => 255],
            [['host', 'username', 'password'], 'string', 'max' => 200],
            [['port'], 'string', 'max' => 5],
            [['instansi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Instansi::className(), 'targetAttribute' => ['instansi_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'instansi_id' => 'Instansi ID',
            'alamat_situs' => 'Alamat Situs',
            'host' => 'Host',
            'port' => 'Port',
            'username' => 'Username',
            'password' => 'Password',
            'keterangan' => 'Keterangan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstansi()
    {
        return $this->hasOne(Instansi::className(), ['id' => 'instansi_id']);
    }
}
