<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Setting;

/**
 * SettingSearch represents the model behind the search form of `app\models\Setting`.
 */
class SettingSearch extends Setting
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['admin_setting_data', 'id', 'site_logo', 'api_pegawai', 'kode_kabupaten', 'created_at', 'updated_at'], 'integer'],
            [['url_open_data','title', 'sub_title', 'skpd', 'nama_kabupaten', 'version'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Setting::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'site_logo' => $this->site_logo,
            'api_pegawai' => $this->api_pegawai,
            'kode_kabupaten' => $this->kode_kabupaten,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'url_open_data', $this->url_open_data])
            ->andFilterWhere(['like', 'sub_title', $this->sub_title])
            ->andFilterWhere(['like', 'skpd', $this->skpd])
            ->andFilterWhere(['like', 'nama_kabupaten', $this->nama_kabupaten])
            ->andFilterWhere(['like', 'version', $this->version]);

        return $dataProvider;
    }
}
