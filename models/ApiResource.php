<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\httpclient\Client;


/**
 * This is the model class for table "sdi_resource".
 *
 * @property int $id
 * @property int $sdi_data_id
 * @property int $api_client_id
 * @property int $upload_file_id
 * @property string $title
 * @property string $name
 * @property int $year
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property ApiClient $apiClient
 * @property SdiData $sdiData
 * @property UploadedFile $uploadFile
 */
class ApiResource extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sdi_resource';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sdi_data_id', 'api_client_id', 'uploaded_file_id', 'year', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['title', 'name'], 'string', 'max' => 255],
            [['api_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApiClient::className(), 'targetAttribute' => ['api_client_id' => 'id']],
            [['sdi_data_id'], 'exist', 'skipOnError' => true, 'targetClass' => SdiData::className(), 'targetAttribute' => ['sdi_data_id' => 'id']],
            // [['uploaded_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => UploadedFile::className(), 'targetAttribute' => ['upload_file_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sdi_data_id' => Yii::t('app', 'Sdi Data ID'),
            'api_client_id' => Yii::t('app', 'Api Client ID'),
            'uploaded_file_id' => Yii::t('app', 'Upload File ID'),
            'title' => Yii::t('app', 'Judul'),
            'name' => Yii::t('app', 'Nama'),
            'year' => Yii::t('app', 'Tahun'),
            'created_at' => Yii::t('app', 'Dibuat Pada'),
            'updated_at' => Yii::t('app', 'Diupdate Pada'),
            'created_by' => Yii::t('app', 'Dibuat Oleh'),
            'updated_by' => Yii::t('app', 'Diupdate Oleh'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApiClient()
    {
        return $this->hasOne(ApiClient::className(), ['id' => 'api_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSdiData()
    {
        return $this->hasOne(SdiData::className(), ['id' => 'sdi_data_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadFile()
    {
        return $this->hasOne(UploadedFile::className(), ['id' => 'upload_file_id']);
    }

    public function getUserCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUserUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function behaviors()
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'name',
                'ensureUnique'=>true,
            ],
        ];
    }

    public function getListTahun()
    {
        $year = [];
        for ($i = date('Y'); $i >= 2000; $i--) {
            $year[] = ['id'=>$i, 'year'=>$i];
        }
        return ArrayHelper::map($year, 'id', 'year');
    }

    public function getListMethod()
    {
        $method = [];
        $method[] = ['id'=>'GET', 'method'=>'GET'];
        $method[] = ['id'=>'POST', 'method'=>'POST'];
        // $method[] = ['id'=>'POST', 'method'=>''];
        return ArrayHelper::map($method, 'id', 'method');
    }

    public function getApiQueriesParam()
    {
        $querys = $this->apiClient->apiQueries;
        $gets = Yii::$app->request->get();
        unset($gets['id']);
        
        foreach ($querys as $key => $value) {
               $queryVal[$value['attribute']] = $value['value'];
           }

        if (!empty($gets)) {
            // if param page empty() get data from apiQueries
            // $queryLimit['limit'] = $this->getQuerysValue('attribute','limit')['value'];
            $queryLimit[$this->getQuerysValue('label','Offset')['attribute']] = (empty($gets['page'])) ? $this->getQuerysValue('attribute','offset')['value'] : $gets['page']*$this->getQuerysValue('attribute','limit')['value']-$this->getQuerysValue('attribute','limit')['value'];
            $queryVal = array_merge($queryVal,$queryLimit,$gets);
        }
        // echo '<pre>';
        // print_r($queryVal);
        // print_r(array_search('Offset', array_column($arrayQuerys, 'label')));
        // print_r($queryLimit);
        // print_r(array_merge($queryVal,$queryLimit));
        // exit();
        // echo '</pre>';

        return $queryVal; 
    }

    public function getQuerysValue($field,$value)
    {
        $querys = $this->apiClient->apiQueries;
        $arrayQuerys = ArrayHelper::toArray($querys);
        foreach($arrayQuerys as $key => $query)
        {
          if ( $query[$field] === $value )
            return $arrayQuerys[$key];
            // echo '<pre>';
            // print_r(array_search('Offset', array_column($arrayQuerys, 'label')));
            // // print_r(array_search('Limit', $arrayQuerys));
            // print_r($key);
            // exit();
            // echo '</pre>';      
        }
        return false;
    }

    public function getBodiesValue($field,$value)
    {
        $querys = $this->apiClient->apiBodies;
        $arrayBodies = ArrayHelper::toArray($querys);

        foreach($arrayBodies as $key => $query)
        {
          if ( $query[$field] === $value )
            return $arrayBodies[$key];
            // echo '<pre>';
            // print_r(array_search('Offset', array_column($arrayQuerys, 'label')));
            // // print_r(array_search('Limit', $arrayQuerys));
            // print_r($key);
            // exit();
            // echo '</pre>';      
        }
        return false;
    }


    public function getApiClientData()
    {
        $url = $this->apiClient->url;
        $method = $this->apiClient->method;

        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport' // only cURL supports the options we need
        ]);

        try {

            $response = $client->createRequest()
                ->setMethod($this->apiClient->method)
                ->setUrl($this->apiClient->url)
                ->setData($this->getApiQueriesParam())
                ->setOptions([
                    CURLOPT_CONNECTTIMEOUT => 3, // connection timeout
                    CURLOPT_TIMEOUT => 10, // data receiving timeout
                ])
                ->send();
                // echo '<pre>';
                // print_r($response);
                // echo '<br>';
                // // print_r();
                // exit();
                // echo '</pre>';
            
            if ($response->isOk) {
                $responseData = $response->getData(); 
                return $responseData;
            }else{
                return [];
            }
        } catch (\yii\httpclient\Exception $e) {
            // echo 'string';
            return [];
        }
            
    }

    public function getCountApiData()
    {
        $data  = $this->getApiClientData();
        $total = $this->getBodiesValue('label','Total')['attribute'];

        if (empty($data)) {
            $count = 0;
        }else{
            $count = $data[$total];
        }

        // echo '<pre>';
        // print_r($count);
        // exit();
        // echo '</pre>';
        return $count;
    }

    public function getDataApiAttribute()
    {
        $datas = $this->apiClient->apiDatas;
        foreach ($datas as $value) {
            $attrApi[$value['attribute']] = $value['label'];             
        }
        return $attrApi;
    }

    public function getApiData()
    {
        $apiData = $this->getApiClientData();
        if (!empty($apiData[$this->apiClient->segment])) {
            $apiData = $apiData[$this->apiClient->segment];
        }else{
            $apiData[] = ['a'=>'asda','b'=>'asda'];
        }
        foreach ($apiData as $value) {

            $apiDatax[] = array_filter($value,fn ($key) => in_array($key, array_keys($this->getDataApiAttribute())),ARRAY_FILTER_USE_KEY);

        }

        // echo '<pre>';
        // print_r($apiDatax);
        // echo '<br>';
        // // print_r();
        // exit();
        // echo '</pre>';
        
        return $apiDatax;

    }

    public function getApiAttributeGridView()
    {
        $data = $this->apiClient->apiDatas;
        $changeKeyArray[] = ['class' => 'yii\grid\SerialColumn'];
        // $changeKeyArray[] = ['class' => 'yii\grid\DataColumn'];
        foreach ($data as $key => $value) {
            // if (!empty($value)) {
                $changeKeyArray[] = [
                    // 'class' => 'yii\grid\DataColumn',
                    'label' => $value->label,
                    'attribute' => $value->attribute,
                    'filter' => Html::input('text', $value->attribute, (empty(Yii::$app->request->get($value->attribute))) ? '' : Yii::$app->request->get($value->attribute), ['placeholder' => 'Cari '.$value->label,'class'=>'form-control']),
                    'format' => 'html',
                ];
            
        }

        // echo '<pre>';
        // print_r($changeKeyArray);
        // exit();
        // echo '</pre>';

        return $changeKeyArray;
    }

    public function getApiProvider()
    {
        $dataProvider = new \app\components\ArrayDataProvider([
            'allModels' =>  $this->getApiData(),
            'totalCount' => $this->getCountApiData(),
            'pagination' => [
                'pageSize' => $this->getQuerysValue('label','Limit')['value'],
            ],
            // 'sort' => [
            //     'attributes' => $this->getDataApiAttribute(),
            // ],
        ]);
        // echo '<pre>';
        // print_r($this->getApiData($page=null));
        // print_r($this->getApiData(Yii::$app->request->get('page')));
        // print_r($this->getApiClientData());
        // print_r($dataProvider);
        // exit();
        // echo '</pre>';

        return $dataProvider;
    }

}
