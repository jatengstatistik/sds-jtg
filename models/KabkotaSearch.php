<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kabkota;

/**
 * KabkotaSearch represents the model behind the search form of `app\models\Kabkota`.
 */
class KabkotaSearch extends Kabkota
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'updated_at', 'created_at'], 'integer'],
            [['kode_kab', 'tipe_kab', 'nama_kab', 'detail_kab', 'ibu_kota', 'kepala_kab'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Kabkota::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'kode_kab', $this->kode_kab])
            ->andFilterWhere(['like', 'tipe_kab', $this->tipe_kab])
            ->andFilterWhere(['like', 'nama_kab', $this->nama_kab])
            ->andFilterWhere(['like', 'detail_kab', $this->detail_kab])
            ->andFilterWhere(['like', 'ibu_kota', $this->ibu_kota])
            ->andFilterWhere(['like', 'kepala_kab', $this->kepala_kab]);

        return $dataProvider;
    }
}
