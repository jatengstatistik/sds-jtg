<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "instansi".
 *
 * @property int $id
 * @property string $nama_instansi
 * @property string $singkatan_instansi
 * @property string $alamat_instansi
 * @property string $telepon_instansi
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Data[] $datas
 * @property StatusSds[] $statusSds
 * @property User[] $users
 */
class Instansi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'instansi';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'integer'],
            [['jenis_instansi_id'],'safe'],
            [['nama_instansi', 'singkatan_instansi', 'alamat_instansi', 'telepon_instansi'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jenis_instansi_id' => Yii::t('app', 'Jenis Instansi'),
            'nama_instansi' => Yii::t('app', 'Nama Instansi'),
            'singkatan_instansi' => Yii::t('app', 'Singkatan Instansi'),
            'alamat_instansi' => Yii::t('app', 'Alamat Instansi'),
            'telepon_instansi' => Yii::t('app', 'Telepon Instansi'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDatas()
    {
        return $this->hasMany(Data::className(), ['instansi_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisInstansi()
    {
        return $this->hasOne(JenisInstansi::className(), ['id' => 'jenis_instansi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusSds()
    {
        return $this->hasMany(StatusSds::className(), ['instansi_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['instansi_id' => 'id']);
    }
}
