<?php

namespace app\models;

use Yii;
use kartik\tree\TreeView;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "new_tree".
 *
 * @property string $id
 * @property int $root
 * @property int $lft
 * @property int $rgt
 * @property int $lvl
 * @property string $name
 * @property int $data_api_id
 * @property string $slug_new_tree
 * @property string $deskripsi
 * @property string $icon
 * @property int $icon_type
 * @property int $active
 * @property int $selected
 * @property int $disabled
 * @property int $readonly
 * @property int $visible
 * @property int $collapsed
 * @property int $movable_u
 * @property int $movable_d
 * @property int $movable_l
 * @property int $movable_r
 * @property int $removable
 * @property int $removable_all
 */
class NewTree extends \kartik\tree\models\Tree
{

    // use \kartik\tree\models\TreeTrait;

    const status_aktif = '1';
    const status_tidak_aktif = '0';

    public $list_status = [
        self::status_aktif => 'Aktif',
        self::status_tidak_aktif => 'Tidak Aktif',
    ];

    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'new_tree';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        // return [
        //     [['root', 'lft', 'rgt', 'lvl', 'data_api_id', 'icon_type', 'active', 'selected', 'disabled', 'readonly', 'visible', 'collapsed', 'movable_u', 'movable_d', 'movable_l', 'movable_r', 'removable', 'removable_all'], 'integer'],
        //     [['lft', 'rgt', 'lvl', 'name'], 'required'],
        //     [['deskripsi'], 'string'],
        //     [['name'], 'string', 'max' => 60],
        //     [['slug_new_tree'], 'string', 'max' => 150],
        //     [['icon'], 'string', 'max' => 255],
        // ];
        
        $rules = parent::rules();

        $rules[] = ['slug_new_tree', 'safe'];
        $rules[] = ['name', 'safe'];
        $rules[] = ['deskripsi', 'safe'];
        $rules[] = ['tree_id', 'safe'];
        $rules[] = ['status', 'safe'];

        return $rules;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        $attributeLabels = parent::attributeLabels();

        $attributeLabels[] = [
            // 'id' => Yii::t('app', 'ID'),
            // 'root' => Yii::t('app', 'Root'),
            // 'lft' => Yii::t('app', 'Lft'),
            // 'rgt' => Yii::t('app', 'Rgt'),
            // 'lvl' => Yii::t('app', 'Lvl'),
            // 'name' => Yii::t('app', 'Name'),
            'tree_id' => Yii::t('app', 'Data Api ID'),
            'status' => Yii::t('app', 'Status'),
            'slug_new_tree' => Yii::t('app', 'Slug New Tree'),
            'deskripsi' => Yii::t('app', 'Deskripsi'),
            // 'icon' => Yii::t('app', 'Icon'),
            // 'icon_type' => Yii::t('app', 'Icon Type'),
            // 'active' => Yii::t('app', 'Active'),
            // 'selected' => Yii::t('app', 'Selected'),
            // 'disabled' => Yii::t('app', 'Disabled'),
            // 'readonly' => Yii::t('app', 'Readonly'),
            // 'visible' => Yii::t('app', 'Visible'),
            // 'collapsed' => Yii::t('app', 'Collapsed'),
            // 'movable_u' => Yii::t('app', 'Movable U'),
            // 'movable_d' => Yii::t('app', 'Movable D'),
            // 'movable_l' => Yii::t('app', 'Movable L'),
            // 'movable_r' => Yii::t('app', 'Movable R'),
            // 'removable' => Yii::t('app', 'Removable'),
            // 'removable_all' => Yii::t('app', 'Removable All'),
        ];

        return $attributeLabels;
    }

    public function getStatus($status)
    {
        return $this->list_status[$status];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => SluggableBehavior::className(),
            'attribute' => 'name',
            'slugAttribute' => 'slug_new_tree',
            'ensureUnique' => true,
        ];

        return $behaviors;
    }

    public function getParameterApi()
    {
        return $this->hasOne(ParameterApi::className(), ['new_tree_id'  => 'id']);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'tree_id']);
    }

    // public function getParameter($depth = 1, $glue = ' &raquo; ', $currCss = 'kv-crumb-active', $new = 'Untitled')
    // {
    //     /**
    //      * @var Tree $this
    //      */
    //     if ($this->isNewRecord || empty($this)) {
    //         return $currCss ? Html::tag('span', $new, ['class' => $currCss]) : $new;
    //     }
    //     $depth = empty($depth) ? null : intval($depth);
    //     $module = TreeView::module();
    //     $nameAttribute = ArrayHelper::getValue($module->dataStructure, 'nameAttribute', 'name');
    //     $crumbNodes = $depth === null ? $this->parents()->all() : $this->parents($depth - 1)->all();
    //     $crumbNodes[] = $this;

    //     $i = 1;
    //     $len = count($crumbNodes);
    //     $crumbs = [];
    //     foreach ($crumbNodes as $node) {
    //         $name = $node->nameAttribute;
    //         // ->slug_parameter_api;
    //         // dd($name);
    //         if ($i === $len && $currCss) {
    //             $name = Html::tag('span', $name, ['class' => $currCss]);
    //         }
    //         $crumbs[] = $name;
    //         $endCrumbs = end($crumbs);
    //         $i++;
    //     }
    //     // return $endCrumbs;
    //     return implode($glue, $crumbs);
    // }
}
