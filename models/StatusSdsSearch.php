<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StatusSds;

/**
 * StatusSdsSearch represents the model behind the search form of `app\models\StatusSds`.
 */
class StatusSdsSearch extends StatusSds
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['alamat_situs'], 'string', 'max' => 255],
            [['instansi_id','alamat_situs', 'host', 'port', 'username', 'password', 'keterangan'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StatusSds::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('instansi');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'alamat_situs' => $this->alamat_situs,
            'instansi_id' => $this->instansi_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->orFilterWhere(['like', 'alamat_situs', $this->alamat_situs])
            ->orFilterWhere(['like', 'instansi.nama_instansi', $this->instansi_id])
            ->orFilterWhere(['like', 'alamat_situs', $this->alamat_situs])
            ->orFilterWhere(['like', 'host', $this->host])
            ->orFilterWhere(['like', 'port', $this->port])
            ->orFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}
