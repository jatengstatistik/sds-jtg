<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "rpjm_capaian".
 *
 * @property int $id
 * @property int $indikator_id
 * @property int $tahun_data
 * @property string $target_rkpd
 * @property string $target_dppa
 * @property string $capaian
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property RpjmIndikator $indikator
 */
class RpjmCapaian extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rpjm_capaian';
    }

    public function behaviors()
    {
       return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            // 'sluggable' => [
            //     'class' => SluggableBehavior::className(),
            //     'attribute' => 'name',
            //     'slugAttribute' => 'slug',
            // ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['indikator_id', 'tahun_data', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['target_rkpd', 'target_dppa', 'capaian'], 'string', 'max' => 50],
            [['indikator_id'], 'exist', 'skipOnError' => true, 'targetClass' => RpjmIndikator::className(), 'targetAttribute' => ['indikator_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'indikator_id' => 'Indikator ID',
            'tahun_data' => 'Tahun Data',
            'target_rkpd' => 'Target Rkpd',
            'target_dppa' => 'Target Dppa',
            'capaian' => 'Capaian',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndikator()
    {
        return $this->hasOne(RpjmIndikator::className(), ['id' => 'indikator_id']);
    }

    /**
     * {@inheritdoc}
     * @return RpjmCapaianQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RpjmCapaianQuery(get_called_class());
    }
}
