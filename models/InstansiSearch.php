<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Instansi;

/**
 * InstansiSearch represents the model behind the search form of `app\models\Instansi`.
 */
class InstansiSearch extends Instansi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['nama_instansi', 'singkatan_instansi', 'alamat_instansi', 'telepon_instansi','jenis_instansi_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Instansi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('jenisInstansi');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'jenis_instansi_id' => $this->jenis_instansi_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->orFilterWhere(['like', 'nama_instansi', $this->nama_instansi])
            ->orFilterWhere(['like', 'singkatan_instansi', $this->singkatan_instansi])
            ->orFilterWhere(['like', 'alamat_instansi', $this->alamat_instansi])
            ->orFilterWhere(['like', 'jenis_instansi.jenis_instansi', $this->jenis_instansi_id])
            ->orFilterWhere(['like', 'telepon_instansi', $this->telepon_instansi]);

        return $dataProvider;
    }
}
