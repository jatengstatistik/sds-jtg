<?php

namespace app\models;

use PhpOffice\PhpSpreadsheet\IOFactory;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\data\ArrayDataProvider;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "data_storage".
 *
 * @property int $id
 * @property int $aplikasi_id
 * @property string $nama_data
 * @property string $deskripsi_data
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Aplikasi $aplikasi
 */
class DataStorage extends \yii\db\ActiveRecord
{
    // public $file_store;
    // public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'data_storage';
    }

    public function behaviors()
    {
       return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'nama_data',
                'slugAttribute' => 'slug_data',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['aplikasi_id', 'instansi_id','nama_data'], 'required'],
            [['aplikasi_id', 'instansi_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['nama_data', 'deskripsi_data','analisis','informasi'], 'string'],
            [['file'],'file','skipOnEmpty' => true, 'extensions' => 'xlsx'],
            [['aplikasi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Aplikasi::className(), 'targetAttribute' => ['aplikasi_id' => 'id']],
            [['instansi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Instansi::className(), 'targetAttribute' => ['instansi_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'aplikasi_id' => Yii::t('app', 'Nama Aplikasi'),
            'instansi_id' => Yii::t('app', 'Instansi'),
            'nama_data' => Yii::t('app', 'Judul Data'),
            'file' => Yii::t('app', 'File Name'),
            'deskripsi_data' => Yii::t('app', 'Deskripsi Data'),
            'analisis' => Yii::t('app', 'Analisis Mendalam'),
            'informasi' => Yii::t('app', 'Analisis Deskriptif'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAplikasi()
    {
        return $this->hasOne(Aplikasi::className(), ['id' => 'aplikasi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstansi()
    {
        return $this->hasOne(Instansi::className(), ['id' => 'instansi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiupdate()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDibuat()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        if (!empty($this->file) && file_exists(Yii::getAlias("@uploads/storage_data/{$this->file}"))) {
            unlink(Yii::getAlias("@uploads/storage_data/{$this->file}"));
        }
    }

    public function loadFile($pathFile='')
    {
        $inputFileName = Yii::$app->basePath.$pathFile;

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($inputFileName);
        $spreadsheetMetaData = $spreadsheet->getProperties();
        $sheetData   = [
            'title' => $spreadsheetMetaData->getTitle(),
            'data' => $spreadsheet->getActiveSheet()->toArray(),
        ];
        return $sheetData;
    }

    public function getFileAttributeGridView($sheetData='')
    {
        
        $changeKeyArray[] = ['class' => 'yii\grid\SerialColumn'];
        foreach ($sheetData['data'][0] as $key => $value) {
            $changeKeyArray[] = [
                'attribute' => $key,
                'label' => ucwords($value),
            ];
        }

        return $changeKeyArray;
    }

    public function getFileData($sheetData='')
    {
        // Memisahkan Nama Field pada baris 1 dengan Data pada baris selanjutnya;
        // $this->printData($sheetData['data']);
        $data = array_splice($sheetData['data'],1);

        return $data;
    }

    public function getDataProvider($dataset)
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => $this->getFileData($dataset),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => $this->getFileAttributeGridView($dataset),
            ],
        ]);

        return $dataProvider;
    }

    private function printData($dataPrint)
    {
        echo '<pre>';
        print_r($dataPrint);
        echo '</pre';
    }
}
