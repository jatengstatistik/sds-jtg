<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "skpd".
 *
 * @property int $id Kode Master SKPD Auto Increment
 * @property string $kode_skpd Kode SKPD = (2 digit kode Kabkota + 2 digit Kode SKPD)
 * @property string $nama_skpd Nama SKPD
 * @property string $akronim_skpd Singkatan SKPD
 * @property string $alamat_skpd Alamat SKPD
 * @property string $telpon_skpd Telpon SKPD
 * @property string $nama_pic_skpd Nama Kontak Person SKPD
 * @property string $telepon_pic_skpd Telp. Kontak Person SKPD
 * @property int $created_at
 * @property int $updated_at
 */
class Skpd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'skpd';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['kode_skpd'], 'string', 'max' => 4],
            [['nama_skpd', 'alamat_skpd'], 'string', 'max' => 255],
            [['akronim_skpd', 'telpon_skpd', 'nama_pic_skpd', 'telepon_pic_skpd'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Kode Master SKPD Auto Increment'),
            'kode_skpd' => Yii::t('app', 'Kode SKPD'),
            'nama_skpd' => Yii::t('app', 'Nama SKPD'),
            'akronim_skpd' => Yii::t('app', 'Singkatan SKPD'),
            'alamat_skpd' => Yii::t('app', 'Alamat SKPD'),
            'telpon_skpd' => Yii::t('app', 'Telpon SKPD'),
            'nama_pic_skpd' => Yii::t('app', 'Nama Kontak Person SKPD'),
            'telepon_pic_skpd' => Yii::t('app', 'Telp. Kontak Person SKPD'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
