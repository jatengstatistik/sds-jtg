<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ApiClient]].
 *
 * @see ApiClient
 */
class ApiClientQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ApiClient[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ApiClient|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
