<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DataDasar;

/**
 * DataDasarSearch represents the model behind the search form of `app\models\DataDasar`.
 */
class DataDasarSearch extends DataDasar
{
    // add the public attributes that will be used to store the data to be search
    public $nama_organisasi;
    public $nama_urusan;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','tahun_data', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['file_name'],'string'],
            [['files','organisasi_id', 'urusan_id','nama_organisasi','nama_urusan'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DataDasar::find();

        // add join with name related model
        $query->joinWith(['organisasi','urusan']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // Important: here is how we set up the sorting
        // The key is the attribute name on our "TourSearch" instance
        $dataProvider->sort->attributes['nama_organisasi'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['rpjm_organisasi.nama_organisasi' => SORT_ASC],
            'desc' => ['rpjm_organisasi.nama_organisasi' => SORT_DESC],
        ];
        // Lets do the same with country now
        $dataProvider->sort->attributes['nama_urusan'] = [
            'asc' => ['rpjm_urusan.nama_urusan' => SORT_ASC],
            'desc' => ['rpjm_urusan.nama_urusan' => SORT_DESC],
        ];

        // No search? Then return data Provider
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tahun_data' => $this->tahun_data,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'file_name', $this->file_name])
            ->orFilterWhere(['like', 'rpjm_urusan.nama_urusan', $this->nama_urusan])
            ->orFilterWhere(['like', 'rpjm_organisasi.nama_organisasi', $this->nama_organisasi])
            ->andFilterWhere(['like', 'files', $this->files]);

        return $dataProvider;
    }
}
