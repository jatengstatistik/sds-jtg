<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;   

/**
 * This is the model class for table "setting".
 *
 * @property int $id
 * @property string $title
 * @property string $sub_title
 * @property int $site_logo
 * @property int $api_pegawai
 * @property string $skpd
 * @property string $nama_kabupaten
 * @property int $kode_kabupaten
 * @property string $version
 * @property int $created_at
 * @property int $updated_at
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'setting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['site_logo'], 'file'],
            [['admin_setting_data','title', 'sub_title', 'api_pegawai', 'skpd', 'nama_kabupaten', 'kode_kabupaten', 'version', 'created_at', 'updated_at'], 'required'],
            [['admin_setting_data','kode_kabupaten', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['url_open_data','site_logo', 'api_pegawai','sub_title', 'skpd', 'nama_kabupaten'], 'string', 'max' => 255],
            [['version'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'sub_title' => Yii::t('app', 'Sub Title'),
            'url_open_data' => Yii::t('app', 'Alamat Open Data'),
            'site_logo' => Yii::t('app', 'Site Logo'),
            'api_pegawai' => Yii::t('app', 'Api Pegawai'),
            'skpd' => Yii::t('app', 'Skpd'),
            'nama_kabupaten' => Yii::t('app', 'Nama Kabupaten'),
            'kode_kabupaten' => Yii::t('app', 'Kode Kabupaten'),
            'version' => Yii::t('app', 'Version'),
            'admin_setting_data'=> Yii::t('app', 'Admin Setting Data'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
