<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "data_api".
 *
 * @property int $id
 * @property int $data_id
 * @property string $nama_api
 * @property string $url_api
 * @property int $ordering
 *
 * @property StrukturData[] $strukturDatas
 */
class ParameterApi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parameter_api';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['new_tree_id'], 'integer'],
            [['parameter_api'], 'string', 'max' => 50],
            [['slug_parameter_api'], 'string', 'max' => 150]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'new_tree_id' => Yii::t('app', 'Data ID'),
            'parameter_api' => Yii::t('app', 'Parameter'),
            'slug_parameter_api' => Yii::t('app', 'Slug Nama Api'),
            // 'url_api' => Yii::t('app', 'Url Api'),
            // 'ordering' => Yii::t('app', 'Ordering'),
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'parameter_api',
                'slugAttribute' => 'slug_parameter_api',
                // 'immutable' => true,
                'ensureUnique'=>true,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    // public function getStrukturDatas()
    // {
    //     return $this->hasMany(StrukturData::className(), ['data_api_id' => 'id']);
    // }

    public function getNewTree()
    {
        return $this->hasOne(NewTree::className(), ['id' => 'new_tree_id']);
    }

    public function getStrukturData()
    {
        return $this->hasMany(StrukturData::className(), ['parameter_api_id' => 'id']);
    }

    public function getByOrder($angka, $data_id)
    {
        return static::find()
            ->where(['data_id' => $data_id])
            ->andWhere(['ordering' => $angka])
            ->one();
    }
}
