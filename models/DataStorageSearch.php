<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DataStorage;

/**
 * DataStorageSearch represents the model behind the search form of `app\models\DataStorage`.
 */
class DataStorageSearch extends DataStorage
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'updated_by'], 'integer'],
            [['nama_data', 'deskripsi_data','analisis','informasi'],'string'],
            [['aplikasi_id','instansi_id', 'created_by' ,'file'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DataStorage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // Nama Relasi
        $query->joinWith(['aplikasi','instansi','dibuat']);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'aplikasi_id' => $this->aplikasi_id,
            'instansi_id' => $this->instansi_id,
            'file' => $this->file,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'nama_data', $this->nama_data])
            ->andFilterWhere(['like', 'file', $this->file])
            ->orFilterWhere(['like', 'aplikasi.nama_aplikasi', $this->aplikasi_id])
            ->orFilterWhere(['like', 'instansi.nama_instansi', $this->instansi_id])
            ->orFilterWhere(['like', 'user.nama_lengkap', $this->created_by])
            ->andFilterWhere(['like', 'informasi', $this->informasi])
            ->andFilterWhere(['like', 'analisis', $this->analisis])
            ->andFilterWhere(['like', 'deskripsi_data', $this->deskripsi_data]);

        return $dataProvider;
    }
}
