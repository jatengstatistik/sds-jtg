<?php
namespace app\models;

use Yii;
use hscstudio\mimin\models\AuthAssignment;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\httpclient\Client;
use yii\web\IdentityInterface;
/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    public $new_password;
    public $repeat_password;
    public $old_password;
    // public $new_password;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // ['status', 'default', 'value' => self::STATUS_ACTIVE],
            // ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['nip'], 'required'],
            ['nip', 'match', 'pattern' => '/^\d{18}$/', 'message' => 'Panjang NIP 18 Digit Angka.'],
            ['nip', 'unique', 'targetAttribute' => 'nip', 'message' => Yii::t('app','NIP Sudah Terdaftar')],
            [['nama_lengkap'],'required'],
            [['jabatan'],'required'],
            // [['email'], 'email'],
            // [['email', 'password_hash'], 'string', 'max' => 255],
            ['api_key_od','string'],
            ['status','integer'],
            ['instansi_id','integer'],
            [['old_password', 'new_password', 'repeat_password'], 'string', 'min' => 6],
            [['repeat_password'], 'compare', 'compareAttribute' => 'new_password'],
            [['old_password', 'new_password', 'repeat_password'], 'required', 'when' => function ($model) {
                return (!empty($model->new_password));
            }, 'whenClient' => "function (attribute, value) {
                return ($('#user-new_password').val().length>0);
            }"],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            // 'tree_id' => Yii::t('app', 'Tree ID'),
            'nama_lengkap' => Yii::t('app', 'Nama Lengkap'),
            'nip' => Yii::t('app', 'Nomor Induk Pegawai'),
            'jabatan' => Yii::t('app', 'Jabatan'),
            'api_key_od' => Yii::t('app', 'Api Key Open Data'),
            // 'username' => Yii::t('app', 'Username'),
            // 'password' => Yii::t('app', 'Password'),
            // 'token' => Yii::t('app', 'Token'),
            'instansi_id' => Yii::t('app', 'Instansi'),
            'created_at' => Yii::t('app', 'Dibuat Pada'),
            'updated_at' => Yii::t('app', 'Dibuat Pada'),
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['password'] = ['old_password', 'new_password', 'repeat_password'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }
    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByNip($nip)
    {
        return static::findOne(['nip' => $nip, 'status' => self::STATUS_ACTIVE]);
    }
    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }
    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }
    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles()
    {
        return $this->hasMany(AuthAssignment::className(), [
            'user_id' => 'id',
        ]);
    }

    public function getInstansi()
    {
        return $this->hasOne(Instansi::className(), ['id' => 'instansi_id']);
    }

    public function getPegawai($id='')
    {
        $user = User::findOne($id);
        /*API Pegawai*/
        // $apiPegawai = 'http://simpeg.bkd.jatengprov.go.id/webservice/identitas';
        $client = new Client(['baseUrl' => Setting::find(1)->one()->api_pegawai]);
        $pegawaiResponse = $client->get(['nip' => $user->nip])->send();

        if ($pegawaiResponse->headers->get('content-length') > '2') {
            $pegawai = (object) $pegawaiResponse->data;
        }else{
            $pegawai = (object) [
                'kode'=>'',
                'nama'=>'',
                'tmp_lahir'=>'',
                'tgl_lahir'=>'',
                'jk'=>'',
                'agama'=>'',
                'email'=>'',
                'alamat'=>'',
                'gol'=>'',
                'tmtgol'=>'',
                'pendidikan'=>'',
                'jabatan'=>'',
                'eselon'=>'',
                'tmtjab'=>'',
                'unitkerja'=>'',
                'instansi'=>'',
                'nik'=>'',
                'npwp'=>'',
                'hp'=>'',
            ];
        }
        return $pegawai;
        /*End API Pegawai*/
    }

    public function getPns($nip='')
    {
        # code...
        $client = new Client(['baseUrl' => Setting::find(1)->one()->api_pegawai]);
        $pegawaiResponse = $client->get(['nip' => $nip])->send();

        if ($pegawaiResponse->headers->get('content-length') > '2') {
            $pegawai = (object) $pegawaiResponse->data;
        }else{
            $pegawai = (object) [
                'kode'=>'',
                'nama'=>'',
                'tmp_lahir'=>'',
                'tgl_lahir'=>'',
                'jk'=>'',
                'agama'=>'',
                'email'=>'',
                'alamat'=>'',
                'gol'=>'',
                'tmtgol'=>'',
                'pendidikan'=>'',
                'jabatan'=>'',
                'eselon'=>'',
                'tmtjab'=>'',
                'unitkerja'=>'',
                'instansi'=>'',
                'nik'=>'',
                'npwp'=>'',
                'hp'=>'',
            ];
        }
        return $pegawai;
    }
}