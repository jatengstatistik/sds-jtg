<?php

namespace app\models;

use Yii;
use app\components\UploadBehavior;
use app\models\FileModel;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\httpclient\Client;

/**
 * This is the model class for table "sdi_data".
 *
 * @property int $id
 * @property int $instansi_id
 * @property int $api_client_id
 * @property int $uploaded_file_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * @property int $lock
 * @property string $title
 * @property int $year
 *
 * @property ApiClient $apiClient
 * @property Instansi $instansi
 * @property UploadedFile $uploadedFile
 */
class ExcelResource extends \yii\db\ActiveRecord
{
    // public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sdi_resource';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['year','uploaded_file_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            // 'api_client_id','year','instansi_id', 'lock', 'sdi_category_id', 'deleted_by'
            [['title','name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            // [['api_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApiClient::className(), 'targetAttribute' => ['api_client_id' => 'id']],
            // [['instansi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Instansi::className(), 'targetAttribute' => ['instansi_id' => 'id']],
            // [['file'], 'file','extensions' => 'xlsx' ,'maxSize'=>1024 * 1024 * 3],
            // [['uploaded_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => UploadedFile::className(), 'targetAttribute' => ['uploaded_file_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Judul'),
            'year' => Yii::t('app', 'Tahun'),
            // 'sdi_category_id' => Yii::t('app', 'Categories'),
            // 'instansi_id' => Yii::t('app', 'Instansi'),
            // 'api_client_id' => Yii::t('app', 'Api Client'),
            'uploaded_file_id' => Yii::t('app', 'Uploaded File'),
            'created_at' => Yii::t('app', 'Dibuat Pada'),
            'updated_at' => Yii::t('app', 'Diupdate Pada'),
            'created_by' => Yii::t('app', 'Dibuat Oleh'),
            'updated_by' => Yii::t('app', 'Diupdate Oleh'),
            // 'deleted_by' => Yii::t('app', 'Dihapus Oleh'),
            // 'lock' => Yii::t('app', 'Lock'),
        ];
    }

    public function behaviors()
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'name',
                'ensureUnique'=>true,
            ],
            'upload' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'file', // required, use to receive input file
                'savedAttribute' => 'uploaded_file_id', // optional, use to link model with saved file.
                'uploadPath' => '@runtime/upload/sdi-files', // saved directory. default to '@runtime/upload'
                'autoSave' => true, // when true then uploaded file will be save before ActiveRecord::save()
                'autoDelete' => true, // when true then uploaded file will deleted before ActiveRecord::delete()
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApiClient()
    {
        return $this->hasOne(ApiClient::className(), ['id' => 'api_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstansi()
    {
        return $this->hasOne(Instansi::className(), ['id' => 'instansi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadedFile()
    {
        return $this->hasOne(FileModel::className(), ['id' => 'uploaded_file_id']);
    }

    public function getUserCreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUserUpdated()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function getListInstansi()
    {
        $merge = [];
        $list = Instansi::find()->Where('jenis_instansi_id < 3')->asArray()->all();
        foreach ($list as $value) {
            $merge[] = [
                'id' => $value['id'],
                // 'nama' => $value['nama'].' - '.strtoupper(substr($value['nama'], 0,3)),
                'nama' => $value['nama_instansi'].' - '.$value['singkatan_instansi'],
            ];
        }
        return ArrayHelper::map($merge, 'id', 'nama');
    }

    public function getListMethod()
    {
        $method = [];
        $method[] = ['id'=>'GET', 'method'=>'GET'];
        $method[] = ['id'=>'POST', 'method'=>'POST'];
        // $method[] = ['id'=>'POST', 'method'=>''];
        return ArrayHelper::map($method, 'id', 'method');
    }

    public function getListTahun()
    {
        $year = [];
        for ($i = date('Y'); $i >= 2000; $i--) {
            $year[] = ['id'=>$i, 'year'=>$i];
        }
        return ArrayHelper::map($year, 'id', 'year');
    }


/*
*
* Bagian Get Data Excel 
*
*/

    public function getDataExcel()
    {
        // get From Param Url
        $get = Yii::$app->request->get();
        // Limit
        $chunkSize = (empty($get['per-page'])) ? '10' : Yii::$app->request->get('per-page');
        // offset / startRow
        $startRow = (empty($get['page'])) ? '1' : ($get['page']*$chunkSize)-$chunkSize+1;
        // end / endRow
        $endRow = $startRow + $chunkSize - 1;


        $inputFileName = $this->uploadedFile->filename;

        // $cache = new MyCustomPsr16Implementation();

        // \PhpOffice\PhpSpreadsheet\Settings::setCache($cache);
        /**  Identify the type of $inputFileName  **/
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);

        /**  Create a new Reader of the type that has been identified  **/
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $reader->setReadDataOnly(true);
        $reader->setReadEmptyCells(false);
        

        /**  Create a new Instance of our Read Filter  **/
        $chunkFilter = new ChunkReadFilter();

        /**  Tell the Reader that we want to use the Read Filter  **/
        $reader->setReadFilter($chunkFilter);
        // $chunkFilter->setRows(20,200);
        $chunkFilter->setRows($startRow,$endRow);

        /**  Loop to read our worksheet in "chunk size" blocks  **/
        // for ($startRow = 2; $startRow <= $chunkSize*$startRow; $startRow += $chunkSize) {
        //     /**  Tell the Read Filter which rows we want this iteration  **/
        //     $chunkFilter->setRows($startRow,$chunkSize);
        //     /**  Load only the rows that match our filter  **/
        //     $spreadsheet = $reader->load($inputFileName);
        //     //    Do some processing here
        //     // $spreadsheetMetaData = $spreadsheet->getProperties();
        //     $data = $spreadsheet->getActiveSheet()->toArray(null,true,true,true);
        // }
        
        $spreadsheet = $reader->load($inputFileName);
        $data = $spreadsheet->getActiveSheet()->toArray(null,true,true,true);
        // $cleanData = $this->getFileData($data);

        // echo '<pre>';
        // print_r($chunkSize);
        // echo '<br>';
        // print_r($startRow);
        // echo '<br>';
        // print_r($endRow);
        // echo '<br>';
        // print_r($data);
        // exit();
        // echo '</pre>';

        return $data;
    }

    public function getFileData($data=array())
    {
        $data = $this->getDataExcel();
        foreach ($data as $key => $value) {
            foreach ($data[1] as $k => $fival) {
                $clean[strtolower(preg_replace('/[^a-zA-Z0-9\']/', '',$fival))] = $value[$k];
            }
            $a[] = $clean;
        } 
        
        // Delete First Row
        $clean = $a;
        array_shift($clean);
        // echo '<pre>';
        // print_r($clean);
        // echo '<br>';
        // print_r($data);
        // exit();
        // echo '</pre>';
        return $clean;
    }

    public function getCountFileData()
    {
        // try {
        //     set_time_limit(2);
        //     $data = $this->getFileData();
        //     $count = count($data);
        // } catch (Exception $e) {
            // Limit Data Rows
            $count = 100000;
            
        // }


        // echo '<pre>';
        // print_r($count);
        // exit();
        // echo '</pre>';
        return $count;
    }

    public function getFileAttributeGridView()
    {
        $data = $this->getDataExcel();
        $changeKeyArray[] = ['class' => 'yii\grid\SerialColumn'];
        foreach ($data[1] as $key => $value) {
            if (!empty($value)) {
                $changeKeyArray[] = [
                    'attribute' => strtolower(preg_replace("/[^a-zA-Z0-9]/", "", $value)),
                    'label' => ucwords(preg_replace("/[^a-zA-Z0-90-9]/", " ", $value)),
                ];
            }
        }
        // echo '<pre>';
        // print_r($changeKeyArray);
        // exit();
        // echo '</pre>';

        return $changeKeyArray;
    }


    public function getDataProvider()
    {
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $this->getFileData(),
            'totalCount' => $this->getCountFileData(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => $this->getFileAttributeGridView(),
            ],
        ]);

        return $dataProvider;
    }


}

class MyReadFilter implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter
{
    private $startRow = 0;

    private $endRow = 0;

    private $columns = [];

    public function __construct($startRow, $endRow, $columns)
    {
        $this->startRow = $startRow;
        $this->endRow = $endRow;
        $this->columns = $columns;
    }

    public function readCell($column, $row, $worksheetName = '')
    {
        if ($row >= $this->startRow && $row <= $this->endRow) {
            if (in_array($column, $this->columns)) {
                return true;
            }
        }

        return false;
    }
}

/**  Define a Read Filter class implementing \PhpOffice\PhpSpreadsheet\Reader\IReadFilter  */
class ChunkReadFilter implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter
{
    private $startRow = 0;
    private $endRow   = 0;

    /**  Set the list of rows that we want to read  */
    public function setRows($startRow, $chunkSize) {
        $this->startRow = $startRow;
        $this->endRow   = $startRow + $chunkSize;
    }

    public function readCell($column, $row, $worksheetName = '') {
        //  Only read the heading row, and the configured rows
        if (($row == 1) || ($row >= $this->startRow && $row < $this->endRow)) {
            return true;
        }
        return false;
    }
}
