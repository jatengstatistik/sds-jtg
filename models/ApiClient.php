<?php

namespace app\models;

use Yii;
use \app\models\base\ApiClient as BaseApiClient;

/**
 * This is the model class for table "api_client".
 */
class ApiClient extends BaseApiClient
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['instansi_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'deleted_by', 'lock'], 'integer'],
            [['title', 'notes', 'url', 'segment'], 'string', 'max' => 255],
            [['method'], 'string', 'max' => 20],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
