<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "sdi_resource".
 *
 * @property int $id
 * @property int $sdi_data_id
 * @property int $api_client_id
 * @property int $upload_file_id
 * @property string $title
 * @property string $name
 * @property int $year
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property ApiClient $apiClient
 * @property SdiData $sdiData
 * @property UploadedFile $uploadFile
 */
class SdiResource extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sdi_resource';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sdi_data_id', 'api_client_id', 'uploaded_file_id', 'year', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['title', 'name'], 'string', 'max' => 255],
            [['api_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApiClient::className(), 'targetAttribute' => ['api_client_id' => 'id']],
            [['sdi_data_id'], 'exist', 'skipOnError' => true, 'targetClass' => SdiData::className(), 'targetAttribute' => ['sdi_data_id' => 'id']],
            // [['uploaded_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => UploadedFile::className(), 'targetAttribute' => ['upload_file_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sdi_data_id' => Yii::t('app', 'Sdi Data ID'),
            'api_client_id' => Yii::t('app', 'Api Client ID'),
            'uploaded_file_id' => Yii::t('app', 'Upload File ID'),
            'title' => Yii::t('app', 'Judul'),
            'name' => Yii::t('app', 'Nama'),
            'year' => Yii::t('app', 'Tahun'),
            'created_at' => Yii::t('app', 'Dibuat Pada'),
            'updated_at' => Yii::t('app', 'Diupdate Pada'),
            'created_by' => Yii::t('app', 'Dibuat Oleh'),
            'updated_by' => Yii::t('app', 'Diupdate Oleh'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApiClient()
    {
        return $this->hasOne(ApiClient::className(), ['id' => 'api_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSdiData()
    {
        return $this->hasOne(SdiData::className(), ['id' => 'sdi_data_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadFile()
    {
        return $this->hasOne(FileModel::className(), ['id' => 'uploaded_file_id']);
    }

    public function behaviors()
    {
        return [
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            'sluggable' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'name',
                'ensureUnique'=>true,
            ],
        ];
    }

    public function getListTahun()
    {
        $year = [];
        for ($i = date('Y'); $i >= 2000; $i--) {
            $year[] = ['id'=>$i, 'year'=>$i];
        }
        return ArrayHelper::map($year, 'id', 'year');
    }

    public function getListMethod()
    {
        $method = [];
        $method[] = ['id'=>'GET', 'method'=>'GET'];
        $method[] = ['id'=>'POST', 'method'=>'POST'];
        // $method[] = ['id'=>'POST', 'method'=>''];
        return ArrayHelper::map($method, 'id', 'method');
    }


}
