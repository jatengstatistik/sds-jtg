<?php 
namespace app\models;

use kartik\tree\models\TreeTrait;
use yii\base\Model;

/**
* 
*/
class CustomTreeTrait extends Model
{
	use TreeTrait {
        getBreadcrumbs as protected breadcrumbs;
    }

	public function getBreadcrumbs($depth = 1, $glue = ' &raquo; ', $currCss = 'kv-crumb-active', $new = 'Untitled')
    {
        /**
         * @var Tree $this
         */
        if ($this->isNewRecord || empty($this)) {
            return $currCss ? Html::tag('span', $new, ['class' => $currCss]) : $new;
        }
        $depth = empty($depth) ? null : intval($depth);
        $module = TreeView::module();
        $nameAttribute = ArrayHelper::getValue($module->dataStructure, 'nameAttribute', 'name');
        $crumbNodes = $depth === null ? $this->parents()->all() : $this->parents($depth - 1)->all();
        $crumbNodes[] = $this;
        $i = 1;
        $len = count($crumbNodes);
        $crumbs = [];
        foreach ($crumbNodes as $node) {
            $name = $node->$nameAttribute;
            if ($i === $len && $currCss) {
                $name = Html::tag('span', $name, ['class' => $currCss]);
            }
            $crumbs[] = $name;
            $i++;
        }
        return implode($glue, $crumbs);
    }
}


?>