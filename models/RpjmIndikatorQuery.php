<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RpjmIndikator]].
 *
 * @see RpjmIndikator
 */
class RpjmIndikatorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return RpjmIndikator[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return RpjmIndikator|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
