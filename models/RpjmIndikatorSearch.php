<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RpjmIndikator;

/**
 * RpjmIndikatorSearch represents the model behind the search form of `app\models\RpjmIndikator`.
 */
class RpjmIndikatorSearch extends RpjmIndikator
{
    // add the public attributes that will be used to store the data to be search
    public $nama_organisasi;
    public $nama_urusan;
    public $dibuat;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'organisasi_id', 'urusan_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['indikator', 'slug_indikator', 'satuan','nama_organisasi','nama_urusan','dibuat'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RpjmIndikator::find();

        // add join with name related model
        $query->joinWith(['organisasi','urusan','createdBy']);

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // Important: here is how we set up the sorting
        // The key is the attribute name on our "TourSearch" instance
        $dataProvider->sort->attributes['nama_organisasi'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['rpjm_organisasi.nama_organisasi' => SORT_ASC],
            'desc' => ['rpjm_organisasi.nama_organisasi' => SORT_DESC],
        ];
        // Lets do the same with country now
        $dataProvider->sort->attributes['nama_urusan'] = [
            'asc' => ['rpjm_urusan.nama_urusan' => SORT_ASC],
            'desc' => ['rpjm_urusan.nama_urusan' => SORT_DESC],
        ];
        // Lets do the same with country now
        $dataProvider->sort->attributes['dibuat'] = [
            'asc' => ['user.nama_lengkap' => SORT_ASC],
            'desc' => ['user.nama_lengkap' => SORT_DESC],
        ];

        // No search? Then return data Provider
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'organisasi_id' => $this->organisasi_id,
            'urusan_id' => $this->urusan_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'indikator', $this->indikator])
            ->orFilterWhere(['like', 'rpjm_urusan.nama_urusan', $this->nama_urusan])
            ->orFilterWhere(['like', 'rpjm_organisasi.nama_organisasi', $this->nama_organisasi])
            ->orFilterWhere(['like', 'user.nama_lengkap', $this->dibuat])
            ->andFilterWhere(['like', 'slug_indikator', $this->slug_indikator])
            ->andFilterWhere(['like', 'satuan', $this->satuan]);

        return $dataProvider;
    }
}
