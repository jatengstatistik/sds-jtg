<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "jenis_instansi".
 *
 * @property int $id
 * @property string $jenis_instansi
 * @property string $keterangan_jenis_instansi
 * @property int $created_at
 * @property int $updated_at
 */
class JenisInstansi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jenis_instansi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'integer'],
            [['jenis_instansi', 'keterangan_jenis_instansi'], 'string', 'max' => 50],
        ];
    }

     /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jenis_instansi' => Yii::t('app', 'Jenis Instansi'),
            'keterangan_jenis_instansi' => Yii::t('app', 'Keterangan Jenis Instansi'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
