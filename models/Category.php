<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "tree".
 *
 * @property int $id
 * @property int $root
 * @property int $lft
 * @property int $rgt
 * @property int $lvl
 * @property string $name
 * @property string $slug
 * @property string $icon
 * @property int $icon_type
 * @property int $active
 * @property int $selected
 * @property int $disabled
 * @property int $readonly
 * @property int $visible
 * @property int $collapsed
 * @property int $movable_u
 * @property int $movable_d
 * @property int $movable_l
 * @property int $movable_r
 * @property int $removable
 * @property int $removable_all
 */
class Category extends \kartik\tree\models\Tree
{

    const status_aktif = '1';
    const status_tidak_aktif = '0';

    public $list_status = [
        self::status_aktif => 'Aktif',
        self::status_tidak_aktif => 'Tidak Aktif',
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tree';
    }

    /**
     * {@inheritdoc}
     */
    // public function rules()
    // {
    //     return [
    //         [['root','aplikasi_id', 'lft', 'rgt', 'lvl', 'icon_type', 'active', 'selected', 'disabled', 'readonly', 'visible', 'collapsed', 'movable_u', 'movable_d', 'movable_l', 'movable_r', 'removable', 'removable_all'], 'integer'],
    //         [['lft', 'rgt', 'lvl', 'name'], 'required'],
    //         [['name'], 'string', 'max' => 60],
    //         [['slug'], 'string', 'max' => 150],
    //         [['icon'], 'string', 'max' => 255],
    //     ];
    // }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        $attributeLabels = parent::attributeLabels();
        $attributeLabels[] = [
            // 'id' => Yii::t('app', 'ID'),
            // 'root' => Yii::t('app', 'Root'),
            // 'lft' => Yii::t('app', 'Lft'),
            // 'rgt' => Yii::t('app', 'Rgt'),
            // 'lvl' => Yii::t('app', 'Lvl'),
            'name' => Yii::t('app', 'Nama Data'),
            'deskripsi' => Yii::t('app', 'Deskripsi'),
            'status' => Yii::t('app', 'Status'),
            'slug' => Yii::t('app', 'Slug'),
            // 'icon' => Yii::t('app', 'Icon'),
            // 'icon_type' => Yii::t('app', 'Icon Type'),
            // 'active' => Yii::t('app', 'Active'),
            // 'selected' => Yii::t('app', 'Selected'),
            // 'disabled' => Yii::t('app', 'Disabled'),
            // 'readonly' => Yii::t('app', 'Readonly'),
            // 'visible' => Yii::t('app', 'Visible'),
            // 'collapsed' => Yii::t('app', 'Collapsed'),
            // 'movable_u' => Yii::t('app', 'Movable U'),
            // 'movable_d' => Yii::t('app', 'Movable D'),
            // 'movable_l' => Yii::t('app', 'Movable L'),
            // 'movable_r' => Yii::t('app', 'Movable R'),
            // 'removable' => Yii::t('app', 'Removable'),
            'aplikasi_id' => Yii::t('app', 'Aplikasi'),
            // 'removable_all' => Yii::t('app', 'Removable All'),
        ];

        return $attributeLabels;
    }
    

    public function rules()
    {
        $rules = parent::rules();
        // $rules[] = ['description', 'safe'];
        $rules[] = ['slug', 'safe'];
        $rules[] = ['aplikasi_id', 'safe'];
        $rules[] = ['deskripsi', 'safe'];
        $rules[] = ['status', 'safe'];
        $rules[] = ['name', 'required'];
        // $rules[] = ['deskripsi_data', 'safe'];
        // $rules[] = ['url', 'safe'];
        return $rules;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => SluggableBehavior::className(),
            'attribute' => 'name',
            'slugAttribute' => 'slug',
            'ensureUnique'=>true,
        ];

        return $behaviors;
    }

    public function getStatus($status)
    {
        return $this->list_status[$status];
    }

    // public function isDisabled()
    // {
    //     if (Yii::$app->user->username !== 'admin') {
    //         return true;
    //     }
    //     return parent::isDisabled();
    // }

    public function getAplikasi()
    {
        return $this->hasOne(Aplikasi::className(), ['id' => 'aplikasi_id']);
    }

    public function getNewTree()
    {
        return $this->hasOne(NewTree::className(), ['tree_id' => 'id']);
    }
    
    public function getRoot($root)
    {
        return static::find()->where(['root' => $root])->one()->name;
    }

}
