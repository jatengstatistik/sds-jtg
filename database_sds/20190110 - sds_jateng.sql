-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u3
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 10, 2019 at 02:37 PM
-- Server version: 5.5.60-0+deb8u1
-- PHP Version: 5.6.38-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `c14db_sds_jateng`
--

-- --------------------------------------------------------

--
-- Table structure for table `aplikasi`
--

CREATE TABLE IF NOT EXISTS `aplikasi` (
`id` int(11) NOT NULL,
  `nama_aplikasi` varchar(150) DEFAULT NULL,
  `slug_nama_aplikasi` varchar(155) DEFAULT NULL,
  `url_aplikasi` varchar(150) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `instansi_id` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aplikasi`
--

INSERT INTO `aplikasi` (`id`, `nama_aplikasi`, `slug_nama_aplikasi`, `url_aplikasi`, `username`, `password`, `token`, `instansi_id`, `created_at`, `updated_at`) VALUES
(19, 'SIM RS Tugurejo', 'sim-rs-tugurejo', 'http://data.jatengprov.go.id/apps/api/bridge/sds/tugurejo', 'admin123', 'rahasiaadmin123', '', 39, 2147483647, 2147483647),
(20, 'Simpeg BKD Jateng', 'simpeg-bkd-jateng', 'http://data.jatengprov.go.id/apps/api/bridge/sds/bkd', 'admin123', 'adminrahasia123', '', 41, 2147483647, 2147483647),
(22, 'SDS Demo', 'sds-demo', 'http://data.jatengprov.go.id/apps/api/bridge/sds/sds', 'admin123', '123456', '', 41, 2147483647, 2147483647),
(23, 'SIM RS Margono', 'sim-rs-margono', 'http://data.jatengprov.go.id/apps/api/bridge/sds/margono', 'admin123', '123456', '', 42, 2147483647, 2147483647),
(24, 'SIM RS Kelet', 'sim-rs-kelet', 'http://data.jatengprov.go.id/apps/api/bridge/sds/kelet', 'admin123', '123456', '', 43, 2147483647, 2147483647),
(25, 'SIM RS Soedjarwadi', 'sim-rs-soedjarwadi', 'http://data.jatengprov.go.id/apps/api/bridge/sds/sujarwadi', 'admin123', '123456', '', 44, 2147483647, 2147483647),
(26, 'Sistem Informasi Gender dan Anak (SIGA)', 'sistem-informasi-gender-dan-anak-siga', 'http://data.jatengprov.go.id/apps/api/bridge/sds/dp3akb', 'admin123', '123456', '', 45, 2147483647, 2147483647),
(27, 'Aplikasi Keuangan BPKAD', 'aplikasi-keuangan-bpkad', 'https://bpkad.jatengprov.go.id/webservices', '198112142009011004', '123456', '', 40, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('Administrator', '10', 1543369571),
('Administrator', '4', 1535258318),
('Super Administrator', '3', 1532922964);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, 1531667601, 1531667601),
('/aplikasi/*', 2, NULL, NULL, NULL, 1538908083, 1538908083),
('/data/*', 2, NULL, NULL, NULL, 1535258213, 1535258213),
('/debug/*', 2, NULL, NULL, NULL, 1535284797, 1535284797),
('/debug/default/*', 2, NULL, NULL, NULL, 1535284798, 1535284798),
('/debug/user/*', 2, NULL, NULL, NULL, 1535284799, 1535284799),
('/gii/*', 2, NULL, NULL, NULL, 1535284800, 1535284800),
('/gii/default/*', 2, NULL, NULL, NULL, 1535284801, 1535284801),
('/instansi/*', 2, NULL, NULL, NULL, 1535258172, 1535258172),
('/integrasi-data/*', 2, NULL, NULL, NULL, 1543306933, 1543306933),
('/jenis-instansi/*', 2, NULL, NULL, NULL, 1543301059, 1543301059),
('/kabkota/*', 2, NULL, NULL, NULL, 1535258171, 1535258171),
('/mimin/*', 2, NULL, NULL, NULL, 1535241408, 1535241408),
('/mimin/role/*', 2, NULL, NULL, NULL, 1535284805, 1535284805),
('/mimin/route/*', 2, NULL, NULL, NULL, 1535284805, 1535284805),
('/mimin/user/*', 2, NULL, NULL, NULL, 1535284808, 1535284808),
('/sector/*', 2, NULL, NULL, NULL, 1535258189, 1535258189),
('/setting/*', 2, NULL, NULL, NULL, 1543390839, 1543390839),
('/setting/update', 2, NULL, NULL, NULL, 1543392062, 1543392062),
('/setting/view', 2, NULL, NULL, NULL, 1543392059, 1543392059),
('/site/*', 2, NULL, NULL, NULL, 1532947156, 1532947156),
('/skpd/*', 2, NULL, NULL, NULL, 1532947203, 1532947203),
('/status-sds/*', 2, NULL, NULL, NULL, 1533120501, 1533120501),
('/tipe-data/*', 2, NULL, NULL, NULL, 1536074575, 1536074575),
('/treemanager/*', 2, NULL, NULL, NULL, 1535241147, 1535241147),
('/treemanager/node/*', 2, NULL, NULL, NULL, 1535241154, 1535241154),
('/user/*', 2, NULL, NULL, NULL, 1535258252, 1535258252),
('Admin SKPD', 1, NULL, NULL, NULL, 1532947142, 1535412003),
('Administrator', 1, NULL, NULL, NULL, 1535258138, 1546851911),
('Sds Status', 1, NULL, NULL, NULL, 1533120496, 1535240054),
('Super Administrator', 1, NULL, NULL, NULL, 1531661172, 1544589313);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('Super Administrator', '/*'),
('Administrator', '/aplikasi/*'),
('Super Administrator', '/aplikasi/*'),
('Admin SKPD', '/data/*'),
('Administrator', '/data/*'),
('Super Administrator', '/data/*'),
('Super Administrator', '/debug/*'),
('Super Administrator', '/debug/default/*'),
('Super Administrator', '/debug/user/*'),
('Super Administrator', '/gii/*'),
('Super Administrator', '/gii/default/*'),
('Administrator', '/instansi/*'),
('Super Administrator', '/instansi/*'),
('Administrator', '/integrasi-data/*'),
('Super Administrator', '/integrasi-data/*'),
('Administrator', '/jenis-instansi/*'),
('Super Administrator', '/jenis-instansi/*'),
('Administrator', '/kabkota/*'),
('Super Administrator', '/kabkota/*'),
('Super Administrator', '/mimin/*'),
('Super Administrator', '/mimin/role/*'),
('Super Administrator', '/mimin/route/*'),
('Administrator', '/mimin/user/*'),
('Super Administrator', '/mimin/user/*'),
('Admin SKPD', '/sector/*'),
('Super Administrator', '/sector/*'),
('Administrator', '/setting/*'),
('Super Administrator', '/setting/update'),
('Super Administrator', '/setting/view'),
('Admin SKPD', '/site/*'),
('Administrator', '/site/*'),
('Sds Status', '/site/*'),
('Super Administrator', '/site/*'),
('Administrator', '/skpd/*'),
('Super Administrator', '/skpd/*'),
('Administrator', '/status-sds/*'),
('Sds Status', '/status-sds/*'),
('Super Administrator', '/status-sds/*'),
('Super Administrator', '/tipe-data/*'),
('Admin SKPD', '/treemanager/*'),
('Administrator', '/treemanager/*'),
('Super Administrator', '/treemanager/*'),
('Admin SKPD', '/treemanager/node/*'),
('Administrator', '/treemanager/node/*'),
('Super Administrator', '/treemanager/node/*'),
('Administrator', '/user/*'),
('Super Administrator', '/user/*');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE IF NOT EXISTS `data` (
`id` int(11) NOT NULL,
  `tree_id` int(11) DEFAULT NULL,
  `deskripsi` varchar(50) DEFAULT NULL COMMENT 'Deskripsi',
  `status` enum('1','0') DEFAULT NULL,
  `instansi_id` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`id`, `tree_id`, `deskripsi`, `status`, `instansi_id`, `created_at`, `updated_at`) VALUES
(1, 7, 'Halo 123', '1', 1, 123123123, 123123123),
(3, 10, 'Halo pasien 123', '1', NULL, NULL, NULL),
(4, 12, 'Jumlah Pegawai Golongan Berdasarkan Jenis Kelamin', '1', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `instansi`
--

CREATE TABLE IF NOT EXISTS `instansi` (
`id` int(11) NOT NULL,
  `jenis_instansi_id` int(11) NOT NULL,
  `nama_instansi` varchar(255) DEFAULT NULL,
  `singkatan_instansi` varchar(255) DEFAULT NULL,
  `alamat_instansi` varchar(255) DEFAULT NULL,
  `telepon_instansi` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instansi`
--

INSERT INTO `instansi` (`id`, `jenis_instansi_id`, `nama_instansi`, `singkatan_instansi`, `alamat_instansi`, `telepon_instansi`, `created_at`, `updated_at`) VALUES
(1, 0, 'Kabupaten Banjarnegara', NULL, '', NULL, 1531667859, 1531667859),
(3, 0, 'Kabupaten Banyumas', NULL, '', NULL, 1531667900, 1531667900),
(4, 0, 'Kabupaten Batang', NULL, '', NULL, 1531668273, 1531668273),
(5, 0, 'Kabupaten Blora', NULL, '', NULL, 1531668312, 1531668312),
(7, 0, 'Kabupaten Boyolali', NULL, '', NULL, 1531668439, 1531668439),
(8, 0, 'Kabupaten Brebes', NULL, '', NULL, 1531668456, 1531668456),
(9, 0, 'Kabupaten Demak', NULL, '', NULL, 1531668488, 1531668488),
(10, 0, 'Kabupaten Grobogan', NULL, '', NULL, 1531668511, 1531668511),
(11, 0, 'Kabupaten Jepara', NULL, '', NULL, 1531668529, 1531668529),
(12, 0, 'Kabupaten Karanganyar', NULL, '', NULL, 1531668545, 1531668545),
(13, 0, 'Kabupaten Kebumen', NULL, '', NULL, 1531668561, 1531668561),
(14, 0, 'Kabupaten Kendal', NULL, '', NULL, 1531668579, 1531668579),
(15, 0, 'Kabupaten Klaten', NULL, '', NULL, 1531668596, 1531668596),
(16, 0, 'Kabupaten Kudus', NULL, '', NULL, 1531668614, 1531668614),
(17, 0, 'Kabupaten Magelang', NULL, '', NULL, 1531668631, 1531668631),
(18, 0, 'Kabupaten Pati', NULL, '', NULL, 1531668649, 1531668649),
(19, 0, 'Kabupaten Pekalongan', NULL, '', NULL, 1531668666, 1531668666),
(20, 0, 'Kabupaten Pemalang', NULL, '', NULL, 1531668682, 1531668682),
(21, 0, 'Kabupaten Purbalingga', NULL, '', NULL, 1531668699, 1531668699),
(22, 0, 'Kabupaten Purworejo', NULL, '', NULL, 1531668715, 1531668715),
(23, 0, 'Kabupaten Rembang', NULL, '', NULL, 1531668732, 1531668732),
(24, 0, 'Kabupaten Semarang', NULL, '', NULL, 1531668750, 1531668750),
(25, 0, 'Kabupaten Sragen', NULL, '', NULL, 1531668767, 1531668767),
(26, 0, 'Kabupaten Sukoharjo', NULL, '', NULL, 1531668784, 1531668784),
(27, 0, 'Kabupaten Tegal', NULL, '', NULL, 1531668800, 1531668800),
(28, 0, 'Kabupaten Temanggung', NULL, '', NULL, 1531668816, 1531668816),
(29, 0, 'Kabupaten Wonogiri', NULL, '', NULL, 1531668833, 1531668833),
(30, 0, 'Kabupaten Wonosobo', NULL, '', NULL, 1531668850, 1531668850),
(31, 0, 'Kota Pekalongan', NULL, '', NULL, 1531668869, 1531668869),
(32, 0, 'Kota Salatiga', NULL, '', NULL, 1531668885, 1531668885),
(33, 0, 'Kota Semarang', NULL, '', NULL, 1531668902, 1531668902),
(34, 0, 'Kota Surakarta', NULL, '', NULL, 1531668936, 1531668936),
(35, 0, 'Kota Tegal', NULL, '', NULL, 1531668969, 1531668969),
(36, 0, 'Kota Magelang', NULL, '', NULL, 1531668991, 1531668991),
(37, 0, 'Kabupaten Cilacap', NULL, '', NULL, 1531671088, 1531671088),
(38, 1, 'Dinas Komunikasi dan Informatika', 'Diskominfo', 'Jl. Mentri Supeno I No 2 Semarang', '0248319140', NULL, 1543301990),
(39, 1, 'Rumah Sakit Umum Daerah Tugurejo', 'RSUD Tugurejo', 'Jl. Walisongo KM. 8,5, Tambakaji, Ngaliyan, Tambakaji, Ngaliyan, Kota Semarang, Jawa Tengah 50185', '', NULL, 1543370372),
(40, 1, 'Badan Pengelola Keuangan dan Aset Daerah', 'BPKAD', 'Jl. Taman Menteri Supeno No. 2 ', '0248311174', NULL, 1543302076),
(41, 1, 'Badan Kepegawaian Daerah', 'BKD', 'Jl. Stadion Selatan No. 1, Semarang', '0248318846', NULL, 1543302129),
(42, 1, 'RSUD Prof. Dr. Margono Soekarjo', 'RSUD Margono', 'Jl. Dr. Gumbreg No.1 Purwokerto, Kabupaten Banyumas Provinsi Jawa Tengah, Kode Pos 53146', '0281632708', NULL, 1543302139),
(43, 1, 'RSUD Kelet', 'RSUD Kelet', '', '', NULL, 1543302115),
(44, 1, 'RSUD Soedjarwadi', 'RSUD Sodjarwadi', '', '', NULL, 1543302102),
(45, 1, 'Dinas Pemberdayaan Perempuan, Perlindungan Anak, Pengendalian Penduduk Dan Keluarga Berencana', 'DP3AKB', 'Jl. Pamularsih No. 28 Semarang 50148', '', NULL, 1543302089);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_instansi`
--

CREATE TABLE IF NOT EXISTS `jenis_instansi` (
`id` int(11) NOT NULL,
  `jenis_instansi` varchar(80) NOT NULL,
  `keterangan_jenis_instansi` varchar(200) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_instansi`
--

INSERT INTO `jenis_instansi` (`id`, `jenis_instansi`, `keterangan_jenis_instansi`, `created_at`, `updated_at`) VALUES
(1, 'SKPD', 'Satuan Kerja Perangkat Daerah', 0, 0),
(2, 'BUMD', 'Badan Usaha Milih Daerah', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1531454941),
('m130524_201442_init', 1531454970),
('m140506_102106_rbac_init', 1531454949),
('m151024_072453_create_route_table', 1531454957),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1531454950),
('m181002_142147_new_tree', 1538500732),
('m230416_200116_tree', 1532859718);

-- --------------------------------------------------------

--
-- Table structure for table `new_tree`
--

CREATE TABLE IF NOT EXISTS `new_tree` (
`id` bigint(20) NOT NULL,
  `root` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL,
  `lvl` smallint(5) NOT NULL,
  `child_allowed` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(60) NOT NULL,
  `tree_id` int(11) DEFAULT NULL,
  `slug_new_tree` varchar(150) DEFAULT NULL,
  `deskripsi` text,
  `icon` varchar(255) DEFAULT NULL,
  `icon_type` smallint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `selected` tinyint(1) NOT NULL DEFAULT '0',
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `readonly` tinyint(1) NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `collapsed` tinyint(1) NOT NULL DEFAULT '0',
  `movable_u` tinyint(1) NOT NULL DEFAULT '1',
  `movable_d` tinyint(1) NOT NULL DEFAULT '1',
  `movable_l` tinyint(1) NOT NULL DEFAULT '1',
  `movable_r` tinyint(1) NOT NULL DEFAULT '1',
  `removable` tinyint(1) NOT NULL DEFAULT '1',
  `removable_all` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `new_tree`
--

INSERT INTO `new_tree` (`id`, `root`, `lft`, `status`, `rgt`, `lvl`, `child_allowed`, `name`, `tree_id`, `slug_new_tree`, `deskripsi`, `icon`, `icon_type`, `active`, `selected`, `disabled`, `readonly`, `visible`, `collapsed`, `movable_u`, `movable_d`, `movable_l`, `movable_r`, `removable`, `removable_all`) VALUES
(1, 23, 1, 0, 2, 0, 0, 'Input Data', 43, 'input-data', 'Input Data melalui Button ROOT', '', 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0),
(28, 28, 1, 1, 2, 0, 1, 'Tingkat Pendidikan', 29, 'tingkat-pendidikan', 'Data PNS Perdasarkan Pegawai Pendidikan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(30, 30, 1, 0, 4, 0, 1, 'Belanja Bulanan', 36, 'belanja-bulanan', 'Belanja Bulanan Provinsi Jawa Tengah', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(31, 30, 2, 0, 3, 1, 1, 'Bulan', 36, 'bulan', 'Rincian Belanja Bulanan Prov Jateng', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(35, 35, 1, 1, 2, 0, 1, 'Kejadian Luar Biasa', 38, 'kejadian-luar-biasa', 'Kejadian Luar Biasa di Lingkungan RSUD Tugurejo', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(36, 36, 1, 0, 2, 0, 1, 'Alat Kesehatan', 39, 'alat-kesehatan', 'Ketersediaan Alat Kesehatan di RS Tugurejo', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(37, 37, 1, 0, 2, 0, 1, 'Kejadian Luar Biasa', 41, 'kejadian-luar-biasa-2', 'Data Kejadian Luar Biasa di RSUD Margono', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(38, 38, 1, 0, 2, 0, 1, 'Data Rumah Sakit', 42, 'data-rumah-sakit', 'Data Rumah Sakit RSUD', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(39, 39, 1, 0, 2, 0, 1, 'Informasi Tempat Tidur', 48, 'informasi-tempat-tidur', 'Daftar Tempat Tidut', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(40, 40, 1, 0, 2, 0, 1, 'Tenaga Kesehatan', 43, 'tenaga-kesehatan', 'Data Tenaga Kesehatan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(41, 41, 1, 0, 2, 0, 1, 'Ketersediaan Alat Kesehatan', 44, 'ketersediaan-alat-kesehatan', 'Data Alat Kesehatan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(42, 42, 1, 0, 2, 0, 1, 'Informasi Tempat Tidur', 45, 'informasi-tempat-tidur-2', 'Data Informasi Tempat Tidur', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(43, 43, 1, 0, 2, 0, 1, 'Informasi Tempat Tidur', 49, 'informasi-tempat-tidur-3', 'Ketersediaan Tembat Tidur Kosong', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(44, 44, 1, 0, 2, 0, 1, 'Tenaga Kesehatan', 50, 'tenaga-kesehatan-2', 'Data Tenaga Kesehatan di RSUD Kelet', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(45, 45, 1, 1, 6, 0, 1, 'Belanja Bulanan', 51, 'belanja-bulanan-2', '', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(46, 45, 2, 1, 5, 1, 1, 'Bulan', 51, 'bulan-2', '', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(47, 47, 1, 1, 2, 0, 1, 'Pegawai Golongan', 12, 'pegawai-golongan', 'Data Pegawai Berdasar Golongan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(48, 45, 3, 1, 4, 2, 1, 'Jenis', 51, 'jenis', '', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(50, 50, 1, 1, 4, 0, 1, 'Pegawai Diklat', 53, 'pegawai-diklat', '', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(51, 50, 2, 1, 3, 1, 1, 'Diklat', 53, 'diklat', '', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(52, 52, 1, 1, 4, 0, 1, 'Diklat Pegawai', 54, 'diklat-pegawai', '', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(53, 52, 2, 1, 3, 1, 1, 'Diklat', 54, 'diklat-2', '', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `parameter_api`
--

CREATE TABLE IF NOT EXISTS `parameter_api` (
`id` int(11) NOT NULL,
  `new_tree_id` int(11) DEFAULT NULL,
  `parameter_api` varchar(50) DEFAULT NULL,
  `slug_parameter_api` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parameter_api`
--

INSERT INTO `parameter_api` (`id`, `new_tree_id`, `parameter_api`, `slug_parameter_api`) VALUES
(10, 13, 'pegawai-golongan', 'pegawai-golongan'),
(11, 15, 'apps1', 'apps1'),
(12, 18, 'golongan', 'golongan'),
(13, 30, 'data', 'data'),
(14, 31, 'bulan', 'bulan'),
(15, 1, 'parameter_api', 'parameterapi'),
(16, 2, 'pegawai-golongan', 'pegawai-golongan-2'),
(17, 25, 'Data 2', 'data-2-2'),
(18, 35, 'kejadian-luar-biasa', 'kejadian-luar-biasa'),
(19, 36, 'alat-kesehatan', 'alat-kesehatan'),
(20, 37, 'kejadian-luar-biasa', 'kejadian-luar-biasa-2'),
(21, 38, 'data-rumah-sakit', 'data-rumah-sakit'),
(22, 40, 'tenaga-kesehatan', 'tenaga-kesehatan'),
(23, 41, 'ketersediaan-alat-kesehatan', 'ketersediaan-alat-kesehatan'),
(24, 42, 'informasi-tempat-tidur', 'informasi-tempat-tidur'),
(25, 39, 'informasi-tempat-tidur', 'informasi-tempat-tidur-2'),
(26, 43, 'data', 'data-11'),
(27, 44, 'data', 'data-12'),
(28, 28, 'tingkat-pendidikan', 'tingkat-pendidikan'),
(29, 45, 'belanja-bulanan', 'belanja-bulanan'),
(30, 46, 'bulan', 'bulan-2'),
(31, 47, 'pegawai-golongan', 'pegawai-golongan-3'),
(32, 48, 'jenis', 'jenis'),
(33, 50, 'data', 'data-2'),
(34, 51, 'diklat', 'diklat'),
(35, 52, 'data', 'data-14'),
(36, 53, 'diklat', 'diklat-2');

-- --------------------------------------------------------

--
-- Table structure for table `route`
--

CREATE TABLE IF NOT EXISTS `route` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `route`
--

INSERT INTO `route` (`name`, `alias`, `type`, `status`) VALUES
('/*', '*', '', 1),
('/aplikasi/*', '*', 'aplikasi', 1),
('/aplikasi/create', 'create', 'aplikasi', 1),
('/aplikasi/delete', 'delete', 'aplikasi', 1),
('/aplikasi/index', 'index', 'aplikasi', 1),
('/aplikasi/update', 'update', 'aplikasi', 1),
('/aplikasi/view', 'view', 'aplikasi', 1),
('/data/*', '*', 'data', 1),
('/data/create', 'create', 'data', 1),
('/data/get-data-api', 'get-data-api', 'data', 1),
('/data/index', 'index', 'data', 1),
('/data/new-tree', 'new-tree', 'data', 1),
('/data/update', 'update', 'data', 1),
('/debug/*', '*', 'debug', 1),
('/debug/default/*', '*', 'debug/default', 1),
('/debug/default/db-explain', 'db-explain', 'debug/default', 1),
('/debug/default/download-mail', 'download-mail', 'debug/default', 1),
('/debug/default/index', 'index', 'debug/default', 1),
('/debug/default/toolbar', 'toolbar', 'debug/default', 1),
('/debug/default/view', 'view', 'debug/default', 1),
('/debug/user/*', '*', 'debug/user', 1),
('/debug/user/reset-identity', 'reset-identity', 'debug/user', 1),
('/debug/user/set-identity', 'set-identity', 'debug/user', 1),
('/gii/*', '*', 'gii', 1),
('/gii/default/*', '*', 'gii/default', 1),
('/gii/default/action', 'action', 'gii/default', 1),
('/gii/default/diff', 'diff', 'gii/default', 1),
('/gii/default/index', 'index', 'gii/default', 1),
('/gii/default/preview', 'preview', 'gii/default', 1),
('/gii/default/view', 'view', 'gii/default', 1),
('/instansi/*', '*', 'instansi', 1),
('/instansi/create', 'create', 'instansi', 1),
('/instansi/delete', 'delete', 'instansi', 1),
('/instansi/index', 'index', 'instansi', 1),
('/instansi/update', 'update', 'instansi', 1),
('/instansi/view', 'view', 'instansi', 1),
('/integrasi-data/*', '*', 'integrasi-data', 1),
('/integrasi-data/index', 'index', 'integrasi-data', 1),
('/jenis-instansi/*', '*', 'jenis-instansi', 1),
('/jenis-instansi/create', 'create', 'jenis-instansi', 1),
('/jenis-instansi/delete', 'delete', 'jenis-instansi', 1),
('/jenis-instansi/index', 'index', 'jenis-instansi', 1),
('/jenis-instansi/update', 'update', 'jenis-instansi', 1),
('/jenis-instansi/view', 'view', 'jenis-instansi', 1),
('/kabkota/*', '*', 'kabkota', 1),
('/kabkota/create', 'create', 'kabkota', 1),
('/kabkota/delete', 'delete', 'kabkota', 1),
('/kabkota/index', 'index', 'kabkota', 1),
('/kabkota/update', 'update', 'kabkota', 1),
('/kabkota/view', 'view', 'kabkota', 1),
('/mimin/*', '*', 'mimin', 1),
('/mimin/role/*', '*', 'mimin/role', 1),
('/mimin/role/create', 'create', 'mimin/role', 1),
('/mimin/role/delete', 'delete', 'mimin/role', 1),
('/mimin/role/index', 'index', 'mimin/role', 1),
('/mimin/role/permission', 'permission', 'mimin/role', 1),
('/mimin/role/update', 'update', 'mimin/role', 1),
('/mimin/role/view', 'view', 'mimin/role', 1),
('/mimin/route/*', '*', 'mimin/route', 1),
('/mimin/route/create', 'create', 'mimin/route', 1),
('/mimin/route/delete', 'delete', 'mimin/route', 1),
('/mimin/route/generate', 'generate', 'mimin/route', 1),
('/mimin/route/index', 'index', 'mimin/route', 1),
('/mimin/route/update', 'update', 'mimin/route', 1),
('/mimin/route/view', 'view', 'mimin/route', 1),
('/mimin/user/*', '*', 'mimin/user', 1),
('/mimin/user/create', 'create', 'mimin/user', 1),
('/mimin/user/delete', 'delete', 'mimin/user', 1),
('/mimin/user/index', 'index', 'mimin/user', 1),
('/mimin/user/update', 'update', 'mimin/user', 1),
('/mimin/user/view', 'view', 'mimin/user', 1),
('/setting/*', '*', 'setting', 1),
('/setting/create', 'create', 'setting', 1),
('/setting/delete', 'delete', 'setting', 1),
('/setting/index', 'index', 'setting', 1),
('/setting/update', 'update', 'setting', 1),
('/setting/view', 'view', 'setting', 1),
('/site/*', '*', 'site', 1),
('/site/about', 'about', 'site', 1),
('/site/captcha', 'captcha', 'site', 1),
('/site/contact', 'contact', 'site', 1),
('/site/error', 'error', 'site', 1),
('/site/index', 'index', 'site', 1),
('/site/login', 'login', 'site', 1),
('/site/logout', 'logout', 'site', 1),
('/skpd/*', '*', 'skpd', 1),
('/skpd/create', 'create', 'skpd', 1),
('/skpd/delete', 'delete', 'skpd', 1),
('/skpd/index', 'index', 'skpd', 1),
('/skpd/update', 'update', 'skpd', 1),
('/skpd/view', 'view', 'skpd', 1),
('/status-sds/*', '*', 'status-sds', 1),
('/status-sds/create', 'create', 'status-sds', 1),
('/status-sds/delete', 'delete', 'status-sds', 1),
('/status-sds/index', 'index', 'status-sds', 1),
('/status-sds/update', 'update', 'status-sds', 1),
('/status-sds/view', 'view', 'status-sds', 1),
('/treemanager/*', '*', 'treemanager', 1),
('/treemanager/node/*', '*', 'treemanager/node', 1),
('/treemanager/node/manage', 'manage', 'treemanager/node', 1),
('/treemanager/node/move', 'move', 'treemanager/node', 1),
('/treemanager/node/remove', 'remove', 'treemanager/node', 1),
('/treemanager/node/save', 'save', 'treemanager/node', 1),
('/user/*', '*', 'user', 1),
('/user/create', 'create', 'user', 1),
('/user/delete', 'delete', 'user', 1),
('/user/index', 'index', 'user', 1),
('/user/update', 'update', 'user', 1),
('/user/view', 'view', 'user', 1);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
`id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `site_logo` varchar(255) NOT NULL,
  `api_pegawai` varchar(255) NOT NULL,
  `skpd` varchar(255) NOT NULL,
  `nama_kabupaten` varchar(255) NOT NULL,
  `kode_kabupaten` int(2) NOT NULL,
  `version` varchar(50) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `title`, `sub_title`, `site_logo`, `api_pegawai`, `skpd`, `nama_kabupaten`, `kode_kabupaten`, `version`, `created_at`, `updated_at`) VALUES
(1, 'Single Data System', 'Single Data System Jawa Tengah', 'http://sds.jatengprov.go.id/dev/web/assets/d15a1663/img/sds-logo.png', 'http://simpeg.bkd.jatengprov.go.id/webservice/identitas', 'Dinas Komunikasi dan Informatika', 'Pemerintah Provinsi Jawa Tengah', 33, '2.0.0', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `skpd`
--

CREATE TABLE IF NOT EXISTS `skpd` (
`id` int(11) NOT NULL COMMENT 'Kode Master SKPD Auto Increment',
  `kode_skpd` char(4) DEFAULT NULL COMMENT 'Kode SKPD = (2 digit kode Kabkota + 2 digit Kode SKPD)',
  `nama_skpd` varchar(255) DEFAULT NULL COMMENT 'Nama SKPD',
  `akronim_skpd` varchar(50) DEFAULT NULL COMMENT 'Singkatan SKPD',
  `alamat_skpd` varchar(255) DEFAULT NULL COMMENT 'Alamat SKPD',
  `telpon_skpd` varchar(50) DEFAULT NULL COMMENT 'Telpon SKPD',
  `nama_pic_skpd` varchar(50) DEFAULT NULL COMMENT 'Nama Kontak Person SKPD',
  `telepon_pic_skpd` varchar(50) DEFAULT NULL COMMENT 'Telp. Kontak Person SKPD',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status_sds`
--

CREATE TABLE IF NOT EXISTS `status_sds` (
`id` int(11) NOT NULL,
  `instansi_id` int(11) DEFAULT NULL,
  `alamat_situs` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_sds`
--

INSERT INTO `status_sds` (`id`, `instansi_id`, `alamat_situs`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 14, 'http://sds.kendalkab.go.id/', 'Ok,\r\nsds.kendalkab.go.id\r\n\r\nhttps://cpanel.kendalkab.go.id:2083\r\n\r\n+===================================+\r\n| New Account Info                  |\r\n+===================================+\r\n| Domain: sds.kendalkab.go.id\r\n| Ip: 118.97.18.38 (n)\r\n| HasCgi: y\r\n| UserName: sdskendalkabgo\r\n| PassWord: SDSK3nd4l\r\n| CpanelMod: paper_lantern\r\n| HomeRoot: /home2\r\n| Quota: 1.95 GB\r\n| NameServer1: ns3.telkomhosting.com\r\n| NameServer2: ns4.telkomhosting.com\r\n| NameServer3: \r\n| NameServer4: \r\n| Contact Email: ahmad.hasan.basyari@gmail.com\r\n| Package: SILVER_2_GB\r\n| Feature List: default\r\n| Language: en\r\n+===================================+\r\n...Done', NULL, 1531788273),
(2, 4, 'http://sds.batangkab.go.id/', 'Ok', NULL, NULL),
(3, 32, '-', 'Sedang di siapkan Hosting, Sementara dilakukan Instalasi di Local', NULL, 1531789261),
(4, 34, 'http://sds.surakarta.go.id/', 'Ok,\r\nhttp://sds.surakarta.go.id\r\n\r\nDB user : c3singledata\r\nDB pass : singledatasystemkotasurakarta123\r\nDB name	: c3sds\r\n\r\nFTP user : kelurahansingledata\r\nFTP pass : sds123kotasurakarta\r\nakses DB https://sds.surakarta.go.id/phpmyadmin', NULL, 1531788440),
(5, 7, '', 'Belum Menyiapkan Hosting', NULL, NULL),
(6, 25, 'http://sds.sragenkab.go.id/', 'Ok,\r\n[15:16, 6/27/2018] Munawal Ulfiyanto: Alamat : \r\nhttp://sds.sragenkab.go.id\r\n\r\nKonfigurasi DB mysql :\r\nHost : localhost\r\nUser : sds\r\nPassword : kominfo2018!\r\n\r\nPhpMyAdmin\r\nAlamat : http://222.124.206.55/phpMyAdmin\r\n(Case sensitive)\r\nUser : sds\r\nPassword : kominfo2018!\r\n\r\nFtp\r\nAlamat : ftp://sds.sragenkab.go.id\r\nUser : sds\r\nPassword : kominfo2018!\r\n[15:17, 6/27/2018] Munawal Ulfiyanto: Database name : sds', NULL, 1531788511),
(8, 19, '-', 'Sedang di siapkan Hosting, Sementara dilakukan Instalasi di Local', NULL, 1532337368),
(9, 26, 'http://sds.sukoharjokab.go.id/', 'Ok, Akses Hosting belum ada.', NULL, 1531788707),
(10, 13, 'http://sds.kebumenkab.go.id/', 'Ok, Akses Hosting belum ada.', NULL, 1531789125),
(11, 37, 'http://sds.cilacapkab.go.id/', 'ok,\r\nFtpnya : sdscilacap_ftp\r\nPass : sdsc1l4c4p18\r\n\r\nDatabasenya username : sdscilacap_u\r\nPass : sdsc1l4c4p18', NULL, 1532337318),
(12, 16, '', 'Belum menyiapkan Hosting, Masih terdapat kegiatan saat di kunjungi', 1531671330, 1531671330),
(13, 18, 'sds.patikab.go.id', 'hak akses belum dimintakan', 1531788260, 1531788317),
(14, 10, 'sds.grobogankab.go.id', 'hak akses belum dimintakan', 1531788383, 1531788383),
(15, 11, 'sds.jepara.go.id', ' Via sftp\r\nUsername : sds\r\npassword: sds kabupaten jepara\r\nDomain sds.jepara.go.id\r\nPort 50023\r\nDatabase\r\nsds.jepara.go.id/opensesame\r\nDb : sds_db\r\nUser : sds_user\r\nPass : sds_password', 1531788542, 1531788542),
(16, 22, 'sds.purworejokab.go.id', 'Ok,\r\ncpanel \r\nUsername : purwokab\r\nPass : c2B5bq21lY', 1531788607, 1536051253),
(17, 21, 'sds.purbalinggakab.go.id', 'hak akses belum dimintakan', 1531788660, 1531788660),
(18, 9, '-', 'server down, sedang menyurati diskominfo provinsi bidang tik untuk meminta hosting untuk aplikasi sds', 1531789246, 1531789246),
(19, 12, 'http://sds.karanganyarkab.go.id', 'ok,\r\ndomain: sds.karanganyarkab.go.id\r\ncwp: sds.karanganyarkab.go.id:2083\r\nuser: sds\r\npass: s1n9l3d4t4syst3mj00s', 1532334911, 1532337082),
(20, 1, 'http://sds.banjarnegarakab.go.id/', 'ok, \r\nCPANEL SINGLE DATA\r\n\r\nsds.banjarnegarakab.go.id/cpanel\r\n\r\nuser : sdsbna\r\npassword : 56#Multi-Country', 1532334971, 1532337029),
(21, 33, 'http://sds.semarangkota.go.id/', 'ok, \r\nsedang mengembangkan (http://sidadu.semarangkota.go.id)\r\nAkses Cpanel\r\nHost : https://webhost.semarangkota.go.id:2083/\r\nUser : sdssemarangkotag\r\nPass :sds_semarangkota', 1532921942, 1532921942),
(22, 20, 'http://sds.pemalangkab.go.id', 'oke,\r\n    - use : root\r\n    - pass: kominfopml2018\r\n#Ssh------------\r\n IP : 180.250.149.28\r\nPort: 5035', 1536629873, 1536629873),
(23, 29, 'http://sds.wonogirikab.go.id', '\r\n\r\nini Open Data\r\n[14:47, 9/10/2018] Fandi: Akses ssh ke 103.30.181.240\r\nUser : kominfo\r\nPassword : w0n0g1r1kab18\r\n[14:47, 9/10/2018] Fandi: http://sds.wonogirikab.go.id', 1541087101, 1541087425);

-- --------------------------------------------------------

--
-- Table structure for table `struktur_data`
--

CREATE TABLE IF NOT EXISTS `struktur_data` (
`id` int(11) NOT NULL,
  `parameter_api_id` int(11) DEFAULT NULL,
  `nama_struktur` varchar(50) DEFAULT NULL COMMENT 'Nama yang akan di tampilkan',
  `parameter` varchar(50) DEFAULT NULL COMMENT 'Field yang akan dibaca APInya',
  `relasi` enum('0','1') DEFAULT NULL,
  `grafik` enum('x','y','tidak') DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `struktur_data`
--

INSERT INTO `struktur_data` (`id`, `parameter_api_id`, `nama_struktur`, `parameter`, `relasi`, `grafik`, `created_at`, `updated_at`) VALUES
(16, 10, 'Golongan / Ruang', 'golongan', '', NULL, NULL, NULL),
(17, 10, 'Jumlah Pegawai', 'jumlah', '', NULL, NULL, NULL),
(18, 11, 'Nama', 'nama', '', NULL, NULL, NULL),
(19, 11, 'Golongan', 'gol', '', NULL, NULL, NULL),
(20, 11, 'Jumlah', 'jumlah', '', NULL, NULL, NULL),
(21, 12, 'Nama Pegawai', 'nama_pegawai', '', NULL, NULL, NULL),
(22, 12, 'NIP', 'nip', '', NULL, NULL, NULL),
(23, 12, 'Jabatan', 'jabatan', '', NULL, NULL, NULL),
(24, 12, 'Instansi', 'dinas', '', NULL, NULL, NULL),
(25, 13, 'Bulan', 'bulan', '1', 'x', NULL, NULL),
(26, 13, 'Jumlah', 'jumlah', '0', 'y', NULL, NULL),
(27, 14, 'Jenis Belanja', 'jenis', '0', 'x', NULL, NULL),
(28, 14, 'Jumlah', 'jumlah', '0', 'y', NULL, NULL),
(29, 15, 'Label', 'Field Data', '0', 'tidak', NULL, NULL),
(31, 16, 'Golongan', 'gol', '0', 'x', NULL, NULL),
(32, 16, 'Jumlah', 'jml', '0', 'y', NULL, NULL),
(33, 17, 'Nama Data', 'nama_data', '', NULL, NULL, NULL),
(34, 17, 'Umur Data', 'umur_data', '', NULL, NULL, NULL),
(35, 18, 'Jenis Penyakit', 'Jenis_penyakit', '0', 'tidak', NULL, NULL),
(36, 18, 'Tahun Kejadian', 'Tahun', '0', 'tidak', NULL, NULL),
(37, 18, 'Jumlah Pasien', 'Jumlah', '0', 'tidak', NULL, NULL),
(38, 19, 'Jenis Alat', 'jenis_alat', '0', 'tidak', NULL, NULL),
(39, 19, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(40, 19, 'Fungsi', 'fungsi', '0', 'tidak', NULL, NULL),
(41, 20, 'Tahun', 'tahun', '0', 'tidak', NULL, NULL),
(42, 20, 'Bulan', 'bulan', '0', 'tidak', NULL, NULL),
(43, 20, 'Jenis Penyakit', 'jenispenyakit', '0', 'tidak', NULL, NULL),
(44, 20, 'Keterangan', 'keterangan', '0', 'tidak', NULL, NULL),
(45, 20, 'Jumlah Pasien', 'jumlah', '0', 'tidak', NULL, NULL),
(46, 21, 'Nama Rumah Sakit', 'nama_rs', '0', 'tidak', NULL, NULL),
(47, 21, 'Klasifikasi', 'klasifikasi', '0', 'tidak', NULL, NULL),
(48, 21, 'Tipe Rumah Sakit', 'tipe_rs', '0', 'tidak', NULL, NULL),
(49, 21, 'Akreditasi', 'akreditasi', '0', 'tidak', NULL, NULL),
(50, 21, 'Jenis Layanan', 'jenis_layanan', '0', 'tidak', NULL, NULL),
(51, 21, 'Alamat Rumah Sakit', 'alamat_rs', '0', 'tidak', NULL, NULL),
(52, 21, 'No Tlp', 'no_telp', '0', 'tidak', NULL, NULL),
(53, 21, 'Fax', 'fax', '0', 'tidak', NULL, NULL),
(54, 21, 'E- Mail', 'email', '0', 'tidak', NULL, NULL),
(55, 21, 'Website', 'website', '0', 'tidak', NULL, NULL),
(56, 21, 'Jumlah Tempat Tidur', 'jumlah_tt', '0', 'tidak', NULL, NULL),
(57, 21, 'BOR', 'bor', '0', 'tidak', NULL, NULL),
(58, 21, 'LOS', 'los', '0', 'tidak', NULL, NULL),
(59, 21, 'TOI', 'toi', '0', 'tidak', NULL, NULL),
(60, 22, 'Nama Pegawai', 'nama_pegawai', '0', 'tidak', NULL, NULL),
(61, 22, 'NIP/NIK', 'nip_nik', '0', 'tidak', NULL, NULL),
(63, 22, 'TTL', 'ttl', '0', 'tidak', NULL, NULL),
(64, 22, 'Alamat', 'alamat_pegawai', '0', 'tidak', NULL, NULL),
(65, 22, 'Jenis Tenaga', 'jenis_tenaga', '0', 'tidak', NULL, NULL),
(66, 22, 'Jenis Kelamin', 'jenis_kelamin', '0', 'tidak', NULL, NULL),
(67, 22, 'Status Kepegawaian', 'status_kepegawaian', '0', 'tidak', NULL, NULL),
(68, 22, 'No Hp', 'no_hp_pegawai', '0', 'tidak', NULL, NULL),
(69, 22, 'Pendidikan', 'pendidikan_pegawai', '0', 'tidak', NULL, NULL),
(70, 23, 'Nama Alat', 'nama_alat', '0', 'tidak', NULL, NULL),
(71, 23, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(72, 23, 'Fungsi', 'fungsi', '0', 'tidak', NULL, NULL),
(73, 24, 'Bangsal', 'BANGSAL', '0', 'tidak', NULL, NULL),
(74, 24, 'Jenis', 'JENIS', '0', 'tidak', NULL, NULL),
(75, 24, 'Kelas 3', 'KELAS3', '0', 'tidak', NULL, NULL),
(76, 24, 'Kelas 2', 'KELAS2', '0', 'tidak', NULL, NULL),
(77, 24, 'Kelas 1', 'KELAS1', '0', 'tidak', NULL, NULL),
(78, 24, 'Utama', 'UTAMA', '0', 'tidak', NULL, NULL),
(79, 24, 'VIP', 'VIP', '0', 'tidak', NULL, NULL),
(80, 24, 'VIP A', 'VIPA', '0', 'tidak', NULL, NULL),
(81, 24, 'VIP B', 'VIPB', '0', 'tidak', NULL, NULL),
(82, 24, 'VVIP A', 'VVIPA', '0', 'tidak', NULL, NULL),
(83, 24, 'VVIP B', 'VVIPB', '0', 'tidak', NULL, NULL),
(84, 24, 'Super VVIP', 'SUPERVVIP', '0', 'tidak', NULL, NULL),
(85, 25, 'Bangsal', 'BANGSAL', '0', 'tidak', NULL, NULL),
(86, 25, 'Jenis', 'JENIS', '0', 'tidak', NULL, NULL),
(87, 25, 'Kelas 3', 'KELAS3', '0', 'tidak', NULL, NULL),
(88, 25, 'Kelas 2', 'KELAS2', '0', 'tidak', NULL, NULL),
(89, 25, 'Kelas 1', 'KELAS1', '0', 'tidak', NULL, NULL),
(90, 25, 'Utama', 'UTAMA', '0', 'tidak', NULL, NULL),
(91, 25, 'VIP', 'VIP', '0', 'tidak', NULL, NULL),
(92, 25, 'Intensif', 'INTEMSIF', '0', 'tidak', NULL, NULL),
(93, 25, 'Non Kelas', 'NONKELAS', '0', 'tidak', NULL, NULL),
(94, 26, 'Bangsal', 'BANGSAL', '0', 'x', NULL, NULL),
(95, 26, 'Kelas 1', 'KELAS1', '0', 'y', NULL, NULL),
(96, 26, 'Kelas 2', 'KELAS2', '0', 'tidak', NULL, NULL),
(97, 27, 'Nama Pegawai', 'nama_pegawai', '0', NULL, NULL, NULL),
(98, 27, 'NIP/NIK', 'nip_nik', '0', NULL, NULL, NULL),
(99, 27, 'Tempat Lahir', 'tempat_lahir', '0', NULL, NULL, NULL),
(100, 27, 'Tanggal Lahir', 'tanggal_lahir', '0', NULL, NULL, NULL),
(101, 27, 'Jenis Kelamin', 'jenis_kelamin', '0', NULL, NULL, NULL),
(102, 27, 'Status Kepegawaian', 'status_kepegawaian', '0', NULL, NULL, NULL),
(103, 27, 'No HP', 'no_hp_pegawai', '0', NULL, NULL, NULL),
(104, 27, 'Pendidikan', 'pendidikan_pegawai', '0', NULL, NULL, NULL),
(105, 26, 'Kelas 3', 'KELAS3', '0', 'tidak', NULL, NULL),
(106, 28, 'Tingkat Pendidikan', 'tkdik', '0', 'x', NULL, NULL),
(107, 28, 'Jumlah', 'jml', '0', 'y', NULL, NULL),
(108, 29, 'Bulan', 'bulan', '1', 'x', NULL, NULL),
(109, 29, 'Jumlah', 'jumlah', '0', 'y', NULL, NULL),
(110, 30, 'Jenis', 'jenis', '1', 'tidak', NULL, NULL),
(111, 30, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(112, 31, 'Golongan', 'gol', '0', 'tidak', NULL, NULL),
(113, 31, 'Jumlah', 'jml', '0', 'tidak', NULL, NULL),
(114, 32, 'No SP2D', 'no_sp2d', '0', 'tidak', NULL, NULL),
(115, 32, 'Tanggal SP2D', 'tanggal', '0', 'tidak', NULL, NULL),
(116, 32, 'Instansi', 'instansi', '0', 'tidak', NULL, NULL),
(117, 32, 'Nominal', 'nominal', '0', 'tidak', NULL, NULL),
(118, 32, 'Uraian', 'uraian', '0', 'tidak', NULL, NULL),
(119, 33, 'Diklat', 'diklat', '1', 'y', NULL, NULL),
(120, 33, 'Jumlah', 'jumlah', '0', 'x', NULL, NULL),
(121, 34, 'Nama', 'nama', '0', 'tidak', NULL, NULL),
(122, 34, 'nip', 'nip', '0', 'tidak', NULL, NULL),
(123, 35, 'Nama Diklat', 'diklat', '1', 'tidak', NULL, NULL),
(124, 35, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(125, 36, 'Nama', 'nama', '0', 'tidak', NULL, NULL),
(126, 36, 'Instansi', 'instansi', '0', 'tidak', NULL, NULL),
(127, 36, 'Diklat Tahun', 'tahun', '0', 'tidak', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tree`
--

CREATE TABLE IF NOT EXISTS `tree` (
`id` bigint(20) NOT NULL,
  `root` int(11) DEFAULT NULL,
  `aplikasi_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `child_allowed` tinyint(4) NOT NULL DEFAULT '0',
  `deskripsi` text NOT NULL,
  `status` tinyint(2) NOT NULL,
  `lvl` smallint(5) NOT NULL,
  `name` varchar(60) NOT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `icon_type` smallint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `selected` tinyint(1) NOT NULL DEFAULT '0',
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `readonly` tinyint(1) NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `collapsed` tinyint(1) NOT NULL DEFAULT '0',
  `movable_u` tinyint(1) NOT NULL DEFAULT '1',
  `movable_d` tinyint(1) NOT NULL DEFAULT '1',
  `movable_l` tinyint(1) NOT NULL DEFAULT '1',
  `movable_r` tinyint(1) NOT NULL DEFAULT '1',
  `removable` tinyint(1) NOT NULL DEFAULT '1',
  `removable_all` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tree`
--

INSERT INTO `tree` (`id`, `root`, `aplikasi_id`, `lft`, `rgt`, `child_allowed`, `deskripsi`, `status`, `lvl`, `name`, `slug`, `icon`, `icon_type`, `active`, `selected`, `disabled`, `readonly`, `visible`, `collapsed`, `movable_u`, `movable_d`, `movable_l`, `movable_r`, `removable`, `removable_all`) VALUES
(1, 1, NULL, 1, 34, 1, '', 0, 0, 'Kesehatan', 'kesehatan', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(2, 2, NULL, 1, 2, 1, '', 0, 0, 'Lingkungan Hidup', NULL, '', 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(3, 3, NULL, 1, 6, 1, '', 0, 0, 'Pariwisata', 'pariwisata', '', 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(5, 5, NULL, 1, 6, 1, '', 0, 0, 'Energi', 'energi-2', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(8, 3, NULL, 2, 5, 1, '', 0, 1, 'asdas', 'asdas', '', 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(9, 3, NULL, 3, 4, 1, '', 0, 2, 'dsdad', 'dsdad', '', 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(10, 1, NULL, 2, 3, 1, '', 0, 1, 'Pasien', 'pasien', '', 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(11, 11, NULL, 1, 18, 1, '', 0, 0, 'Tata Kelola Pemerintah', 'tata-kelola-pemerintah-2', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(12, 11, 20, 2, 3, 1, 'Data Pegawai Berdasarkan Golongan', 1, 1, 'Pegawai Golongan', 'pegawai-golongan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(13, 1, NULL, 4, 5, 1, '', 0, 1, 'Data B', 'data-b', '', 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(14, 14, NULL, 1, 2, 1, '', 0, 0, 'Lingkungan Hidup', 'lingkungan-hidup', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(15, 15, NULL, 1, 2, 1, '', 0, 0, 'Pariwisata', 'pariwisata-2', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(16, 16, NULL, 1, 2, 1, '', 0, 0, 'Sosial', 'sosial', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(17, 17, NULL, 1, 2, 1, '', 0, 0, 'Energi', 'energi', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(18, 18, NULL, 1, 2, 1, '', 0, 0, 'Pangan', 'pangan', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(19, 19, NULL, 1, 2, 1, '', 0, 0, 'Maritim', 'maritim', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(20, 20, NULL, 1, 4, 1, '', 0, 0, 'Ekonomi', 'ekonomi', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(21, 21, NULL, 1, 2, 1, '', 0, 0, 'Industri', 'industri', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(22, 22, NULL, 1, 2, 1, '', 0, 0, 'Infrastruktur', 'infrastruktur', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(23, 23, NULL, 1, 2, 1, '', 0, 0, 'Pendidikan dan Kebudayaan', 'pendidikan-dan-kebudayaan', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(24, 24, NULL, 1, 2, 1, '', 0, 0, 'Kepemudaan dan Olahraga', 'kepemudaan-dan-olahraga', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(25, 25, NULL, 1, 2, 1, '', 0, 0, 'Indikator Makro', 'indikator-makro', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(27, 11, 15, 4, 5, 1, '', 0, 1, 'adsasdasdas', 'adsasdasdas', '', 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(28, 11, 12, 6, 7, 1, '', 0, 1, 'Energi', 'energi-3', '', 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(29, 11, 20, 8, 9, 1, 'Data Pegawai Berdasarkan Pendidikan', 1, 1, 'Pegawai Pendidikan', 'pegawai-pendidikan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(30, 11, NULL, 10, 11, 1, '', 0, 1, '123', '123', '', 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(31, 11, NULL, 12, 13, 1, '', 0, 1, 'Energi1', 'energi1', '', 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(32, 11, 17, 14, 15, 1, '', 0, 1, 'Energi3', 'energi3', '', 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(34, 5, NULL, 2, 5, 1, '', 0, 1, 'a', 'a', '', 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(35, 5, NULL, 3, 4, 1, '', 0, 2, 'b', 'b', '', 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(37, 1, NULL, 6, 11, 1, '', 0, 1, 'RSUD Tugurejo', 'rsud-tugurejo', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(38, 1, 19, 7, 8, 1, 'Kejadian Penyakir Luarbiasa di RSUD Tugurejo', 1, 2, 'Kejadian Luar Biasa', 'kejadian-luar-biasa', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(39, 1, 19, 9, 10, 1, 'Ketersediaan Alat Kesehatan Di Rumah Salit Tugurejo', 1, 2, 'Alat Kesehatan', 'alat-kesehatan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(40, 1, NULL, 12, 23, 1, '', 0, 1, 'RSUD Margono', 'rsud-margono', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(41, 1, 23, 13, 14, 1, 'Data Kejadian Luar Biasa RSUD Margono', 1, 2, 'Kejadian Luar Biasa', 'kejadian-luar-biasa-2', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(42, 1, 23, 15, 16, 1, 'Data Rumah Sakit RS Tugurejo', 1, 2, 'Data Rumah Sakit', 'data-rumah-sakit', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(43, 1, 23, 17, 18, 1, 'Data Tenaga Kesehatan RSUD Margono', 1, 2, 'Tenaga Kesehatan', 'tenaga-kesehatan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(44, 1, 23, 19, 20, 1, 'Data Alkes RSUD Margono', 1, 2, 'Alat Kesehatan', 'alat-kesehatan-2', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(45, 1, 23, 21, 22, 1, 'Data Ketersediaann Ruang Tempat Tidur', 1, 2, 'Informasi Tempat Tidur', 'informasi-tempat-tidur', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(46, 1, NULL, 24, 27, 1, '', 0, 1, 'RSUD Sodjarwadi', 'rsud-sodjarwadi', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(47, 1, NULL, 28, 33, 1, '', 0, 1, 'RSUD Kelet', 'rsud-kelet', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(48, 1, 25, 25, 26, 1, 'Data Ketersediaan Tempat Tidur', 1, 2, 'Ketersediaan Tempat Tidur', 'ketersediaan-tempat-tidur', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(49, 1, 24, 29, 30, 1, 'Ketersediaan Tempat Tidur RSUD Kelet', 1, 2, 'Ketersediaan Tempat Tidur', 'ketersediaan-tempat-tidur-2', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(50, 1, 24, 31, 32, 1, 'Data Tenaga Kesehatan', 1, 2, 'Tenaga Kesehatan', 'tenaga-kesehatan-2', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(51, 20, 27, 2, 3, 1, 'Data Belanja Provinsi Jawa Tengah', 1, 1, 'Belanja Bulanan', 'belanja-bulanan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(54, 11, 20, 16, 17, 1, '', 1, 1, 'Diklat Pegawai', 'diklat-pegawai', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `instansi_id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nip` varchar(18) COLLATE utf8_unicode_ci NOT NULL,
  `nama_lengkap` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telepon_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `instansi_id`, `username`, `auth_key`, `nip`, `nama_lengkap`, `password_hash`, `password_reset_token`, `email`, `telepon_user`, `status`, `created_at`, `updated_at`) VALUES
(3, 40, 'mahfud', NULL, '198112142009011004', NULL, '$2y$13$6hkbImUlS3QWYRqvCfOYC.Y55SUoEC2hRZScuz32Zzt78E/V4T33m', NULL, 'mahfud@gmail.com', '', 10, 1532844132, 1539093536),
(4, 38, 'ekosri', NULL, '196503171986031009', NULL, '$2y$13$qG4xXhhQMEX1qN18ahJkXe/.y.mVlq0lYqVI89sggI9.ppB.hjuQG', NULL, 'echoos.09@gmail.com', '', 10, 1533172182, 1539093477),
(10, 38, NULL, NULL, '196304031993111002', NULL, '$2y$13$V45QPlssyX1kNNm4dnQv9O3wZfymdysahHwI5gIqrUtfUfkvPRH3.', NULL, '', '', 10, 1543369374, 1543369461);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aplikasi`
--
ALTER TABLE `aplikasi`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
 ADD PRIMARY KEY (`item_name`,`user_id`), ADD KEY `auth_assignment_user_id_idx` (`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
 ADD PRIMARY KEY (`name`), ADD KEY `rule_name` (`rule_name`), ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
 ADD PRIMARY KEY (`parent`,`child`), ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
 ADD PRIMARY KEY (`name`);

--
-- Indexes for table `data`
--
ALTER TABLE `data`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_data_instansi` (`instansi_id`);

--
-- Indexes for table `instansi`
--
ALTER TABLE `instansi`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_instansi`
--
ALTER TABLE `jenis_instansi`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
 ADD PRIMARY KEY (`version`);

--
-- Indexes for table `new_tree`
--
ALTER TABLE `new_tree`
 ADD PRIMARY KEY (`id`), ADD KEY `tree_NK1` (`root`), ADD KEY `tree_NK2` (`lft`), ADD KEY `tree_NK3` (`rgt`), ADD KEY `tree_NK4` (`lvl`), ADD KEY `tree_NK5` (`active`);

--
-- Indexes for table `parameter_api`
--
ALTER TABLE `parameter_api`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `route`
--
ALTER TABLE `route`
 ADD PRIMARY KEY (`name`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skpd`
--
ALTER TABLE `skpd`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_sds`
--
ALTER TABLE `status_sds`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_status_sds_instansi` (`instansi_id`);

--
-- Indexes for table `struktur_data`
--
ALTER TABLE `struktur_data`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tree`
--
ALTER TABLE `tree`
 ADD PRIMARY KEY (`id`), ADD KEY `tree_NK1` (`root`), ADD KEY `tree_NK2` (`lft`), ADD KEY `tree_NK3` (`rgt`), ADD KEY `tree_NK4` (`lvl`), ADD KEY `tree_NK5` (`active`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`), ADD UNIQUE KEY `nip` (`nip`), ADD UNIQUE KEY `password_reset_token` (`password_reset_token`), ADD KEY `FK_user_instansi` (`instansi_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aplikasi`
--
ALTER TABLE `aplikasi`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `data`
--
ALTER TABLE `data`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `instansi`
--
ALTER TABLE `instansi`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `jenis_instansi`
--
ALTER TABLE `jenis_instansi`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `new_tree`
--
ALTER TABLE `new_tree`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `parameter_api`
--
ALTER TABLE `parameter_api`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `skpd`
--
ALTER TABLE `skpd`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Kode Master SKPD Auto Increment';
--
-- AUTO_INCREMENT for table `status_sds`
--
ALTER TABLE `status_sds`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `struktur_data`
--
ALTER TABLE `struktur_data`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=128;
--
-- AUTO_INCREMENT for table `tree`
--
ALTER TABLE `tree`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `data`
--
ALTER TABLE `data`
ADD CONSTRAINT `FK_data_instansi` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `status_sds`
--
ALTER TABLE `status_sds`
ADD CONSTRAINT `FK_status_sds_instansi` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
ADD CONSTRAINT `FK_user_instansi` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
