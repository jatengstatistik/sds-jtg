-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: mariadb
-- Generation Time: Sep 24, 2019 at 01:24 AM
-- Server version: 10.3.16-MariaDB-1:10.3.16+maria~bionic
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apps_docker`
--

-- --------------------------------------------------------

--
-- Table structure for table `aplikasi`
--

CREATE TABLE `aplikasi` (
  `id` int(11) NOT NULL,
  `instansi_id` int(11) NOT NULL,
  `nama_aplikasi` varchar(150) DEFAULT NULL,
  `slug_nama_aplikasi` varchar(155) DEFAULT NULL,
  `url_aplikasi` varchar(150) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aplikasi`
--

INSERT INTO `aplikasi` (`id`, `instansi_id`, `nama_aplikasi`, `slug_nama_aplikasi`, `url_aplikasi`, `username`, `password`, `token`, `created_at`, `updated_at`) VALUES
(19, 39, 'SIM RS Tugurejo', 'sim-rs-tugurejo', 'http://data.jatengprov.go.id/apps/api/bridge/sds/tugurejo', 'admin123', 'rahasiaadmin123', '', 2147483647, 2147483647),
(20, 41, 'Simpeg BKD Jateng', 'simpeg-bkd-jateng', 'http://data.jatengprov.go.id/apps/api/bridge/sds/bkd', 'admin123', 'adminrahasia123', '', 2147483647, 2147483647),
(22, 41, 'SDS Demo', 'sds-demo', 'http://data.jatengprov.go.id/apps/api/bridge/sds/sds', 'admin123', '123456', '', 2147483647, 2147483647),
(23, 42, 'SIM RS Margono', 'sim-rs-margono', 'http://data.jatengprov.go.id/apps/api/bridge/sds/margono', 'admin123', '123456', '', 2147483647, 2147483647),
(24, 43, 'SIM RS Kelet', 'sim-rs-kelet', 'http://data.jatengprov.go.id/apps/api/bridge/sds/kelet', 'admin123', '123456', '', 2147483647, 2147483647),
(25, 44, 'SIM RS Soedjarwadi', 'sim-rs-soedjarwadi', 'http://data.jatengprov.go.id/apps/api/bridge/sds/sujarwadi', 'admin123', '123456', '', 2147483647, 2147483647),
(26, 45, 'Sistem Informasi Gender dan Anak (SIGA)', 'sistem-informasi-gender-dan-anak-siga', 'http://dp3akb.jatengprov.go.id/siga/app', '', '', '', 2147483647, 2147483647),
(27, 40, 'Aplikasi Keuangan BPKAD', 'aplikasi-keuangan-bpkad', 'https://bpkad.jatengprov.go.id/webservices', '198112142009011004', '123456', '', NULL, NULL),
(28, 49, 'Irigasi dan Air Baku', 'irigasi-dan-air-baku', 'http://pusdataru.jatengprov.go.id/webservice/api_op_iab', '', '', '', NULL, NULL),
(29, 48, 'SIM Padu PK', 'sim-padu-pk', 'http://data.jatengprov.go.id/apps/api/bridge/sds/bappeda', '', '', '', NULL, NULL),
(30, 50, 'Web GIS Jalan', 'web-gis-jalan', 'http://dpubinmarcipka.jatengprov.go.id/webgis/apisds/services.php', '', '', '', NULL, NULL),
(31, 52, 'One Touch Statistics', 'one-touch-statistics', 'http://data.jatengprov.go.id/apps/api/bridge/sds/bps', '198112142009011004', '123456', '', NULL, NULL),
(32, 53, 'SIM RSJ Arif Zainudin', 'sim-rsj-arif-zainudin', 'http://api.rsjd-arifzainudin.co.id/SDS', '198112142009011004', '123456', '', NULL, NULL),
(33, 66, 'SiKs', 'siks', 'http://siks.dinsos.jatengprov.go.id/api/siks', '', '', '', NULL, NULL),
(34, 70, 'SIM RSJ Dr. Amino Gondohutomo', 'sim-rsj-dr-amino-gondohutomo', 'http://api.rs-amino.jatengprov.go.id/sds/services/', '', '', '', NULL, NULL),
(35, 49, 'Tatonas', 'tatonas', 'http://pusdataru.jatengprov.go.id/webservice/api_tatonas', '', '', '', NULL, NULL),
(36, 38, 'Data RPJM 2013 - 2018', 'data-rpjm-2013---2018', NULL, NULL, NULL, NULL, NULL, NULL),
(37, 38, 'BDT', 'bdt', NULL, NULL, NULL, NULL, NULL, NULL),
(38, 72, 'SiJablay', 'sijablay', 'http://sijablay.dpmptsp.jatengprov.go.id/api/dpmptsp', '', '', '', NULL, NULL),
(39, 57, 'PDK Jateng', 'pdk-jateng', 'https://dataguru.pdkjateng.go.id/api', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('Admin SKPD', '34', 1554707238),
('Admin SKPD', '35', 1563794942),
('Admin SKPD', '37', 1564728153),
('Administrator', '13', 1551167951),
('Administrator', '14', 1548062081),
('Administrator', '16', 1548734719),
('Administrator', '4', 1551056936),
('Analis Data dan Informasi', '15', 1550219914),
('Analis Data dan Informasi', '18', 1550222200),
('Analis Data dan Informasi', '19', 1550217205),
('Analis Data dan Informasi', '20', 1550217457),
('Analis Data dan Informasi', '21', 1550217485),
('Analis Data dan Informasi', '22', 1550217507),
('Analis Data dan Informasi', '23', 1550217530),
('Analis Data dan Informasi', '24', 1550218029),
('Analis Data dan Informasi', '25', 1550221678),
('Analis Data dan Informasi', '26', 1550219624),
('Analis Data dan Informasi', '27', 1552029002),
('Analis Data dan Informasi', '28', 1550218226),
('Analis Data dan Informasi', '3', 1551337034),
('Analis Data dan Informasi', '33', 1551839802),
('Analis Data dan Informasi', '36', 1564538223),
('Super Administrator', '10', 1563792567),
('Super Administrator', '32', 1551858471);

-- --------------------------------------------------------

--
-- Table structure for table `auth_instansi_user`
--

CREATE TABLE `auth_instansi_user` (
  `user_id` int(11) DEFAULT NULL,
  `instansi_id` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_instansi_user`
--

INSERT INTO `auth_instansi_user` (`user_id`, `instansi_id`, `created_at`) VALUES
(19, 54, 1550217205),
(19, 54, 1550217205),
(20, 58, 1550217457),
(20, 58, 1550217457),
(21, 56, 1550217485),
(21, 56, 1550217485),
(22, 57, 1550217507),
(22, 57, 1550217507),
(23, 45, 1550217530),
(23, 45, 1550217530),
(24, 38, 1550218029),
(24, 38, 1550218029),
(28, 38, 1550218226),
(28, 38, 1550218226),
(26, 38, 1550219624),
(26, 38, 1550219624),
(26, 51, 1550219624),
(26, 57, 1550219624),
(26, 59, 1550219624),
(15, 38, 1550219915),
(15, 38, 1550219915),
(15, 60, 1550219915),
(15, 61, 1550219915),
(15, 62, 1550219915),
(25, 38, 1550221678),
(25, 38, 1550221679),
(25, 54, 1550221679),
(25, 63, 1550221679),
(25, 64, 1550221679),
(25, 65, 1550221679),
(29, 38, 1550221926),
(29, 38, 1550221926),
(29, 41, 1550221926),
(29, 66, 1550221926),
(29, 67, 1550221926),
(29, 68, 1550221926),
(18, 38, 1550222200),
(18, 38, 1550222200),
(18, 39, 1550222200),
(18, 42, 1550222200),
(18, 43, 1550222200),
(18, 44, 1550222200),
(18, 45, 1550222200),
(18, 53, 1550222201),
(18, 69, 1550222201),
(18, 70, 1550222201),
(30, 21, 1550472512),
(30, 21, 1550472512),
(4, 38, 1551056936),
(4, 38, 1551056936),
(4, 39, 1551056937),
(4, 40, 1551056937),
(4, 41, 1551056937),
(4, 42, 1551056937),
(4, 43, 1551056937),
(4, 44, 1551056937),
(4, 45, 1551056937),
(4, 48, 1551056937),
(4, 49, 1551056937),
(4, 50, 1551056937),
(4, 51, 1551056937),
(4, 52, 1551056937),
(4, 53, 1551056937),
(4, 54, 1551056937),
(4, 55, 1551056937),
(4, 56, 1551056937),
(4, 57, 1551056937),
(4, 58, 1551056937),
(4, 59, 1551056937),
(4, 60, 1551056937),
(4, 61, 1551056937),
(4, 62, 1551056937),
(4, 63, 1551056937),
(4, 64, 1551056937),
(4, 65, 1551056937),
(4, 66, 1551056937),
(4, 67, 1551056937),
(4, 68, 1551056937),
(4, 69, 1551056937),
(4, 70, 1551056937),
(13, 38, 1551167951),
(13, 1, 1551167951),
(13, 3, 1551167951),
(13, 4, 1551167951),
(13, 5, 1551167951),
(13, 7, 1551167952),
(13, 8, 1551167952),
(13, 9, 1551167952),
(13, 10, 1551167952),
(13, 11, 1551167953),
(13, 12, 1551167953),
(13, 13, 1551167953),
(13, 14, 1551167954),
(13, 15, 1551167954),
(13, 16, 1551167954),
(13, 17, 1551167954),
(13, 18, 1551167954),
(13, 19, 1551167954),
(13, 20, 1551167955),
(13, 21, 1551167955),
(13, 22, 1551167955),
(13, 23, 1551167955),
(13, 24, 1551167955),
(13, 25, 1551167955),
(13, 26, 1551167955),
(13, 27, 1551167955),
(13, 28, 1551167955),
(13, 29, 1551167955),
(13, 30, 1551167955),
(13, 31, 1551167955),
(13, 32, 1551167955),
(13, 33, 1551167955),
(13, 34, 1551167955),
(13, 35, 1551167955),
(13, 36, 1551167955),
(13, 37, 1551167955),
(13, 38, 1551167955),
(13, 39, 1551167955),
(13, 40, 1551167955),
(13, 41, 1551167956),
(13, 42, 1551167956),
(13, 43, 1551167956),
(13, 44, 1551167956),
(13, 45, 1551167956),
(13, 48, 1551167956),
(13, 49, 1551167956),
(13, 50, 1551167956),
(13, 51, 1551167956),
(13, 52, 1551167956),
(13, 53, 1551167956),
(13, 54, 1551167956),
(13, 55, 1551167956),
(13, 56, 1551167956),
(13, 57, 1551167957),
(13, 58, 1551167957),
(13, 59, 1551167957),
(13, 60, 1551167957),
(13, 61, 1551167957),
(13, 62, 1551167957),
(13, 63, 1551167957),
(13, 64, 1551167957),
(13, 65, 1551167957),
(13, 66, 1551167957),
(13, 67, 1551167957),
(13, 68, 1551167957),
(13, 69, 1551167957),
(13, 70, 1551167958),
(3, 40, 1551337034),
(3, 40, 1551337034),
(33, 71, 1551839802),
(33, 71, 1551839802),
(32, 38, 1551858471),
(32, 1, 1551858471),
(32, 3, 1551858471),
(32, 4, 1551858471),
(32, 5, 1551858471),
(32, 7, 1551858471),
(32, 8, 1551858471),
(32, 9, 1551858471),
(32, 10, 1551858471),
(32, 11, 1551858471),
(32, 12, 1551858471),
(32, 13, 1551858471),
(32, 14, 1551858471),
(32, 15, 1551858471),
(32, 16, 1551858471),
(32, 17, 1551858471),
(32, 18, 1551858471),
(32, 19, 1551858471),
(32, 20, 1551858471),
(32, 21, 1551858471),
(32, 22, 1551858471),
(32, 23, 1551858471),
(32, 24, 1551858471),
(32, 25, 1551858471),
(32, 26, 1551858471),
(32, 27, 1551858471),
(32, 28, 1551858472),
(32, 29, 1551858472),
(32, 30, 1551858472),
(32, 31, 1551858472),
(32, 32, 1551858472),
(32, 33, 1551858472),
(32, 34, 1551858472),
(32, 35, 1551858472),
(32, 36, 1551858472),
(32, 37, 1551858472),
(32, 38, 1551858472),
(32, 39, 1551858472),
(32, 40, 1551858472),
(32, 41, 1551858472),
(32, 42, 1551858472),
(32, 43, 1551858472),
(32, 44, 1551858472),
(32, 45, 1551858472),
(32, 48, 1551858472),
(32, 49, 1551858472),
(32, 50, 1551858472),
(32, 51, 1551858472),
(32, 52, 1551858472),
(32, 53, 1551858472),
(32, 54, 1551858472),
(32, 55, 1551858472),
(32, 56, 1551858472),
(32, 57, 1551858472),
(32, 58, 1551858472),
(32, 59, 1551858472),
(32, 60, 1551858472),
(32, 61, 1551858472),
(32, 62, 1551858472),
(32, 63, 1551858472),
(32, 64, 1551858472),
(32, 65, 1551858472),
(32, 66, 1551858472),
(32, 67, 1551858472),
(32, 68, 1551858472),
(32, 69, 1551858472),
(32, 70, 1551858472),
(32, 71, 1551858473),
(27, 38, 1552029002),
(27, 40, 1552029002),
(27, 41, 1552029002),
(27, 42, 1552029003),
(27, 43, 1552029003),
(27, 44, 1552029003),
(27, 45, 1552029003),
(27, 48, 1552029003),
(27, 49, 1552029003),
(27, 50, 1552029003),
(27, 51, 1552029003),
(27, 52, 1552029005),
(27, 53, 1552029005),
(27, 54, 1552029006),
(27, 55, 1552029006),
(27, 56, 1552029006),
(27, 57, 1552029006),
(27, 58, 1552029006),
(27, 59, 1552029006),
(27, 60, 1552029006),
(27, 61, 1552029006),
(27, 62, 1552029006),
(27, 63, 1552029007),
(27, 64, 1552029007),
(27, 65, 1552029007),
(27, 66, 1552029007),
(27, 67, 1552029007),
(27, 68, 1552029007),
(27, 69, 1552029007),
(27, 70, 1552029007),
(27, 71, 1552029007),
(34, 49, 1554709835),
(16, 1, 1556778964),
(16, 3, 1556778964),
(16, 4, 1556778964),
(16, 5, 1556778964),
(16, 7, 1556778964),
(16, 8, 1556778964),
(16, 9, 1556778964),
(16, 10, 1556778964),
(16, 11, 1556778964),
(16, 12, 1556778964),
(16, 13, 1556778965),
(16, 14, 1556778965),
(16, 15, 1556778965),
(16, 16, 1556778965),
(16, 17, 1556778965),
(16, 18, 1556778965),
(16, 19, 1556778965),
(16, 20, 1556778965),
(16, 21, 1556778965),
(16, 22, 1556778965),
(16, 23, 1556778965),
(16, 24, 1556778965),
(16, 25, 1556778965),
(16, 26, 1556778965),
(16, 27, 1556778965),
(16, 28, 1556778965),
(16, 29, 1556778965),
(16, 30, 1556778965),
(16, 31, 1556778965),
(16, 32, 1556778965),
(16, 33, 1556778965),
(16, 34, 1556778965),
(16, 35, 1556778965),
(16, 36, 1556778965),
(16, 37, 1556778965),
(16, 38, 1556778965),
(16, 39, 1556778965),
(16, 40, 1556778966),
(16, 41, 1556778966),
(16, 42, 1556778966),
(16, 43, 1556778966),
(16, 44, 1556778966),
(16, 45, 1556778966),
(16, 48, 1556778966),
(16, 49, 1556778966),
(16, 50, 1556778966),
(16, 51, 1556778966),
(16, 52, 1556778966),
(16, 53, 1556778966),
(16, 54, 1556778966),
(16, 55, 1556778966),
(16, 56, 1556778966),
(16, 57, 1556778966),
(16, 58, 1556778966),
(16, 59, 1556778967),
(16, 60, 1556778967),
(16, 61, 1556778967),
(16, 62, 1556778967),
(16, 63, 1556778967),
(16, 64, 1556778967),
(16, 65, 1556778967),
(16, 66, 1556778967),
(16, 67, 1556778967),
(16, 68, 1556778967),
(16, 69, 1556778967),
(16, 70, 1556778967),
(16, 71, 1556778967),
(14, 1, 1557982658),
(14, 3, 1557982658),
(14, 4, 1557982658),
(14, 5, 1557982658),
(14, 7, 1557982658),
(14, 8, 1557982659),
(14, 9, 1557982659),
(14, 10, 1557982659),
(14, 11, 1557982659),
(14, 12, 1557982659),
(14, 13, 1557982659),
(14, 14, 1557982659),
(14, 15, 1557982659),
(14, 16, 1557982660),
(14, 17, 1557982660),
(14, 18, 1557982660),
(14, 19, 1557982660),
(14, 20, 1557982661),
(14, 21, 1557982661),
(14, 22, 1557982661),
(14, 23, 1557982661),
(14, 24, 1557982661),
(14, 25, 1557982661),
(14, 26, 1557982661),
(14, 27, 1557982661),
(14, 28, 1557982661),
(14, 29, 1557982661),
(14, 30, 1557982661),
(14, 31, 1557982661),
(14, 32, 1557982661),
(14, 33, 1557982661),
(14, 34, 1557982661),
(14, 35, 1557982661),
(14, 36, 1557982661),
(14, 37, 1557982661),
(14, 38, 1557982661),
(14, 39, 1557982662),
(14, 40, 1557982662),
(14, 41, 1557982662),
(14, 42, 1557982662),
(14, 43, 1557982662),
(14, 44, 1557982662),
(14, 45, 1557982662),
(14, 48, 1557982662),
(14, 49, 1557982662),
(14, 50, 1557982662),
(14, 51, 1557982662),
(14, 52, 1557982662),
(14, 53, 1557982662),
(14, 54, 1557982662),
(14, 55, 1557982662),
(14, 56, 1557982662),
(14, 57, 1557982662),
(14, 58, 1557982663),
(14, 59, 1557982663),
(14, 60, 1557982663),
(14, 61, 1557982663),
(14, 62, 1557982663),
(14, 63, 1557982663),
(14, 64, 1557982663),
(14, 65, 1557982663),
(14, 66, 1557982663),
(14, 67, 1557982663),
(14, 68, 1557982663),
(14, 69, 1557982663),
(14, 70, 1557982663),
(14, 71, 1557982663),
(10, 38, 1563792567),
(10, 1, 1563792567),
(10, 3, 1563792567),
(10, 4, 1563792567),
(10, 5, 1563792567),
(10, 7, 1563792567),
(10, 8, 1563792567),
(10, 9, 1563792567),
(10, 10, 1563792567),
(10, 11, 1563792567),
(10, 12, 1563792567),
(10, 13, 1563792567),
(10, 14, 1563792567),
(10, 15, 1563792567),
(10, 16, 1563792567),
(10, 17, 1563792567),
(10, 18, 1563792567),
(10, 19, 1563792567),
(10, 20, 1563792567),
(10, 21, 1563792567),
(10, 22, 1563792567),
(10, 23, 1563792567),
(10, 24, 1563792567),
(10, 25, 1563792567),
(10, 26, 1563792567),
(10, 27, 1563792567),
(10, 28, 1563792567),
(10, 29, 1563792568),
(10, 30, 1563792568),
(10, 31, 1563792568),
(10, 32, 1563792568),
(10, 33, 1563792568),
(10, 34, 1563792568),
(10, 35, 1563792568),
(10, 36, 1563792568),
(10, 37, 1563792568),
(10, 39, 1563792568),
(10, 40, 1563792568),
(10, 41, 1563792568),
(10, 42, 1563792568),
(10, 43, 1563792568),
(10, 44, 1563792568),
(10, 45, 1563792568),
(10, 48, 1563792568),
(10, 49, 1563792568),
(10, 50, 1563792568),
(10, 51, 1563792568),
(10, 52, 1563792568),
(10, 53, 1563792568),
(10, 54, 1563792568),
(10, 55, 1563792568),
(10, 56, 1563792568),
(10, 57, 1563792568),
(10, 58, 1563792568),
(10, 59, 1563792568),
(10, 60, 1563792568),
(10, 61, 1563792568),
(10, 62, 1563792568),
(10, 63, 1563792568),
(10, 64, 1563792568),
(10, 65, 1563792568),
(10, 66, 1563792568),
(10, 67, 1563792568),
(10, 68, 1563792568),
(10, 69, 1563792568),
(10, 70, 1563792568),
(10, 71, 1563792568),
(10, 72, 1563792568),
(35, 72, 1563794942),
(35, 1, 1563794942),
(35, 3, 1563794942),
(35, 4, 1563794942),
(35, 5, 1563794942),
(35, 7, 1563794942),
(35, 8, 1563794942),
(35, 9, 1563794942),
(35, 10, 1563794942),
(35, 11, 1563794942),
(35, 12, 1563794942),
(35, 13, 1563794942),
(35, 14, 1563794942),
(35, 15, 1563794942),
(35, 16, 1563794942),
(35, 17, 1563794942),
(35, 18, 1563794942),
(35, 19, 1563794942),
(35, 20, 1563794942),
(35, 21, 1563794942),
(35, 22, 1563794942),
(35, 23, 1563794942),
(35, 24, 1563794942),
(35, 25, 1563794942),
(35, 26, 1563794942),
(35, 27, 1563794942),
(35, 28, 1563794942),
(35, 29, 1563794942),
(35, 30, 1563794942),
(35, 31, 1563794942),
(35, 32, 1563794942),
(35, 33, 1563794942),
(35, 34, 1563794942),
(35, 35, 1563794942),
(35, 36, 1563794942),
(35, 37, 1563794942),
(35, 38, 1563794942),
(35, 39, 1563794942),
(35, 40, 1563794942),
(35, 41, 1563794942),
(35, 42, 1563794942),
(35, 43, 1563794942),
(35, 44, 1563794942),
(35, 45, 1563794942),
(35, 48, 1563794942),
(35, 49, 1563794942),
(35, 50, 1563794942),
(35, 51, 1563794942),
(35, 52, 1563794942),
(35, 53, 1563794942),
(35, 54, 1563794942),
(35, 55, 1563794942),
(35, 56, 1563794942),
(35, 57, 1563794942),
(35, 58, 1563794942),
(35, 59, 1563794942),
(35, 60, 1563794942),
(35, 61, 1563794942),
(35, 62, 1563794942),
(35, 63, 1563794942),
(35, 64, 1563794942),
(35, 65, 1563794942),
(35, 66, 1563794942),
(35, 67, 1563794942),
(35, 68, 1563794942),
(35, 69, 1563794942),
(35, 70, 1563794942),
(35, 71, 1563794942),
(36, 66, 1564538223),
(37, 57, 1564728153);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, 1531667601, 1531667601),
('/aplikasi/*', 2, NULL, NULL, NULL, 1538908083, 1538908083),
('/aplikasi/create', 2, NULL, NULL, NULL, 1551839921, 1551839921),
('/aplikasi/index', 2, NULL, NULL, NULL, 1551839944, 1551839944),
('/aplikasi/update', 2, NULL, NULL, NULL, 1551839940, 1551839940),
('/aplikasi/view', 2, NULL, NULL, NULL, 1551839943, 1551839943),
('/data/*', 2, NULL, NULL, NULL, 1535258213, 1535258213),
('/data/create', 2, NULL, NULL, NULL, 1551839976, 1551839976),
('/debug/*', 2, NULL, NULL, NULL, 1535284797, 1535284797),
('/debug/default/*', 2, NULL, NULL, NULL, 1535284798, 1535284798),
('/debug/user/*', 2, NULL, NULL, NULL, 1535284799, 1535284799),
('/gii/*', 2, NULL, NULL, NULL, 1535284800, 1535284800),
('/gii/default/*', 2, NULL, NULL, NULL, 1535284801, 1535284801),
('/instansi/*', 2, NULL, NULL, NULL, 1535258172, 1535258172),
('/integrasi-data/*', 2, NULL, NULL, NULL, 1543306933, 1543306933),
('/jenis-instansi/*', 2, NULL, NULL, NULL, 1543301059, 1543301059),
('/kabkota/*', 2, NULL, NULL, NULL, 1535258171, 1535258171),
('/mimin/*', 2, NULL, NULL, NULL, 1535241408, 1535241408),
('/mimin/role/*', 2, NULL, NULL, NULL, 1535284805, 1535284805),
('/mimin/route/*', 2, NULL, NULL, NULL, 1535284805, 1535284805),
('/mimin/user/*', 2, NULL, NULL, NULL, 1535284808, 1535284808),
('/sector/*', 2, NULL, NULL, NULL, 1535258189, 1535258189),
('/setting/*', 2, NULL, NULL, NULL, 1543390839, 1543390839),
('/setting/update', 2, NULL, NULL, NULL, 1543392062, 1543392062),
('/setting/view', 2, NULL, NULL, NULL, 1543392059, 1543392059),
('/site/*', 2, NULL, NULL, NULL, 1532947156, 1532947156),
('/site/index', 2, NULL, NULL, NULL, 1563794907, 1563794907),
('/skpd/*', 2, NULL, NULL, NULL, 1532947203, 1532947203),
('/status-sds/*', 2, NULL, NULL, NULL, 1533120501, 1533120501),
('/status-sds/index', 2, NULL, NULL, NULL, 1549588685, 1549588685),
('/status-sds/view', 2, NULL, NULL, NULL, 1549588687, 1549588687),
('/tipe-data/*', 2, NULL, NULL, NULL, 1536074575, 1536074575),
('/treemanager/*', 2, NULL, NULL, NULL, 1535241147, 1535241147),
('/treemanager/node/*', 2, NULL, NULL, NULL, 1535241154, 1535241154),
('/user/*', 2, NULL, NULL, NULL, 1535258252, 1535258252),
('Admin SKPD', 1, NULL, NULL, NULL, 1532947142, 1563851052),
('Administrator', 1, NULL, NULL, NULL, 1535258138, 1546851911),
('Analis Data dan Informasi', 1, NULL, NULL, NULL, 1549588599, 1551840219),
('Sds Status', 1, NULL, NULL, NULL, 1533120496, 1535240054),
('Super Administrator', 1, NULL, NULL, NULL, 1531661172, 1568696731);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('Admin SKPD', '/data/*'),
('Admin SKPD', '/integrasi-data/*'),
('Admin SKPD', '/sector/*'),
('Admin SKPD', '/site/*'),
('Admin SKPD', '/site/index'),
('Admin SKPD', '/treemanager/*'),
('Admin SKPD', '/treemanager/node/*'),
('Administrator', '/aplikasi/*'),
('Administrator', '/data/*'),
('Administrator', '/instansi/*'),
('Administrator', '/integrasi-data/*'),
('Administrator', '/jenis-instansi/*'),
('Administrator', '/kabkota/*'),
('Administrator', '/mimin/user/*'),
('Administrator', '/setting/*'),
('Administrator', '/site/*'),
('Administrator', '/skpd/*'),
('Administrator', '/status-sds/*'),
('Administrator', '/treemanager/*'),
('Administrator', '/treemanager/node/*'),
('Administrator', '/user/*'),
('Analis Data dan Informasi', '/aplikasi/create'),
('Analis Data dan Informasi', '/aplikasi/index'),
('Analis Data dan Informasi', '/aplikasi/update'),
('Analis Data dan Informasi', '/aplikasi/view'),
('Analis Data dan Informasi', '/data/*'),
('Analis Data dan Informasi', '/integrasi-data/*'),
('Analis Data dan Informasi', '/site/*'),
('Analis Data dan Informasi', '/status-sds/index'),
('Analis Data dan Informasi', '/status-sds/view'),
('Analis Data dan Informasi', '/treemanager/*'),
('Analis Data dan Informasi', '/treemanager/node/*'),
('Sds Status', '/site/*'),
('Sds Status', '/status-sds/*'),
('Super Administrator', '/*'),
('Super Administrator', '/aplikasi/*'),
('Super Administrator', '/data/*'),
('Super Administrator', '/debug/*'),
('Super Administrator', '/debug/default/*'),
('Super Administrator', '/debug/user/*'),
('Super Administrator', '/gii/*'),
('Super Administrator', '/gii/default/*'),
('Super Administrator', '/instansi/*'),
('Super Administrator', '/integrasi-data/*'),
('Super Administrator', '/jenis-instansi/*'),
('Super Administrator', '/kabkota/*'),
('Super Administrator', '/mimin/*'),
('Super Administrator', '/mimin/role/*'),
('Super Administrator', '/mimin/route/*'),
('Super Administrator', '/mimin/user/*'),
('Super Administrator', '/sector/*'),
('Super Administrator', '/setting/update'),
('Super Administrator', '/setting/view'),
('Super Administrator', '/site/*'),
('Super Administrator', '/skpd/*'),
('Super Administrator', '/status-sds/*'),
('Super Administrator', '/tipe-data/*'),
('Super Administrator', '/treemanager/*'),
('Super Administrator', '/treemanager/node/*'),
('Super Administrator', '/user/*');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_storage`
--

CREATE TABLE `data_storage` (
  `id` int(11) NOT NULL,
  `aplikasi_id` int(11) NOT NULL,
  `instansi_id` int(11) NOT NULL,
  `nama_data` varchar(255) NOT NULL,
  `slug_data` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `deskripsi_data` varchar(255) DEFAULT NULL,
  `analisis_data` text DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `instansi`
--

CREATE TABLE `instansi` (
  `id` int(11) NOT NULL,
  `jenis_instansi_id` int(11) NOT NULL,
  `nama_instansi` varchar(255) DEFAULT NULL,
  `singkatan_instansi` varchar(255) DEFAULT NULL,
  `alamat_instansi` varchar(255) DEFAULT NULL,
  `telepon_instansi` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instansi`
--

INSERT INTO `instansi` (`id`, `jenis_instansi_id`, `nama_instansi`, `singkatan_instansi`, `alamat_instansi`, `telepon_instansi`, `created_at`, `updated_at`) VALUES
(1, 3, 'Kabupaten Banjarnegara', NULL, '', NULL, 1531667859, 1531667859),
(3, 3, 'Kabupaten Banyumas', NULL, '', NULL, 1531667900, 1531667900),
(4, 3, 'Kabupaten Batang', NULL, '', NULL, 1531668273, 1531668273),
(5, 3, 'Kabupaten Blora', NULL, '', NULL, 1531668312, 1531668312),
(7, 3, 'Kabupaten Boyolali', NULL, '', NULL, 1531668439, 1531668439),
(8, 3, 'Kabupaten Brebes', NULL, '', NULL, 1531668456, 1531668456),
(9, 3, 'Kabupaten Demak', NULL, '', NULL, 1531668488, 1531668488),
(10, 3, 'Kabupaten Grobogan', NULL, '', NULL, 1531668511, 1531668511),
(11, 3, 'Kabupaten Jepara', NULL, '', NULL, 1531668529, 1531668529),
(12, 3, 'Kabupaten Karanganyar', NULL, '', NULL, 1531668545, 1531668545),
(13, 3, 'Kabupaten Kebumen', NULL, '', NULL, 1531668561, 1531668561),
(14, 3, 'Kabupaten Kendal', NULL, '', NULL, 1531668579, 1531668579),
(15, 3, 'Kabupaten Klaten', NULL, '', NULL, 1531668596, 1531668596),
(16, 3, 'Kabupaten Kudus', NULL, '', NULL, 1531668614, 1531668614),
(17, 3, 'Kabupaten Magelang', NULL, '', NULL, 1531668631, 1531668631),
(18, 3, 'Kabupaten Pati', NULL, '', NULL, 1531668649, 1531668649),
(19, 3, 'Kabupaten Pekalongan', NULL, '', NULL, 1531668666, 1531668666),
(20, 3, 'Kabupaten Pemalang', NULL, '', NULL, 1531668682, 1531668682),
(21, 3, 'Kabupaten Purbalingga', NULL, '', NULL, 1531668699, 1531668699),
(22, 3, 'Kabupaten Purworejo', NULL, '', NULL, 1531668715, 1531668715),
(23, 3, 'Kabupaten Rembang', NULL, '', NULL, 1531668732, 1531668732),
(24, 3, 'Kabupaten Semarang', NULL, '', NULL, 1531668750, 1531668750),
(25, 3, 'Kabupaten Sragen', NULL, '', NULL, 1531668767, 1531668767),
(26, 3, 'Kabupaten Sukoharjo', NULL, '', NULL, 1531668784, 1531668784),
(27, 3, 'Kabupaten Tegal', NULL, '', NULL, 1531668800, 1531668800),
(28, 3, 'Kabupaten Temanggung', NULL, '', NULL, 1531668816, 1531668816),
(29, 3, 'Kabupaten Wonogiri', NULL, '', NULL, 1531668833, 1531668833),
(30, 3, 'Kabupaten Wonosobo', NULL, '', NULL, 1531668850, 1531668850),
(31, 3, 'Kota Pekalongan', NULL, '', NULL, 1531668869, 1531668869),
(32, 3, 'Kota Salatiga', NULL, '', NULL, 1531668885, 1531668885),
(33, 3, 'Kota Semarang', NULL, '', NULL, 1531668902, 1531668902),
(34, 3, 'Kota Surakarta', NULL, '', NULL, 1531668936, 1531668936),
(35, 3, 'Kota Tegal', NULL, '', NULL, 1531668969, 1531668969),
(36, 3, 'Kota Magelang', NULL, '', NULL, 1531668991, 1531668991),
(37, 3, 'Kabupaten Cilacap', NULL, '', NULL, 1531671088, 1531671088),
(38, 1, 'Dinas Komunikasi dan Informatika', 'Diskominfo', 'Jl. Mentri Supeno I No 2 Semarang', '0248319140', NULL, 1543301990),
(39, 1, 'Rumah Sakit Umum Daerah Tugurejo', 'RSUD Tugurejo', 'Jl. Walisongo KM. 8,5, Tambakaji, Ngaliyan, Tambakaji, Ngaliyan, Kota Semarang, Jawa Tengah 50185', '', NULL, 1543370372),
(40, 1, 'Badan Pengelola Keuangan dan Aset Daerah', 'BPKAD', 'Jl. Taman Menteri Supeno No. 2 ', '0248311174', NULL, 1543302076),
(41, 1, 'Badan Kepegawaian Daerah', 'BKD', 'Jl. Stadion Selatan No. 1, Semarang', '0248318846', NULL, 1543302129),
(42, 1, 'RSUD Prof. Dr. Margono Soekarjo', 'RSUD Margono', 'Jl. Dr. Gumbreg No.1 Purwokerto, Kabupaten Banyumas Provinsi Jawa Tengah, Kode Pos 53146', '0281632708', NULL, 1543302139),
(43, 1, 'RSUD Kelet', 'RSUD Kelet', '', '', NULL, 1543302115),
(44, 1, 'RSUD Soedjarwadi', 'RSUD Sodjarwadi', '', '', NULL, 1543302102),
(45, 1, 'Dinas Pemberdayaan Perempuan, Perlindungan Anak, Pengendalian Penduduk Dan Keluarga Berencana', 'DP3AKB', 'Jl. Pamularsih No. 28 Semarang 50148', '', NULL, 1543302089),
(48, 1, 'Badan Perencanaan dan Pembangunan Daerah', 'BAPPEDA', '', '', 1547556667, 1547556667),
(49, 1, 'Dinas Pekerjaan Umum Sumber Daya Air dan Penataan Ruang', 'DPUSDATARU', 'Jl. Madukoro Blok AA-BB Semarang ', '(024)7608201', 1547685141, 1547685141),
(50, 1, 'Dinas Pekerjaan Umum Bina Marga dan Cipta Karya', 'DPUBINMARCIPKA', 'Jl. Madukoro Blok AA-BB ,', '(024) 7608368', 1547685640, 1547685640),
(51, 1, 'Badan Pengembangan Sumber Daya Manusia Daerah', 'BPSDMD', 'Jl. Setia Budi No.201A, Srondol Kulon, Semarang, Kota Semarang, Jawa Tengah 50263', '024 7473066', 1547685898, 1547685898),
(52, 4, 'Badan Pusat Statistik', 'BPS Provinsi Jawa Tengah', '', '', 1548032700, 1548032700),
(53, 1, 'RSJD Arif Zainudin', 'RSJD Surakarta', '', '', 1549954245, 1549954245),
(54, 1, 'DPRD Provinsi Jawa Tengah', 'Sekretaris DPRD', '', '', 1550216906, 1550220311),
(55, 1, 'Badan Pengembangan Sumber Daya Manusia', 'BPSDM', '', '', 1550216961, 1550216961),
(56, 1, 'Badan Kesatuan Bangsa Politik dan Perlindungan Masyarakat', 'KESBANGPOLLINMAS', '', '', 1550217031, 1550217031),
(57, 1, 'Dinas Pendidikan Dan Kebudayaan', 'DISDIKBUD', '', '', 1550217064, 1550217064),
(58, 1, 'Dinas Energi dan Sumber Data Mineral', 'DInas ESDM', '', '', 1550217096, 1550217096),
(59, 1, 'Badan Penghubung', 'Badan Penghubung', '', '', 1550219594, 1550219594),
(60, 1, 'Dinas Kesehatan', 'DINKES', '', '', 1550219665, 1550219665),
(61, 1, 'Dinas Pemberdayaan Masyarakat Desa, Kependudukan dan Pencatatan Sipil Provinsi Jawa Tengah', 'DISPERMASDESDUKCAPIL', '', '', 1550219763, 1550219763),
(62, 1, 'Badan Penanggulangan Bencana Daerah', 'BPBD', '', '', 1550219848, 1550219848),
(63, 1, 'Dinas Kepemudaan Olah Raga dan Pariwisata Provinsi Jawa Tengah', 'Disporapar', '', '', 1550220150, 1550220150),
(64, 1, 'Satuan Polisi Pamong Praja Provinsi Jawa Tengah', 'SATPOL PP', '', '', 1550220184, 1550220184),
(65, 1, 'Inspektorat', 'Inspektorat', '', '', 1550220212, 1550220346),
(66, 1, 'Dinas Sosial Provinsi Jawa Tengah', 'DINSOS', '', '', 1550221752, 1550221752),
(67, 1, 'Dinas Tenaga Kerja dan Transmigrasi', 'DIsnakertrans', '', '', 1550221780, 1550221780),
(68, 1, 'Dinas Kearsipan dan Perpustakaan', 'Dinas Arpus', '', '', 1550221801, 1550221801),
(69, 1, 'RSUD dr. Moewardi Surakarta', 'RSUD dr. Moewardi ', '', '', 1550222073, 1550222073),
(70, 1, 'RSJD Dr. Amino Gondohutomo', 'RSJD Amino', '', '', 1550222113, 1550222113),
(71, 1, 'Dinas Kelautan dan Perikanan', 'DINLUTKAN', '', '', 1551839738, 1551839738),
(72, 1, 'Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu', 'DPMPTSP', 'Jl. Mgr Sugiyopranoto No.1, Pendrikan Kidul, Kec. Semarang Tengah, Kota Semarang, Jawa Tengah 50131', '0243547091', 1563792219, 1563792219);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_instansi`
--

CREATE TABLE `jenis_instansi` (
  `id` int(11) NOT NULL,
  `jenis_instansi` varchar(80) NOT NULL,
  `keterangan_jenis_instansi` varchar(200) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_instansi`
--

INSERT INTO `jenis_instansi` (`id`, `jenis_instansi`, `keterangan_jenis_instansi`, `created_at`, `updated_at`) VALUES
(1, 'SKPD', 'Satuan Kerja Perangkat Daerah', 0, 0),
(2, 'BUMD', 'Badan Usaha Milih Daerah', 0, 0),
(3, 'Kab/Kota', 'Kabupaten Kota se Jateng', 0, 0),
(4, 'Instansi Vertikal', 'Instansi Vertikal', 1547781215, 1547781215);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1531454941),
('m130524_201442_init', 1531454970),
('m140506_102106_rbac_init', 1531454949),
('m151024_072453_create_route_table', 1531454957),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1531454950),
('m181002_142147_new_tree', 1538500732),
('m230416_200116_tree', 1532859718);

-- --------------------------------------------------------

--
-- Table structure for table `new_tree`
--

CREATE TABLE `new_tree` (
  `id` bigint(20) NOT NULL,
  `tree_id` bigint(20) NOT NULL,
  `root` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `lvl` smallint(5) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 0,
  `child_allowed` tinyint(1) NOT NULL DEFAULT 1,
  `name` varchar(60) NOT NULL,
  `slug_new_tree` varchar(150) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `icon_type` smallint(1) NOT NULL DEFAULT 1,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `selected` tinyint(1) NOT NULL DEFAULT 0,
  `disabled` tinyint(1) NOT NULL DEFAULT 0,
  `readonly` tinyint(1) NOT NULL DEFAULT 0,
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `collapsed` tinyint(1) NOT NULL DEFAULT 0,
  `movable_u` tinyint(1) NOT NULL DEFAULT 1,
  `movable_d` tinyint(1) NOT NULL DEFAULT 1,
  `movable_l` tinyint(1) NOT NULL DEFAULT 1,
  `movable_r` tinyint(1) NOT NULL DEFAULT 1,
  `removable` tinyint(1) NOT NULL DEFAULT 1,
  `removable_all` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `new_tree`
--

INSERT INTO `new_tree` (`id`, `tree_id`, `root`, `lft`, `rgt`, `lvl`, `status`, `child_allowed`, `name`, `slug_new_tree`, `deskripsi`, `icon`, `icon_type`, `active`, `selected`, `disabled`, `readonly`, `visible`, `collapsed`, `movable_u`, `movable_d`, `movable_l`, `movable_r`, `removable`, `removable_all`) VALUES
(1, 1, 1, 1, 2, 0, 0, 0, 'Input Data', 'input-data', 'Input Data ', '', 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0),
(28, 29, 28, 1, 2, 0, 1, 1, 'Tingkat Pendidikan', 'tingkat-pendidikan', 'Data PNS Perdasarkan Pegawai Pendidikan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(35, 38, 35, 1, 2, 0, 1, 1, 'Kejadian Luar Biasa', 'kejadian-luar-biasa', 'Kejadian Luar Biasa di Lingkungan RSUD Tugurejo', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(36, 39, 36, 1, 2, 0, 0, 1, 'Alat Kesehatan', 'alat-kesehatan', 'Ketersediaan Alat Kesehatan di RS Tugurejo', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(39, 48, 39, 1, 2, 0, 0, 1, 'Informasi Tempat Tidur', 'informasi-tempat-tidur', 'Daftar Tempat Tidut', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(40, 43, 40, 1, 2, 0, 0, 1, 'Tenaga Kesehatan', 'tenaga-kesehatan', 'Data Tenaga Kesehatan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(41, 44, 41, 1, 2, 0, 0, 1, 'Ketersediaan Alat Kesehatan', 'ketersediaan-alat-kesehatan', 'Data Alat Kesehatan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(42, 45, 42, 1, 2, 0, 0, 1, 'Informasi Tempat Tidur', 'informasi-tempat-tidur-2', 'Data Informasi Tempat Tidur', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(43, 49, 43, 1, 2, 0, 0, 1, 'Informasi Tempat Tidur', 'informasi-tempat-tidur-3', 'Ketersediaan Tembat Tidur Kosong', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(44, 50, 44, 1, 2, 0, 0, 1, 'Tenaga Kesehatan', 'tenaga-kesehatan-2', 'Data Tenaga Kesehatan di RSUD Kelet', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(45, 51, 45, 1, 6, 0, 1, 1, 'Belanja Bulanan', 'belanja-bulanan-2', '', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(46, 51, 45, 2, 5, 1, 1, 1, 'Bulan', 'bulan-2', '', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(47, 12, 47, 1, 2, 0, 1, 1, 'Pegawai Golongan', 'pegawai-golongan', 'Data Pegawai Berdasar Golongan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(48, 51, 45, 3, 4, 2, 1, 1, 'Jenis', 'jenis', '', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(54, 55, 54, 1, 2, 0, 1, 1, 'Data Waduk', 'data-waduk', 'Data Waduk Jawa Tengah', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(55, 56, 55, 1, 2, 0, 1, 1, 'Indikator Gender dan Anak', 'indikator-gender-dan-anak', 'Indikator Gender dan Anak', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(56, 57, 56, 1, 2, 0, 1, 1, 'Rumah Tangga Miskin', 'rumah-tangga-miskin', 'Sistem Informasi Terpadu Penanggulangan Kemiskinan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(57, 58, 57, 1, 2, 0, 1, 1, 'Rumah Tidak Ber Listrik', 'rumah-tidak-ber-listrik', 'Rumah Tidak Ber Listrik Jateng Kab Kota', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(58, 59, 58, 1, 2, 0, 1, 1, 'Rumah Tidak Layak Huni', 'rumah-tidak-layak-huni', 'Rumah Tidak Layak Huni Kabupaten Kota Jateng', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(59, 60, 59, 1, 2, 0, 1, 1, 'Rumah Tidak Berjamban', 'rumah-tidak-berjamban', 'Rumah Tangga Tidak Berjamban', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(60, 61, 60, 1, 2, 0, 1, 1, 'Desil Kesejahteraan', 'desil-kesejahteraan', 'Desil Kkesejahteraan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(61, 62, 61, 1, 2, 0, 1, 1, 'Lapangan Pekerjaan', 'lapangan-pekerjaan', 'Data Lapangan Pekerjaan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(63, 54, 63, 1, 2, 0, 1, 1, 'Diklat Pegawai', 'diklat-pegawai', 'Jumlah Diklat Pegawai', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(64, 64, 64, 1, 2, 0, 1, 1, 'Jenis Kelamin Pegawai', 'jenis-kelamin-pegawai', 'Data Pegawai Berdasarkan Jenis Kelamin', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(65, 65, 65, 1, 2, 0, 1, 1, 'Indeks Pembangunan Manusia', 'indeks-pembangunan-manusia', 'Indeks Pembangunan Manusia Series', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(66, 67, 66, 1, 2, 0, 1, 1, 'Ketersediaan Tempat Tidur', 'ketersediaan-tempat-tidur', 'Data Ketersediaan Tempat tidur RSJ Surakarta', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(67, 68, 67, 1, 2, 0, 1, 1, 'Alat Kesehatan', 'alat-kesehatan-2', 'Data Alat Kesehatan RSJ Surakarta', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(68, 69, 68, 1, 2, 0, 1, 1, 'Data Rumah Sakit', 'data-rumah-sakit', 'Data Rumah Sakit RSJ Arif Zainudin', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(69, 70, 69, 1, 2, 0, 1, 1, 'Tenaga Kesehatan', 'tenaga-kesehatan-3', 'Data Tenaga Kesehatan RSJD Arif Zainudin', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(70, 71, 70, 1, 2, 0, 1, 1, 'Panti Sosial', 'panti-sosial', 'Daa Panti Sosial Jawa Tengah', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(71, 73, 71, 1, 4, 0, 1, 1, 'Ketersediaan Kamar', 'ketersediaan-kamar', 'Data ketersediaaan kamar RSJ Amino Ghondohutomo', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(72, 73, 71, 2, 3, 1, 1, 1, 'Data Ketersediaan Berdasarkan Kelas', 'data-ketersediaan-berdasarkan-kelas', 'Ketersediaan kamar berdasarkan kelas', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(73, 74, 73, 1, 2, 0, 1, 1, 'Data Bendung', 'data-bendung', '', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(74, 75, 74, 1, 2, 0, 1, 1, 'Curah Hujan', 'curah-hujan', '', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(75, 76, 75, 1, 2, 0, 1, 1, 'Tinggi Muka Air', 'tinggi-muka-air', '', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(77, 78, 77, 1, 2, 0, 1, 1, 'Data Rekap Perizinan', 'data-rekap-perizinan', '', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(78, 79, 78, 1, 2, 0, 1, 1, 'Rekap Realisasi Investasi', 'rekap-realisasi-investasi', '', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(79, 80, 79, 1, 2, 0, 1, 1, 'Jumlah Sekolah', 'jumlah-sekolah', 'Jumlah Sekolah', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(80, 81, 80, 1, 2, 0, 1, 1, 'Tenaga Pendidik Jateng', 'tenaga-pendidik-jateng', 'Rekap-GTTPTT', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(81, 82, 81, 1, 2, 0, 1, 1, 'Data Pegawai Golongan', 'data-pegawai-golongan', '', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `parameter_api`
--

CREATE TABLE `parameter_api` (
  `id` int(11) NOT NULL,
  `new_tree_id` bigint(20) NOT NULL,
  `parameter_api` varchar(50) DEFAULT NULL,
  `slug_parameter_api` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parameter_api`
--

INSERT INTO `parameter_api` (`id`, `new_tree_id`, `parameter_api`, `slug_parameter_api`) VALUES
(15, 1, 'parameter_api', 'parameterapi'),
(18, 35, 'kejadian-luar-biasa', 'kejadian-luar-biasa'),
(19, 36, 'alat-kesehatan', 'alat-kesehatan'),
(22, 40, 'tenaga-kesehatan', 'tenaga-kesehatan'),
(23, 41, 'ketersediaan-alat-kesehatan', 'ketersediaan-alat-kesehatan'),
(24, 42, 'informasi-tempat-tidur', 'informasi-tempat-tidur'),
(25, 39, 'informasi-tempat-tidur', 'informasi-tempat-tidur-2'),
(26, 43, 'informasi-tempat-tidur', 'informasi-tempat-tidur-3'),
(27, 44, 'tenaga-kesehatan', 'tenaga-kesehatan-2'),
(28, 28, 'tingkat-pendidikan', 'tingkat-pendidikan'),
(29, 45, 'belanja-bulanan', 'belanja-bulanan'),
(30, 46, 'bulan', 'bulan-2'),
(31, 47, 'pegawai-golongan', 'pegawai-golongan-3'),
(32, 48, 'jenis', 'jenis'),
(37, 54, 'waduk', 'waduk'),
(38, 55, 'indikators', 'indikators'),
(39, 56, 'rt-miskin-kab-kota', 'rt-miskin-kab-kota'),
(40, 57, 'rumah-tidak-listrik-kab-kota', 'rumah-tidak-listrik-kab-kota'),
(41, 58, 'rtlh', 'rtlh'),
(42, 59, 'rt-tidak-jamban', 'rt-tidak-jamban'),
(43, 60, 'desil-kesejahteraan', 'desil-kesejahteraan'),
(44, 61, 'lapangan-pekerjaan', 'lapangan-pekerjaan'),
(51, 63, 'pegawai-diklat', 'pegawai-diklat'),
(52, 64, 'pegawai-jk', 'pegawai-jk'),
(53, 65, 'ipm-series', 'ipm-series'),
(54, 66, 'kapasitas-tt', 'kapasitas-tt'),
(55, 67, 'alat-kesehatan', 'alat-kesehatan-2'),
(56, 68, 'data-rs', 'data-rs'),
(57, 69, 'tenaga-kesehatan', 'tenaga-kesehatan-3'),
(58, 70, 'panti', 'panti'),
(59, 71, 'ketersediaan-kamar', 'ketersediaan-kamar'),
(60, 73, 'bendung', 'bendung'),
(61, 74, 'curah_hujan', 'curahhujan'),
(62, 75, 'tinggi_muka_air', 'tinggimukaair'),
(64, 77, 'RekapPerizinan', 'rekapperizinan'),
(65, 78, 'RekapInvestasi', 'rekapinvestasi'),
(66, 79, 'jumlah-sekolah', 'jumlah-sekolah'),
(67, 80, 'Rekap-GTTPTT', 'rekap-gttptt'),
(68, 81, 'pegawai-golongan', 'pegawai-golongan');

-- --------------------------------------------------------

--
-- Table structure for table `route`
--

CREATE TABLE `route` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `route`
--

INSERT INTO `route` (`name`, `alias`, `type`, `status`) VALUES
('/*', '*', '', 1),
('/aplikasi/*', '*', 'aplikasi', 1),
('/aplikasi/create', 'create', 'aplikasi', 1),
('/aplikasi/delete', 'delete', 'aplikasi', 1),
('/aplikasi/index', 'index', 'aplikasi', 1),
('/aplikasi/update', 'update', 'aplikasi', 1),
('/aplikasi/view', 'view', 'aplikasi', 1),
('/data/*', '*', 'data', 1),
('/data/create', 'create', 'data', 1),
('/data/get-data-api', 'get-data-api', 'data', 1),
('/data/index', 'index', 'data', 1),
('/data/new-tree', 'new-tree', 'data', 1),
('/data/update', 'update', 'data', 1),
('/debug/*', '*', 'debug', 1),
('/debug/default/*', '*', 'debug/default', 1),
('/debug/default/db-explain', 'db-explain', 'debug/default', 1),
('/debug/default/download-mail', 'download-mail', 'debug/default', 1),
('/debug/default/index', 'index', 'debug/default', 1),
('/debug/default/toolbar', 'toolbar', 'debug/default', 1),
('/debug/default/view', 'view', 'debug/default', 1),
('/debug/user/*', '*', 'debug/user', 1),
('/debug/user/reset-identity', 'reset-identity', 'debug/user', 1),
('/debug/user/set-identity', 'set-identity', 'debug/user', 1),
('/gii/*', '*', 'gii', 1),
('/gii/default/*', '*', 'gii/default', 1),
('/gii/default/action', 'action', 'gii/default', 1),
('/gii/default/diff', 'diff', 'gii/default', 1),
('/gii/default/index', 'index', 'gii/default', 1),
('/gii/default/preview', 'preview', 'gii/default', 1),
('/gii/default/view', 'view', 'gii/default', 1),
('/instansi/*', '*', 'instansi', 1),
('/instansi/create', 'create', 'instansi', 1),
('/instansi/delete', 'delete', 'instansi', 1),
('/instansi/index', 'index', 'instansi', 1),
('/instansi/update', 'update', 'instansi', 1),
('/instansi/view', 'view', 'instansi', 1),
('/integrasi-data/*', '*', 'integrasi-data', 1),
('/integrasi-data/index', 'index', 'integrasi-data', 1),
('/jenis-instansi/*', '*', 'jenis-instansi', 1),
('/jenis-instansi/create', 'create', 'jenis-instansi', 1),
('/jenis-instansi/delete', 'delete', 'jenis-instansi', 1),
('/jenis-instansi/index', 'index', 'jenis-instansi', 1),
('/jenis-instansi/update', 'update', 'jenis-instansi', 1),
('/jenis-instansi/view', 'view', 'jenis-instansi', 1),
('/kabkota/*', '*', 'kabkota', 1),
('/kabkota/create', 'create', 'kabkota', 1),
('/kabkota/delete', 'delete', 'kabkota', 1),
('/kabkota/index', 'index', 'kabkota', 1),
('/kabkota/update', 'update', 'kabkota', 1),
('/kabkota/view', 'view', 'kabkota', 1),
('/mimin/*', '*', 'mimin', 1),
('/mimin/role/*', '*', 'mimin/role', 1),
('/mimin/role/create', 'create', 'mimin/role', 1),
('/mimin/role/delete', 'delete', 'mimin/role', 1),
('/mimin/role/index', 'index', 'mimin/role', 1),
('/mimin/role/permission', 'permission', 'mimin/role', 1),
('/mimin/role/update', 'update', 'mimin/role', 1),
('/mimin/role/view', 'view', 'mimin/role', 1),
('/mimin/route/*', '*', 'mimin/route', 1),
('/mimin/route/create', 'create', 'mimin/route', 1),
('/mimin/route/delete', 'delete', 'mimin/route', 1),
('/mimin/route/generate', 'generate', 'mimin/route', 1),
('/mimin/route/index', 'index', 'mimin/route', 1),
('/mimin/route/update', 'update', 'mimin/route', 1),
('/mimin/route/view', 'view', 'mimin/route', 1),
('/mimin/user/*', '*', 'mimin/user', 1),
('/mimin/user/create', 'create', 'mimin/user', 1),
('/mimin/user/delete', 'delete', 'mimin/user', 1),
('/mimin/user/index', 'index', 'mimin/user', 1),
('/mimin/user/update', 'update', 'mimin/user', 1),
('/mimin/user/view', 'view', 'mimin/user', 1),
('/setting/*', '*', 'setting', 1),
('/setting/create', 'create', 'setting', 1),
('/setting/delete', 'delete', 'setting', 1),
('/setting/index', 'index', 'setting', 1),
('/setting/update', 'update', 'setting', 1),
('/setting/view', 'view', 'setting', 1),
('/site/*', '*', 'site', 1),
('/site/about', 'about', 'site', 1),
('/site/captcha', 'captcha', 'site', 1),
('/site/contact', 'contact', 'site', 1),
('/site/error', 'error', 'site', 1),
('/site/index', 'index', 'site', 1),
('/site/login', 'login', 'site', 1),
('/site/logout', 'logout', 'site', 1),
('/skpd/*', '*', 'skpd', 1),
('/skpd/create', 'create', 'skpd', 1),
('/skpd/delete', 'delete', 'skpd', 1),
('/skpd/index', 'index', 'skpd', 1),
('/skpd/update', 'update', 'skpd', 1),
('/skpd/view', 'view', 'skpd', 1),
('/status-sds/*', '*', 'status-sds', 1),
('/status-sds/create', 'create', 'status-sds', 1),
('/status-sds/delete', 'delete', 'status-sds', 1),
('/status-sds/index', 'index', 'status-sds', 1),
('/status-sds/update', 'update', 'status-sds', 1),
('/status-sds/view', 'view', 'status-sds', 1),
('/treemanager/*', '*', 'treemanager', 1),
('/treemanager/node/*', '*', 'treemanager/node', 1),
('/treemanager/node/manage', 'manage', 'treemanager/node', 1),
('/treemanager/node/move', 'move', 'treemanager/node', 1),
('/treemanager/node/remove', 'remove', 'treemanager/node', 1),
('/treemanager/node/save', 'save', 'treemanager/node', 1),
('/user/*', '*', 'user', 1),
('/user/create', 'create', 'user', 1),
('/user/delete', 'delete', 'user', 1),
('/user/index', 'index', 'user', 1),
('/user/update', 'update', 'user', 1),
('/user/view', 'view', 'user', 1);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `site_logo` varchar(255) NOT NULL,
  `api_pegawai` varchar(255) NOT NULL,
  `skpd` varchar(255) NOT NULL,
  `nama_kabupaten` varchar(255) NOT NULL,
  `kode_kabupaten` int(2) NOT NULL,
  `admin_setting_data` tinyint(4) NOT NULL DEFAULT 0,
  `version` varchar(50) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `title`, `sub_title`, `site_logo`, `api_pegawai`, `skpd`, `nama_kabupaten`, `kode_kabupaten`, `admin_setting_data`, `version`, `created_at`, `updated_at`) VALUES
(1, 'Single Data System', 'Single Data System Jawa Tengah', 'uploads/web_assets/sds-logo.png', 'http://simpeg.bkd.jatengprov.go.id/webservice/identitas', 'Dinas Komunikasi dan Informatika', 'Pemerintah Provinsi Jawa Tengah', 33, 0, '2.0.0', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `skpd`
--

CREATE TABLE `skpd` (
  `id` int(11) NOT NULL COMMENT 'Kode Master SKPD Auto Increment',
  `kode_skpd` char(4) DEFAULT NULL COMMENT 'Kode SKPD = (2 digit kode Kabkota + 2 digit Kode SKPD)',
  `nama_skpd` varchar(255) DEFAULT NULL COMMENT 'Nama SKPD',
  `akronim_skpd` varchar(50) DEFAULT NULL COMMENT 'Singkatan SKPD',
  `alamat_skpd` varchar(255) DEFAULT NULL COMMENT 'Alamat SKPD',
  `telpon_skpd` varchar(50) DEFAULT NULL COMMENT 'Telpon SKPD',
  `nama_pic_skpd` varchar(50) DEFAULT NULL COMMENT 'Nama Kontak Person SKPD',
  `telepon_pic_skpd` varchar(50) DEFAULT NULL COMMENT 'Telp. Kontak Person SKPD',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status_sds`
--

CREATE TABLE `status_sds` (
  `id` int(11) NOT NULL,
  `instansi_id` int(11) NOT NULL,
  `alamat_situs` varchar(100) DEFAULT NULL,
  `host` varchar(50) DEFAULT NULL,
  `port` varchar(5) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_sds`
--

INSERT INTO `status_sds` (`id`, `instansi_id`, `alamat_situs`, `host`, `port`, `username`, `password`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 14, 'http://sds.kendalkab.go.id/', 'https://cpanel.kendalkab.go.id', '2083', 'sdskendalkabgo', 'SDSK3nd4l', 'Ok,\r\nsds.kendalkab.go.id\r\n\r\nhttps://cpanel.kendalkab.go.id:2083\r\n\r\n+===================================+\r\n| New Account Info                  |\r\n+===================================+\r\n| Domain: sds.kendalkab.go.id\r\n| Ip: 118.97.18.38 (n)\r\n| HasCgi: y\r\n| UserName: sdskendalkabgo\r\n| PassWord: SDSK3nd4l\r\n| CpanelMod: paper_lantern\r\n| HomeRoot: /home2\r\n| Quota: 1.95 GB\r\n| NameServer1: ns3.telkomhosting.com\r\n| NameServer2: ns4.telkomhosting.com\r\n| NameServer3: \r\n| NameServer4: \r\n| Contact Email: ahmad.hasan.basyari@gmail.com\r\n| Package: SILVER_2_GB\r\n| Feature List: default\r\n| Language: en\r\n+===================================+\r\n...Done', NULL, 1531788273),
(2, 4, 'http://sds.batangkab.go.id/', 'https://sds.batangkab.go.id', '2083', 'sdsbtg', 'ONEdataProject!', 'sds.batangkab.go.id/cpanel\r\nUser sdsbtg\r\nPass ONEdataProject!', NULL, 1547431242),
(3, 32, 'http://sds.salatiga.go.id', '', '', '', '', 'Update\r\n20190122 - Terinstall SDS V2\r\n', NULL, 1531789261),
(4, 34, 'http://sds.surakarta.go.id/', 'sds.surakarta.go.id', '21', 'kelurahansingledata', 'sds123kotasurakarta', 'Ok,\r\nhttp://sds.surakarta.go.id\r\n\r\nDB user : c3singledata\r\nDB pass : singledatasystemkotasurakarta123\r\nDB name	: c3sds\r\n\r\nFTP user : kelurahansingledata\r\nFTP pass : sds123kotasurakarta\r\nakses DB https://sds.surakarta.go.id/phpmyadmin', NULL, 1531788440),
(5, 7, 'http://sds.boyolali.go.id', '', '', '198112142009011004', '123456', '20190122 - Terinstal SDS V2', NULL, NULL),
(6, 25, 'http://sds.sragenkab.go.id/', 'sds.sragenkab.go.id', '21', 'sds', 'kominfo2018!', 'Ok,\r\n[15:16, 6/27/2018] Munawal Ulfiyanto: Alamat : \r\nhttp://sds.sragenkab.go.id\r\n\r\nKonfigurasi DB mysql :\r\nHost : localhost\r\nUser : sds\r\nPassword : kominfo2018!\r\n\r\nPhpMyAdmin\r\nAlamat : http://222.124.206.55/phpMyAdmin\r\n(Case sensitive)\r\nUser : sds\r\nPassword : kominfo2018!\r\n\r\nFtp\r\nAlamat : ftp://sds.sragenkab.go.id\r\nUser : sds\r\nPassword : kominfo2018!\r\n[15:17, 6/27/2018] Munawal Ulfiyanto: Database name : sds', NULL, 1531788511),
(8, 19, 'http://sds.pekalongankab.go.id', '', '', '', '', '20190130 - Sudah Online', NULL, 1532337368),
(9, 26, 'http://sds.sukoharjokab.go.id/', NULL, NULL, NULL, NULL, 'Ok, Akses Hosting belum ada.', NULL, 1531788707),
(10, 13, 'http://sds.kebumenkab.go.id/', NULL, NULL, NULL, NULL, 'Ok, Akses Hosting belum ada.', NULL, 1531789125),
(11, 37, 'http://sds.cilacapkab.go.id/', NULL, NULL, NULL, NULL, 'ok,\r\nFtpnya : sdscilacap_ftp\r\nPass : sdsc1l4c4p18\r\n\r\nDatabasenya username : sdscilacap_u\r\nPass : sdsc1l4c4p18', NULL, 1532337318),
(12, 16, 'http://sds.kuduskab.go.id', '203.130.205.188', '2244', 'sdskudus', '5d5kudu5@2018', '20190122 - Terinstal SDS', 1531671330, 1531671330),
(13, 18, 'https://sds.patikab.go.id/', '', '', '', '', 'hak akses belum dimintakan', 1531788260, 1531788317),
(14, 10, 'http://sds.grobogan.go.id/', '', '', '198112142009011004', '123456', 'hak akses belum dimintakan', 1531788383, 1531788383),
(15, 11, 'http://sds.jepara.go.id', '', '', '', '', ' Via sftp\r\nUsername : sds\r\npassword: sds kabupaten jepara\r\nDomain sds.jepara.go.id\r\nPort 50023\r\nDatabase\r\nsds.jepara.go.id/opensesame\r\nDb : sds_db\r\nUser : sds_user\r\nPass : sds_password', 1531788542, 1531788542),
(16, 22, 'http://sds.purworejokab.go.id/', '', '', '198112142009011004', '123456', 'Ok,\r\ncpanel \r\nUsername : purwokab\r\nPass : c2B5bq21lY', 1531788607, 1536051253),
(17, 21, 'https://sds.purbalinggakab.go.id/', '', '', '198112142009011004', '123456', 'hak akses belum dimintakan\r\n\r\nLaporan\r\n20190122 - Forbidden/ Tidak dapat Di Akses', 1531788660, 1531788660),
(18, 9, 'http://sds.demakkab.go.id', '103.47.60.182', '22', 'sdsserveradmin', 'sdsserverdemak', 'Ok,', 1531789246, 1531789246),
(19, 12, 'http://sds.karanganyarkab.go.id', 'https://sds.karanganyarkab.go.id', '2083', 'sds', 's1n9l3d4t4syst3mj00s', 'ok,\r\ndomain: sds.karanganyarkab.go.id\r\ncwp: sds.karanganyarkab.go.id:2083\r\nuser: sds\r\npass: s1n9l3d4t4syst3mj00s', 1532334911, 1532337082),
(20, 1, 'http://sds.banjarnegarakab.go.id/', 'https://sds.banjarnegarakab.go.id', '2083', 'sdsbna', '56#Multi-Country', 'ok, \r\nCPANEL SINGLE DATA\r\n\r\nsds.banjarnegarakab.go.id/cpanel\r\n\r\nuser : sdsbna\r\npassword : 56#Multi-Country', 1532334971, 1532337029),
(21, 33, 'http://sds.semarangkota.go.id/', 'https://webhost.semarangkota.go.id', '2083', 'sdssemarangkotag', 'sds_semarangkota', 'ok, \r\nsedang mengembangkan (http://sidadu.semarangkota.go.id)\r\nAkses Cpanel\r\nHost : https://webhost.semarangkota.go.id:2083/\r\nUser : sdssemarangkotag\r\nPass :sds_semarangkota', 1532921942, 1532921942),
(22, 20, 'http://sds.pemalangkab.go.id', '180.250.149.28', '5035', 'root', 'kominfopml2018', 'oke,\r\n    - use : root\r\n    - pass: kominfopml2018\r\n#Ssh------------\r\n IP : 180.250.149.28\r\nPort: 5035', 1536629873, 1536629873),
(23, 29, 'http://sds.wonogirikab.go.id', '103.30.181.240', '22', 'kominfo', 'w0n0g1r1kab18', '\r\nini Open Data\r\n[14:47, 9/10/2018] Fandi: Akses ssh ke 103.30.181.240\r\nUser : kominfo\r\nPassword : w0n0g1r1kab18\r\n[14:47, 9/10/2018] Fandi: http://sds.wonogirikab.go.id', 1541087101, 1541087425),
(24, 27, 'http://sds.tegalkab.go.id', '', '', '', '', 'Update\r\n20190122 - Terinstall SDS V2', NULL, NULL),
(25, 23, 'http://sds.rembangkab.go.id', '', '', '', '', 'Update\r\n20190122 - Terinstal SDS', NULL, NULL),
(26, 3, 'http://sds.banyumaskab.go.id', '', '', '', '', 'Update\r\n20190122 - Terinstal SDS', NULL, NULL),
(27, 5, 'http://sds.blorakab.go.id/', '36.89.22.123', '22', 'root', 'sdsblora^%$321', 'ok', NULL, NULL),
(28, 8, 'http://sds.brebeskab.go.id', '', '', '', '', '', NULL, NULL),
(29, 15, '-', '', '', '', '', '', NULL, NULL),
(30, 17, 'http://sds.magelangkab.go.id', '', '', '', '', 'Update\r\n20190122 - Terinstal SDS', NULL, NULL),
(31, 24, '-', '', '', '', '', '', NULL, NULL),
(32, 28, '-', '', '', '', '', '', NULL, NULL),
(33, 30, 'https://sds.wonosobokab.go.id/', '', '', '', '', '', NULL, NULL),
(34, 31, 'http://sds.pekalongankab.go.id', '', '', '', '', '', NULL, NULL),
(35, 35, 'http://sds.tegalkab.go.id', '', '', '', '', 'Update\r\n20190122 - Terinstall SDS V2', NULL, NULL),
(36, 36, 'http://sds.magelangkota.go.id', '', '', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `struktur_data`
--

CREATE TABLE `struktur_data` (
  `id` int(11) NOT NULL,
  `parameter_api_id` int(11) NOT NULL,
  `nama_struktur` varchar(50) DEFAULT NULL COMMENT 'Nama yang akan di tampilkan',
  `parameter` varchar(50) DEFAULT NULL COMMENT 'Field yang akan dibaca APInya',
  `relasi` enum('0','1') DEFAULT NULL,
  `grafik` enum('x','y','tidak') DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `struktur_data`
--

INSERT INTO `struktur_data` (`id`, `parameter_api_id`, `nama_struktur`, `parameter`, `relasi`, `grafik`, `created_at`, `updated_at`) VALUES
(29, 15, 'Label', 'Field Data', '0', 'tidak', NULL, NULL),
(35, 18, 'Jenis Penyakit', 'Jenis_penyakit', '0', 'tidak', NULL, NULL),
(36, 18, 'Tahun Kejadian', 'Tahun', '0', 'tidak', NULL, NULL),
(37, 18, 'Jumlah Pasien', 'Jumlah', '0', 'tidak', NULL, NULL),
(38, 19, 'Jenis Alat', 'jenis_alat', '0', 'tidak', NULL, NULL),
(39, 19, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(40, 19, 'Fungsi', 'fungsi', '0', 'tidak', NULL, NULL),
(60, 22, 'Nama Pegawai', 'nama_pegawai', '0', 'tidak', NULL, NULL),
(61, 22, 'NIP/NIK', 'nip_nik', '0', 'tidak', NULL, NULL),
(63, 22, 'TTL', 'ttl', '0', 'tidak', NULL, NULL),
(64, 22, 'Alamat', 'alamat_pegawai', '0', 'tidak', NULL, NULL),
(65, 22, 'Jenis Tenaga', 'jenis_tenaga', '0', 'tidak', NULL, NULL),
(66, 22, 'Jenis Kelamin', 'jenis_kelamin', '0', 'tidak', NULL, NULL),
(67, 22, 'Status Kepegawaian', 'status_kepegawaian', '0', 'tidak', NULL, NULL),
(68, 22, 'No Hp', 'no_hp_pegawai', '0', 'tidak', NULL, NULL),
(69, 22, 'Pendidikan', 'pendidikan_pegawai', '0', 'tidak', NULL, NULL),
(70, 23, 'Nama Alat', 'nama_alat', '0', 'tidak', NULL, NULL),
(71, 23, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(72, 23, 'Fungsi', 'fungsi', '0', 'tidak', NULL, NULL),
(73, 24, 'Bangsal', 'bangsal', '0', 'tidak', NULL, NULL),
(74, 24, 'Jenis', 'jenis', '0', 'tidak', NULL, NULL),
(75, 24, 'Kelas 3', 'kelas_3', '0', 'tidak', NULL, NULL),
(76, 24, 'Kelas 2', 'kelas_2', '0', 'tidak', NULL, NULL),
(77, 24, 'Kelas 1', 'kelas_1', '0', 'tidak', NULL, NULL),
(78, 24, 'Utama', 'utama', '0', 'tidak', NULL, NULL),
(80, 24, 'VIP A', 'vip_a', '0', 'tidak', NULL, NULL),
(81, 24, 'VIP B', 'vip_b', '0', 'tidak', NULL, NULL),
(82, 24, 'VVIP A', 'vvip_a', '0', 'tidak', NULL, NULL),
(83, 24, 'VVIP B', 'vvip_b', '0', 'tidak', NULL, NULL),
(84, 24, 'Super VVIP', 'super_vvip', '0', 'tidak', NULL, NULL),
(85, 25, 'Bangsal', 'BANGSAL', '0', 'tidak', NULL, NULL),
(86, 25, 'Jenis', 'JENIS', '0', 'tidak', NULL, NULL),
(87, 25, 'Kelas 3', 'KELAS3', '0', 'tidak', NULL, NULL),
(88, 25, 'Kelas 2', 'KELAS2', '0', 'tidak', NULL, NULL),
(89, 25, 'Kelas 1', 'KELAS1', '0', 'tidak', NULL, NULL),
(90, 25, 'Utama', 'UTAMA', '0', 'tidak', NULL, NULL),
(91, 25, 'VIP', 'VIP', '0', 'tidak', NULL, NULL),
(92, 25, 'Intensif', 'INTEMSIF', '0', 'tidak', NULL, NULL),
(93, 25, 'Non Kelas', 'NONKELAS', '0', 'tidak', NULL, NULL),
(94, 26, 'Bangsal', 'BANGSAL', '0', 'x', NULL, NULL),
(95, 26, 'Kelas 1', 'KELAS1', '0', 'y', NULL, NULL),
(96, 26, 'Kelas 2', 'KELAS2', '0', 'tidak', NULL, NULL),
(97, 27, 'Nama Pegawai', 'nama_pegawai', '0', 'tidak', NULL, NULL),
(98, 27, 'NIP/NIK', 'nip_nik', '0', 'tidak', NULL, NULL),
(99, 27, 'Tempat Lahir', 'tempat_lahir', '0', 'tidak', NULL, NULL),
(100, 27, 'Tanggal Lahir', 'tanggal_lahir', '0', 'tidak', NULL, NULL),
(101, 27, 'Jenis Kelamin', 'jenis_kelamin', '0', 'tidak', NULL, NULL),
(102, 27, 'Status Kepegawaian', 'status_kepegawaian', '0', 'tidak', NULL, NULL),
(103, 27, 'No HP', 'no_hp_pegawai', '0', 'tidak', NULL, NULL),
(104, 27, 'Pendidikan', 'pendidikan_pegawai', '0', 'tidak', NULL, NULL),
(105, 26, 'Kelas 3', 'KELAS3', '0', 'tidak', NULL, NULL),
(106, 28, 'Tingkat Pendidikan', 'tkdik', '0', 'x', NULL, NULL),
(107, 28, 'Jumlah', 'jml', '0', 'y', NULL, NULL),
(108, 29, 'Bulan', 'bulan', '1', 'x', NULL, NULL),
(109, 29, 'Jumlah', 'jumlah', '0', 'y', NULL, NULL),
(110, 30, 'Jenis', 'jenis', '1', 'tidak', NULL, NULL),
(111, 30, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(112, 31, 'Golongan', 'gol', '0', 'tidak', NULL, NULL),
(113, 31, 'Jumlah', 'jml', '0', 'tidak', NULL, NULL),
(114, 32, 'No SP2D', 'no_sp2d', '0', 'tidak', NULL, NULL),
(115, 32, 'Tanggal SP2D', 'tanggal', '0', 'tidak', NULL, NULL),
(116, 32, 'Instansi', 'instansi', '0', 'tidak', NULL, NULL),
(117, 32, 'Nominal', 'nominal', '0', 'tidak', NULL, NULL),
(118, 32, 'Uraian', 'uraian', '0', 'tidak', NULL, NULL),
(128, 37, 'Nama Waduk', 'namawaduk', '0', 'tidak', NULL, NULL),
(129, 37, 'Peimpah', 'pelimpahpeil', '0', 'tidak', NULL, NULL),
(130, 38, 'Nama Indikator', 'name', '0', 'tidak', NULL, NULL),
(131, 38, 'Pria', 'pria', '0', 'tidak', NULL, NULL),
(132, 38, 'Wanita', 'wanita', '0', 'tidak', NULL, NULL),
(133, 38, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(134, 38, 'SKPD', 'skpd_name', '0', 'tidak', NULL, NULL),
(135, 38, 'Tahun', 'tahun', '0', 'tidak', NULL, NULL),
(136, 39, 'Kabupaten', 'kab_kota', '0', 'x', NULL, NULL),
(137, 39, 'Jumlah', 'jml', '0', 'y', NULL, NULL),
(138, 40, 'Kabupaten', 'kabkota', '0', 'tidak', NULL, NULL),
(139, 40, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(140, 41, 'Prioritas', 'prioritas', '0', 'tidak', NULL, NULL),
(141, 41, 'Nama', 'kabkota', '0', 'tidak', NULL, NULL),
(142, 41, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(143, 42, 'Kabupaten', 'kabkota', '0', 'tidak', NULL, NULL),
(144, 42, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(145, 43, 'Desil', 'desil', '0', 'tidak', NULL, NULL),
(146, 43, 'Kabupaten', 'kabkota', '0', 'tidak', NULL, NULL),
(147, 43, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(148, 44, 'Pekerjaan', 'pekerjaan', '0', 'tidak', NULL, NULL),
(149, 44, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(150, 51, 'Nama Diklat', 'dikstru', '0', 'x', NULL, NULL),
(151, 51, 'Jumlah', 'jml', '0', 'y', NULL, NULL),
(152, 52, 'Jenis Kelamin', 'jenkel', '0', 'tidak', NULL, NULL),
(153, 52, 'Jumlah', 'jml', '0', 'tidak', NULL, NULL),
(154, 53, 'Tahun', 'tahun', '0', 'x', NULL, NULL),
(155, 53, 'IPM Jateng', 'ipm_jateng', '0', 'y', NULL, NULL),
(156, 53, 'IPM Nasional', 'ipm_nas', '0', 'tidak', NULL, NULL),
(157, 54, 'Kelas', 'Kelas', '0', 'tidak', NULL, NULL),
(158, 54, 'Jumlah TT', 'Jumlah_TT', '0', 'tidak', NULL, NULL),
(159, 54, 'TT Terpakai', 'TT_terpakai', '0', 'tidak', NULL, NULL),
(160, 54, 'TT Kosong', 'TT_kosong', '0', 'tidak', NULL, NULL),
(161, 54, 'Tarif', 'Tarif', '0', 'tidak', NULL, NULL),
(162, 55, 'Nama Alat', 'nama_alat', '0', 'tidak', NULL, NULL),
(163, 55, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(164, 55, 'Fungsi', 'fungsi', '0', 'tidak', NULL, NULL),
(165, 56, 'Nama RS', 'nama_rs', '0', 'tidak', NULL, NULL),
(166, 56, 'Klasifikasi', 'klasifikasi', '0', 'tidak', NULL, NULL),
(167, 56, 'Tipe RS', 'tipe_rs', '0', 'tidak', NULL, NULL),
(168, 56, 'Akreditasi', 'akreditasi', '0', 'tidak', NULL, NULL),
(169, 56, 'Jenis Layanan', 'jenis_layanan', '0', 'tidak', NULL, NULL),
(170, 56, 'Alamat', 'alamat_rs', '0', 'tidak', NULL, NULL),
(171, 56, 'Tlp', 'no_tlp', '0', 'tidak', NULL, NULL),
(172, 56, 'Fax', 'fax', '0', 'tidak', NULL, NULL),
(173, 56, 'Email', 'email', '0', 'tidak', NULL, NULL),
(174, 56, 'website', 'website', '0', 'tidak', NULL, NULL),
(175, 56, 'Jumlah TT', 'jumlah_tt', '0', 'tidak', NULL, NULL),
(176, 56, 'BOR', 'bor', '0', 'tidak', NULL, NULL),
(177, 56, 'LOS', 'los', '0', 'tidak', NULL, NULL),
(178, 56, 'TOI', 'toi', '0', 'tidak', NULL, NULL),
(179, 57, 'Nama Pegawai', 'nama_pegawai', '0', 'tidak', NULL, NULL),
(180, 57, 'NIP/NIK', 'nip_nik', '0', 'tidak', NULL, NULL),
(181, 57, 'Alamat', 'alamat_pegawai', '0', 'tidak', NULL, NULL),
(182, 57, 'Jenis Tenaga', 'jenis_tenaga', '0', 'tidak', NULL, NULL),
(183, 57, 'Jenis Kelamin', 'jenis_kelamin', '0', 'tidak', NULL, NULL),
(184, 57, 'Status Kepegawaian', 'status_kepegawaian', '0', 'tidak', NULL, NULL),
(185, 57, 'No Tlp', 'no_hp_pegawai', '0', 'tidak', NULL, NULL),
(186, 57, 'Pendidikan', 'pendidikan_pegawai', '0', 'tidak', NULL, NULL),
(187, 58, 'Nama Panti', 'nama', '0', 'x', NULL, NULL),
(188, 58, 'Jumlah', 'jumlah', '0', 'y', NULL, NULL),
(189, 59, 'Kelas', 'kelas', '0', 'x', NULL, NULL),
(190, 59, 'Ketersediaan', 'ketersediaan', '0', 'y', NULL, NULL),
(191, 37, 'Pelimpah Vol', 'pelimpahvolume', '0', 'tidak', NULL, NULL),
(192, 37, 'Rnc Peli', 'rencanapeil', '0', 'tidak', NULL, NULL),
(193, 37, 'Renc Vol', 'rencanavolume', '0', 'tidak', NULL, NULL),
(194, 37, 'Realisasi Volume', 'realisasipeil', '0', 'tidak', NULL, NULL),
(195, 37, 'Realisasi Volume', 'realisasivolume', '0', 'tidak', NULL, NULL),
(196, 37, 'Luas Layanan', 'luaslayanan', '0', 'tidak', NULL, NULL),
(197, 60, 'Bendung', 'kabkota_bendungdi_sungai', '0', 'tidak', NULL, NULL),
(198, 60, 'Sawah Irigasi', 'sawahirigasi', '0', 'tidak', NULL, NULL),
(199, 60, 'Q Limpas', 'qlimpas', '0', 'tidak', NULL, NULL),
(200, 60, 'Intake Kanan', 'intakekanan', '0', 'tidak', NULL, NULL),
(201, 60, 'Inteke Kiri', 'intakekiri', '0', 'tidak', NULL, NULL),
(202, 60, 'Q Sungai', 'qSungai', '0', 'tidak', NULL, NULL),
(203, 60, 'Q Kebutuhan', 'qKebutuhan', '0', 'tidak', NULL, NULL),
(204, 60, 'Faktor K', 'faktork', '0', 'tidak', NULL, NULL),
(205, 61, 'Nama Lokasi', 'nm_lokasi', '0', 'tidak', NULL, NULL),
(206, 61, 'Curah Hujan', 'curah_hujan', '0', 'tidak', NULL, NULL),
(207, 61, 'Latitude', 'latitude', '0', 'tidak', NULL, NULL),
(208, 61, 'Longitude', 'longitude', '0', 'tidak', NULL, NULL),
(209, 61, 'Updated', 'waktu_data', '0', 'tidak', NULL, NULL),
(210, 62, 'Nama Lokasi', 'nm_lokasi', '0', 'tidak', NULL, NULL),
(211, 62, 'Tinggi Muka Air', 'tinggimukaair', '0', 'tidak', NULL, NULL),
(212, 62, 'Latitude', 'latitude', '0', 'tidak', NULL, NULL),
(213, 62, 'Longitude', 'longitude', '0', 'tidak', NULL, NULL),
(214, 62, 'Updated', 'waktu_data', '0', 'tidak', NULL, NULL),
(217, 64, 'Nama Izin', 'izin_id', '0', 'tidak', NULL, NULL),
(218, 64, 'SOP', 'sop', '0', 'tidak', NULL, NULL),
(219, 64, 'Jumlah Izin', 'jumlah_izin', '0', 'tidak', NULL, NULL),
(220, 64, 'Bulan', 'bulan', '0', 'tidak', NULL, NULL),
(221, 64, 'Izin', 'izin', '0', 'tidak', NULL, NULL),
(222, 64, 'Non Izin', 'nonizin', '0', 'tidak', NULL, NULL),
(223, 64, 'Izin Selesai (%)', 'persen_sop', '0', 'tidak', NULL, NULL),
(224, 65, 'Sektor', 'sektor', '0', 'tidak', NULL, NULL),
(235, 65, 'Nama Perusahaan', 'nama_perusahaan', '0', 'tidak', NULL, NULL),
(236, 65, 'Cetak Bidang Usaha', 'cetak_bid_usaha', '0', 'tidak', NULL, NULL),
(237, 65, 'Kabupaten/Kota', 'kab_kota', '0', 'tidak', NULL, NULL),
(238, 65, 'Provinsi', 'provinsi', '0', 'tidak', NULL, NULL),
(239, 65, 'Negara', 'negara', '0', 'tidak', NULL, NULL),
(240, 65, 'Tambahan Investasi (dalam $US Ribu/Rp Juta)', 'tambahan_invest', '0', 'tidak', NULL, NULL),
(241, 65, 'Total Investasi (dalam $US Ribu/Rp Juta)', 'total_invest', '0', 'tidak', NULL, NULL),
(242, 65, 'Tahun', 'tahun', '0', 'tidak', NULL, NULL),
(243, 65, 'PMA/PMDN', 'pma_pmdn', '0', 'tidak', NULL, NULL),
(244, 66, 'Kab/Kota', 'nama_wilayah', '0', 'tidak', NULL, NULL),
(245, 66, 'TK Negeri', 'tk_negeri', '0', 'tidak', NULL, NULL),
(246, 66, 'TK Swasta', 'tk_swasta', '0', 'tidak', NULL, NULL),
(247, 66, 'SD Negeri', 'sd_negeri', '0', 'tidak', NULL, NULL),
(248, 66, 'SD Swasta', 'sd_swasta', '0', 'tidak', NULL, NULL),
(249, 66, 'SMP Negeri', 'smp_negeri', '0', 'tidak', NULL, NULL),
(250, 66, 'SMP Swasta', 'smp_swasta', '0', 'tidak', NULL, NULL),
(251, 66, 'SMA Negeri', 'sma_negeri', '0', 'tidak', NULL, NULL),
(252, 66, 'SMA Swasta', 'sma_swasta', '0', 'tidak', NULL, NULL),
(253, 66, 'SMK Negeri', 'smk_negeri', '0', 'tidak', NULL, NULL),
(254, 66, 'SMK Swasta', 'smk_swasta', '0', 'tidak', NULL, NULL),
(255, 66, 'SLB Negeri', 'slb_negeri', '0', 'tidak', NULL, NULL),
(256, 66, 'SLB Swasta', 'slb_swasta', '0', 'tidak', NULL, NULL),
(257, 67, 'Kabupaten/Kota', 'rayon', '0', 'tidak', NULL, NULL),
(258, 67, 'Nama Sekolah', 'nm_sekolah', '0', 'tidak', NULL, NULL),
(259, 67, 'GTT Jam mengajar > 24 dibayar Provinsi', 'gtt_lbh24apbd', '0', 'tidak', NULL, NULL),
(260, 67, 'GTT Jam mengajar < 24 dibayar Provinsi', 'gtt_krg24apbd', '0', 'tidak', NULL, NULL),
(261, 67, 'GTT Jam mengajar > 24 dibayar PSM', 'gtt_lbh24psm', '0', 'tidak', NULL, NULL),
(262, 67, 'GTT Jam mengajar < 24 dibayar PSM', 'gtt_krg24psm', '0', 'tidak', NULL, NULL),
(263, 67, 'GTT Jam mengajar > 24 dibayar BOS', 'gtt_lbh24bos', '0', 'tidak', NULL, NULL),
(264, 67, 'GTT Jam mengajar < 24 dibayar BOS', 'gtt_krg24bos', '0', 'tidak', NULL, NULL),
(265, 67, 'GTT Jam mengajar > 24 dibayar BOP', 'gtt_lbh24bop', '0', 'tidak', NULL, NULL),
(266, 67, 'GTT Jam mengajar < 24 dibayar BOP', 'gtt_krg24bop', '0', 'tidak', NULL, NULL),
(267, 67, 'GTT Jam mengajar > 24 dibayar PLA', 'gtt_lbh24pla', '0', 'tidak', NULL, NULL),
(268, 67, 'GTT Jam mengajar < 24 dibayar PLA', 'gtt_krg24pla', '0', 'tidak', NULL, NULL),
(269, 67, 'PTT dibayar Provinsi', 'ptt_apbd', '0', 'tidak', NULL, NULL),
(270, 67, 'PTT dibayar PSM', 'ptt_psm', '0', 'tidak', NULL, NULL),
(271, 67, 'PTT dibayar BOS', 'ptt_bos', '0', 'tidak', NULL, NULL),
(272, 67, 'PTT dibayar BOP', 'ptt_bop', '0', 'tidak', NULL, NULL),
(273, 67, 'PTT dibayar PLA', 'ptt_pla', '0', 'tidak', NULL, NULL),
(274, 68, 'Golongan', 'gol', '0', 'x', NULL, NULL),
(275, 68, 'Jumlah', 'jumlah', '0', 'y', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tree`
--

CREATE TABLE `tree` (
  `id` bigint(20) NOT NULL,
  `aplikasi_id` int(11) DEFAULT NULL,
  `root` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `child_allowed` tinyint(4) NOT NULL DEFAULT 0,
  `deskripsi` text NOT NULL,
  `status` tinyint(2) NOT NULL,
  `lvl` smallint(5) NOT NULL,
  `name` varchar(60) NOT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `icon_type` smallint(1) NOT NULL DEFAULT 1,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `selected` tinyint(1) NOT NULL DEFAULT 0,
  `disabled` tinyint(1) NOT NULL DEFAULT 0,
  `readonly` tinyint(1) NOT NULL DEFAULT 0,
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `collapsed` tinyint(1) NOT NULL DEFAULT 0,
  `movable_u` tinyint(1) NOT NULL DEFAULT 1,
  `movable_d` tinyint(1) NOT NULL DEFAULT 1,
  `movable_l` tinyint(1) NOT NULL DEFAULT 1,
  `movable_r` tinyint(1) NOT NULL DEFAULT 1,
  `removable` tinyint(1) NOT NULL DEFAULT 1,
  `removable_all` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tree`
--

INSERT INTO `tree` (`id`, `aplikasi_id`, `root`, `lft`, `rgt`, `child_allowed`, `deskripsi`, `status`, `lvl`, `name`, `slug`, `icon`, `icon_type`, `active`, `selected`, `disabled`, `readonly`, `visible`, `collapsed`, `movable_u`, `movable_d`, `movable_l`, `movable_r`, `removable`, `removable_all`) VALUES
(1, NULL, 1, 1, 40, 1, '', 0, 0, 'Kesehatan', 'kesehatan', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(2, NULL, 2, 1, 2, 1, '', 0, 0, 'Lingkungan Hidup', NULL, '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(3, NULL, 3, 1, 2, 1, '', 0, 0, 'Pariwisata', 'pariwisata', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(5, NULL, 5, 1, 2, 1, '', 0, 0, 'Energi', 'energi-2', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(11, NULL, 11, 1, 10, 1, '', 0, 0, 'Tata Kelola Pemerintah', 'tata-kelola-pemerintah-2', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(12, 20, 11, 2, 3, 1, 'Data Pegawai Berdasarkan Golongan', 1, 1, 'Pegawai Golongan', 'pegawai-golongan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(14, NULL, 14, 1, 2, 1, '', 0, 0, 'Lingkungan Hidup', 'lingkungan-hidup', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(15, NULL, 15, 1, 2, 1, '', 0, 0, 'Pariwisata', 'pariwisata-2', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(16, NULL, 16, 1, 18, 1, '', 0, 0, 'Sosial', 'sosial', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(17, NULL, 17, 1, 2, 1, '', 0, 0, 'Energi', 'energi', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(18, NULL, 18, 1, 2, 1, '', 0, 0, 'Pangan', 'pangan', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(19, NULL, 19, 1, 2, 1, '', 0, 0, 'Maritim', 'maritim', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(20, NULL, 20, 1, 8, 1, '', 0, 0, 'Ekonomi', 'ekonomi', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(21, NULL, 21, 1, 2, 1, '', 0, 0, 'Industri', 'industri', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(22, NULL, 22, 1, 10, 1, '', 0, 0, 'Infrastruktur', 'infrastruktur', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(23, NULL, 23, 1, 6, 1, '', 0, 0, 'Pendidikan dan Kebudayaan', 'pendidikan-dan-kebudayaan', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(24, NULL, 24, 1, 2, 1, '', 0, 0, 'Kepemudaan dan Olahraga', 'kepemudaan-dan-olahraga', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(25, NULL, 25, 1, 6, 1, '', 0, 0, 'Indikator Makro', 'indikator-makro', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(29, 20, 11, 4, 5, 1, 'Data Pegawai Berdasarkan Pendidikan', 1, 1, 'Pegawai Pendidikan', 'pegawai-pendidikan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(37, NULL, 1, 2, 7, 1, '', 0, 1, 'RSUD Tugurejo', 'rsud-tugurejo', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(38, 19, 1, 3, 4, 1, 'Kejadian Penyakir Luarbiasa di RSUD Tugurejo', 1, 2, 'Kejadian Luar Biasa', 'kejadian-luar-biasa', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(39, 19, 1, 5, 6, 1, 'Ketersediaan Alat Kesehatan Di Rumah Salit Tugurejo', 1, 2, 'Alat Kesehatan', 'alat-kesehatan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(40, NULL, 1, 8, 15, 1, '', 0, 1, 'RSUD Margono', 'rsud-margono', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(43, 23, 1, 9, 10, 1, 'Data Tenaga Kesehatan RSUD Margono', 1, 2, 'Tenaga Kesehatan', 'tenaga-kesehatan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(44, 23, 1, 11, 12, 1, 'Data Alkes RSUD Margono', 1, 2, 'Alat Kesehatan', 'alat-kesehatan-2', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(45, 23, 1, 13, 14, 1, 'Data Ketersediaann Ruang Tempat Tidur', 1, 2, 'Informasi Tempat Tidur', 'informasi-tempat-tidur', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(46, NULL, 1, 16, 19, 1, '', 0, 1, 'RSUD Sodjarwadi', 'rsud-sodjarwadi', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(47, NULL, 1, 20, 25, 1, '', 0, 1, 'RSUD Kelet', 'rsud-kelet', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(48, 25, 1, 17, 18, 1, 'Data Ketersediaan Tempat Tidur', 1, 2, 'Ketersediaan Tempat Tidur', 'ketersediaan-tempat-tidur', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(49, 24, 1, 21, 22, 1, 'Ketersediaan Tempat Tidur RSUD Kelet', 1, 2, 'Ketersediaan Tempat Tidur', 'ketersediaan-tempat-tidur-2', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(50, 24, 1, 23, 24, 1, 'Data Tenaga Kesehatan', 1, 2, 'Tenaga Kesehatan', 'tenaga-kesehatan-2', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(51, 27, 20, 2, 3, 1, 'Data Belanja Provinsi Jawa Tengah', 1, 1, 'Belanja Bulanan', 'belanja-bulanan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(54, 20, 11, 6, 7, 1, '', 1, 1, 'Diklat Pegawai', 'diklat-pegawai', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(55, 28, 22, 2, 3, 1, '', 1, 1, 'Data Waduk', 'data-waduk', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(56, 26, 16, 2, 3, 1, 'Gender dan Anak', 1, 1, 'Gender dan Anak', 'gender-dan-anak', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(57, 29, 16, 4, 5, 1, 'Sistem Informasi Terpadu Penanggulangan Kemiskinan', 1, 1, 'Rumah Tangga Miskin', 'rumah-tangga-miskin', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(58, 29, 16, 6, 7, 1, 'Data Rumah Tidak Ber Listrik Kab Kota', 1, 1, 'Rumah Tidak Berlistrik', 'rumah-tidak-berlistrik', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(59, 29, 16, 8, 9, 1, 'Rumah Tidak Layak Huni Kabupaten dan Kota Se Jawa Tengah', 1, 1, 'Rumah Tidak Layak Huni', 'rumah-tidak-layak-huni', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(60, 29, 16, 10, 11, 1, 'Rumah Tidak Berjamban', 1, 1, 'Rumah Tidak Berjamban', 'rumah-tidak-berjamban', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(61, 29, 16, 12, 13, 1, '', 1, 1, 'Desil Kesejahteraan', 'desil-kesejahteraan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(62, 29, 16, 14, 15, 1, '', 1, 1, 'Lapangan Kerja', 'lapangan-kerja', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(64, 20, 11, 8, 9, 1, '', 1, 1, 'Pegawai Jenis Kelamin', 'pegawai-jenis-kelamin', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(65, 31, 25, 2, 3, 1, '', 1, 1, 'Indeks Pembangunan Manusia', 'indeks-pembangunan-manusia', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(66, NULL, 1, 26, 35, 1, '', 1, 1, 'RSJD Arif Zainudin', 'rsjd-arif-zainudin', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(67, 32, 1, 27, 28, 1, '', 0, 2, 'Ketersedian Tempat Tidur', 'ketersedian-tempat-tidur', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(68, 32, 1, 29, 30, 1, '', 0, 2, 'Alat Kesehatan', 'alat-kesehatan-3', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(69, 32, 1, 31, 32, 1, 'Data Rumah Sakit RSJ Arif Zainudin', 0, 2, 'Data Rumah Sakit', 'data-rumah-sakit', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(70, 32, 1, 33, 34, 1, '', 0, 2, 'Tenaga Kesehatan', 'tenaga-kesehatan-3', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(71, 33, 16, 16, 17, 0, '', 1, 1, 'Panti Sosial', 'panti-sosial', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(72, NULL, 1, 36, 39, 1, '', 1, 1, 'RSJ Dr. Amino Ghondohutomo', 'rsj-dr-amino-ghondohutomo', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(73, 34, 1, 37, 38, 1, 'Data Keterisian Kamar', 1, 2, 'Ketersediaan Kamar', 'ketersediaan-kamar', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(74, 28, 22, 4, 5, 1, 'Data Kondisi bendung Jawa Tengah', 1, 1, 'Data Bendung', 'data-bendung', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(75, 35, 22, 6, 7, 1, '', 1, 1, 'Data Curah Hujan', 'data-curah-hujan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(76, 35, 22, 8, 9, 1, '', 1, 1, 'Data Tinggi Muka Air', 'data-tinggi-muka-air', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(78, 38, 20, 4, 5, 0, '', 1, 1, 'Rekap Perizinan', 'rekap-perizinan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(79, 38, 20, 6, 7, 0, '', 1, 1, 'Rekap Investasi', 'rekap-investasi', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(80, 39, 23, 2, 3, 0, '', 1, 1, 'Jumlah Sekolah', 'jumlah-sekolah', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(81, 39, 23, 4, 5, 0, '', 1, 1, 'Tenaga Pendidik', 'tenaga-pendidik', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(82, 22, 25, 4, 5, 0, '', 1, 1, 'Data Pegawai Golongan', 'data-pegawai-golongan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `instansi_id` int(11) NOT NULL,
  `nip` varchar(18) COLLATE utf8_unicode_ci NOT NULL,
  `nama_lengkap` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jabatan` varchar(155) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `instansi_id`, `nip`, `nama_lengkap`, `jabatan`, `status`, `password_hash`, `password_reset_token`, `username`, `email`, `auth_key`, `created_at`, `updated_at`) VALUES
(3, 40, '198112142009011004', 'MAHFUD ARDIANTO, S.Kom', 'Pengadministrasi Keuangan', 10, '$2y$13$6hkbImUlS3QWYRqvCfOYC.Y55SUoEC2hRZScuz32Zzt78E/V4T33m', NULL, 'mahfud', 'mahfud@gmail.com', NULL, 1532844132, 1548064889),
(4, 38, '196503171986031009', 'EKO SRI DARMINTO, SH', 'KASI STATISTIK SOSIAL POLITIK HUKUM DAN HAM', 10, '$2y$13$qG4xXhhQMEX1qN18ahJkXe/.y.mVlq0lYqVI89sggI9.ppB.hjuQG', NULL, 'ekosri', 'echoos.09@gmail.com', NULL, 1533172182, 1548064874),
(10, 38, '196304031993111002', 'Ir. ARIEF BOEDIJANTO, M.Si', 'KABID STATISTIK', 10, '$2y$13$V45QPlssyX1kNNm4dnQv9O3wZfymdysahHwI5gIqrUtfUfkvPRH3.', NULL, NULL, '', NULL, 1543369374, 1548064846),
(13, 38, '196712071993032002', 'Dra. RATNA DEWAJATI, MT', 'SEKRETARIS', 10, '$2y$13$ca96RUA5kfx6pgEPQTPQM.3ipcWrZYTC132V.OFxz1A4lQUXOhAUu', NULL, NULL, NULL, NULL, 1548057244, 1548064821),
(14, 38, '196405281994021001', 'Ir. DJOKO SARWONO, M.Si', 'KASI STATISTIK EKONOMI DAN INFRASTRUKTUR', 10, '$2y$13$DSQb6xJqWMjLRBgWucM9POw/qMzCmzZURRL/.N3xXoznRS.Kud64i', NULL, NULL, NULL, NULL, 1548062074, 1548064802),
(15, 38, '196111261990062001', ' RAJ.MARIA HARNENY CHRISTIANTI, SH,MM', 'Analis Statistik, Data Dan Pelaporan', 0, '$2y$13$ELMTLHgZk7SLgMV5/1z0ueLNiWhH1DYm9FuDNaILdqsrRZc1hxCl.', NULL, NULL, NULL, NULL, 1548383318, 1551336951),
(16, 38, '196410261989092001', 'RIENA RETNANINGRUM, SH', 'KEPALA DINAS KOMUNIKASI DAN INFORMATIKA', 10, '$2y$13$ROkmf8CToMjQLxORIn7rcueqOMljtKXJbS1ddnm9ao3JxV7uxNDa6', NULL, NULL, NULL, NULL, 1548734704, 1548734704),
(18, 38, '198007142011012005', 'ENDAH TRI NUGRAHENI, S.Si', 'Analis Data Dan Informasi', 10, '$2y$13$5guFJGb.HlBsqebQdNjh0udOhGhYyJtkAQby4hdzCGfHPM97Xum.K', NULL, NULL, NULL, NULL, 1549589111, 1549589111),
(19, 54, '197909172009011006', 'IRWAN DWI NUGROHO', 'Editor', 0, '$2y$13$1ch/r4sB6zBGfZ7Q6w1EcuTOzTMwNTpjey198myv2wclDQ9zP219O', NULL, NULL, NULL, NULL, 1550217173, 1551336981),
(20, 58, '197410142008011005', 'ISMU PANDOYO, S.I.Kom', 'Penyusun Bahan Program Dan Pelaporan', 0, '$2y$13$9eNlXZ5004Hi7KtfcPE10eGP.gFIyDnLVr2GFNslBg1TIfVzcCbyS', NULL, NULL, NULL, NULL, 1550217303, 1551336933),
(21, 56, '197507032010011016', 'YOGO PRAYOGO, ST', 'Sekretaris Pimpinan', 0, '$2y$13$PX/03NEKBXrOYNeokX9bw.5OqJzQUCWNPUhD/.BpDaH2n.JMWqMtu', NULL, NULL, NULL, NULL, 1550217342, 1551336890),
(22, 57, '198506122003121001', 'HERNANTYO BUANA, S.STP', 'Penyusun Program Dan Laporan', 0, '$2y$13$0SPY2Whan0chXMhNZvqObOOPnf58f0AtBjJAiMdZZ752qOUbhCDZa', NULL, NULL, NULL, NULL, 1550217380, 1551336871),
(23, 45, '198001051998102001', 'INDRI RACHMADEWI, S.STP', 'Analis Kesejahteraan Dan Perlindungan Anak', 0, '$2y$13$s0.2oG7rjVpvKT2ixTa8qOdtBirG3OYPNycPErtvJ3FCmhg6LbDxy', NULL, NULL, NULL, NULL, 1550217425, 1551336849),
(24, 38, '196504291995021001', 'Drs. KARTIKA HADI', 'Analis Statistik, Data Dan Pelaporan', 0, '$2y$13$pkT.tZWrFzSOzjvzTCu07uPott/XBp4pLpEclRpp7ZER9IsJbDwZG', NULL, NULL, NULL, NULL, 1550217920, 1551336826),
(25, 38, '197207061993021002', 'MOCH NUGROHO HARTANTO, SE', 'Analis Statistik, Data Dan Pelaporan', 0, '$2y$13$pduCnnnbLc0lpgNYlM.Z5uNcNcpGYsaxlmkuEr26Br6x.1QYX8HDy', NULL, NULL, NULL, NULL, 1550217973, 1551336800),
(26, 38, '196110081985032005', 'ERNA HARIANTI, S.Sos', 'Analis Statistik, Data Dan Pelaporan', 0, '$2y$13$LapbnLj2zbK/3zv39BTEXeIGOJPZI0QJb1GloSHLt77UbiKHY..f2', NULL, NULL, NULL, NULL, 1550217989, 1551336775),
(27, 38, '198606022014022001', 'EMMA RACHMAWATI, S.Si.', NULL, 10, '$2y$13$pu5SSfTwH9G6ofw5yaB9ruXAjf.YhxBCoBer5n4s71SC2csjE.PBS', NULL, NULL, NULL, NULL, 1550218009, 1552028945),
(28, 38, '196508181996102001', 'Dra. EKO ANTINI AGUSTININGRUM', 'Pengadministrasi Kepelabuhan', 0, '$2y$13$Kgx3M3Eswe5OZv/mO227oeBkfOzpG4EIdWHz/jSgBRcwT34Cx.qTS', NULL, NULL, NULL, NULL, 1550218114, 1551336741),
(29, 38, '196502072007011006', 'DJALMO PRAKOSA', 'Analis Statistik, Data Dan Pelaporan', 0, '$2y$13$M0r0pFvswATekmUi/MiB7.QKXcAyQVTxF9EaAvH0cwJSnA7v0QZma', NULL, NULL, NULL, NULL, 1550221843, 1551336725),
(30, 21, '196805051989031006', '', '', 0, '$2y$13$Dcn6p.nzcAl2IXR03KX8quWVSBytC2y1Iztp2Pp0s8zvsEQ4JSVLK', NULL, NULL, NULL, NULL, 1550472314, 1551336701),
(31, 60, '197906172009032005', 'LAILA ERNI YUSNITA, SKM,M.Kes', 'Analis Manajemen Informasi DanPengem-bangan Kesehatan', 0, '$2y$13$uxs.dIILx.edCc4ixo61tegIef36AliKQKwKLuNRAoqBPHwSRTPmu', NULL, NULL, NULL, NULL, 1550718910, 1551337825),
(32, 38, '197105011991011001', 'Drs. MUHAMMAD AGUNG HIKMATI, M.Si', 'KABID E-GOVERNMENT', 10, '$2y$13$NHblt6kzdwepZF3jndm61.lQmbHnISTK7PduKHKutzwSSAXLEfoCu', NULL, NULL, NULL, NULL, 1551337460, 1551337460),
(33, 71, '197508152005011022', 'YUSMANTO, SPi, M.Si', 'KASUBBAG PROGRAM', 10, '$2y$13$94N4rBn/k11157lxYr1Cve3rlgxAkU49wiOzlhJYIYINwLe1594AK', NULL, NULL, NULL, NULL, 1551839758, 1551839758),
(34, 49, '196903231991101001', 'ASHARI', 'Analis Pengembangan dan Sistem Informasi Sumber Daya Air', 10, '$2y$13$2SS.scVwBKBQyzpmeenEfOHL2eP8QZ2g19U5f05k3obA1RDX/zdAW', NULL, NULL, NULL, NULL, 1554685269, 1554685269),
(35, 72, '198704302006021001', 'FAJAR EFENDI, S.STP, MM', 'KASI PENGEMBANGAN SISTEM INFORMASI', 10, '$2y$13$RYaMbhNbfHAcwnEF22wdJOfWF.Ar/gjDkLCP1bVAtdoB6uQMR8FLG', NULL, NULL, NULL, NULL, 1563793596, 1563793596),
(36, 66, '197806302009032005', 'ELLIYA CHARIROH, S.Sos, MPSSp', 'KASI PENGELOLAAN DATA KEMISKINAN', 10, '$2y$13$o0bCbaBwBrs7Glq/RU1XFe3KvF0FTMVXrEIhfdz3erAmumO0Dt6a6', NULL, NULL, NULL, NULL, 1564538206, 1564538206),
(37, 57, '196910152003121007', 'ROBBERTO AGUNG NUGROHO, S.Pd', 'KASUBBAG PROGRAM', 10, '$2y$13$p/jVZEwldfCUI9lbzyQcVORx2iXw2sKbXNasOfxDRNgjKmRORLCAi', NULL, NULL, NULL, NULL, 1564718260, 1564718260);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aplikasi`
--
ALTER TABLE `aplikasi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_aplikasi_instansi` (`instansi_id`);

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `auth_assignment_user_id_idx` (`user_id`);

--
-- Indexes for table `auth_instansi_user`
--
ALTER TABLE `auth_instansi_user`
  ADD KEY `FK_auth_instansi_user_user` (`user_id`),
  ADD KEY `FK_auth_instansi_user_instansi` (`instansi_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `data_storage`
--
ALTER TABLE `data_storage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_data_storage_aplikasi` (`aplikasi_id`),
  ADD KEY `FK_data_storage_instansi` (`instansi_id`);

--
-- Indexes for table `instansi`
--
ALTER TABLE `instansi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_instansi_jenis_instansi` (`jenis_instansi_id`);

--
-- Indexes for table `jenis_instansi`
--
ALTER TABLE `jenis_instansi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `new_tree`
--
ALTER TABLE `new_tree`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tree_NK1` (`root`),
  ADD KEY `tree_NK2` (`lft`),
  ADD KEY `tree_NK3` (`rgt`),
  ADD KEY `tree_NK4` (`lvl`),
  ADD KEY `tree_NK5` (`active`),
  ADD KEY `FK_new_tree_tree` (`tree_id`);

--
-- Indexes for table `parameter_api`
--
ALTER TABLE `parameter_api`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_parameter_api_new_tree` (`new_tree_id`);

--
-- Indexes for table `route`
--
ALTER TABLE `route`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skpd`
--
ALTER TABLE `skpd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_sds`
--
ALTER TABLE `status_sds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_status_sds_instansi` (`instansi_id`);

--
-- Indexes for table `struktur_data`
--
ALTER TABLE `struktur_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_struktur_data_parameter_api` (`parameter_api_id`);

--
-- Indexes for table `tree`
--
ALTER TABLE `tree`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tree_NK1` (`root`),
  ADD KEY `tree_NK2` (`lft`),
  ADD KEY `tree_NK3` (`rgt`),
  ADD KEY `tree_NK4` (`lvl`),
  ADD KEY `tree_NK5` (`active`),
  ADD KEY `FK_tree_aplikasi` (`aplikasi_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nip` (`nip`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`),
  ADD KEY `FK_user_instansi` (`instansi_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aplikasi`
--
ALTER TABLE `aplikasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `data_storage`
--
ALTER TABLE `data_storage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `instansi`
--
ALTER TABLE `instansi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `jenis_instansi`
--
ALTER TABLE `jenis_instansi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `new_tree`
--
ALTER TABLE `new_tree`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `parameter_api`
--
ALTER TABLE `parameter_api`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `skpd`
--
ALTER TABLE `skpd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Kode Master SKPD Auto Increment';

--
-- AUTO_INCREMENT for table `status_sds`
--
ALTER TABLE `status_sds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `struktur_data`
--
ALTER TABLE `struktur_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=276;

--
-- AUTO_INCREMENT for table `tree`
--
ALTER TABLE `tree`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `aplikasi`
--
ALTER TABLE `aplikasi`
  ADD CONSTRAINT `FK_aplikasi_instansi` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_instansi_user`
--
ALTER TABLE `auth_instansi_user`
  ADD CONSTRAINT `FK_auth_instansi_user_instansi` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_auth_instansi_user_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `data_storage`
--
ALTER TABLE `data_storage`
  ADD CONSTRAINT `FK_data_storage_aplikasi` FOREIGN KEY (`aplikasi_id`) REFERENCES `aplikasi` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_data_storage_instansi` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `instansi`
--
ALTER TABLE `instansi`
  ADD CONSTRAINT `FK_instansi_jenis_instansi` FOREIGN KEY (`jenis_instansi_id`) REFERENCES `jenis_instansi` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `new_tree`
--
ALTER TABLE `new_tree`
  ADD CONSTRAINT `FK_new_tree_tree` FOREIGN KEY (`tree_id`) REFERENCES `tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `parameter_api`
--
ALTER TABLE `parameter_api`
  ADD CONSTRAINT `FK_parameter_api_new_tree` FOREIGN KEY (`new_tree_id`) REFERENCES `new_tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `status_sds`
--
ALTER TABLE `status_sds`
  ADD CONSTRAINT `FK_status_sds_instansi` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `struktur_data`
--
ALTER TABLE `struktur_data`
  ADD CONSTRAINT `FK_struktur_data_parameter_api` FOREIGN KEY (`parameter_api_id`) REFERENCES `parameter_api` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tree`
--
ALTER TABLE `tree`
  ADD CONSTRAINT `FK_tree_aplikasi` FOREIGN KEY (`aplikasi_id`) REFERENCES `aplikasi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_user_instansi` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
