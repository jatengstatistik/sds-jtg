-- MySQL dump 10.15  Distrib 10.0.36-MariaDB, for debian-linux-gnueabihf (armv7l)
--
-- Host: localhost    Database: sds_jateng_kabkota
-- ------------------------------------------------------
-- Server version	10.0.36-MariaDB-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aplikasi`
--

DROP TABLE IF EXISTS `aplikasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aplikasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_aplikasi` varchar(150) DEFAULT NULL,
  `slug_nama_aplikasi` varchar(155) DEFAULT NULL,
  `url_aplikasi` varchar(150) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `instansi_id` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aplikasi`
--

LOCK TABLES `aplikasi` WRITE;
/*!40000 ALTER TABLE `aplikasi` DISABLE KEYS */;
INSERT INTO `aplikasi` VALUES (22,'SDS Demo','sds-demo','http://data.jatengprov.go.id/apps/api/bridge/sds/sds','admin123','123456','',38,2147483647,2147483647);
/*!40000 ALTER TABLE `aplikasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `auth_assignment_user_id_idx` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` VALUES ('Super Administrator','3',1532922964);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` VALUES ('/*',2,NULL,NULL,NULL,1531667601,1531667601),('/aplikasi/*',2,NULL,NULL,NULL,1538908083,1538908083),('/data/*',2,NULL,NULL,NULL,1535258213,1535258213),('/debug/*',2,NULL,NULL,NULL,1535284797,1535284797),('/debug/default/*',2,NULL,NULL,NULL,1535284798,1535284798),('/debug/user/*',2,NULL,NULL,NULL,1535284799,1535284799),('/gii/*',2,NULL,NULL,NULL,1535284800,1535284800),('/gii/default/*',2,NULL,NULL,NULL,1535284801,1535284801),('/instansi/*',2,NULL,NULL,NULL,1535258172,1535258172),('/integrasi-data/*',2,NULL,NULL,NULL,1543306933,1543306933),('/jenis-instansi/*',2,NULL,NULL,NULL,1543301059,1543301059),('/kabkota/*',2,NULL,NULL,NULL,1535258171,1535258171),('/mimin/*',2,NULL,NULL,NULL,1535241408,1535241408),('/mimin/role/*',2,NULL,NULL,NULL,1535284805,1535284805),('/mimin/route/*',2,NULL,NULL,NULL,1535284805,1535284805),('/mimin/user/*',2,NULL,NULL,NULL,1535284808,1535284808),('/sector/*',2,NULL,NULL,NULL,1535258189,1535258189),('/setting/*',2,NULL,NULL,NULL,1543390839,1543390839),('/setting/update',2,NULL,NULL,NULL,1543392062,1543392062),('/setting/view',2,NULL,NULL,NULL,1543392059,1543392059),('/site/*',2,NULL,NULL,NULL,1532947156,1532947156),('/skpd/*',2,NULL,NULL,NULL,1532947203,1532947203),('/status-sds/*',2,NULL,NULL,NULL,1533120501,1533120501),('/tipe-data/*',2,NULL,NULL,NULL,1536074575,1536074575),('/treemanager/*',2,NULL,NULL,NULL,1535241147,1535241147),('/treemanager/node/*',2,NULL,NULL,NULL,1535241154,1535241154),('/user/*',2,NULL,NULL,NULL,1535258252,1535258252),('Admin SKPD',1,NULL,NULL,NULL,1532947142,1535412003),('Administrator',1,NULL,NULL,NULL,1535258138,1535258262),('Sds Status',1,NULL,NULL,NULL,1533120496,1535240054),('Super Administrator',1,NULL,NULL,NULL,1531661172,1536074648);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` VALUES ('Admin SKPD','/data/*'),('Admin SKPD','/sector/*'),('Admin SKPD','/site/*'),('Admin SKPD','/treemanager/*'),('Admin SKPD','/treemanager/node/*'),('Administrator','/aplikasi/*'),('Administrator','/data/*'),('Administrator','/instansi/*'),('Administrator','/jenis-instansi/*'),('Administrator','/kabkota/*'),('Administrator','/mimin/user/*'),('Administrator','/site/*'),('Administrator','/skpd/*'),('Administrator','/status-sds/*'),('Administrator','/treemanager/*'),('Administrator','/treemanager/node/*'),('Administrator','/user/*'),('Sds Status','/site/*'),('Sds Status','/status-sds/*'),('Super Administrator','/*'),('Super Administrator','/aplikasi/*'),('Super Administrator','/data/*'),('Super Administrator','/debug/*'),('Super Administrator','/debug/default/*'),('Super Administrator','/debug/user/*'),('Super Administrator','/gii/*'),('Super Administrator','/gii/default/*'),('Super Administrator','/instansi/*'),('Super Administrator','/integrasi-data/*'),('Super Administrator','/jenis-instansi/*'),('Super Administrator','/kabkota/*'),('Super Administrator','/mimin/*'),('Super Administrator','/mimin/role/*'),('Super Administrator','/mimin/route/*'),('Super Administrator','/mimin/user/*'),('Super Administrator','/sector/*'),('Super Administrator','/setting/update'),('Super Administrator','/setting/view'),('Super Administrator','/site/*'),('Super Administrator','/skpd/*'),('Super Administrator','/status-sds/*'),('Super Administrator','/tipe-data/*'),('Super Administrator','/treemanager/*'),('Super Administrator','/treemanager/node/*'),('Super Administrator','/user/*');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data`
--

DROP TABLE IF EXISTS `data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tree_id` int(11) DEFAULT NULL,
  `deskripsi` varchar(50) DEFAULT NULL COMMENT 'Deskripsi',
  `status` enum('1','0') DEFAULT NULL,
  `instansi_id` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_data_instansi` (`instansi_id`),
  CONSTRAINT `FK_data_instansi` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data`
--

LOCK TABLES `data` WRITE;
/*!40000 ALTER TABLE `data` DISABLE KEYS */;
INSERT INTO `data` VALUES (1,7,'Halo 123','1',1,123123123,123123123),(3,10,'Halo pasien 123','1',NULL,NULL,NULL),(4,12,'Jumlah Pegawai Golongan Berdasarkan Jenis Kelamin','1',NULL,NULL,NULL);
/*!40000 ALTER TABLE `data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instansi`
--

DROP TABLE IF EXISTS `instansi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instansi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_instansi_id` int(11) NOT NULL,
  `nama_instansi` varchar(255) DEFAULT NULL,
  `singkatan_instansi` varchar(255) DEFAULT NULL,
  `alamat_instansi` varchar(255) DEFAULT NULL,
  `telepon_instansi` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instansi`
--

LOCK TABLES `instansi` WRITE;
/*!40000 ALTER TABLE `instansi` DISABLE KEYS */;
INSERT INTO `instansi` VALUES (1,0,'Kabupaten Banjarnegara',NULL,'',NULL,1531667859,1531667859),(3,0,'Kabupaten Banyumas',NULL,'',NULL,1531667900,1531667900),(4,0,'Kabupaten Batang',NULL,'',NULL,1531668273,1531668273),(5,0,'Kabupaten Blora',NULL,'',NULL,1531668312,1531668312),(7,0,'Kabupaten Boyolali',NULL,'',NULL,1531668439,1531668439),(8,0,'Kabupaten Brebes',NULL,'',NULL,1531668456,1531668456),(9,0,'Kabupaten Demak',NULL,'',NULL,1531668488,1531668488),(10,0,'Kabupaten Grobogan',NULL,'',NULL,1531668511,1531668511),(11,0,'Kabupaten Jepara',NULL,'',NULL,1531668529,1531668529),(12,0,'Kabupaten Karanganyar',NULL,'',NULL,1531668545,1531668545),(13,0,'Kabupaten Kebumen',NULL,'',NULL,1531668561,1531668561),(14,0,'Kabupaten Kendal',NULL,'',NULL,1531668579,1531668579),(15,0,'Kabupaten Klaten',NULL,'',NULL,1531668596,1531668596),(16,0,'Kabupaten Kudus',NULL,'',NULL,1531668614,1531668614),(17,0,'Kabupaten Magelang',NULL,'',NULL,1531668631,1531668631),(18,0,'Kabupaten Pati',NULL,'',NULL,1531668649,1531668649),(19,0,'Kabupaten Pekalongan',NULL,'',NULL,1531668666,1531668666),(20,0,'Kabupaten Pemalang',NULL,'',NULL,1531668682,1531668682),(21,0,'Kabupaten Purbalingga',NULL,'',NULL,1531668699,1531668699),(22,0,'Kabupaten Purworejo',NULL,'',NULL,1531668715,1531668715),(23,0,'Kabupaten Rembang',NULL,'',NULL,1531668732,1531668732),(24,0,'Kabupaten Semarang',NULL,'',NULL,1531668750,1531668750),(25,0,'Kabupaten Sragen',NULL,'',NULL,1531668767,1531668767),(26,0,'Kabupaten Sukoharjo',NULL,'',NULL,1531668784,1531668784),(27,0,'Kabupaten Tegal',NULL,'',NULL,1531668800,1531668800),(28,0,'Kabupaten Temanggung',NULL,'',NULL,1531668816,1531668816),(29,0,'Kabupaten Wonogiri',NULL,'',NULL,1531668833,1531668833),(30,0,'Kabupaten Wonosobo',NULL,'',NULL,1531668850,1531668850),(31,0,'Kota Pekalongan',NULL,'',NULL,1531668869,1531668869),(32,0,'Kota Salatiga',NULL,'',NULL,1531668885,1531668885),(33,0,'Kota Semarang',NULL,'',NULL,1531668902,1531668902),(34,0,'Kota Surakarta',NULL,'',NULL,1531668936,1531668936),(35,0,'Kota Tegal',NULL,'',NULL,1531668969,1531668969),(36,0,'Kota Magelang',NULL,'',NULL,1531668991,1531668991),(37,0,'Kabupaten Cilacap',NULL,'',NULL,1531671088,1531671088),(38,1,'Dinas Komunikasi dan Informatika','Diskominfo','Jl. Mentri Supeno I No 2 Semarang','0248319140',NULL,1543301990),(39,1,'Rumah Sakit Umum Daerah Tugurejo','RSUD Tugurejo','Jl. Walisongo KM. 8,5, Tambakaji, Ngaliyan, Tambakaji, Ngaliyan, Kota Semarang, Jawa Tengah 50185','',NULL,1543370372),(40,1,'Badan Pengelola Keuangan dan Aset Daerah','BPKAD','Jl. Taman Menteri Supeno No. 2 ','0248311174',NULL,1543302076),(41,1,'Badan Kepegawaian Daerah','BKD','Jl. Stadion Selatan No. 1, Semarang','0248318846',NULL,1543302129),(42,1,'RSUD Prof. Dr. Margono Soekarjo','RSUD Margono','Jl. Dr. Gumbreg No.1 Purwokerto, Kabupaten Banyumas Provinsi Jawa Tengah, Kode Pos 53146','0281632708',NULL,1543302139),(43,1,'RSUD Kelet','RSUD Kelet','','',NULL,1543302115),(44,1,'RSUD Soedjarwadi','RSUD Sodjarwadi','','',NULL,1543302102),(45,1,'Dinas Pemberdayaan Perempuan, Perlindungan Anak, Pengendalian Penduduk Dan Keluarga Berencana','DP3AKB','Jl. Pamularsih No. 28 Semarang 50148','',NULL,1543302089);
/*!40000 ALTER TABLE `instansi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jenis_instansi`
--

DROP TABLE IF EXISTS `jenis_instansi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jenis_instansi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_instansi` varchar(80) NOT NULL,
  `keterangan_jenis_instansi` varchar(200) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jenis_instansi`
--

LOCK TABLES `jenis_instansi` WRITE;
/*!40000 ALTER TABLE `jenis_instansi` DISABLE KEYS */;
INSERT INTO `jenis_instansi` VALUES (1,'SKPD','Satuan Kerja Perangkat Daerah',0,0),(2,'BUMD','Badan Usaha Milih Daerah',0,0);
/*!40000 ALTER TABLE `jenis_instansi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1531454941),('m130524_201442_init',1531454970),('m140506_102106_rbac_init',1531454949),('m151024_072453_create_route_table',1531454957),('m170907_052038_rbac_add_index_on_auth_assignment_user_id',1531454950),('m181002_142147_new_tree',1538500732),('m230416_200116_tree',1532859718);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `new_tree`
--

DROP TABLE IF EXISTS `new_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `new_tree` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tree_id` bigint(20) NOT NULL,
  `root` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL,
  `lvl` smallint(5) NOT NULL,
  `child_allowed` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(60) NOT NULL,
  `slug_new_tree` varchar(150) DEFAULT NULL,
  `deskripsi` text,
  `icon` varchar(255) DEFAULT NULL,
  `icon_type` smallint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `selected` tinyint(1) NOT NULL DEFAULT '0',
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `readonly` tinyint(1) NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `collapsed` tinyint(1) NOT NULL DEFAULT '0',
  `movable_u` tinyint(1) NOT NULL DEFAULT '1',
  `movable_d` tinyint(1) NOT NULL DEFAULT '1',
  `movable_l` tinyint(1) NOT NULL DEFAULT '1',
  `movable_r` tinyint(1) NOT NULL DEFAULT '1',
  `removable` tinyint(1) NOT NULL DEFAULT '1',
  `removable_all` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `tree_NK1` (`root`),
  KEY `tree_NK2` (`lft`),
  KEY `tree_NK3` (`rgt`),
  KEY `tree_NK4` (`lvl`),
  KEY `tree_NK5` (`active`),
  KEY `FK_new_tree_tree` (`tree_id`),
  CONSTRAINT `FK_new_tree_tree` FOREIGN KEY (`tree_id`) REFERENCES `tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `new_tree`
--

LOCK TABLES `new_tree` WRITE;
/*!40000 ALTER TABLE `new_tree` DISABLE KEYS */;
INSERT INTO `new_tree` VALUES (1,1,1,1,0,2,0,0,'Input Data','input-data','Input Data melalui Button ROOT','',1,0,0,1,1,0,0,1,1,1,1,0,0),(2,26,2,1,1,4,0,1,'Pegawai Golongan','pegawai-golongan','Data PNS Berdasar Golongan','',1,1,0,0,0,1,0,1,1,1,1,1,0),(3,26,2,2,1,3,1,1,'Golongan','golongan','','',1,1,0,0,0,1,0,1,1,1,1,1,0);
/*!40000 ALTER TABLE `new_tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter_api`
--

DROP TABLE IF EXISTS `parameter_api`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter_api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `new_tree_id` bigint(20) NOT NULL,
  `parameter_api` varchar(50) DEFAULT NULL,
  `slug_parameter_api` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_parameter_api_new_tree` (`new_tree_id`),
  CONSTRAINT `FK_parameter_api_new_tree` FOREIGN KEY (`new_tree_id`) REFERENCES `new_tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter_api`
--

LOCK TABLES `parameter_api` WRITE;
/*!40000 ALTER TABLE `parameter_api` DISABLE KEYS */;
INSERT INTO `parameter_api` VALUES (1,2,'data','data'),(2,3,'gol','gol');
/*!40000 ALTER TABLE `parameter_api` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `route`
--

DROP TABLE IF EXISTS `route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `route`
--

LOCK TABLES `route` WRITE;
/*!40000 ALTER TABLE `route` DISABLE KEYS */;
INSERT INTO `route` VALUES ('/*','*','',1),('/aplikasi/*','*','aplikasi',1),('/aplikasi/create','create','aplikasi',1),('/aplikasi/delete','delete','aplikasi',1),('/aplikasi/index','index','aplikasi',1),('/aplikasi/update','update','aplikasi',1),('/aplikasi/view','view','aplikasi',1),('/data/*','*','data',1),('/data/create','create','data',1),('/data/get-data-api','get-data-api','data',1),('/data/index','index','data',1),('/data/new-tree','new-tree','data',1),('/data/update','update','data',1),('/debug/*','*','debug',1),('/debug/default/*','*','debug/default',1),('/debug/default/db-explain','db-explain','debug/default',1),('/debug/default/download-mail','download-mail','debug/default',1),('/debug/default/index','index','debug/default',1),('/debug/default/toolbar','toolbar','debug/default',1),('/debug/default/view','view','debug/default',1),('/debug/user/*','*','debug/user',1),('/debug/user/reset-identity','reset-identity','debug/user',1),('/debug/user/set-identity','set-identity','debug/user',1),('/gii/*','*','gii',1),('/gii/default/*','*','gii/default',1),('/gii/default/action','action','gii/default',1),('/gii/default/diff','diff','gii/default',1),('/gii/default/index','index','gii/default',1),('/gii/default/preview','preview','gii/default',1),('/gii/default/view','view','gii/default',1),('/instansi/*','*','instansi',1),('/instansi/create','create','instansi',1),('/instansi/delete','delete','instansi',1),('/instansi/index','index','instansi',1),('/instansi/update','update','instansi',1),('/instansi/view','view','instansi',1),('/integrasi-data/*','*','integrasi-data',1),('/integrasi-data/index','index','integrasi-data',1),('/jenis-instansi/*','*','jenis-instansi',1),('/jenis-instansi/create','create','jenis-instansi',1),('/jenis-instansi/delete','delete','jenis-instansi',1),('/jenis-instansi/index','index','jenis-instansi',1),('/jenis-instansi/update','update','jenis-instansi',1),('/jenis-instansi/view','view','jenis-instansi',1),('/kabkota/*','*','kabkota',1),('/kabkota/create','create','kabkota',1),('/kabkota/delete','delete','kabkota',1),('/kabkota/index','index','kabkota',1),('/kabkota/update','update','kabkota',1),('/kabkota/view','view','kabkota',1),('/mimin/*','*','mimin',1),('/mimin/role/*','*','mimin/role',1),('/mimin/role/create','create','mimin/role',1),('/mimin/role/delete','delete','mimin/role',1),('/mimin/role/index','index','mimin/role',1),('/mimin/role/permission','permission','mimin/role',1),('/mimin/role/update','update','mimin/role',1),('/mimin/role/view','view','mimin/role',1),('/mimin/route/*','*','mimin/route',1),('/mimin/route/create','create','mimin/route',1),('/mimin/route/delete','delete','mimin/route',1),('/mimin/route/generate','generate','mimin/route',1),('/mimin/route/index','index','mimin/route',1),('/mimin/route/update','update','mimin/route',1),('/mimin/route/view','view','mimin/route',1),('/mimin/user/*','*','mimin/user',1),('/mimin/user/create','create','mimin/user',1),('/mimin/user/delete','delete','mimin/user',1),('/mimin/user/index','index','mimin/user',1),('/mimin/user/update','update','mimin/user',1),('/mimin/user/view','view','mimin/user',1),('/setting/*','*','setting',1),('/setting/create','create','setting',1),('/setting/delete','delete','setting',1),('/setting/index','index','setting',1),('/setting/update','update','setting',1),('/setting/view','view','setting',1),('/site/*','*','site',1),('/site/about','about','site',1),('/site/captcha','captcha','site',1),('/site/contact','contact','site',1),('/site/error','error','site',1),('/site/index','index','site',1),('/site/login','login','site',1),('/site/logout','logout','site',1),('/skpd/*','*','skpd',1),('/skpd/create','create','skpd',1),('/skpd/delete','delete','skpd',1),('/skpd/index','index','skpd',1),('/skpd/update','update','skpd',1),('/skpd/view','view','skpd',1),('/status-sds/*','*','status-sds',1),('/status-sds/create','create','status-sds',1),('/status-sds/delete','delete','status-sds',1),('/status-sds/index','index','status-sds',1),('/status-sds/update','update','status-sds',1),('/status-sds/view','view','status-sds',1),('/treemanager/*','*','treemanager',1),('/treemanager/node/*','*','treemanager/node',1),('/treemanager/node/manage','manage','treemanager/node',1),('/treemanager/node/move','move','treemanager/node',1),('/treemanager/node/remove','remove','treemanager/node',1),('/treemanager/node/save','save','treemanager/node',1),('/user/*','*','user',1),('/user/create','create','user',1),('/user/delete','delete','user',1),('/user/index','index','user',1),('/user/update','update','user',1),('/user/view','view','user',1);
/*!40000 ALTER TABLE `route` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `site_logo` varchar(255) NOT NULL,
  `api_pegawai` varchar(255) NOT NULL,
  `skpd` varchar(255) NOT NULL,
  `nama_kabupaten` varchar(255) NOT NULL,
  `kode_kabupaten` int(2) NOT NULL,
  `version` varchar(50) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting`
--

LOCK TABLES `setting` WRITE;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` VALUES (1,'Single Data System','Single Data System Jawa Tengah','http://sds.jatengprov.go.id/dev/web/assets/d15a1663/img/sds-logo.png','http://simpeg.bkd.jatengprov.go.id/webservice/identitas','Dinas Komunikasi dan Informatika','Pemerintah Provinsi Jawa Tengah',33,'2.0.0',0,0);
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skpd`
--

DROP TABLE IF EXISTS `skpd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skpd` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Kode Master SKPD Auto Increment',
  `kode_skpd` char(4) DEFAULT NULL COMMENT 'Kode SKPD = (2 digit kode Kabkota + 2 digit Kode SKPD)',
  `nama_skpd` varchar(255) DEFAULT NULL COMMENT 'Nama SKPD',
  `akronim_skpd` varchar(50) DEFAULT NULL COMMENT 'Singkatan SKPD',
  `alamat_skpd` varchar(255) DEFAULT NULL COMMENT 'Alamat SKPD',
  `telpon_skpd` varchar(50) DEFAULT NULL COMMENT 'Telpon SKPD',
  `nama_pic_skpd` varchar(50) DEFAULT NULL COMMENT 'Nama Kontak Person SKPD',
  `telepon_pic_skpd` varchar(50) DEFAULT NULL COMMENT 'Telp. Kontak Person SKPD',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skpd`
--

LOCK TABLES `skpd` WRITE;
/*!40000 ALTER TABLE `skpd` DISABLE KEYS */;
/*!40000 ALTER TABLE `skpd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `struktur_data`
--

DROP TABLE IF EXISTS `struktur_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `struktur_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameter_api_id` int(11) NOT NULL,
  `nama_struktur` varchar(50) DEFAULT NULL COMMENT 'Nama yang akan di tampilkan',
  `parameter` varchar(50) DEFAULT NULL COMMENT 'Field yang akan dibaca APInya',
  `relasi` enum('0','1') DEFAULT NULL,
  `grafik` enum('x','y','tidak') DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_struktur_data_parameter_api` (`parameter_api_id`),
  CONSTRAINT `FK_struktur_data_parameter_api` FOREIGN KEY (`parameter_api_id`) REFERENCES `parameter_api` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `struktur_data`
--

LOCK TABLES `struktur_data` WRITE;
/*!40000 ALTER TABLE `struktur_data` DISABLE KEYS */;
INSERT INTO `struktur_data` VALUES (1,1,'Golongan','gol','1','x',NULL,NULL),(2,1,'Jumlah','jml','0','y',NULL,NULL),(3,2,'Nama Pegawai','nama','0','tidak',NULL,NULL),(4,2,'NIP','nip','0','tidak',NULL,NULL);
/*!40000 ALTER TABLE `struktur_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tree`
--

DROP TABLE IF EXISTS `tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tree` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `root` int(11) DEFAULT NULL,
  `aplikasi_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `child_allowed` tinyint(4) NOT NULL DEFAULT '0',
  `deskripsi` text NOT NULL,
  `status` tinyint(2) NOT NULL,
  `lvl` smallint(5) NOT NULL,
  `name` varchar(60) NOT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `icon_type` smallint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `selected` tinyint(1) NOT NULL DEFAULT '0',
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `readonly` tinyint(1) NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `collapsed` tinyint(1) NOT NULL DEFAULT '0',
  `movable_u` tinyint(1) NOT NULL DEFAULT '1',
  `movable_d` tinyint(1) NOT NULL DEFAULT '1',
  `movable_l` tinyint(1) NOT NULL DEFAULT '1',
  `movable_r` tinyint(1) NOT NULL DEFAULT '1',
  `removable` tinyint(1) NOT NULL DEFAULT '1',
  `removable_all` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `tree_NK1` (`root`),
  KEY `tree_NK2` (`lft`),
  KEY `tree_NK3` (`rgt`),
  KEY `tree_NK4` (`lvl`),
  KEY `tree_NK5` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tree`
--

LOCK TABLES `tree` WRITE;
/*!40000 ALTER TABLE `tree` DISABLE KEYS */;
INSERT INTO `tree` VALUES (1,1,NULL,1,2,1,'',0,0,'Kesehatan','kesehatan','',1,1,0,0,1,1,0,1,1,1,1,0,0),(2,2,NULL,1,2,1,'',0,0,'Lingkungan Hidup',NULL,'',1,1,0,0,1,1,0,1,1,1,1,0,0),(3,3,NULL,1,2,1,'',0,0,'Pariwisata','pariwisata','',1,1,0,0,1,1,0,1,1,1,1,0,0),(14,14,NULL,1,2,1,'',0,0,'Lingkungan Hidup','lingkungan-hidup','',1,1,0,0,1,1,0,1,1,1,1,0,0),(15,15,NULL,1,2,1,'',0,0,'Pariwisata','pariwisata-2','',1,1,0,0,1,1,0,1,1,1,1,0,0),(16,16,NULL,1,2,1,'',0,0,'Sosial','sosial','',1,1,0,0,1,1,0,1,1,1,1,0,0),(17,17,NULL,1,2,1,'',0,0,'Energi','energi','',1,1,0,0,1,1,0,1,1,1,1,0,0),(18,18,NULL,1,2,1,'',0,0,'Pangan','pangan','',1,1,0,0,1,1,0,1,1,1,1,0,0),(19,19,NULL,1,2,1,'',0,0,'Maritim','maritim','',1,1,0,0,1,1,0,1,1,1,1,0,0),(20,20,NULL,1,2,1,'',0,0,'Ekonomi','ekonomi','',1,1,0,0,1,1,0,1,1,1,1,0,0),(21,21,NULL,1,2,1,'',0,0,'Industri','industri','',1,1,0,0,1,1,0,1,1,1,1,0,0),(22,22,NULL,1,2,1,'',0,0,'Infrastruktur','infrastruktur','',1,1,0,0,1,1,0,1,1,1,1,0,0),(23,23,NULL,1,2,1,'',0,0,'Pendidikan dan Kebudayaan','pendidikan-dan-kebudayaan','',1,1,0,0,1,1,0,1,1,1,1,0,0),(24,24,NULL,1,2,1,'',0,0,'Kepemudaan dan Olahraga','kepemudaan-dan-olahraga','',1,1,0,0,1,1,0,1,1,1,1,0,0),(25,25,NULL,1,2,1,'',0,0,'Indikator Makro','indikator-makro','',1,1,0,0,1,1,0,1,1,1,1,0,0),(26,26,NULL,1,2,1,'',1,0,'Tata Kelola Pemerintah','tata-kelola-pemerintah','',1,1,0,0,0,1,0,1,1,1,1,1,0);
/*!40000 ALTER TABLE `tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instansi_id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nip` varchar(18) COLLATE utf8_unicode_ci NOT NULL,
  `nama_lengkap` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telepon_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `nip` (`nip`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`),
  KEY `FK_user_instansi` (`instansi_id`),
  CONSTRAINT `FK_user_instansi` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (3,40,'mahfud',NULL,'198112142009011004',NULL,'$2y$13$6hkbImUlS3QWYRqvCfOYC.Y55SUoEC2hRZScuz32Zzt78E/V4T33m',NULL,'mahfud@gmail.com','',10,1532844132,1539093536);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-03  7:59:04
