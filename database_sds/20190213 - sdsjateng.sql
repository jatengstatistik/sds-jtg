-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 13, 2019 at 12:00 PM
-- Server version: 10.3.9-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sdsjateng_c24db_sds_jateng`
--

-- --------------------------------------------------------

--
-- Table structure for table `aplikasi`
--

DROP TABLE IF EXISTS `aplikasi`;
CREATE TABLE IF NOT EXISTS `aplikasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instansi_id` int(11) NOT NULL,
  `nama_aplikasi` varchar(150) DEFAULT NULL,
  `slug_nama_aplikasi` varchar(155) DEFAULT NULL,
  `url_aplikasi` varchar(150) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_aplikasi_instansi` (`instansi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aplikasi`
--

INSERT INTO `aplikasi` (`id`, `instansi_id`, `nama_aplikasi`, `slug_nama_aplikasi`, `url_aplikasi`, `username`, `password`, `token`, `created_at`, `updated_at`) VALUES
(19, 39, 'SIM RS Tugurejo', 'sim-rs-tugurejo', 'http://data.jatengprov.go.id/apps/api/bridge/sds/tugurejo', 'admin123', 'rahasiaadmin123', '', 2147483647, 2147483647),
(20, 41, 'Simpeg BKD Jateng', 'simpeg-bkd-jateng', 'http://data.jatengprov.go.id/apps/api/bridge/sds/bkd', 'admin123', 'adminrahasia123', '', 2147483647, 2147483647),
(22, 41, 'SDS Demo', 'sds-demo', 'http://data.jatengprov.go.id/apps/api/bridge/sds/sds', 'admin123', '123456', '', 2147483647, 2147483647),
(23, 42, 'SIM RS Margono', 'sim-rs-margono', 'http://data.jatengprov.go.id/apps/api/bridge/sds/margono', 'admin123', '123456', '', 2147483647, 2147483647),
(24, 43, 'SIM RS Kelet', 'sim-rs-kelet', 'http://data.jatengprov.go.id/apps/api/bridge/sds/kelet', 'admin123', '123456', '', 2147483647, 2147483647),
(25, 44, 'SIM RS Soedjarwadi', 'sim-rs-soedjarwadi', 'http://data.jatengprov.go.id/apps/api/bridge/sds/sujarwadi', 'admin123', '123456', '', 2147483647, 2147483647),
(26, 45, 'Sistem Informasi Gender dan Anak (SIGA)', 'sistem-informasi-gender-dan-anak-siga', 'http://data.jatengprov.go.id/apps/api/bridge/sds/dp3akb', 'admin123', '123456', '', 2147483647, 2147483647),
(27, 40, 'Aplikasi Keuangan BPKAD', 'aplikasi-keuangan-bpkad', 'https://bpkad.jatengprov.go.id/webservices', '198112142009011004', '123456', '', NULL, NULL),
(28, 49, 'Tatonas', 'tatonas', 'http://data.jatengprov.go.id/apps/api/bridge/sds/dpusdataru', '', '', '', NULL, NULL),
(29, 48, 'SIM Padu PK', 'sim-padu-pk', 'http://data.jatengprov.go.id/apps/api/bridge/sds/bappeda', '', '', '', NULL, NULL),
(30, 50, 'Web GIS Jalan', 'web-gis-jalan', 'http://dpubinmarcipka.jatengprov.go.id/webgis/apisds/services.php', '', '', '', NULL, NULL),
(31, 52, 'One Touch Statistics', 'one-touch-statistics', 'http://data.jatengprov.go.id/apps/api/bridge/sds/bps', '198112142009011004', '123456', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `auth_assignment_user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('Administrator', '10', 1543369571),
('Administrator', '11', 1547793448),
('Administrator', '13', 1548057253),
('Administrator', '14', 1550042485),
('Analis Data Dan Informasi', '15', 1550042477),
('Super Administrator', '3', 1535258318),
('Super Administrator', '4', 1550042500);

-- --------------------------------------------------------

--
-- Table structure for table `auth_instansi_user`
--

DROP TABLE IF EXISTS `auth_instansi_user`;
CREATE TABLE IF NOT EXISTS `auth_instansi_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `instansi_id` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_auth_instansi_user_instansi` (`instansi_id`),
  KEY `FK_auth_instansi_user_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=924 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_instansi_user`
--

INSERT INTO `auth_instansi_user` (`id`, `user_id`, `instansi_id`, `created_at`, `updated_at`) VALUES
(768, 3, 1, 1550040863, NULL),
(769, 3, 3, 1550040863, NULL),
(770, 3, 4, 1550040863, NULL),
(771, 3, 5, 1550040863, NULL),
(772, 3, 7, 1550040863, NULL),
(773, 3, 8, 1550040863, NULL),
(774, 3, 9, 1550040863, NULL),
(775, 3, 10, 1550040863, NULL),
(776, 3, 11, 1550040863, NULL),
(777, 3, 12, 1550040863, NULL),
(778, 3, 13, 1550040863, NULL),
(779, 3, 14, 1550040863, NULL),
(780, 3, 15, 1550040863, NULL),
(781, 3, 16, 1550040863, NULL),
(782, 3, 17, 1550040863, NULL),
(783, 3, 18, 1550040863, NULL),
(784, 3, 19, 1550040863, NULL),
(785, 3, 20, 1550040864, NULL),
(786, 3, 21, 1550040864, NULL),
(787, 3, 22, 1550040864, NULL),
(788, 3, 23, 1550040864, NULL),
(789, 3, 24, 1550040864, NULL),
(790, 3, 25, 1550040864, NULL),
(791, 3, 26, 1550040864, NULL),
(792, 3, 27, 1550040864, NULL),
(793, 3, 28, 1550040864, NULL),
(794, 3, 29, 1550040864, NULL),
(795, 3, 30, 1550040864, NULL),
(796, 3, 31, 1550040864, NULL),
(797, 3, 32, 1550040864, NULL),
(798, 3, 33, 1550040864, NULL),
(799, 3, 34, 1550040864, NULL),
(800, 3, 35, 1550040864, NULL),
(801, 3, 36, 1550040864, NULL),
(802, 3, 37, 1550040864, NULL),
(803, 3, 38, 1550040864, NULL),
(804, 3, 39, 1550040864, NULL),
(805, 3, 40, 1550040864, NULL),
(806, 3, 41, 1550040864, NULL),
(807, 3, 42, 1550040864, NULL),
(808, 3, 43, 1550040864, NULL),
(809, 3, 44, 1550040864, NULL),
(810, 3, 45, 1550040864, NULL),
(811, 3, 48, 1550040864, NULL),
(812, 3, 49, 1550040864, NULL),
(813, 3, 50, 1550040864, NULL),
(814, 3, 51, 1550040864, NULL),
(815, 3, 52, 1550040864, NULL),
(920, 15, 38, 1550042477, NULL),
(921, 15, 39, 1550042477, NULL),
(922, 14, 38, 1550042486, NULL),
(923, 4, 38, 1550042500, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, 1531667601, 1531667601),
('/aplikasi/*', 2, NULL, NULL, NULL, 1538908083, 1538908083),
('/data/*', 2, NULL, NULL, NULL, 1535258213, 1535258213),
('/debug/*', 2, NULL, NULL, NULL, 1535284797, 1535284797),
('/debug/default/*', 2, NULL, NULL, NULL, 1535284798, 1535284798),
('/debug/user/*', 2, NULL, NULL, NULL, 1535284799, 1535284799),
('/gii/*', 2, NULL, NULL, NULL, 1535284800, 1535284800),
('/gii/default/*', 2, NULL, NULL, NULL, 1535284801, 1535284801),
('/instansi/*', 2, NULL, NULL, NULL, 1535258172, 1535258172),
('/integrasi-data/*', 2, NULL, NULL, NULL, 1543306933, 1543306933),
('/jenis-instansi/*', 2, NULL, NULL, NULL, 1543301059, 1543301059),
('/kabkota/*', 2, NULL, NULL, NULL, 1535258171, 1535258171),
('/mimin/*', 2, NULL, NULL, NULL, 1535241408, 1535241408),
('/mimin/role/*', 2, NULL, NULL, NULL, 1535284805, 1535284805),
('/mimin/route/*', 2, NULL, NULL, NULL, 1535284805, 1535284805),
('/mimin/user/*', 2, NULL, NULL, NULL, 1535284808, 1535284808),
('/sector/*', 2, NULL, NULL, NULL, 1535258189, 1535258189),
('/setting/*', 2, NULL, NULL, NULL, 1543390839, 1543390839),
('/setting/update', 2, NULL, NULL, NULL, 1543392062, 1543392062),
('/setting/view', 2, NULL, NULL, NULL, 1543392059, 1543392059),
('/site/*', 2, NULL, NULL, NULL, 1532947156, 1532947156),
('/skpd/*', 2, NULL, NULL, NULL, 1532947203, 1532947203),
('/status-sds/*', 2, NULL, NULL, NULL, 1533120501, 1533120501),
('/tipe-data/*', 2, NULL, NULL, NULL, 1536074575, 1536074575),
('/treemanager/*', 2, NULL, NULL, NULL, 1535241147, 1535241147),
('/treemanager/node/*', 2, NULL, NULL, NULL, 1535241154, 1535241154),
('/user/*', 2, NULL, NULL, NULL, 1535258252, 1535258252),
('Admin SKPD', 1, NULL, NULL, NULL, 1532947142, 1535412003),
('Administrator', 1, NULL, NULL, NULL, 1535258138, 1548314812),
('Analis Data Dan Informasi', 1, NULL, NULL, NULL, 1550042112, 1550042226),
('Sds Status', 1, NULL, NULL, NULL, 1533120496, 1535240054),
('Super Administrator', 1, NULL, NULL, NULL, 1531661172, 1544589313);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('Admin SKPD', '/data/*'),
('Admin SKPD', '/sector/*'),
('Admin SKPD', '/site/*'),
('Admin SKPD', '/treemanager/*'),
('Admin SKPD', '/treemanager/node/*'),
('Administrator', '/aplikasi/*'),
('Administrator', '/data/*'),
('Administrator', '/instansi/*'),
('Administrator', '/integrasi-data/*'),
('Administrator', '/jenis-instansi/*'),
('Administrator', '/kabkota/*'),
('Administrator', '/setting/*'),
('Administrator', '/site/*'),
('Administrator', '/skpd/*'),
('Administrator', '/status-sds/*'),
('Administrator', '/treemanager/*'),
('Administrator', '/treemanager/node/*'),
('Administrator', '/user/*'),
('Analis Data Dan Informasi', '/integrasi-data/*'),
('Analis Data Dan Informasi', '/site/*'),
('Sds Status', '/site/*'),
('Sds Status', '/status-sds/*'),
('Super Administrator', '/*'),
('Super Administrator', '/aplikasi/*'),
('Super Administrator', '/data/*'),
('Super Administrator', '/debug/*'),
('Super Administrator', '/debug/default/*'),
('Super Administrator', '/debug/user/*'),
('Super Administrator', '/gii/*'),
('Super Administrator', '/gii/default/*'),
('Super Administrator', '/instansi/*'),
('Super Administrator', '/integrasi-data/*'),
('Super Administrator', '/jenis-instansi/*'),
('Super Administrator', '/kabkota/*'),
('Super Administrator', '/mimin/*'),
('Super Administrator', '/mimin/role/*'),
('Super Administrator', '/mimin/route/*'),
('Super Administrator', '/mimin/user/*'),
('Super Administrator', '/sector/*'),
('Super Administrator', '/setting/update'),
('Super Administrator', '/setting/view'),
('Super Administrator', '/site/*'),
('Super Administrator', '/skpd/*'),
('Super Administrator', '/status-sds/*'),
('Super Administrator', '/tipe-data/*'),
('Super Administrator', '/treemanager/*'),
('Super Administrator', '/treemanager/node/*'),
('Super Administrator', '/user/*');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `instansi`
--

DROP TABLE IF EXISTS `instansi`;
CREATE TABLE IF NOT EXISTS `instansi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_instansi_id` int(11) NOT NULL,
  `nama_instansi` varchar(255) DEFAULT NULL,
  `singkatan_instansi` varchar(255) DEFAULT NULL,
  `alamat_instansi` varchar(255) DEFAULT NULL,
  `telepon_instansi` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_instansi_jenis_instansi` (`jenis_instansi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instansi`
--

INSERT INTO `instansi` (`id`, `jenis_instansi_id`, `nama_instansi`, `singkatan_instansi`, `alamat_instansi`, `telepon_instansi`, `created_at`, `updated_at`) VALUES
(1, 3, 'Kabupaten Banjarnegara', NULL, '', NULL, 1531667859, 1531667859),
(3, 3, 'Kabupaten Banyumas', NULL, '', NULL, 1531667900, 1531667900),
(4, 3, 'Kabupaten Batang', NULL, '', NULL, 1531668273, 1531668273),
(5, 3, 'Kabupaten Blora', NULL, '', NULL, 1531668312, 1531668312),
(7, 3, 'Kabupaten Boyolali', NULL, '', NULL, 1531668439, 1531668439),
(8, 3, 'Kabupaten Brebes', NULL, '', NULL, 1531668456, 1531668456),
(9, 3, 'Kabupaten Demak', NULL, '', NULL, 1531668488, 1531668488),
(10, 3, 'Kabupaten Grobogan', NULL, '', NULL, 1531668511, 1531668511),
(11, 3, 'Kabupaten Jepara', NULL, '', NULL, 1531668529, 1531668529),
(12, 3, 'Kabupaten Karanganyar', NULL, '', NULL, 1531668545, 1531668545),
(13, 3, 'Kabupaten Kebumen', NULL, '', NULL, 1531668561, 1531668561),
(14, 3, 'Kabupaten Kendal', NULL, '', NULL, 1531668579, 1531668579),
(15, 3, 'Kabupaten Klaten', NULL, '', NULL, 1531668596, 1531668596),
(16, 3, 'Kabupaten Kudus', NULL, '', NULL, 1531668614, 1531668614),
(17, 3, 'Kabupaten Magelang', NULL, '', NULL, 1531668631, 1531668631),
(18, 3, 'Kabupaten Pati', NULL, '', NULL, 1531668649, 1531668649),
(19, 3, 'Kabupaten Pekalongan', NULL, '', NULL, 1531668666, 1531668666),
(20, 3, 'Kabupaten Pemalang', NULL, '', NULL, 1531668682, 1531668682),
(21, 3, 'Kabupaten Purbalingga', NULL, '', NULL, 1531668699, 1531668699),
(22, 3, 'Kabupaten Purworejo', NULL, '', NULL, 1531668715, 1531668715),
(23, 3, 'Kabupaten Rembang', NULL, '', NULL, 1531668732, 1531668732),
(24, 3, 'Kabupaten Semarang', NULL, '', NULL, 1531668750, 1531668750),
(25, 3, 'Kabupaten Sragen', NULL, '', NULL, 1531668767, 1531668767),
(26, 3, 'Kabupaten Sukoharjo', NULL, '', NULL, 1531668784, 1531668784),
(27, 3, 'Kabupaten Tegal', NULL, '', NULL, 1531668800, 1531668800),
(28, 3, 'Kabupaten Temanggung', NULL, '', NULL, 1531668816, 1531668816),
(29, 3, 'Kabupaten Wonogiri', NULL, '', NULL, 1531668833, 1531668833),
(30, 3, 'Kabupaten Wonosobo', NULL, '', NULL, 1531668850, 1531668850),
(31, 3, 'Kota Pekalongan', NULL, '', NULL, 1531668869, 1531668869),
(32, 3, 'Kota Salatiga', NULL, '', NULL, 1531668885, 1531668885),
(33, 3, 'Kota Semarang', NULL, '', NULL, 1531668902, 1531668902),
(34, 3, 'Kota Surakarta', NULL, '', NULL, 1531668936, 1531668936),
(35, 3, 'Kota Tegal', NULL, '', NULL, 1531668969, 1531668969),
(36, 3, 'Kota Magelang', NULL, '', NULL, 1531668991, 1531668991),
(37, 3, 'Kabupaten Cilacap', NULL, '', NULL, 1531671088, 1531671088),
(38, 1, 'Dinas Komunikasi dan Informatika', 'Diskominfo', 'Jl. Mentri Supeno I No 2 Semarang', '0248319140', NULL, 1543301990),
(39, 1, 'Rumah Sakit Umum Daerah Tugurejo', 'RSUD Tugurejo', 'Jl. Walisongo KM. 8,5, Tambakaji, Ngaliyan, Tambakaji, Ngaliyan, Kota Semarang, Jawa Tengah 50185', '', NULL, 1543370372),
(40, 1, 'Badan Pengelola Keuangan dan Aset Daerah', 'BPKAD', 'Jl. Taman Menteri Supeno No. 2 ', '0248311174', NULL, 1543302076),
(41, 1, 'Badan Kepegawaian Daerah', 'BKD', 'Jl. Stadion Selatan No. 1, Semarang', '0248318846', NULL, 1543302129),
(42, 1, 'RSUD Prof. Dr. Margono Soekarjo', 'RSUD Margono', 'Jl. Dr. Gumbreg No.1 Purwokerto, Kabupaten Banyumas Provinsi Jawa Tengah, Kode Pos 53146', '0281632708', NULL, 1543302139),
(43, 1, 'RSUD Kelet', 'RSUD Kelet', '', '', NULL, 1543302115),
(44, 1, 'RSUD Soedjarwadi', 'RSUD Sodjarwadi', '', '', NULL, 1543302102),
(45, 1, 'Dinas Pemberdayaan Perempuan, Perlindungan Anak, Pengendalian Penduduk Dan Keluarga Berencana', 'DP3AKB', 'Jl. Pamularsih No. 28 Semarang 50148', '', NULL, 1543302089),
(48, 1, 'Badan Perencanaan dan Pembangunan Daerah', 'BAPPEDA', '', '', 1547556667, 1547556667),
(49, 1, 'Dinas Pekerjaan Umum Sumber Daya Air dan Penataan Ruang', 'DPUSDATARU', 'Jl. Madukoro Blok AA-BB Semarang ', '(024)7608201', 1547685141, 1547685141),
(50, 1, 'Dinas Pekerjaan Umum Bina Marga dan Cipta Karya', 'DPUBINMARCIPKA', 'Jl. Madukoro Blok AA-BB ,', '(024) 7608368', 1547685640, 1547685640),
(51, 1, 'Badan Pengembangan Sumber Daya Manusia Daerah', 'BPSDMD', 'Jl. Setia Budi No.201A, Srondol Kulon, Semarang, Kota Semarang, Jawa Tengah 50263', '024 7473066', 1547685898, 1547685898),
(52, 4, 'Badan Pusat Statistik', 'BPS Provinsi Jawa Tengah', '', '', 1548032700, 1548032700);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_instansi`
--

DROP TABLE IF EXISTS `jenis_instansi`;
CREATE TABLE IF NOT EXISTS `jenis_instansi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_instansi` varchar(80) NOT NULL,
  `keterangan_jenis_instansi` varchar(200) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_instansi`
--

INSERT INTO `jenis_instansi` (`id`, `jenis_instansi`, `keterangan_jenis_instansi`, `created_at`, `updated_at`) VALUES
(1, 'SKPD', 'Satuan Kerja Perangkat Daerah', 0, 0),
(2, 'BUMD', 'Badan Usaha Milih Daerah', 0, 0),
(3, 'Kab/Kota', 'Kabupaten Kota se Jateng', 0, 0),
(4, 'Instansi Vertikal', 'Instansi Vertikal', 1547781215, 1547781215);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1531454941),
('m130524_201442_init', 1531454970),
('m140506_102106_rbac_init', 1531454949),
('m151024_072453_create_route_table', 1531454957),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1531454950),
('m181002_142147_new_tree', 1538500732),
('m230416_200116_tree', 1532859718);

-- --------------------------------------------------------

--
-- Table structure for table `new_tree`
--

DROP TABLE IF EXISTS `new_tree`;
CREATE TABLE IF NOT EXISTS `new_tree` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tree_id` bigint(20) NOT NULL,
  `root` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `lvl` smallint(5) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT 0,
  `child_allowed` tinyint(1) NOT NULL DEFAULT 1,
  `name` varchar(60) NOT NULL,
  `slug_new_tree` varchar(150) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `icon_type` smallint(1) NOT NULL DEFAULT 1,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `selected` tinyint(1) NOT NULL DEFAULT 0,
  `disabled` tinyint(1) NOT NULL DEFAULT 0,
  `readonly` tinyint(1) NOT NULL DEFAULT 0,
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `collapsed` tinyint(1) NOT NULL DEFAULT 0,
  `movable_u` tinyint(1) NOT NULL DEFAULT 1,
  `movable_d` tinyint(1) NOT NULL DEFAULT 1,
  `movable_l` tinyint(1) NOT NULL DEFAULT 1,
  `movable_r` tinyint(1) NOT NULL DEFAULT 1,
  `removable` tinyint(1) NOT NULL DEFAULT 1,
  `removable_all` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `tree_NK1` (`root`),
  KEY `tree_NK2` (`lft`),
  KEY `tree_NK3` (`rgt`),
  KEY `tree_NK4` (`lvl`),
  KEY `tree_NK5` (`active`),
  KEY `FK_new_tree_tree` (`tree_id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `new_tree`
--

INSERT INTO `new_tree` (`id`, `tree_id`, `root`, `lft`, `rgt`, `lvl`, `status`, `child_allowed`, `name`, `slug_new_tree`, `deskripsi`, `icon`, `icon_type`, `active`, `selected`, `disabled`, `readonly`, `visible`, `collapsed`, `movable_u`, `movable_d`, `movable_l`, `movable_r`, `removable`, `removable_all`) VALUES
(1, 1, 1, 1, 2, 0, 0, 0, 'Input Data', 'input-data', 'Input Data ', '', 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0),
(28, 29, 28, 1, 2, 0, 1, 1, 'Tingkat Pendidikan', 'tingkat-pendidikan', 'Data PNS Perdasarkan Pegawai Pendidikan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(35, 38, 35, 1, 2, 0, 1, 1, 'Kejadian Luar Biasa', 'kejadian-luar-biasa', 'Kejadian Luar Biasa di Lingkungan RSUD Tugurejo', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(36, 39, 36, 1, 2, 0, 0, 1, 'Alat Kesehatan', 'alat-kesehatan', 'Ketersediaan Alat Kesehatan di RS Tugurejo', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(39, 48, 39, 1, 2, 0, 0, 1, 'Informasi Tempat Tidur', 'informasi-tempat-tidur', 'Daftar Tempat Tidut', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(40, 43, 40, 1, 2, 0, 0, 1, 'Tenaga Kesehatan', 'tenaga-kesehatan', 'Data Tenaga Kesehatan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(41, 44, 41, 1, 2, 0, 0, 1, 'Ketersediaan Alat Kesehatan', 'ketersediaan-alat-kesehatan', 'Data Alat Kesehatan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(42, 45, 42, 1, 2, 0, 0, 1, 'Informasi Tempat Tidur', 'informasi-tempat-tidur-2', 'Data Informasi Tempat Tidur', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(43, 49, 43, 1, 2, 0, 0, 1, 'Informasi Tempat Tidur', 'informasi-tempat-tidur-3', 'Ketersediaan Tembat Tidur Kosong', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(44, 50, 44, 1, 2, 0, 0, 1, 'Tenaga Kesehatan', 'tenaga-kesehatan-2', 'Data Tenaga Kesehatan di RSUD Kelet', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(45, 51, 45, 1, 6, 0, 1, 1, 'Belanja Bulanan', 'belanja-bulanan-2', '', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(46, 51, 45, 2, 5, 1, 1, 1, 'Bulan', 'bulan-2', '', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(47, 12, 47, 1, 2, 0, 1, 1, 'Pegawai Golongan', 'pegawai-golongan', 'Data Pegawai Berdasar Golongan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(48, 51, 45, 3, 4, 2, 1, 1, 'Jenis', 'jenis', '', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(54, 55, 54, 1, 2, 0, 1, 1, 'Data Waduk', 'data-waduk', 'Data Waduk Jawa Tengah', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(55, 56, 55, 1, 2, 0, 1, 1, 'Indikator Gender dan Anak', 'indikator-gender-dan-anak', 'Indikator Gender dan Anak', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(56, 57, 56, 1, 2, 0, 1, 1, 'Rumah Tangga Miskin', 'rumah-tangga-miskin', 'Sistem Informasi Terpadu Penanggulangan Kemiskinan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(57, 58, 57, 1, 2, 0, 1, 1, 'Rumah Tidak Ber Listrik', 'rumah-tidak-ber-listrik', 'Rumah Tidak Ber Listrik Jateng Kab Kota', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(58, 59, 58, 1, 2, 0, 1, 1, 'Rumah Tidak Layak Huni', 'rumah-tidak-layak-huni', 'Rumah Tidak Layak Huni Kabupaten Kota Jateng', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(59, 60, 59, 1, 2, 0, 1, 1, 'Rumah Tidak Berjamban', 'rumah-tidak-berjamban', 'Rumah Tangga Tidak Berjamban', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(60, 61, 60, 1, 2, 0, 1, 1, 'Desil Kesejahteraan', 'desil-kesejahteraan', 'Desil Kkesejahteraan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(61, 62, 61, 1, 2, 0, 1, 1, 'Lapangan Pekerjaan', 'lapangan-pekerjaan', 'Data Lapangan Pekerjaan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(63, 54, 63, 1, 2, 0, 1, 1, 'Diklat Pegawai', 'diklat-pegawai', 'Jumlah Diklat Pegawai', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(64, 64, 64, 1, 2, 0, 1, 1, 'Jenis Kelamin Pegawai', 'jenis-kelamin-pegawai', 'Data Pegawai Berdasarkan Jenis Kelamin', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(65, 65, 65, 1, 2, 0, 1, 1, 'Indeks Pembangunan Manusia', 'indeks-pembangunan-manusia', 'Indeks Pembangunan Manusia Series', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `parameter_api`
--

DROP TABLE IF EXISTS `parameter_api`;
CREATE TABLE IF NOT EXISTS `parameter_api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `new_tree_id` bigint(20) NOT NULL,
  `parameter_api` varchar(50) NOT NULL,
  `slug_parameter_api` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_parameter_api_new_tree` (`new_tree_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parameter_api`
--

INSERT INTO `parameter_api` (`id`, `new_tree_id`, `parameter_api`, `slug_parameter_api`) VALUES
(15, 1, 'parameter_api', 'parameterapi'),
(18, 35, 'kejadian-luar-biasa', 'kejadian-luar-biasa'),
(19, 36, 'alat-kesehatan', 'alat-kesehatan'),
(22, 40, 'tenaga-kesehatan', 'tenaga-kesehatan'),
(23, 41, 'ketersediaan-alat-kesehatan', 'ketersediaan-alat-kesehatan'),
(24, 42, 'informasi-tempat-tidur', 'informasi-tempat-tidur'),
(25, 39, 'informasi-tempat-tidur', 'informasi-tempat-tidur-2'),
(26, 43, 'informasi-tempat-tidur', 'informasi-tempat-tidur-3'),
(27, 44, 'tenaga-kesehatan', 'tenaga-kesehatan-2'),
(28, 28, 'tingkat-pendidikan', 'tingkat-pendidikan'),
(29, 45, 'belanja-bulanan', 'belanja-bulanan'),
(30, 46, 'bulan', 'bulan-2'),
(31, 47, 'pegawai-golongan', 'pegawai-golongan-3'),
(32, 48, 'jenis', 'jenis'),
(37, 54, 'waduk', 'waduk'),
(38, 55, 'indikator', 'indikator'),
(39, 56, 'rt-miskin-kab-kota', 'rt-miskin-kab-kota'),
(40, 57, 'rumah-tidak-listrik-kab-kota', 'rumah-tidak-listrik-kab-kota'),
(41, 58, 'rtlh', 'rtlh'),
(42, 59, 'rt-tidak-jamban', 'rt-tidak-jamban'),
(43, 60, 'desil-kesejahteraan', 'desil-kesejahteraan'),
(44, 61, 'lapangan-pekerjaan', 'lapangan-pekerjaan'),
(51, 63, 'pegawai-diklat', 'pegawai-diklat'),
(52, 64, 'pegawai-jk', 'pegawai-jk'),
(53, 65, 'ipm-series', 'ipm-series');

-- --------------------------------------------------------

--
-- Table structure for table `route`
--

DROP TABLE IF EXISTS `route`;
CREATE TABLE IF NOT EXISTS `route` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 1,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `route`
--

INSERT INTO `route` (`name`, `alias`, `type`, `status`) VALUES
('/*', '*', '', 1),
('/aplikasi/*', '*', 'aplikasi', 1),
('/aplikasi/create', 'create', 'aplikasi', 1),
('/aplikasi/delete', 'delete', 'aplikasi', 1),
('/aplikasi/index', 'index', 'aplikasi', 1),
('/aplikasi/update', 'update', 'aplikasi', 1),
('/aplikasi/view', 'view', 'aplikasi', 1),
('/data/*', '*', 'data', 1),
('/data/create', 'create', 'data', 1),
('/data/get-data-api', 'get-data-api', 'data', 1),
('/data/index', 'index', 'data', 1),
('/data/new-tree', 'new-tree', 'data', 1),
('/data/update', 'update', 'data', 1),
('/debug/*', '*', 'debug', 1),
('/debug/default/*', '*', 'debug/default', 1),
('/debug/default/db-explain', 'db-explain', 'debug/default', 1),
('/debug/default/download-mail', 'download-mail', 'debug/default', 1),
('/debug/default/index', 'index', 'debug/default', 1),
('/debug/default/toolbar', 'toolbar', 'debug/default', 1),
('/debug/default/view', 'view', 'debug/default', 1),
('/debug/user/*', '*', 'debug/user', 1),
('/debug/user/reset-identity', 'reset-identity', 'debug/user', 1),
('/debug/user/set-identity', 'set-identity', 'debug/user', 1),
('/gii/*', '*', 'gii', 1),
('/gii/default/*', '*', 'gii/default', 1),
('/gii/default/action', 'action', 'gii/default', 1),
('/gii/default/diff', 'diff', 'gii/default', 1),
('/gii/default/index', 'index', 'gii/default', 1),
('/gii/default/preview', 'preview', 'gii/default', 1),
('/gii/default/view', 'view', 'gii/default', 1),
('/instansi/*', '*', 'instansi', 1),
('/instansi/create', 'create', 'instansi', 1),
('/instansi/delete', 'delete', 'instansi', 1),
('/instansi/index', 'index', 'instansi', 1),
('/instansi/update', 'update', 'instansi', 1),
('/instansi/view', 'view', 'instansi', 1),
('/integrasi-data/*', '*', 'integrasi-data', 1),
('/integrasi-data/index', 'index', 'integrasi-data', 1),
('/jenis-instansi/*', '*', 'jenis-instansi', 1),
('/jenis-instansi/create', 'create', 'jenis-instansi', 1),
('/jenis-instansi/delete', 'delete', 'jenis-instansi', 1),
('/jenis-instansi/index', 'index', 'jenis-instansi', 1),
('/jenis-instansi/update', 'update', 'jenis-instansi', 1),
('/jenis-instansi/view', 'view', 'jenis-instansi', 1),
('/kabkota/*', '*', 'kabkota', 1),
('/kabkota/create', 'create', 'kabkota', 1),
('/kabkota/delete', 'delete', 'kabkota', 1),
('/kabkota/index', 'index', 'kabkota', 1),
('/kabkota/update', 'update', 'kabkota', 1),
('/kabkota/view', 'view', 'kabkota', 1),
('/mimin/*', '*', 'mimin', 1),
('/mimin/role/*', '*', 'mimin/role', 1),
('/mimin/role/create', 'create', 'mimin/role', 1),
('/mimin/role/delete', 'delete', 'mimin/role', 1),
('/mimin/role/index', 'index', 'mimin/role', 1),
('/mimin/role/permission', 'permission', 'mimin/role', 1),
('/mimin/role/update', 'update', 'mimin/role', 1),
('/mimin/role/view', 'view', 'mimin/role', 1),
('/mimin/route/*', '*', 'mimin/route', 1),
('/mimin/route/create', 'create', 'mimin/route', 1),
('/mimin/route/delete', 'delete', 'mimin/route', 1),
('/mimin/route/generate', 'generate', 'mimin/route', 1),
('/mimin/route/index', 'index', 'mimin/route', 1),
('/mimin/route/update', 'update', 'mimin/route', 1),
('/mimin/route/view', 'view', 'mimin/route', 1),
('/mimin/user/*', '*', 'mimin/user', 1),
('/mimin/user/create', 'create', 'mimin/user', 1),
('/mimin/user/delete', 'delete', 'mimin/user', 1),
('/mimin/user/index', 'index', 'mimin/user', 1),
('/mimin/user/update', 'update', 'mimin/user', 1),
('/mimin/user/view', 'view', 'mimin/user', 1),
('/setting/*', '*', 'setting', 1),
('/setting/create', 'create', 'setting', 1),
('/setting/delete', 'delete', 'setting', 1),
('/setting/index', 'index', 'setting', 1),
('/setting/update', 'update', 'setting', 1),
('/setting/view', 'view', 'setting', 1),
('/site/*', '*', 'site', 1),
('/site/about', 'about', 'site', 1),
('/site/captcha', 'captcha', 'site', 1),
('/site/contact', 'contact', 'site', 1),
('/site/error', 'error', 'site', 1),
('/site/index', 'index', 'site', 1),
('/site/login', 'login', 'site', 1),
('/site/logout', 'logout', 'site', 1),
('/skpd/*', '*', 'skpd', 1),
('/skpd/create', 'create', 'skpd', 1),
('/skpd/delete', 'delete', 'skpd', 1),
('/skpd/index', 'index', 'skpd', 1),
('/skpd/update', 'update', 'skpd', 1),
('/skpd/view', 'view', 'skpd', 1),
('/status-sds/*', '*', 'status-sds', 1),
('/status-sds/create', 'create', 'status-sds', 1),
('/status-sds/delete', 'delete', 'status-sds', 1),
('/status-sds/index', 'index', 'status-sds', 1),
('/status-sds/update', 'update', 'status-sds', 1),
('/status-sds/view', 'view', 'status-sds', 1),
('/treemanager/*', '*', 'treemanager', 1),
('/treemanager/node/*', '*', 'treemanager/node', 1),
('/treemanager/node/manage', 'manage', 'treemanager/node', 1),
('/treemanager/node/move', 'move', 'treemanager/node', 1),
('/treemanager/node/remove', 'remove', 'treemanager/node', 1),
('/treemanager/node/save', 'save', 'treemanager/node', 1),
('/user/*', '*', 'user', 1),
('/user/create', 'create', 'user', 1),
('/user/delete', 'delete', 'user', 1),
('/user/index', 'index', 'user', 1),
('/user/update', 'update', 'user', 1),
('/user/view', 'view', 'user', 1);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `site_logo` varchar(255) NOT NULL,
  `api_pegawai` varchar(255) NOT NULL,
  `skpd` varchar(255) NOT NULL,
  `nama_kabupaten` varchar(255) NOT NULL,
  `kode_kabupaten` int(2) NOT NULL,
  `admin_setting_data` tinyint(4) NOT NULL DEFAULT 0,
  `version` varchar(50) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `title`, `sub_title`, `site_logo`, `api_pegawai`, `skpd`, `nama_kabupaten`, `kode_kabupaten`, `admin_setting_data`, `version`, `created_at`, `updated_at`) VALUES
(1, 'Single Data System', 'Single Data System Jawa Tengah', 'uploads/web_assets/sds-logo.png', 'http://simpeg.bkd.jatengprov.go.id/webservice/identitas', 'Dinas Komunikasi dan Informatika', 'Pemerintah Provinsi Jawa Tengah', 33, 0, '2.0.0', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `skpd`
--

DROP TABLE IF EXISTS `skpd`;
CREATE TABLE IF NOT EXISTS `skpd` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Kode Master SKPD Auto Increment',
  `kode_skpd` char(4) DEFAULT NULL COMMENT 'Kode SKPD = (2 digit kode Kabkota + 2 digit Kode SKPD)',
  `nama_skpd` varchar(255) DEFAULT NULL COMMENT 'Nama SKPD',
  `akronim_skpd` varchar(50) DEFAULT NULL COMMENT 'Singkatan SKPD',
  `alamat_skpd` varchar(255) DEFAULT NULL COMMENT 'Alamat SKPD',
  `telpon_skpd` varchar(50) DEFAULT NULL COMMENT 'Telpon SKPD',
  `nama_pic_skpd` varchar(50) DEFAULT NULL COMMENT 'Nama Kontak Person SKPD',
  `telepon_pic_skpd` varchar(50) DEFAULT NULL COMMENT 'Telp. Kontak Person SKPD',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status_sds`
--

DROP TABLE IF EXISTS `status_sds`;
CREATE TABLE IF NOT EXISTS `status_sds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instansi_id` int(11) NOT NULL,
  `alamat_situs` varchar(100) DEFAULT NULL,
  `host` varchar(50) DEFAULT NULL,
  `port` varchar(5) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_status_sds_instansi` (`instansi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_sds`
--

INSERT INTO `status_sds` (`id`, `instansi_id`, `alamat_situs`, `host`, `port`, `username`, `password`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 14, 'http://sds.kendalkab.go.id/', 'https://cpanel.kendalkab.go.id', '2083', 'sdskendalkabgo', 'SDSK3nd4l', 'Ok,\r\nsds.kendalkab.go.id\r\n\r\nhttps://cpanel.kendalkab.go.id:2083\r\n\r\n+===================================+\r\n| New Account Info                  |\r\n+===================================+\r\n| Domain: sds.kendalkab.go.id\r\n| Ip: 118.97.18.38 (n)\r\n| HasCgi: y\r\n| UserName: sdskendalkabgo\r\n| PassWord: SDSK3nd4l\r\n| CpanelMod: paper_lantern\r\n| HomeRoot: /home2\r\n| Quota: 1.95 GB\r\n| NameServer1: ns3.telkomhosting.com\r\n| NameServer2: ns4.telkomhosting.com\r\n| NameServer3: \r\n| NameServer4: \r\n| Contact Email: ahmad.hasan.basyari@gmail.com\r\n| Package: SILVER_2_GB\r\n| Feature List: default\r\n| Language: en\r\n+===================================+\r\n...Done', NULL, 1531788273),
(2, 4, 'http://sds.batangkab.go.id/', 'https://sds.batangkab.go.id', '2083', 'sdsbtg', 'ONEdataProject!', 'sds.batangkab.go.id/cpanel\r\nUser sdsbtg\r\nPass ONEdataProject!', NULL, 1547431242),
(3, 32, 'http://sds.salatiga.go.id', '', '', '', '', 'Update\r\n20190122 - Terinstall SDS V2\r\n', NULL, 1531789261),
(4, 34, 'http://sds.surakarta.go.id/', 'sds.surakarta.go.id', '21', 'kelurahansingledata', 'sds123kotasurakarta', 'Ok,\r\nhttp://sds.surakarta.go.id\r\n\r\nDB user : c3singledata\r\nDB pass : singledatasystemkotasurakarta123\r\nDB name	: c3sds\r\n\r\nFTP user : kelurahansingledata\r\nFTP pass : sds123kotasurakarta\r\nakses DB https://sds.surakarta.go.id/phpmyadmin', NULL, 1531788440),
(5, 7, 'http://sds.boyolali.go.id', '', '', '', '', '20190122 - Terinstal SDS V2', NULL, NULL),
(6, 25, 'http://sds.sragenkab.go.id/', 'sds.sragenkab.go.id', '21', 'sds', 'kominfo2018!', 'Ok,\r\n[15:16, 6/27/2018] Munawal Ulfiyanto: Alamat : \r\nhttp://sds.sragenkab.go.id\r\n\r\nKonfigurasi DB mysql :\r\nHost : localhost\r\nUser : sds\r\nPassword : kominfo2018!\r\n\r\nPhpMyAdmin\r\nAlamat : http://222.124.206.55/phpMyAdmin\r\n(Case sensitive)\r\nUser : sds\r\nPassword : kominfo2018!\r\n\r\nFtp\r\nAlamat : ftp://sds.sragenkab.go.id\r\nUser : sds\r\nPassword : kominfo2018!\r\n[15:17, 6/27/2018] Munawal Ulfiyanto: Database name : sds', NULL, 1531788511),
(8, 19, '-', NULL, NULL, NULL, NULL, 'Sedang di siapkan Hosting, Sementara dilakukan Instalasi di Local', NULL, 1532337368),
(9, 26, 'http://sds.sukoharjokab.go.id/', NULL, NULL, NULL, NULL, 'Ok, Akses Hosting belum ada.', NULL, 1531788707),
(10, 13, 'http://sds.kebumenkab.go.id/', NULL, NULL, NULL, NULL, 'Ok, Akses Hosting belum ada.', NULL, 1531789125),
(11, 37, 'http://sds.cilacapkab.go.id/', NULL, NULL, NULL, NULL, 'ok,\r\nFtpnya : sdscilacap_ftp\r\nPass : sdsc1l4c4p18\r\n\r\nDatabasenya username : sdscilacap_u\r\nPass : sdsc1l4c4p18', NULL, 1532337318),
(12, 16, 'http://sds.kuduskab.go.id', '', '', '', '', '20190122 - Terinstal SDS', 1531671330, 1531671330),
(13, 18, 'sds.patikab.go.id', NULL, NULL, NULL, NULL, 'hak akses belum dimintakan', 1531788260, 1531788317),
(14, 10, 'sds.grobogan.go.id', '', '', '', '', 'hak akses belum dimintakan', 1531788383, 1531788383),
(15, 11, 'sds.jepara.go.id', NULL, NULL, NULL, NULL, ' Via sftp\r\nUsername : sds\r\npassword: sds kabupaten jepara\r\nDomain sds.jepara.go.id\r\nPort 50023\r\nDatabase\r\nsds.jepara.go.id/opensesame\r\nDb : sds_db\r\nUser : sds_user\r\nPass : sds_password', 1531788542, 1531788542),
(16, 22, 'sds.purworejokab.go.id', NULL, NULL, NULL, NULL, 'Ok,\r\ncpanel \r\nUsername : purwokab\r\nPass : c2B5bq21lY', 1531788607, 1536051253),
(17, 21, '- sds.purbalinggakab.go.id', '', '', '', '', 'hak akses belum dimintakan\r\n\r\nLaporan\r\n20190122 - Forbidden', 1531788660, 1531788660),
(18, 9, '-', NULL, NULL, NULL, NULL, 'server down, sedang menyurati diskominfo provinsi bidang tik untuk meminta hosting untuk aplikasi sds', 1531789246, 1531789246),
(19, 12, 'http://sds.karanganyarkab.go.id', 'https://sds.karanganyarkab.go.id', '2083', 'sds', 's1n9l3d4t4syst3mj00s', 'ok,\r\ndomain: sds.karanganyarkab.go.id\r\ncwp: sds.karanganyarkab.go.id:2083\r\nuser: sds\r\npass: s1n9l3d4t4syst3mj00s', 1532334911, 1532337082),
(20, 1, 'http://sds.banjarnegarakab.go.id/', 'https://sds.banjarnegarakab.go.id', '2083', 'sdsbna', '56#Multi-Country', 'ok, \r\nCPANEL SINGLE DATA\r\n\r\nsds.banjarnegarakab.go.id/cpanel\r\n\r\nuser : sdsbna\r\npassword : 56#Multi-Country', 1532334971, 1532337029),
(21, 33, 'http://sds.semarangkota.go.id/', 'https://webhost.semarangkota.go.id', '2083', 'sdssemarangkotag', 'sds_semarangkota', 'ok, \r\nsedang mengembangkan (http://sidadu.semarangkota.go.id)\r\nAkses Cpanel\r\nHost : https://webhost.semarangkota.go.id:2083/\r\nUser : sdssemarangkotag\r\nPass :sds_semarangkota', 1532921942, 1532921942),
(22, 20, 'http://sds.pemalangkab.go.id', '180.250.149.28', '5035', 'root', 'kominfopml2018', 'oke,\r\n    - use : root\r\n    - pass: kominfopml2018\r\n#Ssh------------\r\n IP : 180.250.149.28\r\nPort: 5035', 1536629873, 1536629873),
(23, 29, 'http://sds.wonogirikab.go.id', '103.30.181.240', '22', 'kominfo', 'w0n0g1r1kab18', '\r\nini Open Data\r\n[14:47, 9/10/2018] Fandi: Akses ssh ke 103.30.181.240\r\nUser : kominfo\r\nPassword : w0n0g1r1kab18\r\n[14:47, 9/10/2018] Fandi: http://sds.wonogirikab.go.id', 1541087101, 1541087425),
(24, 27, 'http://sds.tegalkab.go.id', '', '', '', '', 'Update\r\n20190122 - Terinstall SDS V2', NULL, NULL),
(25, 23, 'http://sds.rembangkab.go.id', '', '', '', '', 'Update\r\n20190122 - Terinstal SDS', NULL, NULL),
(26, 3, 'http://sds.banyumaskab.go.id', '', '', '', '', 'Update\r\n20190122 - Terinstal SDS', NULL, NULL),
(27, 5, '-', '', '', '', '', '', NULL, NULL),
(28, 8, '-', '', '', '', '', '', NULL, NULL),
(29, 15, '-', '', '', '', '', '', NULL, NULL),
(30, 17, 'http://sds.magelangkab.go.id', '', '', '', '', 'Update\r\n20190122 - Terinstal SDS', NULL, NULL),
(31, 24, '-', '', '', '', '', '', NULL, NULL),
(32, 28, '-', '', '', '', '', '', NULL, NULL),
(33, 30, '-', '', '', '', '', '', NULL, NULL),
(34, 31, 'http://sds.pekalongankab.go.id', '', '', '', '', '', NULL, NULL),
(35, 35, 'http://sds.tegalkab.go.id', '', '', '', '', 'Update\r\n20190122 - Terinstall SDS V2', NULL, NULL),
(36, 36, 'http://sds.magelangkota.go.id', '', '', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `struktur_data`
--

DROP TABLE IF EXISTS `struktur_data`;
CREATE TABLE IF NOT EXISTS `struktur_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameter_api_id` int(11) NOT NULL,
  `nama_struktur` varchar(50) DEFAULT NULL COMMENT 'Nama yang akan di tampilkan',
  `parameter` varchar(50) DEFAULT NULL COMMENT 'Field yang akan dibaca APInya',
  `relasi` enum('0','1') DEFAULT NULL,
  `grafik` enum('x','y','tidak') DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_struktur_data_parameter_api` (`parameter_api_id`)
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `struktur_data`
--

INSERT INTO `struktur_data` (`id`, `parameter_api_id`, `nama_struktur`, `parameter`, `relasi`, `grafik`, `created_at`, `updated_at`) VALUES
(29, 15, 'Label', 'Field Data', '0', 'tidak', NULL, NULL),
(35, 18, 'Jenis Penyakit', 'Jenis_penyakit', '0', 'tidak', NULL, NULL),
(36, 18, 'Tahun Kejadian', 'Tahun', '0', 'tidak', NULL, NULL),
(37, 18, 'Jumlah Pasien', 'Jumlah', '0', 'tidak', NULL, NULL),
(38, 19, 'Jenis Alat', 'jenis_alat', '0', 'tidak', NULL, NULL),
(39, 19, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(40, 19, 'Fungsi', 'fungsi', '0', 'tidak', NULL, NULL),
(60, 22, 'Nama Pegawai', 'nama_pegawai', '0', 'tidak', NULL, NULL),
(61, 22, 'NIP/NIK', 'nip_nik', '0', 'tidak', NULL, NULL),
(63, 22, 'TTL', 'ttl', '0', 'tidak', NULL, NULL),
(64, 22, 'Alamat', 'alamat_pegawai', '0', 'tidak', NULL, NULL),
(65, 22, 'Jenis Tenaga', 'jenis_tenaga', '0', 'tidak', NULL, NULL),
(66, 22, 'Jenis Kelamin', 'jenis_kelamin', '0', 'tidak', NULL, NULL),
(67, 22, 'Status Kepegawaian', 'status_kepegawaian', '0', 'tidak', NULL, NULL),
(68, 22, 'No Hp', 'no_hp_pegawai', '0', 'tidak', NULL, NULL),
(69, 22, 'Pendidikan', 'pendidikan_pegawai', '0', 'tidak', NULL, NULL),
(70, 23, 'Nama Alat', 'nama_alat', '0', 'tidak', NULL, NULL),
(71, 23, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(72, 23, 'Fungsi', 'fungsi', '0', 'tidak', NULL, NULL),
(73, 24, 'Bangsal', 'bangsal', '0', 'tidak', NULL, NULL),
(74, 24, 'Jenis', 'jenis', '0', 'tidak', NULL, NULL),
(75, 24, 'Kelas 3', 'kelas_3', '0', 'tidak', NULL, NULL),
(76, 24, 'Kelas 2', 'kelas_2', '0', 'tidak', NULL, NULL),
(77, 24, 'Kelas 1', 'kelas_1', '0', 'tidak', NULL, NULL),
(78, 24, 'Utama', 'utama', '0', 'tidak', NULL, NULL),
(80, 24, 'VIP A', 'vip_a', '0', 'tidak', NULL, NULL),
(81, 24, 'VIP B', 'vip_b', '0', 'tidak', NULL, NULL),
(82, 24, 'VVIP A', 'vvip_a', '0', 'tidak', NULL, NULL),
(83, 24, 'VVIP B', 'vvip_b', '0', 'tidak', NULL, NULL),
(84, 24, 'Super VVIP', 'super_vvip', '0', 'tidak', NULL, NULL),
(85, 25, 'Bangsal', 'BANGSAL', '0', 'tidak', NULL, NULL),
(86, 25, 'Jenis', 'JENIS', '0', 'tidak', NULL, NULL),
(87, 25, 'Kelas 3', 'KELAS3', '0', 'tidak', NULL, NULL),
(88, 25, 'Kelas 2', 'KELAS2', '0', 'tidak', NULL, NULL),
(89, 25, 'Kelas 1', 'KELAS1', '0', 'tidak', NULL, NULL),
(90, 25, 'Utama', 'UTAMA', '0', 'tidak', NULL, NULL),
(91, 25, 'VIP', 'VIP', '0', 'tidak', NULL, NULL),
(92, 25, 'Intensif', 'INTEMSIF', '0', 'tidak', NULL, NULL),
(93, 25, 'Non Kelas', 'NONKELAS', '0', 'tidak', NULL, NULL),
(94, 26, 'Bangsal', 'BANGSAL', '0', 'x', NULL, NULL),
(95, 26, 'Kelas 1', 'KELAS1', '0', 'y', NULL, NULL),
(96, 26, 'Kelas 2', 'KELAS2', '0', 'tidak', NULL, NULL),
(97, 27, 'Nama Pegawai', 'nama_pegawai', '0', 'tidak', NULL, NULL),
(98, 27, 'NIP/NIK', 'nip_nik', '0', 'tidak', NULL, NULL),
(99, 27, 'Tempat Lahir', 'tempat_lahir', '0', 'tidak', NULL, NULL),
(100, 27, 'Tanggal Lahir', 'tanggal_lahir', '0', 'tidak', NULL, NULL),
(101, 27, 'Jenis Kelamin', 'jenis_kelamin', '0', 'tidak', NULL, NULL),
(102, 27, 'Status Kepegawaian', 'status_kepegawaian', '0', 'tidak', NULL, NULL),
(103, 27, 'No HP', 'no_hp_pegawai', '0', 'tidak', NULL, NULL),
(104, 27, 'Pendidikan', 'pendidikan_pegawai', '0', 'tidak', NULL, NULL),
(105, 26, 'Kelas 3', 'KELAS3', '0', 'tidak', NULL, NULL),
(106, 28, 'Tingkat Pendidikan', 'tkdik', '0', 'x', NULL, NULL),
(107, 28, 'Jumlah', 'jml', '0', 'y', NULL, NULL),
(108, 29, 'Bulan', 'bulan', '1', 'x', NULL, NULL),
(109, 29, 'Jumlah', 'jumlah', '0', 'y', NULL, NULL),
(110, 30, 'Jenis', 'jenis', '1', 'tidak', NULL, NULL),
(111, 30, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(112, 31, 'Golongan', 'gol', '0', 'tidak', NULL, NULL),
(113, 31, 'Jumlah', 'jml', '0', 'tidak', NULL, NULL),
(114, 32, 'No SP2D', 'no_sp2d', '0', 'tidak', NULL, NULL),
(115, 32, 'Tanggal SP2D', 'tanggal', '0', 'tidak', NULL, NULL),
(116, 32, 'Instansi', 'instansi', '0', 'tidak', NULL, NULL),
(117, 32, 'Nominal', 'nominal', '0', 'tidak', NULL, NULL),
(118, 32, 'Uraian', 'uraian', '0', 'tidak', NULL, NULL),
(128, 37, 'Nama Waduk', 'namawaduk', '0', 'tidak', NULL, NULL),
(129, 37, 'Peimpah', 'pelimpahpeil', '0', 'tidak', NULL, NULL),
(130, 38, 'Nama Indikator', 'name', '0', 'tidak', NULL, NULL),
(131, 38, 'Pria', 'pria', '0', 'tidak', NULL, NULL),
(132, 38, 'Wanita', 'wanita', '0', 'tidak', NULL, NULL),
(133, 38, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(134, 38, 'SKPD', 'skpd_name', '0', 'tidak', NULL, NULL),
(135, 38, 'Tahun', 'tahun', '0', 'tidak', NULL, NULL),
(136, 39, 'Kabupaten', 'kab_kota', '0', 'tidak', NULL, NULL),
(137, 39, 'Jumlah', 'jml', '0', 'tidak', NULL, NULL),
(138, 40, 'Kabupaten', 'kabkota', '0', 'tidak', NULL, NULL),
(139, 40, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(140, 41, 'Prioritas', 'prioritas', '0', 'tidak', NULL, NULL),
(141, 41, 'Nama', 'kabkota', '0', 'tidak', NULL, NULL),
(142, 41, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(143, 42, 'Kabupaten', 'kabkota', '0', 'tidak', NULL, NULL),
(144, 42, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(145, 43, 'Desil', 'desil', '0', 'tidak', NULL, NULL),
(146, 43, 'Kabupaten', 'kabkota', '0', 'tidak', NULL, NULL),
(147, 43, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(148, 44, 'Pekerjaan', 'pekerjaan', '0', 'tidak', NULL, NULL),
(149, 44, 'Jumlah', 'jumlah', '0', 'tidak', NULL, NULL),
(150, 51, 'Nama Diklat', 'dikstru', '0', 'tidak', NULL, NULL),
(151, 51, 'Jumlah', 'jml', '0', 'tidak', NULL, NULL),
(152, 52, 'Jenis Kelamin', 'jenkel', '0', 'tidak', NULL, NULL),
(153, 52, 'Jumlah', 'jml', '0', 'tidak', NULL, NULL),
(154, 53, 'Tahun', 'tahun', '0', 'tidak', NULL, NULL),
(155, 53, 'IPM Jateng', 'ipm_jateng', '0', 'tidak', NULL, NULL),
(156, 53, 'IPM Nasional', 'ipm_nas', '0', 'tidak', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tree`
--

DROP TABLE IF EXISTS `tree`;
CREATE TABLE IF NOT EXISTS `tree` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `aplikasi_id` int(11) DEFAULT NULL,
  `root` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `child_allowed` tinyint(4) NOT NULL DEFAULT 0,
  `deskripsi` text NOT NULL,
  `status` tinyint(2) NOT NULL,
  `lvl` smallint(5) NOT NULL,
  `name` varchar(60) NOT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `icon_type` smallint(1) NOT NULL DEFAULT 1,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `selected` tinyint(1) NOT NULL DEFAULT 0,
  `disabled` tinyint(1) NOT NULL DEFAULT 0,
  `readonly` tinyint(1) NOT NULL DEFAULT 0,
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `collapsed` tinyint(1) NOT NULL DEFAULT 0,
  `movable_u` tinyint(1) NOT NULL DEFAULT 1,
  `movable_d` tinyint(1) NOT NULL DEFAULT 1,
  `movable_l` tinyint(1) NOT NULL DEFAULT 1,
  `movable_r` tinyint(1) NOT NULL DEFAULT 1,
  `removable` tinyint(1) NOT NULL DEFAULT 1,
  `removable_all` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `tree_NK1` (`root`),
  KEY `tree_NK2` (`lft`),
  KEY `tree_NK3` (`rgt`),
  KEY `tree_NK4` (`lvl`),
  KEY `tree_NK5` (`active`),
  KEY `FK_tree_aplikasi` (`aplikasi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tree`
--

INSERT INTO `tree` (`id`, `aplikasi_id`, `root`, `lft`, `rgt`, `child_allowed`, `deskripsi`, `status`, `lvl`, `name`, `slug`, `icon`, `icon_type`, `active`, `selected`, `disabled`, `readonly`, `visible`, `collapsed`, `movable_u`, `movable_d`, `movable_l`, `movable_r`, `removable`, `removable_all`) VALUES
(1, NULL, 1, 1, 26, 1, '', 0, 0, 'Kesehatan', 'kesehatan', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(2, NULL, 2, 1, 2, 1, '', 0, 0, 'Lingkungan Hidup', NULL, '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(3, NULL, 3, 1, 2, 1, '', 0, 0, 'Pariwisata', 'pariwisata', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(5, NULL, 5, 1, 2, 1, '', 0, 0, 'Energi', 'energi-2', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(11, NULL, 11, 1, 10, 1, '', 0, 0, 'Tata Kelola Pemerintah', 'tata-kelola-pemerintah-2', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(12, 20, 11, 2, 3, 1, 'Data Pegawai Berdasarkan Golongan', 1, 1, 'Pegawai Golongan', 'pegawai-golongan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(14, NULL, 14, 1, 2, 1, '', 0, 0, 'Lingkungan Hidup', 'lingkungan-hidup', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(15, NULL, 15, 1, 2, 1, '', 0, 0, 'Pariwisata', 'pariwisata-2', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(16, NULL, 16, 1, 16, 1, '', 0, 0, 'Sosial', 'sosial', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(17, NULL, 17, 1, 2, 1, '', 0, 0, 'Energi', 'energi', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(18, NULL, 18, 1, 2, 1, '', 0, 0, 'Pangan', 'pangan', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(19, NULL, 19, 1, 2, 1, '', 0, 0, 'Maritim', 'maritim', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(20, NULL, 20, 1, 4, 1, '', 0, 0, 'Ekonomi', 'ekonomi', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(21, NULL, 21, 1, 2, 1, '', 0, 0, 'Industri', 'industri', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(22, NULL, 22, 1, 4, 1, '', 0, 0, 'Infrastruktur', 'infrastruktur', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(23, NULL, 23, 1, 2, 1, '', 0, 0, 'Pendidikan dan Kebudayaan', 'pendidikan-dan-kebudayaan', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(24, NULL, 24, 1, 2, 1, '', 0, 0, 'Kepemudaan dan Olahraga', 'kepemudaan-dan-olahraga', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(25, NULL, 25, 1, 6, 1, '', 0, 0, 'Indikator Makro', 'indikator-makro', '', 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0),
(29, 20, 11, 4, 5, 1, 'Data Pegawai Berdasarkan Pendidikan', 1, 1, 'Pegawai Pendidikan', 'pegawai-pendidikan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(37, NULL, 1, 2, 7, 1, '', 0, 1, 'RSUD Tugurejo', 'rsud-tugurejo', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(38, 19, 1, 3, 4, 1, 'Kejadian Penyakir Luarbiasa di RSUD Tugurejo', 1, 2, 'Kejadian Luar Biasa', 'kejadian-luar-biasa', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(39, 19, 1, 5, 6, 1, 'Ketersediaan Alat Kesehatan Di Rumah Salit Tugurejo', 1, 2, 'Alat Kesehatan', 'alat-kesehatan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(40, NULL, 1, 8, 15, 1, '', 0, 1, 'RSUD Margono', 'rsud-margono', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(43, 23, 1, 9, 10, 1, 'Data Tenaga Kesehatan RSUD Margono', 1, 2, 'Tenaga Kesehatan', 'tenaga-kesehatan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(44, 23, 1, 11, 12, 1, 'Data Alkes RSUD Margono', 1, 2, 'Alat Kesehatan', 'alat-kesehatan-2', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(45, 23, 1, 13, 14, 1, 'Data Ketersediaann Ruang Tempat Tidur', 1, 2, 'Informasi Tempat Tidur', 'informasi-tempat-tidur', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(46, NULL, 1, 16, 19, 1, '', 0, 1, 'RSUD Sodjarwadi', 'rsud-sodjarwadi', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(47, NULL, 1, 20, 25, 1, '', 0, 1, 'RSUD Kelet', 'rsud-kelet', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(48, 25, 1, 17, 18, 1, 'Data Ketersediaan Tempat Tidur', 1, 2, 'Ketersediaan Tempat Tidur', 'ketersediaan-tempat-tidur', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(49, 24, 1, 21, 22, 1, 'Ketersediaan Tempat Tidur RSUD Kelet', 1, 2, 'Ketersediaan Tempat Tidur', 'ketersediaan-tempat-tidur-2', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(50, 24, 1, 23, 24, 1, 'Data Tenaga Kesehatan', 1, 2, 'Tenaga Kesehatan', 'tenaga-kesehatan-2', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(51, 27, 20, 2, 3, 1, 'Data Belanja Provinsi Jawa Tengah', 1, 1, 'Belanja Bulanan', 'belanja-bulanan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(54, 20, 11, 6, 7, 1, '', 1, 1, 'Diklat Pegawai', 'diklat-pegawai', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(55, 28, 22, 2, 3, 1, '', 1, 1, 'Data Waduk', 'data-waduk', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(56, 26, 16, 2, 3, 1, 'Gender dan Anak', 1, 1, 'Gender dan Anak', 'gender-dan-anak', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(57, 29, 16, 4, 5, 1, 'Sistem Informasi Terpadu Penanggulangan Kemiskinan', 1, 1, 'Rumah Tangga Miskin', 'rumah-tangga-miskin', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(58, 29, 16, 6, 7, 1, 'Data Rumah Tidak Ber Listrik Kab Kota', 1, 1, 'Rumah Tidak Berlistrik', 'rumah-tidak-berlistrik', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(59, 29, 16, 8, 9, 1, 'Rumah Tidak Layak Huni Kabupaten dan Kota Se Jawa Tengah', 1, 1, 'Rumah Tidak Layak Huni', 'rumah-tidak-layak-huni', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(60, 29, 16, 10, 11, 1, 'Rumah Tidak Berjamban', 1, 1, 'Rumah Tidak Berjamban', 'rumah-tidak-berjamban', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(61, 29, 16, 12, 13, 1, '', 1, 1, 'Desil Kesejahteraan', 'desil-kesejahteraan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(62, 29, 16, 14, 15, 1, '', 1, 1, 'Lapangan Kerja', 'lapangan-kerja', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(64, 20, 11, 8, 9, 1, '', 1, 1, 'Pegawai Jenis Kelamin', 'pegawai-jenis-kelamin', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(65, 31, 25, 2, 3, 1, '', 1, 1, 'Indeks Pembangunan Manusia', 'indeks-pembangunan-manusia', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0),
(67, 31, 25, 4, 5, 0, '', 0, 1, 'Kemiskinan', 'kemiskinan', '', 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instansi_id` int(11) NOT NULL,
  `nip` varchar(18) COLLATE utf8_unicode_ci NOT NULL,
  `nama_lengkap` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jabatan` varchar(155) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nip` (`nip`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`),
  KEY `FK_user_instansi` (`instansi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `instansi_id`, `nip`, `nama_lengkap`, `jabatan`, `status`, `password_hash`, `password_reset_token`, `username`, `email`, `auth_key`, `created_at`, `updated_at`) VALUES
(3, 40, '198112142009011004', 'MAHFUD ARDIANTO, S.Kom', 'Pengadministrasi Keuangan', 10, '$2y$13$6hkbImUlS3QWYRqvCfOYC.Y55SUoEC2hRZScuz32Zzt78E/V4T33m', NULL, 'mahfud', 'mahfud@gmail.com', NULL, 1532844132, 1548064889),
(4, 38, '196503171986031009', 'EKO SRI DARMINTO, SH', 'KASI STATISTIK SOSIAL POLITIK HUKUM DAN HAM', 10, '$2y$13$qG4xXhhQMEX1qN18ahJkXe/.y.mVlq0lYqVI89sggI9.ppB.hjuQG', NULL, 'ekosri', 'echoos.09@gmail.com', NULL, 1533172182, 1548064874),
(10, 38, '196304031993111002', 'Ir. ARIEF BOEDIJANTO, M.Si', 'KABID STATISTIK', 10, '$2y$13$V45QPlssyX1kNNm4dnQv9O3wZfymdysahHwI5gIqrUtfUfkvPRH3.', NULL, NULL, '', NULL, 1543369374, 1548064846),
(11, 38, '196506221987031007', 'DADANG SOMANTRI, ATD, MT', 'KEPALA DINAS KOMUNIKASI DAN INFORMATIKA', 10, '$2y$13$ZcjHD67SGV65Ol9VjE6Rxu7zDy1mfubgUrA4TGrUTkFxYOyp0gMta', NULL, NULL, NULL, NULL, 1547793435, 1548064834),
(13, 38, '196712071993032002', 'Dra. RATNA DEWAJATI, MT', 'SEKRETARIS', 10, '$2y$13$ca96RUA5kfx6pgEPQTPQM.3ipcWrZYTC132V.OFxz1A4lQUXOhAUu', NULL, NULL, NULL, NULL, 1548057244, 1548064821),
(14, 38, '196405281994021001', 'Ir. DJOKO SARWONO, M.Si', 'KASI STATISTIK EKONOMI DAN INFRASTRUKTUR', 10, '$2y$13$DSQb6xJqWMjLRBgWucM9POw/qMzCmzZURRL/.N3xXoznRS.Kud64i', NULL, NULL, NULL, NULL, 1548062074, 1548064802),
(15, 38, '198007142011012005', 'ENDAH TRI NUGRAHENI, S.Si', 'Analis Data Dan Informasi', 10, '$2y$13$K05hrWldf2usfzofoHt6NOevahbyNIBrPgkKPTA5Wl70TiBeQQcmi', NULL, NULL, NULL, NULL, 1550042080, 1550042080);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `aplikasi`
--
ALTER TABLE `aplikasi`
  ADD CONSTRAINT `FK_aplikasi_instansi` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_instansi_user`
--
ALTER TABLE `auth_instansi_user`
  ADD CONSTRAINT `FK_auth_instansi_user_instansi` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_auth_instansi_user_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `instansi`
--
ALTER TABLE `instansi`
  ADD CONSTRAINT `FK_instansi_jenis_instansi` FOREIGN KEY (`jenis_instansi_id`) REFERENCES `jenis_instansi` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `new_tree`
--
ALTER TABLE `new_tree`
  ADD CONSTRAINT `FK_new_tree_tree` FOREIGN KEY (`tree_id`) REFERENCES `tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `parameter_api`
--
ALTER TABLE `parameter_api`
  ADD CONSTRAINT `FK_parameter_api_new_tree` FOREIGN KEY (`new_tree_id`) REFERENCES `new_tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `status_sds`
--
ALTER TABLE `status_sds`
  ADD CONSTRAINT `FK_status_sds_instansi` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `struktur_data`
--
ALTER TABLE `struktur_data`
  ADD CONSTRAINT `FK_struktur_data_parameter_api` FOREIGN KEY (`parameter_api_id`) REFERENCES `parameter_api` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tree`
--
ALTER TABLE `tree`
  ADD CONSTRAINT `FK_tree_aplikasi` FOREIGN KEY (`aplikasi_id`) REFERENCES `aplikasi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_user_instansi` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
