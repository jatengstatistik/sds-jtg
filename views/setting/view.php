<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Setting */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-view">

    <h1><?= Html::encode('Site Setting') ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <!-- <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?> -->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'title',
            'sub_title',
            'url_open_data',
            // 'api_pegawai',
            'skpd',
            'nama_kabupaten',
            'kode_kabupaten',
            [
                'attribute' => 'admin_setting_data',
                'format' => 'raw',
                'value' => function ($data) {
                    if ($data->admin_setting_data == 1)
                        return "<span class='label label-danger'>" . 'Active' . "</span>";
                    else
                        return "<span class='label label-primary'>" . 'Disabled' . "</span>";
                }
            ],
            'version',
            // 'created_at',
            // 'updated_at',
        ],
    ]) ?>

</div>
