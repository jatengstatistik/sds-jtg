<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Kabkota */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kabkota-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kode_kab')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipe_kab')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama_kab')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'detail_kab')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ibu_kota')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kepala_kab')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
