<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KabkotaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kabkota-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <!-- <?= $form->field($model, 'id') ?> -->

    <?= $form->field($model, 'kode_kab') ?>

    <?= $form->field($model, 'tipe_kab') ?>

    <?= $form->field($model, 'nama_kab') ?>

    <?= $form->field($model, 'detail_kab') ?>

    <?php // echo $form->field($model, 'ibu_kota') ?>

    <?php // echo $form->field($model, 'kepala_kab') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
