<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JenisInstansiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jenis-instansi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'jenis_instansi') ?>

    <?= $form->field($model, 'keterangan_jenis_instansi') ?>

    <!-- <?= $form->field($model, 'created_at') ?> -->

    <!-- <?= $form->field($model, 'updated_at') ?> -->

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
