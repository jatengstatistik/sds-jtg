<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JenisInstansi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jenis-instansi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jenis_instansi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keterangan_jenis_instansi')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'created_at')->textInput() ?> -->

    <!-- <?= $form->field($model, 'updated_at')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
