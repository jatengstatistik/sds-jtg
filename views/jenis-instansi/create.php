<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\JenisInstansi */

$this->title = Yii::t('app', 'Create Jenis Instansi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Jenis Instansis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenis-instansi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
