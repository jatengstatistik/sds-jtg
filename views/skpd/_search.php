<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SkpdSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="skpd-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'kode_skpd') ?>

    <?= $form->field($model, 'nama_skpd') ?>

    <?= $form->field($model, 'akronim_skpd') ?>

    <?= $form->field($model, 'alamat_skpd') ?>

    <?php // echo $form->field($model, 'telpon_skpd') ?>

    <?php // echo $form->field($model, 'nama_pic_skpd') ?>

    <?php // echo $form->field($model, 'telepon_pic_skpd') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
