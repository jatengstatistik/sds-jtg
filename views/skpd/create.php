<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Skpd */

$this->title = Yii::t('app', 'Create Skpd');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Skpds'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="skpd-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
