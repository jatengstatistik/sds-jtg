<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Skpd */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="skpd-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kode_skpd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama_skpd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'akronim_skpd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamat_skpd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telpon_skpd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama_pic_skpd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telepon_pic_skpd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
