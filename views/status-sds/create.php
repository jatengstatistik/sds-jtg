<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StatusSds */

$this->title = Yii::t('app', 'Create Status Sds');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Status Sds'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="status-sds-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'list_instansi' => $list_instansi,
    ]) ?>

</div>
