<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\StatusSdsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Status SDS');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="status-sds-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Status Sds'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'instansi_id',
            [
                'label' => 'Instansi',
                'attribute' => 'instansi_id',
                'format' => 'raw',
                'value' => function ($data)
                {
                    return Html::a($data->instansi->nama_instansi,['view', 'id' => $data->id]);
                },
            ],
            // 'alamat_situs',
            [
                'label' => 'Alamat Situs',
                'attribute' => 'alamat_situs',
                'format' => 'url',
                'value' => 'alamat_situs',
            ],
            'host',
            'port',
            // [
            //     'label' => 'Keterangan',
            //     'attribute' => 'keterangan',
            //     'format' => 'raw',
            //     'contentOptions' => ['style' => 'width:18%; height:1%; white-space: normal;'],
            //     'value' => 'keterangan',
            // ],
            // 'username',
            // 'password',
            // 'keterangan:ntext',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
