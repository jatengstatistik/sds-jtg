<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StatusSds */

$this->title = Yii::t('app', 'Update Status Sds: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Status Sds'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="status-sds-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'list_instansi' => $list_instansi,
    ]) ?>

</div>
