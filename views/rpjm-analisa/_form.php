<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RpjmAnalisa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rpjm-analisa-form">
<div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
    <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body table-responsive">
            <?php $form = ActiveForm::begin(); ?>

                <!-- <?= $form->field($model, 'indikator_id')->textInput() ?> -->

                <?= $form->field($model, 'deskriptif')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'analisis')->textarea(['rows' => 6]) ?>

                <!-- <?= $form->field($model, 'created_at')->textInput() ?> -->

                <!-- <?= $form->field($model, 'updated_at')->textInput() ?> -->

                <!-- <?= $form->field($model, 'created_by')->textInput() ?> -->

                <!-- <?= $form->field($model, 'updated_by')->textInput() ?> -->

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                    <?= Html::a(Yii::t('app', 'Kembali'), ['/rpjm-indikator/view','id'=>$model->indikator_id], ['class' => 'btn btn-warning']) ?>
                </div>

                <?php ActiveForm::end(); ?>
        </div>
    </div> 

</div>
