<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RpjmAnalisa */

$this->title = Yii::t('app', 'Create Rpjm Analisa');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rpjm Analisas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rpjm-analisa-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
