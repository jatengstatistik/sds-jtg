<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RpjmAnalisa */

$this->title = Yii::t('app', 'Update Rpjm Analisa: {name}', [
    'name' => $model->indikator->indikator,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rpjm Analisas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="rpjm-analisa-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
