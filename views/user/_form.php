<?php

use app\models\Instansi;
use hscstudio\mimin\components\Mimin;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\administrator\models\User */
/* @var $form yii\widgets\ActiveForm */

// $aplikasi = ;
$list_instansi = ArrayHelper::map(Instansi::find()->asArray()->all(), 'id', 'nama_instansi');

?>

<div class="user-form">

    <div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
        <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body table-responsive">

	<?php $form = ActiveForm::begin(); ?>

	<!-- <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?> -->

	<?= $form->field($model, 'nip')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'nama_lengkap')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'jabatan')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'api_key_od')->textInput(['maxlength' => true]) ?>

	<!-- <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?> -->

	<?= $form->field($model, 'instansi_id')->widget(Select2::className(), [
        'data' => $list_instansi,
        'options' => ['placeholder' => 'Pilih'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('Instansi') ?>

	<?= $form->field($model, 'status')->widget(SwitchInput::classname(), [
		'pluginOptions' => [
			'onText' => 'Active',
			'offText' => 'Banned',
		]
	]) ?>

	<?php if (!$model->isNewRecord) { ?>
		<strong> Leave blank if not change password</strong>
		<div class="ui divider"></div>
		<?= $form->field($model, 'new_password') ?>
		<?= $form->field($model, 'repeat_password') ?>
		<?= $form->field($model, 'old_password') ?>
	<?php } ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                <?= ((Mimin::checkRoute($this->context->id.'/index',true))) ? Html::a(Yii::t('app', 'Kembali'), Yii::$app->request->referrer, ['class' => 'btn btn-warning']) : null ?>
            </div>

	<?php ActiveForm::end(); ?>

        </div>
    </div> 

</div>