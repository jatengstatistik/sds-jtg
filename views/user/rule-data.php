<?php

use app\models\Instansi;
use hscstudio\mimin\components\Mimin;
use kartik\select2\Select2;
use kartik\widgets\SwitchInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\administrator\models\User */
/* @var $this yii\web\View */
/* @var $model app\modules\administrator\models\User */
/* @var $form yii\widgets\ActiveForm */

// $this->title = 'Update Rule User Data: ' . ' ' . $model->id;
// $this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
// $this->params['breadcrumbs'][] = 'Rule';
?>
<section class="content-header">
  <h1>
    <?php echo  $this->title = 'Update Rule User Data'; ?>
    <small><?php echo $model->nama_lengkap; ?></small>
  </h1>
  <!-- <ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Forms</a></li>
    <li class="active">General Elements</li>
  </ol> -->
  <?php 
  	$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
	$this->params['breadcrumbs'][] = 'Rule Data';
   ?>
</section>

<section class="content">
<div class="user-rule-data-form">

	<!-- <h1><?= Html::encode($this->title) ?></h1> -->
	<div class="box box-info">
		<div class="box-header with-border">
			<h3 class="box-title"><?= Html::encode($this->title) ?></h3>

			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
			</div>
		<!-- /.box-tools -->
		</div>
		<!-- /.box-header -->
		<div class="box-body" style="">
			<?php $form = ActiveForm::begin([]); ?>
		    <?php
		    echo $form->field($authAssignment, 'item_name')->widget(Select2::classname(), [
		      'data' => $authItems,
		      'options' => [
		        'placeholder' => 'Select role ...',
		        // 'value' => $authAssignment,
		      ],
		      'pluginOptions' => [
		        'allowClear' => true,
		        'multiple' => true,
		      ],
		    ])->label('Role'); ?>

		    <?php
		    echo $form->field($authInstansiUser, 'instansi_id')->widget(Select2::classname(), [
		      // 'value' => $model->instansi->nama_instansi,
		      'data' => $instansi,
		      'options' => [
		        'placeholder' => 'Select instansi ...',
		      ],
		      'pluginOptions' => [
		        'allowClear' => true,
		        'multiple' => true,
		      ],
		    ])->label('Instansi Ampuan'); ?>

		    <div class="form-group">
		        <?= Html::submitButton('Update', [
		            'class' => $authAssignment->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
		            //'data-confirm'=>"Apakah anda yakin akan menyimpan data ini?",
		        ]) ?>
		        <?= ((Mimin::checkRoute($this->context->id.'/index',true))) ? Html::a(Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-warning']) : null ?>
		    </div>
		    <?php ActiveForm::end(); ?>
		</div>
	<!-- /.box-body -->
	</div>
</div>
</section>
