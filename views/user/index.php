<?php

use app\models\Instansi;
use hscstudio\mimin\components\Mimin;
use hscstudio\mimin\models\User;
use kartik\widgets\Select2;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\administrator\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= ((Mimin::checkRoute($this->context->id.'/create',true))) ? Html::a(Yii::t('app', 'Tambah User'), ['create'], ['class' => 'btn btn-success']) : null ?>
    </p>

    <div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
        <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body table-responsive">
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			//'id',
			// 'username',
			//'auth_key',
			//'password_hash',
			//'password_reset_token',
			[
				'label' => 'Nama Lengkap',
				'attribute' => 'nama_lengkap',
				'format' => 'raw',
				'value' => function ($data)
				{
					// $user = new User;
					// return Html::a($user->getPegawai($data->id)->nama,['view', 'id' => $data->id]);
					return Html::a($data->nama_lengkap,['view', 'id' => $data->id]);
				},
			],
			// 'nip',
			[
				'label'=> 'Nip',
				'attribute' => 'nip',
				'format' => 'raw',
				'value'=>function ($data) {
		            return $data->nip;
		        },
			],
			[
				'label' => 'Instansi',
				'attribute' => 'instansi_id',
				'format' => 'raw',
				'filter' => Select2::widget([
					'model' => $searchModel,
					'attribute' => 'instansi_id',
					'data' => ArrayHelper::map(Instansi::find()->asArray()->all(), 'id', 'nama_instansi'),
					'options' => ['placeholder' => 'Pilih ...'],
					'pluginOptions' => [
					    'allowClear' => true
					],
				]),
				'value' => 'instansi.nama_instansi',
			],
			// 'email:email',
			[
				'attribute' => 'roles',
				'format' => 'raw',
				// 'filter' => Select2::widget([
				// 	'model' => $searchModel,
				// 	'attribute' => 'instansi_id',
				// 	'data' => ArrayHelper::map(Instansi::find()->asArray()->all(), 'id', 'nama_instansi'),
				// 	'options' => ['placeholder' => 'Pilih ...'],
				// 	'pluginOptions' => [
				// 	    'allowClear' => true
				// 	],
				// ]),
				'value' => function ($data) {
					$roles = [];
					foreach ($data->roles as $role) {
						$roles[] = $role->item_name;
					}
					// return Html::a(implode(', ', $roles), ['view', 'id' => $data->id]);
					return implode(', ', $roles);
				}
			],
			[
				'attribute' => 'status',
				'format' => 'raw',
				'filter' => Select2::widget([
					'model' => $searchModel,
					'attribute' => 'status',
					'data' => ['0'=>'Banned','10'=>'Active'],
					'options' => ['placeholder' => 'Pilih ...'],
					'pluginOptions' => [
					    'allowClear' => true
					],
				]),
				'options' => [
					'width' => '80px',
				],
				'value' => function ($data) {
					if ($data->status == 10)
						return "<span class='label label-primary'>" . 'Active' . "</span>";
					else
						return "<span class='label label-danger'>" . 'Banned' . "</span>";
				}
			],
			[
				'attribute' => 'created_at',
				'format' => ['date', 'php:d M Y H:i:s'],
				'options' => [
					'width' => '120px',
				],
			],
			[
				'attribute' => 'updated_at',
				'format' => ['date', 'php:d M Y H:i:s'],
				'options' => [
					'width' => '120px',
				],
			],
			[
                'class' => 'yii\grid\ActionColumn',
                'template' => '{rule-data} {view} {update} {delete}',
                'buttons' => [
                    'rule-data' => function ($url,$model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-lock"></span>',
                            // 'format' => 'raw',
                            ['rule-data', 'id' => $model->id],
                            [
                                'title' => 'Rule Data',
                                // 'data-pjax' => '0',
                                // 'target' => '_blank',
                            ]
                        );
                    },
                ],
            ],
		],
	]); ?>
        </div>
    </div> 

    
    <?php Pjax::end(); ?>
</div>