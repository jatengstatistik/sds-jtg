<?php

use hscstudio\mimin\components\Mimin;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\administrator\models\User */

$this->title = $model->nama_lengkap;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">
    <p>
        <?= ((Mimin::checkRoute($this->context->id.'/update',true))) ? Html::a(Yii::t('app', 'Update'), ['update', 'id' => Yii::$app->user->id], ['class' => 'btn btn-primary']) : null ?>
    </p>

    <div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
        <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body table-responsive">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            // [
            //     'label' => 'Nama Pegawai',
            //     'value' => $pegawai->nama,
            // ],
            'nama_lengkap',
            'nip',
            'jabatan',
            'instansi.nama_instansi',
            [
                'attribute' => 'roles',
                'format' => 'raw',
                'value' => function ($data) {
                    $roles = [];
                    foreach ($data->roles as $role) {
                        $roles[] = $role->item_name;
                    }
                    // return Html::a(implode(', ', $roles), ['view', 'id' => $data->id]);
                    return implode(', ', $roles);
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'options' => [
                    'width' => '80px',
                ],
                'value' => function ($data) {
                    if ($data->status == 10)
                        return "<span class='label label-primary'>" . 'Active' . "</span>";
                    else
                        return "<span class='label label-danger'>" . 'Banned' . "</span>";
                }
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d M Y H:i:s'],
                'options' => [
                    'width' => '120px',
                ],
            ],
            [
                'attribute' => 'updated_at',
                'format' => ['date', 'php:d M Y H:i:s'],
                'options' => [
                    'width' => '120px',
                ],
            ],
            'api_key_od',
            // [
            //     'label' => 'Jabatan',
            //     'value' => $pegawai->jabatan,
            // ],
            // [
            //     'label' => 'Instansi',
            //     'value' => $pegawai->instansi,
            // ],
            // 'username',
            // 'auth_key',
            // 'password_hash',
            // 'password_reset_token',
            // 'email:email',
            // 'status',
            // 'created_at',
            // 'updated_at',
        ],
    ]) ?>

         </div>
    </div> 

</div>
