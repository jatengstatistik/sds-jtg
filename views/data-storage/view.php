<?php

use miloschuman\highcharts\Highcharts;
use miloschuman\highcharts\SeriesDataHelper;
use sjaakp\gcharts\BarChart;
use sjaakp\gcharts\PieChart;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\DataStorage */

$this->title = $model->nama_data;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php Pjax::begin(); ?>
<div class="data-storage-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-info']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'nama_data',
            'deskripsi_data:ntext',
            'aplikasi.nama_aplikasi',
            // 'file',
            // 'analisa:ntext',
            // 'created_at',
            // 'updated_at',
        ],
    ]) ?>

</div>

<div class="data-storage-index">

    
    <?php 
        // echo '<pre>';
        // print_r($dataProvider);
        // echo '</pre>';

        // array_shift($attribute);
     ?>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $attribute,
    ]); ?>

    

</div>

<div class="data-storage-index">    
    <?php 
      // $cetakData = new SeriesDataHelper($dataProvider, ['0:string', 1, 2, 3, 4]);

        array_shift($attribute);
        unset($attribute[0]);
        $key=0;
        foreach ($attribute as $value) {
            $key++;
            // $categories[] = $value['label'];
            $series[] = [
              // 'type' => 'spline',
              'type' => 'column',
              'name' => $value['label'],
              'data' => array_column($dataProvider->allModels, $key),
            ];
        }
        // array_shift($categories);

        // echo '<pre>';
        // print_r($dataProvider->allModels);
        // echo '<br>';
        // print_r(array_column($dataProvider->allModels, '0'));
        // echo json_encode($dataProvider);
        // print_r($categories);
        // print_r($series);
        // echo '</pre>';
     ?>
     <?php 

     echo Highcharts::widget([
           'options' => [
              'title' => ['text' => $this->title],
              'xAxis' => [
                 'categories' => array_column($dataProvider->allModels, '0'),
                 
              ],
              // 'yAxis' => [
              //    'title' => ['text' => 'Fruit eaten']
              // ],
              'series' => $series,
            ]
        ]);

      ?>

    <?php
    // BarChart::widget([
    // 'height' => '400px',
    // 'dataProvider' => $dataProvider,
    // 'columns' => $attribute,
    //     'options' => [
    //         'title' => $this->title
    //         ],
    // ]) 
    ?>

    

</div>
<br>
<div class="data-storage-index"> 
  <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            // 'aplikasi_id',
            // 'nama_data',
            // 'file',
            'informasi:ntext',
            'analisis:ntext',
            // 'created_at',
            // 'updated_at',
        ],
    ]) ?>
</div>
<?php Pjax::end(); ?>