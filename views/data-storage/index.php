<?php

use hscstudio\mimin\components\Mimin;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DataStorageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Data');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-storage-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Data'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'aplikasi.nama_aplikasi',

            // 'nama_data',
            [
                'label' => 'Judul Data',
                'attribute' => 'nama_data',
                // 'value' => 'aplikasi.nama_aplikasi',
                'format' => 'raw',
                'value' => function ($data){
                    return Html::a($data->nama_data,['view', 'id' => $data->id]);
                },
            ],
            'deskripsi_data:ntext',
            // [
            //     'label' => 'Aplikasi',
            //     'attribute' => 'aplikasi_id',
            //     'format' => 'raw',
            //     'value' => 'aplikasi.nama_aplikasi',
            // ],
            // 'file',
            // 'created_by',
            [
                'label' => 'Dibuat',
                'attribute' => 'created_by',
                'value' => 'dibuat.nama_lengkap',
                // 'format' => 'raw',
                // 'value' => function ($data){
                    // return Html::a($data->nama_data,['view', 'id' => $data->id]);
                // },   
            ],
            'created_at:datetime',
            //'updated_at',

            // ['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => Mimin::filterActionColumn([
                  'view','update','delete'
                ],$this->context->route),
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
