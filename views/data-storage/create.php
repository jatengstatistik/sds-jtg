<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DataStorage */

$this->title = Yii::t('app', 'Create Data');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Storages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-storage-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'list_aplikasi' => $list_aplikasi,
        'list_instansi' => $list_instansi,
    ]) ?>

</div>
