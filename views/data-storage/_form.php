<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DataStorage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-storage-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <!-- <?= $form->field($model, 'aplikasi_id')->textInput() ?> -->

    <?= $form->field($model, 'aplikasi_id')->widget(Select2::className(), [
        'data' => $list_aplikasi,
        'options' => ['placeholder' => 'Pilih'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'instansi_id')->widget(Select2::className(), [
        'data' => $list_instansi,
        'options' => ['placeholder' => 'Pilih'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'nama_data')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deskripsi_data')->textarea(['rows' => 6]) ?>
    
    <?= $form->field($model, 'file')->fileInput() ?>

    <?= $form->field($model, 'informasi')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'analisis')->textarea(['rows' => 6]) ?>

    <!-- <?= $form->field($model, 'created_at')->textInput() ?> -->

    <!-- <?= $form->field($model, 'updated_at')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
