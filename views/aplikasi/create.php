<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Aplikasi */

$this->title = Yii::t('app', 'Create Aplikasi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Aplikasis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aplikasi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'list_instansi' => $list_instansi
    ]) ?>

</div>
