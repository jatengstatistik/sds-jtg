<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Aplikasi */

$this->title = Yii::t('app', 'Update Aplikasi: ' . $model->id, [
    'nameAttribute' => '' . $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Aplikasis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="aplikasi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'list_instansi' => $list_instansi,
    ]) ?>

</div>
