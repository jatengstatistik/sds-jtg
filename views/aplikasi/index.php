<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use hscstudio\mimin\components\Mimin;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\AplikasiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Aplikasi');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aplikasi-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= ((Mimin::checkRoute($this->context->id.'/create')))? Html::a(Yii::t('app', 'Create Aplikasi'), ['create'], ['class' => 'btn btn-success']) : null ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'tree_id',
            // 'nama_aplikasi',
            [
                'label' => 'Nama Aplikasi',
                'attribute' => 'nama_aplikasi',
                'format' => 'raw',
                'value' => function ($data){
                    return Html::a($data->nama_aplikasi,['view', 'id' => $data->id]);
                },
            ],
            'url_aplikasi',
            // 'slug_nama_aplikasi',
            'username',
            //'password',
            //'token',
            [
                'label' => 'Instansi',
                'attribute' => 'instansi_id',
                'value' => 'instansi.nama_instansi',
            ],
            //'created_at',
            //'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => Mimin::filterActionColumn([
                  'view','update','delete'
                ],$this->context->route),
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
