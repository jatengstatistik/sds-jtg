<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Aplikasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="aplikasi-form">

    <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1-1" data-toggle="tab" aria-expanded="true">Aplikasi</a></li>
              <li class=""><a href="#tab_2-2" data-toggle="tab" aria-expanded="false">Autentifikasi</a></li>
              <li class=""><a href="#tab_3-2" data-toggle="tab" aria-expanded="false">Data Formating</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1-1">
                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'nama_aplikasi')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'instansi_id')->widget(Select2::className(), [
                    'data' => $list_instansi,
                    'options' => ['placeholder' => 'Pilih'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>

                <?php //$form->field($model, 'created_at')->textInput() ?>

                <?php //$form->field($model, 'updated_at')->textInput() ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2-2">
                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'url_aplikasi')->textInput(['maxlength' => true]) ?>

                <?php //$form->field($model, 'slug_nama_aplikasi')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'token')->textInput(['maxlength' => true]) ?>

                <?php //$form->field($model, 'created_at')->textInput() ?>

                <?php //$form->field($model, 'updated_at')->textInput() ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3-2">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                It has survived not only five centuries, but also the leap into electronic typesetting,
                remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                sheets containing Lorem Ipsum passages, and more recently with desktop publishing software
                like Aldus PageMaker including versions of Lorem Ipsum.
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>

</div>
