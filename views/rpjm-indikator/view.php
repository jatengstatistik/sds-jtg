<?php

use hscstudio\mimin\components\Mimin;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\RpjmIndikator */

$this->title = $model->indikator;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rpjm Indikators'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<br><br>
<?php Pjax::begin(); ?>
<div class="data-dasar-action">
    <p>
        <?= ((Mimin::checkRoute($this->context->id.'/delete',true))) ? Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Menghapus indikator ini akan menghapus data capaian beserta analisa, apakah anda setuju...?'),
                'method' => 'post',
            ],
        ]) : null ?>
        <?= ((Mimin::checkRoute('rpjm-analisa/update',true))) ? Html::a(Yii::t('app', 'Update Analisis'), ['rpjm-analisa/update', 'id' => $modelAnalisa->id], ['class' => 'btn btn-primary']) : null ?>
        <?= ((Mimin::checkRoute($this->context->id.'/index',true))) ? Html::a(Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-warning']) : null ?>
    </p>
</div>

<div class="indikator-view">

    <?= $this->render('_detail', [
            'model' => $model,
        ]); ?>

</div>

<div class="capaian-indikator-grid">

    <?= $this->render('_grid-capaian', [
            'searchModelCapaian' => $searchModelCapaian,
            'dataProviderCapaian' => $dataProviderCapaian,
        ]); ?>

</div>

<div class="capaian-indikator-chart">

    <?= $this->render('_chart-capaian', [
            'searchModelCapaian' => $searchModelCapaian,
            'dataProviderCapaian' => $dataProviderCapaian,
        ]); ?>

</div>


<div class="capaian-indikator-analisa">

    <?= $this->render('_analisa', [
            'modelAnalisa' => $modelAnalisa,
        ]); ?>

</div>

<?php Pjax::end(); ?>