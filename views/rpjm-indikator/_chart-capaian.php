<?php 

use dosamigos\chartjs\ChartJs;
use miloschuman\highcharts\Highcharts;
use miloschuman\highcharts\SeriesDataHelper;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Grafik');

?>

<div class="rpjm-capaian-chart">
  <?php 
    $attribute = array_slice(array_keys($dataProviderCapaian->getModels()[0]['attributes']), 3,3);
    foreach ($attribute as $value) {
        $datasets[] = [
          'label' => ucwords(str_replace('_', ' ', $value)),
          'data' => array_column($dataProviderCapaian->getModels(), $value),
          'backgroundColor' => "rgba(".$r= rand(0,255).",".$g= rand(0,255).",".$b= rand(0,255).",".$opacity=0.8.")",
          'borderColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
          'pointBackgroundColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
          'pointBorderColor' => "#fff",
          'pointHoverBackgroundColor' => "#fff",
          'pointHoverBorderColor' => "rgba(".$r.",".$g.",".$b.",".$opacity.")",
        ];
    }

    // echo '<pre>';
    // print_r($attribute);
    // echo '<br>';
    // print_r($series);
    // echo '<pre>';
   ?>

   <div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
        <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body table-responsive">
          <?= ChartJs::widget([
            'type' => 'bar',
            // 'options' => [
            //     'height' => 400,
            //     'width' => 400
            // ],
            'data' => [
                'labels' => array_column($dataProviderCapaian->getModels(), 'tahun_data'),
                'datasets' => $datasets,
            ]
        ]); ?>
        </div>
    </div> 

</div>