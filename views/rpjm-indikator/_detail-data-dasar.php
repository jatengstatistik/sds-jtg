<?php 

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;


 ?>
<div class="data-dasar-view">
    <!-- <?php Pjax::begin(); ?> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p>
        <?= Html::a(Yii::t('app', 'Create Data Dasar'), ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->

<div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
    <div class="box-header with-border">
      <h3 class="box-title"><?= Html::encode('Isi File '.$detailProviderDataDasar->file_name.' - Instansi '.$detailProviderDataDasar->organisasi->nama_organisasi.' - Urusan '.$detailProviderDataDasar->urusan->nama_urusan.' - Tahun '.$detailProviderDataDasar->tahun_data) ?></h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $detailProviderDataDasar->getDataProvider($detailProviderDataDasar->getDataRpjm()),
        // 'filterModel' => $searchModelData,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'indikator',
            'satuan',
            'rkpd',
            'dppa',
            'realisasi',
            [
                'label' => 'Tambah Data',
                // 'attribute' => 'updated_by',
                'format' => 'raw',
                'value' => function ($data) use ($detailProviderDataDasar){
                   return Html::a(Yii::t('app', 'Tambah'), ['create'], [
                        'class' => 'btn btn-warning',
                        'data' => [
                            'confirm' => Yii::t('app', 'Tambah Data Indikator...?'),
                            'method' => 'post',
                            'params' => [
                                'organisasi_id' => $detailProviderDataDasar['organisasi_id'],
                                'urusan_id' => $detailProviderDataDasar['urusan_id'],
                                'indikator' => $data['indikator'],
                                'satuan' => $data['satuan'],
                                'tahun_data' => $detailProviderDataDasar['tahun_data'],
                                'target_rkpd' => $data['rkpd'],
                                'target_dppa' => $data['dppa'],
                                'capaian' => $data['realisasi'],
                            ],
                        ],
                    ]);
                    // return Html::a('Tambah',['xxxx', 
                    //     'indikator' => $data['indikator'],
                    //     'satuan' => $data['satuan'],
                    //     'rkpd' => $data['rkpd'],
                    //     'dppa' => $data['dppa'],
                    //     'realisasi' => $data['realisasi'],
                    // ]);
                    // return $data->updatedBy->nama_lengkap;
                },
            ],

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>
    <!-- <?php Pjax::end(); ?> -->
</div>