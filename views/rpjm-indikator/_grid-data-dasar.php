<?php 

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;


 ?>
<div class="data-dasar-index">
    <!-- <?php Pjax::begin(); ?> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p>
        <?= Html::a(Yii::t('app', 'Create Data Dasar'), ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->

<div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
    <div class="box-header with-border">
      <h3 class="box-title"><?= Html::encode('Data E-Controlling RPJM') ?></h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body table-responsive">
        <?= GridView::widget([
        'dataProvider' => $dataProviderDataDasar,
        'filterModel' => $searchModelDataDasar,
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                // 'id',
                [
                    'label' => 'Nama File',
                    'attribute' => 'file_name',
                    'format' => 'raw',
                    'value' => function ($data){
                        return Html::a($data->file_name,['create', 'id' => $data->id]);
                        // return $data->createdBy->nama_lengkap;
                    },
                ],
                // 'organisasi_id',
                [
                    'label' => 'Organisasi',
                    'attribute' => 'nama_organisasi',
                    'value' => 'organisasi.nama_organisasi',
                    // 'format' => 'raw',
                    // 'value' => function ($data){
                    //     return Html::a($data->file_name,['view', 'id' => $data->id]);
                    //     // return $data->createdBy->nama_lengkap;
                    // },
                ],
                // 'urusan_id',
                [
                    'label' => 'Urusan',
                    'attribute' => 'nama_urusan',
                    'value' => 'urusan.nama_urusan',
                    // 'format' => 'raw',
                    // 'value' => function ($data){
                    //     return Html::a($data->file_name,['view', 'id' => $data->id]);
                    //     // return $data->createdBy->nama_lengkap;
                    // },
                ],
                'tahun_data',
                'created_at:datetime',

                // ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
    <!-- <?php Pjax::end(); ?> -->
</div>