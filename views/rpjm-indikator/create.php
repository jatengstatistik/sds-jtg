<?php

use hscstudio\mimin\components\Mimin;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\RpjmIndikator */

$this->title = Yii::t('app', 'Create Rpjm Indikator');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rpjm Indikators'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- <div class="rpjm-indikator-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div> -->
<?php Pjax::begin(); ?>
<p>
    <?= ((Mimin::checkRoute($this->context->id.'/index',true))) ? Html::a(Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-warning']) : null ?>
</p>
<div class="data-dasar-grid">

    <?= $this->render('_grid-data-dasar', [
            'searchModelDataDasar' => $searchModelDataDasar,
            'dataProviderDataDasar' => $dataProviderDataDasar,
        ]); ?>

</div>

<div class="data-dasar-detail">

    <?= $this->render('_detail-data-dasar', [
            'detailProviderDataDasar' => $detailProviderDataDasar,
        ]); ?>

</div>
<?php Pjax::end(); ?>
