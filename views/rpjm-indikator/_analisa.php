<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DataDasar */

$this->title = Yii::t('app', 'Analisa Indikator Capaian');
?>
<div class="analisa-capaian-indikator">

    <div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
        <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body table-responsive">
            <?= DetailView::widget([
		        'model' => $modelAnalisa,
		        'attributes' => [
		            'deskriptif',
		            'analisis',
		        ],
		    ]) ?>
        </div>
    </div> 

</div>