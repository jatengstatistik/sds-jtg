<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DataDasar */

$this->title = Yii::t('app', $model->indikator);
?>
<div class="data-dasar-view">
    <div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
        <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body table-responsive">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'id',
                    'indikator',
                    'organisasi.nama_organisasi',
                    'urusan.nama_urusan',
                    // 'slug_indikator',
                    'satuan',
                    'created_at:datetime',
                    'updated_at:datetime',
                    [
                        'label' => 'Dibuat Oleh',
                        'attribute' => 'dibuat',
                        // 'value' => 'dibuat.nama_lengkap',
                        'format' => 'raw',
                        'value' => function ($data){
                            // return Html::a($data->file_name,['view', 'id' => $data->id]);
                            return $data->createdBy->nama_lengkap;
                        },
                    ],
                    [
                        'label' => 'Diupdate Oleh',
                        'attribute' => 'diupdate',
                        // 'value' => 'diupdate.nama_lengkap',
                        'format' => 'raw',
                        'value' => function ($data){
                            // return Html::a($data->file_name,['view', 'id' => $data->id]);
                            return $data->updatedBy->nama_lengkap;
                        },
                    ],
                ],
            ]) ?>
        </div>
    </div>     

</div>