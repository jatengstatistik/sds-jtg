<?php

use hscstudio\mimin\components\Mimin;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\RpjmIndikatorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Analisa Indikator RPJM 2019 - 2023');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php Pjax::begin(); ?>
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<p>
    <?= ((Mimin::checkRoute($this->context->id.'/create',true))) ? Html::a(Yii::t('app', 'Tambah Analisa Indikator'), ['create'], ['class' => 'btn btn-success']) : null ?>
</p>
<div class="rpjm-indikator-index">
    <div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
        <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body table-responsive">
        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'organisasi_id',
            // 'indikator',
            [
                'label' => 'Indikator',
                'attribute' => 'indikator',
                // 'value' => 'organisasi.nama_organisasi',
                'format' => 'raw',
                'value' => function ($data){
                    return Html::a($data->indikator,['view', 'id' => $data->id]);
                    // return $data->createdBy->nama_lengkap;
                },
            ],
            [
                'label' => 'Organisasi',
                'attribute' => 'nama_organisasi',
                'value' => 'organisasi.nama_organisasi',
                // 'format' => 'raw',
                // 'value' => function ($data){
                //     return Html::a($data->file_name,['view', 'id' => $data->id]);
                //     // return $data->createdBy->nama_lengkap;
                // },
            ],
            // 'urusan_id',
            [
                'label' => 'Urusan',
                'attribute' => 'nama_urusan',
                'value' => 'urusan.nama_urusan',
                // 'format' => 'raw',
                // 'value' => function ($data){
                //     return Html::a($data->file_name,['view', 'id' => $data->id]);
                //     // return $data->createdBy->nama_lengkap;
                // },
            ],
            // 'slug_indikator',
            //'satuan',
            [
                'label' => 'Dibuat Oleh',
                'attribute' => 'dibuat',
                'value' => 'createdBy.nama_lengkap',
                // 'format' => 'raw',
                // 'value' => function ($data){
                //     return Html::a($data->file_name,['view', 'id' => $data->id]);
                //     // return $data->createdBy->nama_lengkap;
                // },
            ],
            // 'dibuat.nama_lengkap',
            'created_at:datetime',
            //'updated_at',
            // 'created_by',
            //'updated_by',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
    </div>   
    <?php Pjax::end(); ?>
</div>
