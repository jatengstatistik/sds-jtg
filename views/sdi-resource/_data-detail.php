<?php 

use yii\helpers\Html;
use yii\widgets\DetailView;

 ?>
<div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
<div class="box-header with-border">
  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
  <div class="box-tools pull-right">
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
  </div>
</div>
<div class="box-body table-responsive">
    <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                // 'id',
                'title',
                'year',
                // 'instansi_id',
                // [
                //     'attribute' => 'instansi_id',
                //     'format' => 'raw',
                //     'value' => function($data){
                //         return $data->instansi->nama_instansi.' - ('.$data->instansi->singkatan_instansi.')';
                //     },
                // ],
                // 'api_client_id',
                // 'uploaded_file_id',
                // 'created_by',
                // 'updated_by',
                [
                    'attribute' => 'created_by',
                    'format' => 'raw',
                    'value' => function($data){
                        return $data->userCreated->nama_lengkap;
                    },
                ],
                [
                    'attribute' => 'updated_by',
                    'format' => 'raw',
                    'value' => function($data){
                        return $data->userUpdated->nama_lengkap;
                    },
                ],
                'created_at:datetime',
                'updated_at:datetime',
                // 'deleted_by',
                // 'lock',
            ],
        ]) ?>
</div>
</div>