<?php

use hscstudio\mimin\components\Mimin;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\ArsipKwitansi */

$this->title = Yii::t('app', 'Tambah Data ( Api Data )');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Arsip Kwitansis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="arsip-kwitansi-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <div class="form-group">
      <?= ((Mimin::checkRoute($this->context->id.'/index',true))) ? Html::a(Yii::t('app', 'Back'), ['index'], ['class' => 'btn btn-warning']) : null ?>
    	<?php Modal::begin([
              'header'        => '<h2>Panduan Tambah Data (API Data)</h2>',
              'toggleButton'  => [
                                'label' => 'Panduan',
                                'class' => 'btn btn-info',
                              ],
          ]);
          echo '
          <ol>
            <li>Judul Harus di isi</li>
            <li>Tahun Harus di isi</li>
            <li>Instansi Harus di isi</li>
            <li>Url Harus di isi</li>
            <li>Method Harus di isi</li>
            <li>Label pada Query Pertama Harus Limit</li>
            <li>Label pada Query Kedua Harus Offset</li>
            <li>Label pada Body Pertama Harus Total</li>
          </ol> 
          ';
          Modal::end();
      ?>
    </div>

      <div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
        <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
      <div class="box-body table-responsive">
	    <?= $this->render('_api-form', [
	        'model' => $model,
	        'modelApiClient' => $modelApiClient,
	        'modelApiHeader' => $modelApiHeader,
	        'modelApiQuery' => $modelApiQuery,
	        'modelApiBody' => $modelApiBody,
	        'modelApiData' => $modelApiData,
	    ]) ?>
        </div>
    </div>

</div>
