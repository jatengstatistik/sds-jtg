<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\SdiResource */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sdi-resource-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- <?= $form->field($model, 'sdi_data_id')->textInput() ?> -->

    <!-- <?= $form->field($model, 'api_client_id')->textInput() ?> -->

    <!-- <?= $form->field($model, 'upload_file_id')->textInput() ?> -->

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?> -->

    <!-- <?= $form->field($model, 'year')->textInput() ?> -->

    <?= $form->field($model, 'year')->widget(Select2::className(), [
      'data' => $model->listTahun,
      'options' => ['placeholder' => 'Pilih Tahun'],
      'pluginOptions' => [
          'allowClear' => true
      ],
    ]) ?>

    <!-- <?= $form->field($model, 'created_at')->textInput() ?> -->

    <!-- <?= $form->field($model, 'updated_at')->textInput() ?> -->

    <!-- <?= $form->field($model, 'created_by')->textInput() ?> -->

    <!-- <?= $form->field($model, 'updated_by')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
