<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\tree\TreeViewInput;
use app\models\SdiCategory;


/* @var $this yii\web\View */
/* @var $model app\models\SdiData */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
<div class="box-header with-border">
  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
  <div class="box-tools pull-right">
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
  </div>
</div>
<div class="box-body table-responsive">
        <?php $form = ActiveForm::begin([
                    'options' => [ 'enctype' => 'multipart/form-data']
                ]);
            ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'placeholder' => 'Judul'],) ?>

            <?= $form->field($model, 'year')->widget(Select2::className(), [
              'data' => $model->listTahun,
              'options' => ['placeholder' => 'Pilih Tahun'],
              'pluginOptions' => [
                  'allowClear' => true
              ],
            ]) ?>          

            <?= $form->field($model, 'file')->fileInput(); ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
            </div>

            <?php ActiveForm::end(); ?>
  </div>
</div>
