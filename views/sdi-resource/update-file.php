<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SdiResource */

$this->title = Yii::t('app', 'Update Sdi Resource: {name}', [
    'name' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sdi Resources'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="sdi-resource-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-upload', [
        'model' => $model,
    ]) ?>

</div>
