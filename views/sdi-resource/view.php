<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use hscstudio\mimin\components\Mimin;
use yii\data\Pagination;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\SdiResource */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sdi Resources'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sdi-resource-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= ((Mimin::checkRoute('sdi-data/view',true))) ? Html::a(Yii::t('app', 'Back'),['sdi-data/view','id'=>$model->sdi_data_id], ['class' => 'btn btn-warning']) : null ?>
        <?php if (empty($model->uploaded_file_id)): ?>
            <?=((Mimin::checkRoute($this->context->id.'/update-api',true))) ? Html::a(Yii::t('app', 'Update'), ['update-api', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
            <?= ((Mimin::checkRoute($this->context->id.'/delete-api',true))) ? Html::a(Yii::t('app', 'Delete'), ['delete-api', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) : null ?>
        <?php else: ?>
            <?=((Mimin::checkRoute($this->context->id.'/update-file',true))) ? Html::a(Yii::t('app', 'Update'), ['update-file', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
            <?= ((Mimin::checkRoute($this->context->id.'/delete-file',true))) ? Html::a(Yii::t('app', 'Delete'), ['delete-file', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) : null ?>
        <?php endif ?>
    </p>

<?php if (empty($model->uploaded_file_id)): ?>
    <div class="sdi-api-grid">

        <?= $this->render('_api-grid', [
              'model' => $model,
            ]); ?>

    </div>
<?php else: ?>
    <div class="sdi-data-grid">

        <?=  $this->render('_data-grid', [
                'model' => $model,
            ]); ?>

    </div>
<?php endif; ?>

    <div class="sdi-data-detail">

        <?= $this->render('_data-detail', [
                'model' => $model,
            ]); ?>

    </div>

</div>
