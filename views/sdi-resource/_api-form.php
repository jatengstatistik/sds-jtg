<?php

use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\ApiClient */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="api-client-form">
    <?php $form = ActiveForm::begin(['id'=>'dynamic-form']); ?>

    <?= $form->errorSummary($model); ?>
    
    <div class="box-body">
        <?= $form->field($model, 'title')->textInput(['placeholder' => 'Title']) ?>

    	<?= $form->field($model, 'year')->widget(Select2::className(), [
        	'data' => $model->listTahun,
        	'options' => ['placeholder' => 'Pilih Tahun'],
        	'pluginOptions' => [
        	  'allowClear' => true
        	],
    	]) ?>

        <?= $form->field($modelApiClient, 'url')->textInput(['maxlength' => true, 'placeholder' => 'Url']) ?>

        <?= $form->field($modelApiClient, 'method')->widget(Select2::className(), [
          'data' => $model->listMethod,
          'options' => ['placeholder' => 'Pilih'],
          'pluginOptions' => [
              'allowClear' => true
          ],
        ]) ?>

    </div>

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class=""><a href="#tab_1" data-toggle="tab" aria-expanded="false">Header</a></li>
          <li class="active"><a href="#tab_2" data-toggle="tab" aria-expanded="true">Query</a></li>
          <li><a href="#tab_3" data-toggle="tab">Body</a></li>
          <li><a href="#tab_4" data-toggle="tab">Data</a></li>
          <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane" id="tab_1">
                         <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper_header', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-items', // required: css class selector
                            'widgetItem' => '.item', // required: css class
                            // 'limit' => 4, // the maximum times, an element can be cloned (default 999)
                            'min' => 0, // 0 or 1 (default 1)
                            'insertButton' => '.add-item', // css class
                            'deleteButton' => '.remove-item', // css class
                            'model' => $modelApiHeader[0],
                            'formId' => 'dynamic-form',
                            'formFields' => [
                                'label',
                                'attribute',
                                'value',
                            ],
                        ]); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>
                            <i class="glyphicon glyphicon-envelope"></i> Header
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <!-- <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button> -->
                        </div>
                        </h4>
                    </div>
                    <div class="panel-body">

                        <div class="container-items">
                        <?php foreach ($modelApiHeader as $h => $modelApiHeaders): ?>
                            <div class="item panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left">Headers</h3>
                                    <div class="pull-right">
                                        <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                        <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                        // necessary for update action.
                                        if (! $modelApiHeaders->isNewRecord) {
                                            echo Html::activeHiddenInput($modelApiHeaders, "[{$h}]id");
                                        }
                                    ?>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiHeaders, "[{$h}]label")->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiHeaders, "[{$h}]attribute")->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiHeaders, "[{$h}]value")->textInput(['maxlength' => true]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                        <?php DynamicFormWidget::end(); ?>
          </div>
          
          <div class="tab-pane active" id="tab_2">
                         <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper_query', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-items-query', // required: css class selector
                            'widgetItem' => '.item-query', // required: css class
                            // 'limit' => 4, // the maximum times, an element can be cloned (default 999)
                            'min' => 0, // 0 or 1 (default 1)
                            'insertButton' => '.add-item-query', // css class
                            'deleteButton' => '.remove-item-query', // css class
                            'model' => $modelApiQuery[0],
                            'formId' => 'dynamic-form',
                            'formFields' => [
                                'label',
                                'attribute',
                                'value',
                            ],
                        ]); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>
                            <i class="glyphicon glyphicon-envelope"></i> Querys
                            <div class="pull-right">
                                <button type="button" class="add-item-query btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                <!-- <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button> -->
                            </div>
                        </h4>
                    </div>
                    <div class="panel-body">

                        <div class="container-items-query">
                        <?php foreach ($modelApiQuery as $q => $modelApiQuerys): ?>
                            <div class="item-query panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left">Querys</h3>
                                    <div class="pull-right">
                                        <button type="button" class="add-item-query btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                        <button type="button" class="remove-item-query btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                        // necessary for update action.
                                        if (! $modelApiQuerys->isNewRecord) {
                                            echo Html::activeHiddenInput($modelApiQuerys, "[{$q}]id");
                                        }
                                    ?>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiQuerys, "[{$q}]label")->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiQuerys, "[{$q}]attribute")->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiQuerys, "[{$q}]value")->textInput(['maxlength' => true]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                        <?php DynamicFormWidget::end(); ?>
          </div>
          
          <div class="tab-pane" id="tab_3">
                         <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper_body', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-items-body', // required: css class selector
                            'widgetItem' => '.item-body', // required: css class
                            // 'limit' => 4, // the maximum times, an element can be cloned (default 999)
                            'min' => 1, // 0 or 1 (default 1)
                            'insertButton' => '.add-item-body', // css class
                            'deleteButton' => '.remove-item-body', // css class
                            'model' => $modelApiBody[0],
                            'formId' => 'dynamic-form',
                            'formFields' => [
                                'label',
                                'attribute',
                                'value',
                            ],
                        ]); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>
                            <i class="glyphicon glyphicon-envelope"></i> Body
                            <div class="pull-right">
                                <button type="button" class="add-item-body btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                <!-- <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button> -->
                            </div>
                        </h4>
                    </div>
                    <div class="panel-body">

                        <div class="container-items-body">
                        <?php foreach ($modelApiBody as $b => $modelApiBodys): ?>
                            <div class="item-body panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left">Body</h3>
                                    <div class="pull-right">
                                        <button type="button" class="add-item-body btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                        <button type="button" class="remove-item-body btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                        // necessary for update action.
                                        if (! $modelApiBodys->isNewRecord) {
                                            echo Html::activeHiddenInput($modelApiBodys, "[{$b}]id");
                                        }
                                    ?>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiBodys, "[{$b}]label")->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiBodys, "[{$b}]attribute")->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiBodys, "[{$b}]value")->textInput(['maxlength' => true]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
                </div>

                        <?php DynamicFormWidget::end(); ?>
          </div>
          <div class="tab-pane" id="tab_4">
                        <?= $form->field($modelApiClient, 'segment')->textInput(['maxlength' => true, 'placeholder' => 'Segment']) ?>
                        <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper_pagination', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-items-data', // required: css class selector
                            'widgetItem' => '.item-data', // required: css class
                            // 'limit' => 4, // the maximum times, an element can be cloned (default 999)
                            'min' => 0, // 0 or 1 (default 1)
                            'insertButton' => '.add-item-data', // css class
                            'deleteButton' => '.remove-item-data', // css class
                            'model' => $modelApiData[0],
                            'formId' => 'dynamic-form',
                            'formFields' => [
                                'label',
                                'attribute',
                                'value',
                            ],
                        ]); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>
                            <i class="glyphicon glyphicon-envelope"></i> Data
                            <div class="pull-right">
                                <button type="button" class="add-item-data btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            </div>
                        </h4>
                    </div>
                    <div class="panel-body">

                        <div class="container-items-data">
                        <?php foreach ($modelApiData as $d => $modelApiDatas): ?>
                            <div class="item-data panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left">Data</h3>
                                    <div class="pull-right">
                                        <button type="button" class="add-item-data btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                        <button type="button" class="remove-item-data btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                        // necessary for update action.
                                        if (! $modelApiDatas->isNewRecord) {
                                            echo Html::activeHiddenInput($modelApiDatas, "[{$d}]id");
                                        }
                                    ?>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiDatas, "[{$d}]label")->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiDatas, "[{$d}]attribute")->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiDatas, "[{$d}]value")->textInput(['maxlength' => true]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
                </div>

                        <?php DynamicFormWidget::end(); ?>
          </div>
                  </div>
        
        </div>
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>