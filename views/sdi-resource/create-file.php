<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SdiResource */

$this->title = Yii::t('app', 'Create Sdi Resource');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sdi Resources'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sdi-resource-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-upload', [
        'model' => $model,
    ]) ?>

</div>
