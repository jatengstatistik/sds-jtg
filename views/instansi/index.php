<?php

use hscstudio\mimin\components\Mimin;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\InstansiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Instansi');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instansi-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= ((Mimin::checkRoute($this->context->id.'/create')))? Html::a(Yii::t('app', 'Create Instansi'), ['create'], ['class' => 'btn btn-success']) : null ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'nama_instansi',
            'singkatan_instansi',
            'alamat_instansi',
            'telepon_instansi',
            [
                'label' => 'Jenis Instansi',
                'attribute' => 'jenis_instansi_id',
                'value' => 'jenisInstansi.jenis_instansi',
            ],
            //'created_at',
            //'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => Mimin::filterActionColumn([
                  'view','update','delete'
                ],$this->context->route),
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
