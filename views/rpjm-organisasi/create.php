<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RpjmOrganisasi */

$this->title = Yii::t('app', 'Create Rpjm Organisasi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rpjm Organisasis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rpjm-organisasi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
