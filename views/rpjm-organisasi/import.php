<?php 

use hscstudio\mimin\components\Mimin;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$this->title = 'Import Data Organisasi'
?>
<div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
        <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body table-responsive">
			<?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]);?>

			<?= $form->field($modelImport,'fileImport')->fileInput() ?>

			<?= Html::submitButton('Import',['class'=>'btn btn-primary']);?>
			<?= ((Mimin::checkRoute($this->context->id.'/index',true))) ? Html::a(Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-warning']) : null ?>

			<?php ActiveForm::end();?>
        </div>
    </div>