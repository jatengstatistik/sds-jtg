<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RpjmOrganisasi */

$this->title = Yii::t('app', 'Update Rpjm Organisasi: {name}', [
    'name' => $model->nama_organisasi,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rpjm Organisasis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="rpjm-organisasi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
