<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;
use app\models\Setting;

?>
<div class="content-wrapper">
    <section class="content-header" algin="left" >
        <?=
        Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        ) ?>
    </section>

    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> <?= Setting::find(1)->one()->version ?>
    </div>
    <strong>Copyright &copy; <?= (date('Y')=='2018') ? "2018" : "2018 - ".date('Y') ; ?> <a href="#"><?= Setting::find(1)->one()->skpd ?></a></strong> <?= Setting::find(1)->one()->nama_kabupaten ?>.
</footer>