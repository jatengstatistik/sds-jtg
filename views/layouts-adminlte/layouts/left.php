<?php 
use hscstudio\mimin\components\Mimin;
use app\models\Setting;
use yii\helpers\Html;
$setting = Setting::findOne(1);
 ?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="box-body box-profile">
              <a href="<?= Yii::$app->homeUrl; ?>">
                <?= Html::img('@web/'.$setting->site_logo,['class'=>'img-responsive']);?>
              </a>
              <!-- <h3 class="profile-username text-center">Nina Mcintire</h3> -->
              <!-- <p class="text-muted text-center">Software Engineer</p> -->
            </div>
            <!-- <img src="<?= $directoryAsset ?>/img/sds-logo.png" class="img-circle" alt="User Image"/> -->
            <!-- <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div> -->
            <!-- <div class="pull-left info">
                <p><?= Yii::$app->user->id; ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div><br><br> -->
        </div>
        <?php 
        $menuItems = [
            ['label' => ''.Yii::$app->name.' berdasar SDI Jateng', 'options' => ['class' => 'header']],
            [
                'label' => 'Dashboard', 'icon' => 'dashboard', 'url'=>['/site/index'],
            ],
            [
                'label' => 'Kelola Data', 'icon' => 'database', 'url' => ['#'], 'items' => [
                    ['label'=>'Daftar Data', 'icon'=>'object-group', 'url'=>['/sdi-data/index'],],
                    ['label'=>'Kategori Data', 'icon'=>'object-group', 'url'=>['/sdi-category/index'],],
                ],
            ],
            [
                'label' => 'User', 'icon' => 'address-card', 'items' => [
                    ['label'=>'Profile', 'icon'=>'user', 'url'=>['/user/profile']],
                    // ['label'=>'User', 'icon'=>'users', 'url'=>['/user/index']],
                    // ['label'=>'Role', 'icon'=>'user-circle', 'url'=>['/mimin/role']],
                    // ['label'=>'Route', 'icon'=>'object-group', 'url'=>['/mimin/route']],
                    // ['label' => 'Instansi', 'icon' => 'building', 'url' => ['/instansi/index']],
                    // ['label' => 'Jenis Instansi', 'icon' => 'building-o', 'url' => ['/jenis-instansi/index']],
                ],
            ],
            // [
            //     'label' => 'Setting', 'icon' => 'desktop', 'url' => ['#'], 'items' => [
            //         ['label'=>'Site', 'icon'=>'gears', 'url'=>['/setting/view','id'=>'1']],
            //     ],
            // ],
            
            ['label' => 'Master '.Yii::$app->name.'', 'options' => ['class' => 'header']],
            // [
            //     'label' => 'Dashboard', 'icon' => 'dashboard', 'url'=>['/site/index'],
            // ],
            [
                'label' => 'User Management', 'icon' => 'address-card', 'items' => [
                    ['label'=>'Users', 'icon'=>'users', 'url'=>['/user/index']],
                    ['label'=>'Role', 'icon'=>'user-circle', 'url'=>['/mimin/role']],
                    ['label'=>'Route', 'icon'=>'object-group', 'url'=>['/mimin/route']],
                ],
            ],

            
            [
                'label' => 'Master', 'icon' => 'navicon', 'url' => ['#'], 'items' => [
                    ['label' => 'Kelola Data', 'icon' => 'database', 'url' => ['/data/index']],
                    ['label' => 'Aplikasi', 'icon' => 'folder-o', 'url' => ['/aplikasi/index']],
                    ['label' => 'Instansi', 'icon' => 'building', 'url' => ['/instansi/index']],
                    ['label' => 'Jenis Instansi', 'icon' => 'building-o', 'url' => ['/jenis-instansi/index']],
                    ['label' => 'Data Dasar', 'icon' => 'file-o', 'url' => ['/data-dasar/index']],
                ],
            ],
            
            [
                'label' => 'Analis RPJM Jateng 13-18', 'icon' => 'bar-chart', 'url' => ['/data-storage/index']
            ],
            [
                'label' => 'Analis RPJM Jateng 19-23', 'icon' => 'pie-chart', 'url' => ['#'], 'items' => [
                        ['label' => 'Data Instansi', 'icon' => 'files-o', 'url' => ['/rpjm-organisasi/index']],
                        ['label' => 'Data Jenis Urusan', 'icon' => 'files-o', 'url' => ['/rpjm-urusan/index']],
                        ['label' => 'Data Capaian', 'icon' => 'files-o', 'url' => ['/data-dasar/index']],
                        ['label' => 'Analisa Indikator', 'icon' => 'bar-chart', 'url' => ['/rpjm-indikator/index']],
                ],
            ],
            
            [
                'label' => 'Integrasi', 'icon' => 'database', 'url' => ['#'], 'items' => [
                    ['label'=>'Integrasi Data', 'icon'=>'chain', 'url'=>['/integrasi-data/index']],
                    ['label'=>'Integrasi Aplikasi', 'icon'=>'chain', 'url'=>['/status-sds/index']],
                ],
            ],
            [
                'label' => 'Setting', 'icon' => 'desktop', 'url' => ['#'], 'items' => [
                    ['label'=>'Site', 'icon'=>'gears', 'url'=>['/setting/view','id'=>'1']],
                ],
            ],
            
            ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
        ];

        $menuItems = Mimin::filterMenu($menuItems);
        
        echo dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => $menuItems,
            ]
        ) ?>

    </section>

</aside>
