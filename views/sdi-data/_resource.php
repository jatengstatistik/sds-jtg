<?php 

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use hscstudio\mimin\components\Mimin;
use kartik\export\ExportMenu;
 ?>
<div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
    <div class="box-header with-border">
      <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
      <div class="box-tools pull-right">
          <?php
          // echo '<pre>';
          // print_r($dataProviderResource);
          // echo '</pre>';
            // echo ExportMenu::widget([
                // 'dataProvider' => $dataProviderResource,
                // 'columns' => $model->getFileAttributeGridView(),
                // 'clearBuffers' => true,
            // ]);
          ?>
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
    </div> 
    <div class="box-body table-responsive">
    <?php 
    try {
        echo GridView::widget([
            'dataProvider' => $dataProviderResource,
            'filterModel' => $searchModelResource,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                // 'id',
                // 'title',
                [
                    'attribute' => 'title',
                    // 'value' => 'organisasi.nama_organisasi',
                    'format' => 'raw',
                    'value' => function ($model){
                        return Html::a($model->title,['sdi-resource/view', 'id' => $model->id]);
                    },
                ],    
                'year',
                'created_at:datetime',
                'updated_at:datetime',
                //'created_by',
                //'updated_by',

                // ['class' => 'yii\grid\ActionColumn'],
                [
                  'class' => 'yii\grid\ActionColumn',
                  'template' => Mimin::filterActionColumn([
                      // 'view','update','delete'
                    'update','delete','publish'
                  ],$this->context->route),
                  'buttons'=>[
                          'update'=>function ($url, $model) {
                            if (empty($model->uploaded_file_id)):
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['sdi-resource/update-api','id'=>$model->id], ['class' => 'btn btn-warning']);
                            else:
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['sdi-resource/update-file','id'=>$model->id], ['class' => 'btn btn-warning']);
                            endif;
                          },
                          'delete'=>function ($url, $model) {
                            if (empty($model->uploaded_file_id)):
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['sdi-resource/delete-api','id'=>$model->id], ['class' => 'btn btn-danger','','data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ]]);
                            else:
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['sdi-resource/delete-file','id'=>$model->id], ['class' => 'btn btn-danger','','data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ]]);
                            endif;
                          },
                          'publish'=>function ($url, $model) {
                            if (empty($model->uploaded_file_id)):
                                // return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['sdi-resource/delete-api','id'=>$model->id], ['class' => 'btn btn-danger','','data' => [
                                //     'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                //     'method' => 'post',
                                // ]]);
                            else:
                                return Html::a('<span class="glyphicon glyphicon-share-alt"></span>', ['sdi-resource/publish', 'id' => $model->id], [
                                    'class' => 'btn btn-success',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to publish this item?'),
                                        'method' => 'post',
                                ]]);
                            endif;
                          },
                  ],
                ],
            ],
        ]);
    } catch (\Throwable $throwable) {
      echo("Data Tidak Dapat Ditampailkan... ");
    }

    ?>
    </div>
</div>