<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use hscstudio\mimin\components\Mimin;
use yii\data\Pagination;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model app\models\SdiData */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sdi Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sdi-data-view">
    <?php Pjax::begin(); ?>  

    <p>
        <?= ((Mimin::checkRoute($this->context->id.'/index',true))) ? Html::a(Yii::t('app', 'Back'),['index'], ['class' => 'btn btn-warning']) : null ?>
        <?=((Mimin::checkRoute($this->context->id.'/update',true))) ? Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
        <?= ((Mimin::checkRoute($this->context->id.'/delete',true))) ? Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) : null ?>

    </p>

    <div class="sdi-data-detail">

        <?= $this->render('_detail', [
                'model' => $model,
            ]); ?>

    </div>

    <p>
        <?= ((Mimin::checkRoute('sdi-resource/create-file',true))) ? Html::a(Yii::t('app', 'Upload Data'), ['sdi-resource/create-file','sdi_data_id' => $model->id], ['class' => 'btn btn-info']) : null ?>

        <?= ((Mimin::checkRoute('sdi-resource/create-api',true))) ? Html::a(Yii::t('app', 'Integrasi Data'), ['sdi-resource/create-api','sdi_data_id' => $model->id], ['class' => 'btn btn-success']) : null ?>
    </p>
    <div class="sdi-data-detail">

        <?= $this->render('_resource', [
                'searchModelResource' => $searchModelResource,
                'dataProviderResource' => $dataProviderResource,
            ]); ?>

    </div>

<?php Pjax::end(); ?>
</div>
