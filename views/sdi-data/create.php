<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model app\models\SdiData */

$this->title = Yii::t('app', 'Tambah Data');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sdi Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<br>
<div class="sdi-data-create">

<div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
    <div class="box-header with-border">
      <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
    </div>

<div class="box-body table-responsive">
    <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
</div>
</div>

</div>
