<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;
use kartik\tree\TreeViewInput;
use app\models\SdiCategory;


/* @var $this yii\web\View */
/* @var $model app\models\SdiData */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="sdi-data-form">
  <div class="box-body table-responsive">
        <?php $form = ActiveForm::begin();
            ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'placeholder' => 'Judul'],) ?>

            <?= $form->field($model, 'public')->widget(SwitchInput::classname(), [
              'pluginOptions' => [
                'onText' => 'Public',
                'offText' => 'Private',
              ]
            ]) ?>
           
            <?= $form->field($model, 'instansi_id')->widget(Select2::className(), [
              'data' => $model->listInstansi,
              'options' => ['placeholder' => 'Pilih'],
              'pluginOptions' => [
                  'allowClear' => true
              ],
            ]) ?>

            <?= $form->field($model,'sdi_category_id')->widget(TreeViewInput::className(),[
                'name' => 'sdi_category_id',
                'value' => 'false', // preselected values
                'query' => \app\models\SdiCategory::find()->addOrderBy('root, lft'),
                'headingOptions' => ['label' => 'Kategori'],
                'rootOptions' => ['label'=>'<i class="fas fa-tree text-success"></i>'],
                'fontAwesome' => true,
                'asDropdown' => false,
                'multiple' => false,
                'options' => ['disabled' => false]
            ]);?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
            </div>

            <?php ActiveForm::end(); ?>
  </div>
</div>  

