<?php

use hscstudio\mimin\components\Mimin;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SdiDataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sdi Datas');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="sdi-data-index">
<?php Pjax::begin(); ?>
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <p>
        <?= ((Mimin::checkRoute($this->context->id.'/create',true))) ? Html::a(Yii::t('app', 'Tambah Data'), ['create'], ['class' => 'btn btn-success']) : null ?>
        <!-- <?= ((Mimin::checkRoute($this->context->id.'/create',true))) ? Html::a(Yii::t('app', 'Import Data'), ['create'], ['class' => 'btn btn-success']) : null ?> -->
        <!-- <?= ((Mimin::checkRoute($this->context->id.'/api-create',true))) ? Html::a(Yii::t('app', 'Api Data'), ['api-create'], ['class' => 'btn btn-warning']) : null ?> -->
    </p>

    <div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
        <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body table-responsive">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    // 'id',
                    // 'title',
                    [
                        // 'label' => 'Indikator',
                        'attribute' => 'title',
                        // 'value' => 'organisasi.nama_organisasi',
                        'format' => 'raw',
                        'value' => function ($data){
                            return Html::a($data->title,['view', 'id' => $data->id]);
                            // return $data->createdBy->nama_lengkap;
                        },
                    ],            
                    // 'year',
                    [
                        // 'label' => 'Instansi',
                        'attribute' => 'instansi_id',
                        'value' => 'instansi.nama_instansi',
                        // 'format' => 'raw',
                        // 'value' => function ($data){
                        //     return Html::a($data->indikator,['view', 'id' => $data->id]);
                        //     // return $data->createdBy->nama_lengkap;
                        // },
                    ],            
                    // 'instansi_id',
                    // 'api_client_id',
                    // 'uploaded_file_id',
                    'created_at:datetime',
                    //'updated_at',
                    //'created_by',
                    //'updated_by',
                    //'deleted_by',
                    //'lock',

                    // ['class' => 'yii\grid\ActionColumn'],
                    [
                      'class' => 'yii\grid\ActionColumn',
                      'template' => Mimin::filterActionColumn([
                          // 'view','update','delete'
                        'update','delete'
                      ],$this->context->route),
                      'buttons'=>[
                          'update'=>function ($url, $model) {
                              return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update','id'=>$model->id], ['class' => 'btn btn-warning']);
                          },
                          'delete'=>function ($url, $model) {
                              return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete','id'=>$model->id], ['class' => 'btn btn-danger','','data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ]]);
                          },
                      ],
                    ],
                ],
            ]); ?>
        </div>
    </div>

    <?php Pjax::end(); ?>
</div>
