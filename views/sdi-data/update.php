<?php

use yii\helpers\Html;
use hscstudio\mimin\components\Mimin;
/* @var $this yii\web\View */
/* @var $model app\models\SdiData */

$this->title = Yii::t('app', 'Update Sdi Data: {name}', [
    'name' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sdi Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="form-group">
    <?= ((Mimin::checkRoute($this->context->id.'/view',true))) ? Html::a(Yii::t('app', 'Back'), ['view' , 'id' =>$model->id], ['class' => 'btn btn-warning']) : null ?>
</div>

<div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="sdi-data-update">

        <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    </div>
</div>