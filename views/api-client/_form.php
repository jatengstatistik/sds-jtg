<?php

use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApiClient */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="api-client-form">
    <?php $form = ActiveForm::begin(['id'=>'dynamic-form']); ?>

    <?= $form->errorSummary($model); ?>

    <div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Api Client</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
      
    </div>
    
    <div class="box-body">
        <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        <?= $form->field($model, 'title')->textInput(['placeholder' => 'Title']) ?>

        <?= $form->field($model, 'notes')->textInput(['placeholder' => 'Notes']) ?>

        <?= $form->field($model, 'instansi_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\Instansi::find()->orderBy('id')->asArray()->all(), 'id', 'nama_instansi'),
            'options' => ['placeholder' => Yii::t('app', 'Choose Instansi')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

        <?= $form->field($model, 'url')->textInput(['maxlength' => true, 'placeholder' => 'Url']) ?>

        <?= $form->field($model, 'method')->textInput(['maxlength' => true, 'placeholder' => 'Method']) ?>

    </div>
    
    </div>

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class=""><a href="#tab_1" data-toggle="tab" aria-expanded="false">Header</a></li>
          <li class="active"><a href="#tab_2" data-toggle="tab" aria-expanded="true">Query</a></li>
          <li><a href="#tab_3" data-toggle="tab">Body/Data</a></li>
          <li><a href="#tab_4" data-toggle="tab">Pagination</a></li>
          <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane" id="tab_1">
                         <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper_header', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-items', // required: css class selector
                            'widgetItem' => '.item', // required: css class
                            // 'limit' => 4, // the maximum times, an element can be cloned (default 999)
                            'min' => 0, // 0 or 1 (default 1)
                            'insertButton' => '.add-item', // css class
                            'deleteButton' => '.remove-item', // css class
                            'model' => $modelApiHeader[0],
                            'formId' => 'dynamic-form',
                            'formFields' => [
                                'label',
                                'attribute',
                                'value',
                            ],
                        ]); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="glyphicon glyphicon-envelope"></i> Header
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <!-- <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button> -->
                        </div>
                        </h4>
                    </div>
                    <div class="panel-body">

                        <div class="container-items">
                        <?php foreach ($modelApiHeader as $i => $modelApiHeaders): ?>
                            <div class="item panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left">Headers</h3>
                                    <div class="pull-right">
                                        <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                        <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                        // necessary for update action.
                                        if (! $modelApiHeaders->isNewRecord) {
                                            echo Html::activeHiddenInput($modelApiHeaders, "[{$i}]id");
                                        }
                                    ?>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiHeaders, "[{$i}]label")->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiHeaders, "[{$i}]attribute")->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiHeaders, "[{$i}]value")->textInput(['maxlength' => true]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                        <?php DynamicFormWidget::end(); ?>
          </div>
          
          <div class="tab-pane active" id="tab_2">
                         <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper_query', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-items', // required: css class selector
                            'widgetItem' => '.item', // required: css class
                            // 'limit' => 4, // the maximum times, an element can be cloned (default 999)
                            'min' => 0, // 0 or 1 (default 1)
                            'insertButton' => '.add-item', // css class
                            'deleteButton' => '.remove-item', // css class
                            'model' => $modelApiQuery[0],
                            'formId' => 'dynamic-form',
                            'formFields' => [
                                'label',
                                'attribute',
                                'value',
                            ],
                        ]); ?>
                <div class="panel panel-default">
                    <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Querys
                                                <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <!-- <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button> -->
                        </div>
                    </h4></div>
                    <div class="panel-body">

                        <div class="container-items">
                        <?php foreach ($modelApiQuery as $i => $modelApiQuerys): ?>
                            <div class="item panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left">Querys</h3>
                                    <div class="pull-right">
                                        <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                        <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                        // necessary for update action.
                                        if (! $modelApiQuerys->isNewRecord) {
                                            echo Html::activeHiddenInput($modelApiQuerys, "[{$i}]id");
                                        }
                                    ?>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiQuerys, "[{$i}]label")->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiQuerys, "[{$i}]attribute")->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiQuerys, "[{$i}]value")->textInput(['maxlength' => true]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                        <?php DynamicFormWidget::end(); ?>
          </div>
          
          <div class="tab-pane" id="tab_3">
            <?= $form->field($model, 'segment')->textInput(['maxlength' => true, 'placeholder' => 'Segment']) ?>
                         <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper_body', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-items', // required: css class selector
                            'widgetItem' => '.item', // required: css class
                            // 'limit' => 4, // the maximum times, an element can be cloned (default 999)
                            'min' => 0, // 0 or 1 (default 1)
                            'insertButton' => '.add-item', // css class
                            'deleteButton' => '.remove-item', // css class
                            'model' => $modelApiBody[0],
                            'formId' => 'dynamic-form',
                            'formFields' => [
                                'label',
                                'attribute',
                                'value',
                            ],
                        ]); ?>
                <div class="panel panel-default">
                    <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Body
                                                <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <!-- <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button> -->
                        </div>
                    </h4></div>
                    <div class="panel-body">

                        <div class="container-items">
                        <?php foreach ($modelApiBody as $i => $modelApiBodys): ?>
                            <div class="item panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left">Body</h3>
                                    <div class="pull-right">
                                        <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                        <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                        // necessary for update action.
                                        if (! $modelApiBodys->isNewRecord) {
                                            echo Html::activeHiddenInput($modelApiBodys, "[{$i}]id");
                                        }
                                    ?>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiBodys, "[{$i}]label")->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiBodys, "[{$i}]attribute")->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiBodys, "[{$i}]value")->textInput(['maxlength' => true]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
                </div>

                        <?php DynamicFormWidget::end(); ?>
          </div>
          <div class="tab-pane" id="tab_4">
                         <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper_pagination', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                            'widgetBody' => '.container-items', // required: css class selector
                            'widgetItem' => '.item', // required: css class
                            // 'limit' => 4, // the maximum times, an element can be cloned (default 999)
                            'min' => 0, // 0 or 1 (default 1)
                            'insertButton' => '.add-item', // css class
                            'deleteButton' => '.remove-item', // css class
                            'model' => $modelApiPagination[0],
                            'formId' => 'dynamic-form',
                            'formFields' => [
                                'label',
                                'attribute',
                                'value',
                            ],
                        ]); ?>
                <div class="panel panel-default">
                    <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Pagination
                                                <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                        </div>
                    </h4></div>
                    <div class="panel-body">

                        <div class="container-items">
                        <?php foreach ($modelApiPagination as $i => $modelApiPaginations): ?>
                            <div class="item panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left">Pagination</h3>
                                    <div class="pull-right">
                                        <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                        <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                        // necessary for update action.
                                        if (! $modelApiPaginations->isNewRecord) {
                                            echo Html::activeHiddenInput($modelApiPaginations, "[{$i}]id");
                                        }
                                    ?>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiPaginations, "[{$i}]label")->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiPaginations, "[{$i}]attribute")->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= $form->field($modelApiPaginations, "[{$i}]value")->textInput(['maxlength' => true]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
                </div>

                        <?php DynamicFormWidget::end(); ?>
          </div>
                  </div>
        
        </div>
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>