<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApiClient */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Api Client',
]) . ' ' . $modelApiClient->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Api Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $modelApiClient->title, 'url' => ['view', 'id' => $modelApiClient->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="api-client-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
       'model' => $modelApiClient,
            'modelApiHeader' => $modelApiHeader,
            'modelApiQuery' => $modelApiQuery,
            'modelApiBody' => $modelApiBody,
            'modelApiPagination' => $modelApiPagination,
    ]) ?>

</div>
