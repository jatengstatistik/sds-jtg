<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\ApiClient */

$this->title = $model->instansi_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Api Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-client-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('app', 'Api Client').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'instansi.id',
            'label' => Yii::t('app', 'Instansi'),
        ],
        ['attribute' => 'lock', 'visible' => false],
        'title',
        'notes',
        'method',
        'url:url',
        'segment',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerApiBody->totalCount){
    $gridColumnApiBody = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'label',
            'attribute',
            'value',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerApiBody,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-api-body']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Api Body')),
        ],
        'export' => false,
        'columns' => $gridColumnApiBody
    ]);
}
?>

    </div>
    <div class="row">
        <h4>Instansi<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnInstansi = [
        ['attribute' => 'id', 'visible' => false],
        'jenis_instansi_id',
        'nama_instansi',
        'singkatan_instansi',
        'alamat_instansi',
        'telepon_instansi',
    ];
    echo DetailView::widget([
        'model' => $model->instansi,
        'attributes' => $gridColumnInstansi    ]);
    ?>
    
    <div class="row">
<?php
if($providerApiHeader->totalCount){
    $gridColumnApiHeader = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'label',
            'attribute',
            'value',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerApiHeader,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-api-header']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Api Header')),
        ],
        'export' => false,
        'columns' => $gridColumnApiHeader
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerApiQuery->totalCount){
    $gridColumnApiQuery = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'label',
            'attribute',
            'value',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerApiQuery,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-api-query']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Api Query')),
        ],
        'export' => false,
        'columns' => $gridColumnApiQuery
    ]);
}
?>

<div class="row">
    <pre>
            <?php print_r($apiResult); ?>
    </pre>
</div>

    </div>
</div>
