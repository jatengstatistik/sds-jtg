<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DataDasar */
?>
<div class="data-dasar-view">

    <div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
    <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body table-responsive">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                // 'id',
                'file_name',
                'files',
                'tahun_data',
                'created_at:datetime',
                'updated_at:datetime',
                // 'created_by',
                [
                    'label' => 'Created By',
                    'attribute' => 'created_by',
                    'format' => 'raw',
                    'value' => function ($data){
                        // return Html::a($data->nama_data,['view', 'id' => $data->id]);
                        return $data->createdBy->nama_lengkap;
                    },
                ],
                // 'updated_by',
                [
                    'label' => 'Updated By',
                    'attribute' => 'updated_by',
                    'format' => 'raw',
                    'value' => function ($data){
                        // return Html::a($data->nama_data,['view', 'id' => $data->id]);
                        return $data->updatedBy->nama_lengkap;
                    },
                ],
            ],
        ]) ?>
        </div>
    </div> 

</div>