<?php

use hscstudio\mimin\components\Mimin;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\DataDasar */

$this->title = $model->file_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Dasars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?php Pjax::begin(); ?>
<div class="data-dasar-action">
    <p>
        <?= ((Mimin::checkRoute($this->context->id.'/delete',true))) ? Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) : null ?>
        <?= ((Mimin::checkRoute($this->context->id.'/update',true))) ? Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
        <?= ((Mimin::checkRoute($this->context->id.'/index',true))) ? Html::a(Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-warning']) : null ?>
    </p>
</div>

<div class="data-dasar-view">

    <?= $this->render('_detail', [
            'model' => $model,
        ]); ?>

</div>

<div class="data-dasar-grid">

    <?= $this->render('_grid', [
            'dataProviderData' => $dataProviderData,
        ]); ?>

</div>
<?php Pjax::end(); ?>
