<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DataDasar */

$this->title = Yii::t('app', 'Create Data Dasar');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Dasars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-dasar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'list_instansi' => $list_instansi,
        'list_urusan' => $list_urusan,
        'list_tahun' => $list_tahun,
    ]) ?>

</div>
