<?php 

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;



 ?>
<div class="data-dasar-index">

    <div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
    <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body table-responsive">
        <?= GridView::widget([
		        'dataProvider' => $dataProviderData,
		        // 'filterModel' => $searchModelData,
		        'columns' => [
		            ['class' => 'yii\grid\SerialColumn'],

		            'indikator',
		            'satuan',
		            'rkpd',
		            'dppa',
		            'realisasi',
		            // [
		            //     'label' => 'Tambah Data',
		            //     'format' => 'raw',
		            //     'value' => function ($data){
		            //         return Html::a('Tambah',['xxxx', 
		            //         	'indikator' => $data['indikator'],
		            //         	'satuan' => $data['satuan'],
		            //         	'rkpd' => $data['rkpd'],
		            //         	'dppa' => $data['dppa'],
		            //         	'realisasi' => $data['realisasi'],
		            //         ]);
		            //     },
		            // ],

		            // ['class' => 'yii\grid\ActionColumn'],
		        ],
		    ]); ?>
        </div>
    </div> 
    
    
</div>