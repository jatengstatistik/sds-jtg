<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DataDasar */

$this->title = Yii::t('app', 'Update Data Dasar: {name}', [
    'name' => $model->file_name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Dasars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="data-dasar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'list_instansi' => $list_instansi,
        'list_urusan' => $list_urusan,
        'list_tahun' => $list_tahun,
    ]) ?>

</div>
