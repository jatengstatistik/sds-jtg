<?php

use hscstudio\mimin\components\Mimin;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DataDasarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Data Dasar (E-Controling)');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-dasar-index">
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= ((Mimin::checkRoute($this->context->id.'/index',true))) ? Html::a(Yii::t('app', 'Import Data Capain'), ['create'], ['class' => 'btn btn-success']) : null ?>
    </p>

    <div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
    <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body table-responsive">
            <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        // 'id',
                        [
                            'label' => 'Nama File',
                            'attribute' => 'file_name',
                            'format' => 'raw',
                            'value' => function ($data){
                                return Html::a($data->file_name,['view', 'id' => $data->id]);
                                // return $data->createdBy->nama_lengkap;
                            },
                        ],
                        // 'organisasi_id',
                        [
                            'label' => 'Organisasi',
                            'attribute' => 'nama_organisasi',
                            'value' => 'organisasi.nama_organisasi',
                            // 'format' => 'raw',
                            // 'value' => function ($data){
                            //     return Html::a($data->file_name,['view', 'id' => $data->id]);
                            //     // return $data->createdBy->nama_lengkap;
                            // },
                        ],
                        // 'urusan_id',
                        [
                            'label' => 'Urusan',
                            'attribute' => 'nama_urusan',
                            'value' => 'urusan.nama_urusan',
                            // 'format' => 'raw',
                            // 'value' => function ($data){
                            //     return Html::a($data->file_name,['view', 'id' => $data->id]);
                            //     // return $data->createdBy->nama_lengkap;
                            // },
                        ],
                        'tahun_data',
                        'created_at:datetime',
                        // 'updated_at:datetime',
                        // 'created_by',
                        // [
                        //     'label' => 'Created By',
                        //     'attribute' => 'created_by',
                        //     'format' => 'raw',
                        //     'value' => function ($data){
                        //         // return Html::a($data->nama_data,['view', 'id' => $data->id]);
                        //         return $data->createdBy->nama_lengkap;
                        //     },
                        // ],
                        // 'updated_by',
                        // [
                        //     'label' => 'Created By',
                        //     'attribute' => 'created_by',
                        //     'format' => 'raw',
                        //     'value' => function ($data){
                        //         // return Html::a($data->nama_data,['view', 'id' => $data->id]);
                        //         return $data->updatedBy->nama_lengkap;
                        //     },
                        // ],

                        // ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
        </div>
    </div> 
    <?php Pjax::end(); ?>
</div>
