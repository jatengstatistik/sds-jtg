<?php

use hscstudio\mimin\components\Mimin;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DataDasar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-dasar-form">
<div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
    <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body table-responsive">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <!-- <?= $form->field($model, 'files')->textInput() ?> -->

            <?= $form->field($model, 'organisasi_id')->widget(Select2::className(), [
                'data' => $list_instansi,
                'options' => ['placeholder' => 'Pilih'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>

            <?= $form->field($model, 'urusan_id')->widget(Select2::className(), [
                'data' => $list_urusan,
                'options' => ['placeholder' => 'Pilih'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>

            <!-- <?= $form->field($model, 'tahun_data')->textInput() ?> -->

            <?= $form->field($model, 'tahun_data')->widget(Select2::className(), [
                'data' => $list_tahun,
                'options' => ['placeholder' => 'Pilih'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>

            <?= $form->field($model, 'files')->fileInput() ?>

            <!-- <?= $form->field($model, 'updated_at')->textInput() ?> -->

            <!-- <?= $form->field($model, 'created_by')->textInput() ?> -->

            <!-- <?= $form->field($model, 'updated_by')->textInput() ?> -->

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                <?= ((Mimin::checkRoute($this->context->id.'/index',true))) ? Html::a(Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-warning']) : null ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div> 

</div>
