<?php
use kartik\tree\TreeView;
use app\models\SdiCategory;
use app\models\Setting;
use yii\helpers\Html;

echo Html::csrfMetaTags();
echo TreeView::widget([
    'query' => SdiCategory::find()->addOrderBy('root, lft'), 
    'headingOptions' => ['label' => 'Categories'],
    'fontAwesome' => true,     // optional
    'isAdmin' => Setting::find(1)->one()->admin_setting_data,         // optional (toggle to enable admin mode)
    'iconEditSettings'=> [
        'show' => 'list',
        'listData' => [
            'folder' => 'Folder',
            'file' => 'File',
            'mobile' => 'Phone',
            'bell' => 'Bell',
        ]
    ],
    'displayValue' => 1,        // initial display value
    'softDelete' => true,       // defaults to true
    'cacheSettings' => [        
        'enableCache' => true   // defaults to true
    ]
]);