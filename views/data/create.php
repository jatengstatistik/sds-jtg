<?php

use yii\helpers\Html;
use kartik\tree\TreeView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\tree\Module;

/* @var $this yii\web\View */
/* @var $model app\models\Data */

$this->title = Yii::t('app', 'Create Data');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Datas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="data-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php 
        Modal::begin([
            'header' => '<h4>Tambah Data',
            'id' => 'modal',
            'size' => 'modal-lg',
        ]);

        // $data = $model->id;
        echo "<div id='modalContent'></div>";
        Modal::end();
    ?>
    
    <div class="box box-primary">
        <div class="box-body">
            <?= 
                TreeView::widget([
                    // single query fetch to render the tree
                    // use the Product model you have in the previous step
                    'query' => $query,
                    'headingOptions' => ['label' => 'Kategori'],
                    // 'rootOptions' => ['label'=>'<span class="text-primary">Kategori</span>'],
                    // 'topRootAsHeading' => true,
                    'fontAwesome' => false,     // optional
                    'isAdmin' => true,         // optional (toggle to enable admin mode)
                    'iconEditSettings'=> [
                        'show' => 'list',
                        'listData' => [
                            'folder' => 'Folder',
                            'file' => 'File',
                            'mobile' => 'Phone',
                            'bell' => 'Bell',
                        ]
                    ],
                    // 'wrapperTemplate' =>  "{tree}{footer}", 
                    'displayValue' => 1,        // initial display value
                    'softDelete' => true,       // defaults to true
                    'cacheSettings' => [        
                        'enableCache' => true   // defaults to true
                    ],
                    // 'nodeTitle' => 'Kategori',com
                    // 'nodeTitlePlural' => 'Kategori',com

                    'nodeAddlViews' => [
                        Module::VIEW_PART_2 => '@app/views/data/_view',
                    ]
                ]);
            ?>
        </div>
    </div>
    
</div>
