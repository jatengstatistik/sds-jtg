<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

$js = '
jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {
        jQuery(this).html("Struktur Data: " + (index + 1))
    });
});

jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {
        jQuery(this).html("Struktur Data: " + (index + 1))
    });
});

// $("input[type="checkbox"].flat-red, input[type="radio"].flat-red").iCheck({
//       checkboxClass: "icheckbox_flat-green",
//       radioClass   : "iradio_flat-green"
// });

';


$this->registerJs($js);


?>


<div class="form-new-tree">
	<div class="box box-primary">
		<div class="box-header with-border">
			<p class="box-title">Parameter</p>
		</div>
		<div class="box-body">
			<?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

				<?= $form->field($parameter_api, 'parameter_api')->textInput(['placeholder' => 'Parameter Api']) ?>

				<div class="padding-v-md">
			        <div class="line line-dashed"></div>
			    </div>

			    <?php DynamicFormWidget::begin([
			        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
			        'widgetBody' => '.container-items', // required: css class selector
			        'widgetItem' => '.item', // required: css class
			        'limit' => 25, // the maximum times, an element can be cloned (default 999)
			        'min' => 1, // 0 or 1 (default 1)
			        'insertButton' => '.add-item', // css class
			        'deleteButton' => '.remove-item', // css class
			        'model' => $strukturData[0],
			        'formId' => 'dynamic-form',
			        'formFields' => [
			        	'nama_struktur',
						'parameter',
						'relasi',
						'grafik',
			            // 'full_name',
			            // 'address_line1',
			            // 'address_line2',
			            // 'city',
			            // 'state',
			            // 'postal_code',
			        ],
			    ]); ?>
				
				<div class="container-items">
					<?php foreach ($strukturData as $index => $struktur_data): ?>
				    	<div class="item box box-primary">
					    	<div class="box-header with-border">
					    		<p class="box-title">Struktur Tabel</p>
					    		<button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>

					    		<button style="margin-right: 5px;" type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i></button>
					    	</div>
					    	<div class="box-body">
					    		<?php
		                            // necessary for update action.
		                            if (!$struktur_data->isNewRecord) {
		                                echo Html::activeHiddenInput($struktur_data, "[{$index}]id");
		                            }
		                        ?>
					    		<div class="row">
					    			<div class="col-md-3">
					    				<?= $form->field($struktur_data, "[{$index}]nama_struktur")->textInput() ?>
					    			</div>
					    			<div class="col-md-3">
					    				<?= $form->field($struktur_data, "[{$index}]parameter")->textInput() ?>
					    			</div>
					    			<div class="col-md-3">
					    				<?= $form->field($struktur_data, "[{$index}]relasi")->dropDownList(['0' => 'Tidak', '1' => 'Ya']) ?>
					    			</div>
					    			<div class="col-md-3">
					    				<?= $form->field($struktur_data, "[{$index}]grafik")->dropDownList(
					    					[
					    						'tidak' => 'Tidak', 
					    						'x' => 'X',
					    						'y' => 'Y'
					    					]) ?>
					    			</div>
					    		</div>
					    	</div>
					    </div>
				    <?php endforeach ?>
				</div>
			    

			    <?php DynamicFormWidget::end() ?>

			     <div class="form-group">
			        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
			        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Batal</button>
			    </div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>