<?php 
use app\models\CustomTreeTrait;
use app\models\TreeTrait;
use yii\helpers\Html;
use yii\httpclient\Client;

Yii::$app->session->set('getNewTreeId', $node->id);

$parameter_api = $node->parameterApi;
$strukturData = (!empty($parameter_api)) ? $parameter_api->strukturData : null;


// $client = new Client(['baseUrl' => $node->category->aplikasi->url_aplikasi]);
// $clientResponse = $client->get([$node->parameterApi->parameter_api => $node->slug_new_tree])
// ->setMethod('GET')
// ->setUrl($node->category->aplikasi->url_aplikasi,['data'=>'pegawai-golongan'])
 // ->setUrl('http://data.jatengprov.go.id/apps/api/example/sds', ['gol' => $gol])
// ->send();

//get api
// $node->category->aplikasi->url_aplikasi
// dd();

// $data = $clientResponse->data;

// $count = count($data);

// dd($node);
?>

<div class="new-tree-view">
	<?= $form->field($node, 'tree_id')->hiddenInput(['value' => Yii::$app->session->get('new_tree_id')])->label(false) ?>
	<?php //$node->getParameter(null,null,null) ?>
	<?= $form->field($node, 'status')->dropDownList($node->list_status) ?>

    <?= $form->field($node, 'deskripsi')->textArea() ?>

    <?php if (!empty($parameter_api)): ?>
    	<?= $form->field($parameter_api, 'parameter_api')->textinput([
    		'value' => $parameter_api->parameter_api,
    		'readonly' => 'readonly',
    	]) ?>

    	<h3>Struktur Data</h3>
    	
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama Struktur</th>
					<th>Parameter</th>
					<!-- <th>Tipe Data</th> -->
				</tr>
			</thead>
			<tbody>
				<?php foreach ($strukturData as $key => $value): ?>
					<tr>
						<td><?= $key +1 ?></td>
						<td><?= $value->nama_struktur ?></td>
						<td><?= $value->parameter ?></td>
						<td><?php //$value->tipe_data ?></td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>

		<!-- <h3>Data Api</h3>
		<table class="table table-bordered">
			<thead>
				<tr> -->
					<?php //foreach ($strukturData as $strutur_data): ?>
						<!-- <th><?php //$strutur_data->nama_struktur ?></th>	 -->
					<?php //endforeach ?>
					
				<!-- </tr>
			</thead>
			<tbody> -->
				<?php //for($i = 0; $i < $count; $i++) { ?>
				<!-- <tr> -->
					<?php //foreach ($strukturData as $values): ?>
						<!-- <td> -->
							<?php //$data[$i][$values->parameter] ?>
						<!-- </td> -->
					<?php //endforeach ?>
					
				<!-- </tr> -->
				<?php //} ?>
			<!-- </tbody> -->
		<!-- </table> -->

    <?php endif ?>
</div>