<?php

use app\models\Setting;
use kartik\tree\Module;
use kartik\tree\TreeView;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

// $session = Yii::$app->session;
// $get = $session->get('slug_tree');
// $session->remove('slug_tree');
// if ($session->has('slug_tree')) {
//     // $session->remove('slug_tree');
//     $session->get('slug_tree');
//     $session->remove('slug_tree');
// }
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\DataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
// dd(Yii::$app->session->get('tree_id'))
$this->title = Yii::t('app', 'Data');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php 
        // Modal::begin([
        //     'header' => '<h4>Tambah Data',
        //     'id' => 'modal',
        //     'size' => 'modal-lg',
        // ]);

        // // $data = $model->id;
        // echo "<div id='modalContent'></div>";
        // Modal::end();
    ?>
    
    <div class="box box-primary">
        <div class="box-header with-border">
            <?php //Html::button('Tambah', [
                 // 'class' => 'btn btn-primary',
                 // 'id' => 'modalButton',
                 // 'value' => Url::to(['update']),
            //]); ?>

            <?= Html::a('Kelola Data', ['new-tree'], ['class' => 'btn btn-primary']); ?>

        </div>
        <div class="box-body">
            <?= 
                TreeView::widget([
                    // single query fetch to render the tree
                    // use the Product model you have in the previous step
                    'query' => $query,
                    'headingOptions' => ['label' => 'Sektor'],
                    'rootOptions' => ['label'=>'<span class="text-primary">Sektor</span>'],
                    // 'topRootAsHeading' => true,
                    'fontAwesome' => false,     // optional
                    'isAdmin' => Setting::find(1)->one()->admin_setting_data,         // optional (toggle to enable admin mode)
                    'iconEditSettings'=> [
                        'show' => 'list',
                        'listData' => [
                            'folder' => 'Folder',
                            'file' => 'File',
                            'mobile' => 'Phone',
                            'bell' => 'Bell',
                        ]
                    ],
                    // 'wrapperTemplate' =>  "{tree}{footer}", 
                    'displayValue' => 1,        // initial display value
                    'softDelete' => false,       // defaults to true
                    'cacheSettings' => [        
                        'enableCache' => true   // defaults to true
                    ],
                    // 'nodeTitle' => 'Kategori',com
                    // 'nodeTitlePlural' => 'Kategori',com

                    'nodeAddlViews' => [
                        Module::VIEW_PART_2 => '@app/views/data/_view',
                        // Module::VIEW_PART_3 => '',
                    ],
                    'nodeActions' => [
                        // Module::NODE_SAVE => Url::to(['/data/index']),
                    ],
                ]);
            ?>
        </div>
    </div>
    
</div>

<?php 

//#w0-nodeform > div:nth-child(14) > div:nth-child(2)



$js = <<< JS
    $(function () {
        $('#modalButton').click(function(){
            $('#modal').modal('show')
            .find('#modalContent')
            .load($(this).attr('value'));
        });
    });
JS;

$this->registerJs($js);

// $style= <<< CSS

   //w0-nodeform > div:nth-child(14) > div:nth-child(2):hidden;

 # CSS;
 # $this->registerCss($style);

?>