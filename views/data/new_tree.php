<?php 

use app\models\Setting;
use kartik\tree\Module;
use kartik\tree\TreeView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Detail Data');
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="new-data">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php 
        Modal::begin([
            'header' => '<h4>Detail Data</h4>',
            'id' => 'modal',
            'size' => 'modal-lg',
        ]);

        echo "<div id='modalContent'></div>";
        Modal::end();
    ?>
	<div class="box box-primary">
		<div class="box-header with-border">
			<?= Html::button('Struktur Data', [
                 'class' => 'btn btn-primary',
                 'id' => 'modalButton',
                 'value' => Url::to(['create']),
            ]); ?>
            <div class="box-tools pull-right">
                <?= Html::a('Kembali', ['index'], ['class'=>'btn btn-warning btn-block']) ?>
                <!-- <button type="button" href="<?= Url::toRoute(['index']); ?>"  class="btn btn-default btn-block">Kembali</button> -->
              </div>
		</div>
		<div class="box-body">
			<?= TreeView::widget([
			    // single query fetch to render the tree
			    // use the Product model you have in the previous step
			    'query' => $query, 
			    'headingOptions' => ['label' => 'Detail Data'],
                    // 'rootOptions' => ['label'=>'<span class="text-primary">Kategori</span>'],
                    // 'topRootAsHeading' => true,
                    'fontAwesome' => false,     // optional
                    'isAdmin' => Setting::find(1)->one()->admin_setting_data,         // optional (toggle to enable admin mode)
                    'iconEditSettings'=> [
                        'show' => 'list',
                        'listData' => [
                            'folder' => 'Folder',
                            'file' => 'File',
                            'mobile' => 'Phone',
                            'bell' => 'Bell',
                        ]
                    ],
                    // 'wrapperTemplate' =>  "{tree}{footer}", 
                    'displayValue' => 1,        // initial display value
                    'softDelete' => false,       // defaults to true
                    'cacheSettings' => [        
                        'enableCache' => true   // defaults to true
                    ],
                    // 'nodeTitle' => 'Kategori',com
                    // 'nodeTitlePlural' => 'Kategori',com

                    'nodeAddlViews' => [
                        Module::VIEW_PART_2 => '@app/views/data/new_tree_view',
                    ],
                    // 'nodeActions' => [
                        // Module::NODE_SAVE => Url::to(['/data/new-tree']),
                    // ],
			]); ?>
		</div>
	</div>
</div>

<?php 
$js = <<< JS
    $(function () {
        $('#modalButton').click(function(){
            $('#modal').modal('show')
            .find('#modalContent')
            .load($(this).attr('value'));
        });
    });
JS;

$this->registerJs($js);

?>