<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Data */

// $this->title = $model->id;
// $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Datas'), 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-view">

    <h1><?php //Html::encode($this->title) ?></h1>

    <p>
        <?php //Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php //Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            // 'class' => 'btn btn-danger',
            // 'data' => [
            //     'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            //     'method' => 'post',
            // ],
        //]) ?>
    </p>

    <?php //DetailView::widget([
        // 'model' => $model,
        // 'attributes' => [
        //     'id',
        //     'tree_id',
        //     'deskripsi',
        //     'status',
        //     'instansi_id',
        //     'created_at',
        //     'updated_at',
        // ],
    //]) ?>

    <?php Yii::$app->session->set('tree_id', $node->id); ?>

    <?php if (!empty($node->datas)): ?>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th style="text-align: center;">No</th>
                        <th style="text-align: center;">Data</th>
                        <th style="text-align: center;">Detail Data</th>
                    </tr>
                </thead>
                <tbody class="container-items">
                    <?php $no =1; ?>
                    <?php foreach ($node->datas as $key => $model): ?>
                        <tr>
                            <td style="text-align: center;"><?= $no; ?></td>
                            <td>
                                <?= $form->field($model, 'nama_data')->textInput(['readonly' => true]) ?>
                                <?= $form->field($model, 'url')->textInput(['readonly' => true]) ?>
                                <?= $form->field($model, 'deskripsi_data')->textArea(['readonly' => true]) ?>
                            </td>
                            <td>
                                <?php if (!empty($model->detailDatas)): ?>
                                    <?php foreach ($model->detailDatas as $key => $value): ?>
                                    
                                        <?= $form->field($value, 'method')->textInput(['readonly' => true]) ?>
                                        <?= $form->field($value, 'parameter')->textInput(['readonly' => true]) ?>
                                        <?= $form->field($value, 'keterangan')->textArea(['readonly' => true]) ?>
                                    <?php endforeach ?>
                                <?php else: ?>
                                    Belum ada data
                                <?php endif ?>
                            </td>
                        </tr>
                    <?php $no++;?>
                    <?php endforeach ?>
                    
                </tbody>
            </table>
        </div>
    
    <?php endif ?>

</div>
