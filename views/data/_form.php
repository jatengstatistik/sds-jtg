<?php
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use wbraganca\dynamicform\DynamicFormWidget;

// $modelData = $node->datas;
?>


<div class="person-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($modelCategory, 'name')->textInput(['maxlength' => true, 'readonly' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($modelData, 'deskripsi')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($modelData, 'status')->dropDownList($modelData->status_list, ['prompt' => 'Pilih']) ?>
        </div>
    </div>

    <div class="padding-v-md">
        <div class="line line-dashed"></div>
    </div>

    <?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper',
        'widgetBody' => '.container-items',
        'widgetItem' => '.house-item',
        'limit' => 10,
        'min' => 1,
        'insertButton' => '.add-house',
        'deleteButton' => '.remove-house',
        'model' => $modelsDataApi[0],
        'formId' => 'dynamic-form',
        'formFields' => [
            'nama_data',
            'deskripsi_data',
            'url',
        ],
    ]); ?>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th style="text-align: center;">Data Api</th>
                <!-- <th>Deskripsi Data</th> -->
                <!-- <th>URL Data</th> -->
                <th style="width: 450px; text-align: center;">Struktur Data</th>
                <th class="text-center" style="width: 90px;">
                    <button type="button" class="add-house btn btn-success btn-xs">Tambah</button>
                </th>
            </tr>
        </thead>
        <tbody class="container-items">
        <?php foreach ($modelsDataApi as $indexDataApi => $modelDataApi): ?>
            <tr class="house-item">
                <?php
                    // necessary for update action.
                    if (!$modelDataApi->isNewRecord) {
                        echo Html::activeHiddenInput($modelDataApi, "[{$indexDataApi}]id");
                    }
                ?>
                <td class="vcenter">
                    <?= $form->field($modelDataApi, "[{$indexDataApi}]nama_api")->textInput(['maxlength' => true]) ?>

                    <?= $form->field($modelDataApi, "[{$indexDataApi}]url_api")->textInput(['maxlength' => true]) ?>

                    <?php //$form->field($modelDataApi, "[{$indexDataApi}]deskripsi_data")->label(true)->textArea() ?>
                </td>

                <td>
                    <?php 
                    // echo $this->render('_detailData', [
                    //      'form' => $form,
                    //      'modelDataApi' => $modelDataApi,
                    //      'indexDataApi' => $indexDataApi,
                    //      'modelsDetailData' => $modelsDetailData[$indexDataApi],
                    // ]); 
                    ?>
                    <?php DynamicFormWidget::begin([
                        'widgetContainer' => 'dynamicform_inner',
                        'widgetBody' => '.container-rooms',
                        'widgetItem' => '.room-item',
                        'limit' => 5,
                        'min' => 1,
                        'insertButton' => '.add-room',
                        'deleteButton' => '.remove-room',
                        'model' => $modelsStrukturData[$indexDataApi][0],
                        'formId' => 'dynamic-form',
                        'formFields' => [
                            'method',
                            'parameter',
                            'keterangan'
                        ],
                    ]); ?>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Struktur Data</th>
                                <th class="text-center">
                                    <button type="button" class="add-room btn btn-success btn-xs">Tambah</button>
                                </th>
                            </tr>
                        </thead>
                        <tbody class="container-rooms">
                        <?php foreach ($modelsStrukturData[$indexDataApi] as $indexStrukturData => $modelStrukturData): ?>
                            <tr class="room-item">
                                <td class="vcenter">
                                    <?php
                                        // necessary for update action.
                                        if (! $modelStrukturData->isNewRecord) {
                                            echo Html::activeHiddenInput($modelStrukturData, "[{$indexDataApi}][{$indexStrukturData}]id");
                                        }
                                    ?>
                                    <?= $form->field($modelStrukturData, "[{$indexDataApi}][{$indexStrukturData}]nama_struktur")->label(true)->textInput(['maxlength' => true]) ?>

                                    <?= $form->field($modelStrukturData, "[{$indexDataApi}][{$indexStrukturData}]parameter")->label(true)->textInput(['maxlength' => true]) ?>

                                    <?= $form->field($modelStrukturData, "[{$indexDataApi}][{$indexStrukturData}]tipe_data")->label(true)->textInput(['maxlength' => true]) ?>
                                </td>
                                <td class="text-center vcenter" style="width: 90px;">
                                    <button type="button" class="remove-room btn btn-danger btn-xs">Hapus</button>
                                </td>
                            </tr>
                         <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?php DynamicFormWidget::end(); ?>
                </td>
                <td class="text-center vcenter" style="width: 90px; verti">
                    <button type="button" class="remove-house btn btn-danger btn-xs">Hapus</button>
                </td>
            </tr>
         <?php endforeach; ?>
        </tbody>
    </table>
    <?php DynamicFormWidget::end(); ?>
    
    <div class="form-group">
        <?php echo Html::submitButton('Simpan', ['class' => 'btn btn-primary']) ?>
        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Batal</button>
    </div>

    <?php ActiveForm::end(); ?>

</div>