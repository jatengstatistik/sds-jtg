<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Data */

// $this->title = Yii::t('app', 'Update Data: ' . $model->id, [
    // 'nameAttribute' => '' . $model->id,
// ]);
// $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Datas'), 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
// $this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="data-update">

    <h1><?php //Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'tree_id' => $tree_id,
        'modelData' => $modelData,
        'modelCategory' => $modelCategory,
        'modelsDataApi'  => $modelsDataApi,
        'modelsStrukturData' => $modelsStrukturData,
    ]) ?>

</div>
