<?php

use app\models\Aplikasi;
use app\models\Instansi;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;


$aplikasi = Aplikasi::find()->asArray()->all();

$list_aplikasi = ArrayHelper::map($aplikasi, 'id', 'nama_aplikasi');

/* @var $this yii\web\View */
/* @var $model app\models\Data */

// $this->title = $model->id;
// $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Datas'), 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
$session->set('slug_tree', $node->slug);
// $session->set('tree_id', $node->id);

// $model = $node->aplikasi;
    
// die();
?> 
<?php //Yii::$app->session->set('tree_id', $node->id); ?>

<div class="aplikasi-view">
    <?= $form->field($node, 'aplikasi_id')->widget(Select2::className(), [
        'data' => $list_aplikasi,
        'options' => ['placeholder' => 'Pilih'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('Aplikasi') ?>

    <?= $form->field($node, 'status')->dropDownList($node->list_status) ?>

    <?= $form->field($node, 'deskripsi')->textArea() ?>
    
</div>


<!-- <div class="data-view"> -->

    <!-- <h1><?php //Html::encode($this->title) ?></h1> -->

    <!-- <p> -->
        <?php //Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php //Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            // 'class' => 'btn btn-danger',
            // 'data' => [
            //     'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            //     'method' => 'post',
            // ],
        //]) ?>
    <!-- </p> -->

    <?php //DetailView::widget([
        // 'model' => $model,
        // 'attributes' => [
        //     'id',
        //     'tree_id',
        //     'deskripsi',
        //     'status',
        //     'instansi_id',
        //     'created_at',
        //     'updated_at',
        // ],
    //]) ?>

    

    <?php //if (!empty($node->datas)): ?>
        <!-- <div class="row">
            <div class="col-md-6"> -->
                <?php //$form->field($model, 'deskripsi')->textInput(['readonly' => true]) ?>
            <!-- </div>
            <div class="col-md-6"> -->
                <?php //if ($model->status === '1'): ?>
                    <?php //$form->field($model, 'status')->textInput(['readonly' => true, 'value'=> 'Aktif']) ?>
                <?php //else: ?>
                    <?php //$form->field($model, 'status')->textInput(['readonly' => true, 'value'=> 'Tidak Aktif']) ?>
                <?php //endif ?>
            <!-- </div>
        </div> -->
        
    <?php //endif ?>

    <?php // if (!empty($node->datas->dataApi)): ?>
        <!-- <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th style="text-align: center;">No</th>
                        <th style="text-align: center;">Data</th>
                        <th style="text-align: center;">Detail Data</th>
                    </tr>
                </thead>
                <tbody class="container-items"> -->
                    <?php //$no =1; ?>
                    <?php //foreach ($node->datas->dataApi as $key => $dataApi): ?>
                        <!-- <tr> -->
                            <!-- <td style="text-align: center;"><?php //$no; ?></td>
                            <td> -->
                                <?php //$form->field($dataApi, 'nama_api')->textInput(['readonly' => true]) ?>
                                <?php //$form->field($dataApi, 'url_api')->textInput(['readonly' => true]) ?>
                                <?php //$form->field($dataApi, 'deskripsi_data')->textArea(['readonly' => true]) ?>
                            <!-- </td>
                            <td> -->
                                <?php //if (!empty($dataApi->strukturDatas)): ?>
                                    <?php //foreach ($dataApi->strukturDatas as $key => $value): ?>
                                    
                                        <?php //$form->field($value, 'nama_struktur')->textInput(['readonly' => true]) ?>
                                        <?php //$form->field($value, 'parameter')->textInput(['readonly' => true]) ?>
                                        <?php //$form->field($value, 'tipe_data')->textArea(['readonly' => true]) ?>
                                    <?php //endforeach ?>
                                <?php //else: ?>
                                    <!-- Belum ada data -->
                                <?php //endif ?>
                            <!-- </td>
                        </tr> -->
                    <?php //$no++;?>
                    <?php //endforeach ?>
                    
                <!-- </tbody>
            </table>
        </div> -->
    
    <?php // endif ?>

    

<!-- </div> -->