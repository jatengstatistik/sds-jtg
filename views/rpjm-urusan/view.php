<?php

use hscstudio\mimin\components\Mimin;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RpjmUrusan */

$this->title = $model->nama_urusan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rpjm Urusans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="rpjm-urusan-view">

    <p>
        <?= ((Mimin::checkRoute($this->context->id.'/index',true))) ? Html::a(Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-warning']) : null ?>
        <?= ((Mimin::checkRoute($this->context->id.'/update',true))) ? Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : null ?>
        <?= ((Mimin::checkRoute($this->context->id.'/index',true))) ? Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) : null ?>
    </p>

    <div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
        <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body table-responsive">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'id',
                    'nama_urusan',
                    // 'slug_urusan',
                    'created_at:datetime',
                    'updated_at:datetime',
                    [
                        'label' => 'Dibuat Oleh',
                        'attribute' => 'dibuat',
                        // 'value' => 'dibuat.nama_lengkap',
                        'format' => 'raw',
                        'value' => function ($data){
                            // return Html::a($data->file_name,['view', 'id' => $data->id]);
                            return $data->createdBy->nama_lengkap;
                        },
                    ],
                    [
                        'label' => 'Diupdate Oleh',
                        'attribute' => 'diupdate',
                        // 'value' => 'diupdate.nama_lengkap',
                        'format' => 'raw',
                        'value' => function ($data){
                            // return Html::a($data->file_name,['view', 'id' => $data->id]);
                            return $data->updatedBy->nama_lengkap;
                        },
                    ],
                ],
            ]) ?>
        </div>
    </div> 

</div>
