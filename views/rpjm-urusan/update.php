<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RpjmUrusan */

$this->title = Yii::t('app', 'Update Rpjm Urusan: {name}', [
    'name' => $model->nama_urusan,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rpjm Urusans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="rpjm-urusan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
