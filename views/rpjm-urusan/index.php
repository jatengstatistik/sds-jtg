<?php

use hscstudio\mimin\components\Mimin;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\RpjmUrusanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Data Urusan');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rpjm-urusan-index">
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= ((Mimin::checkRoute($this->context->id.'/create',true))) ? Html::a(Yii::t('app', 'Create Rpjm Urusan'), ['create'], ['class' => 'btn btn-success']) : null ?>
        <?= ((Mimin::checkRoute($this->context->id.'/import',true))) ? Html::a(Yii::t('app', 'Import Data Urusan'), ['import'], ['class' => 'btn btn-warning']) : null ?>
    </p>

    <div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
        <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body table-responsive">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    // 'id',
                    'nama_urusan',
                    // 'slug_urusan',
                    'created_at:datetime',
                    'updated_at:datetime',
                    // 'created_by',
                    // 'updated_by',

                    // ['class' => 'yii\grid\ActionColumn'],
                    [
                      'class' => 'yii\grid\ActionColumn',
                      'template' => Mimin::filterActionColumn([
                          'view','update','delete'
                      ],$this->context->route),
                    ],
                ],
            ]); ?>
        </div>
    </div> 

    
    <?php Pjax::end(); ?>
</div>
