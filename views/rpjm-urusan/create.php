<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RpjmUrusan */

$this->title = Yii::t('app', 'Create Rpjm Urusan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rpjm Urusans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rpjm-urusan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
