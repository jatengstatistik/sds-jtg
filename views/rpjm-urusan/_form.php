<?php

use hscstudio\mimin\components\Mimin;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RpjmUrusan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rpjm-urusan-form">

    <div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
        <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body table-responsive">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'nama_urusan')->textInput(['maxlength' => true]) ?>

            <!-- <?= $form->field($model, 'slug_urusan')->textInput(['maxlength' => true]) ?> -->

            <!-- <?= $form->field($model, 'created_at')->textInput() ?> -->

            <!-- <?= $form->field($model, 'updated_at')->textInput() ?> -->

            <!-- <?= $form->field($model, 'created_by')->textInput() ?> -->

            <!-- <?= $form->field($model, 'updated_by')->textInput() ?> -->

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                <?= ((Mimin::checkRoute($this->context->id.'/index',true))) ? Html::a(Yii::t('app', 'Kembali'), ['index'], ['class' => 'btn btn-warning']) : null ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div> 

</div>
