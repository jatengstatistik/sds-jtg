<?php
/* @var $this yii\web\View */
use app\models\Sector;
use kartik\tree\Module;
use miloschuman\highcharts\Highcharts;

use kartik\tree\TreeView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\widgets\ActiveForm;


$this->title = Yii::t('app', 'Dashboard');
$this->params['breadcrumbs'][] = $this->title;


?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="box box-primary">
    <div class="box-header with-border">
        Data
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <?php $form = ActiveForm::begin(['method' => 'GET','action' => array('site/index')]); ?>
                <?php 
                    echo \kartik\tree\TreeViewInput::widget([
                        'name' => 'data',
                        'value' => 'false', // preselected values
                        'query' => $query,
                        'headingOptions' => ['label' => $this->title],
                        'rootOptions' => ['label'=>'<i class="fa fa-tree text-success"></i>'],
                        'fontAwesome' => true,
                        'asDropdown' => true,
                        'multiple' => false,
                        'options' => ['disabled' => false,'onchange'=>'this.form.submit()']
                    ]);
                ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php if (Yii::$app->request->get()): ?>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h4><?= $title ?></h4>
            <p><?= $deskripsi ?></p>
            <p>Instansi: <?= $instansi ?></p>
            <div class="box-tools pull-right">
                <br>
                <p><?php echo "Update : ".date('d M Y H:i:s',time()); ?></p>
            </div>
        </div>
        
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                        <?php foreach ($strukturData as $key => $value): ?>
                            <th><?= $value->nama_struktur?></th>
                        <?php endforeach ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i = 0; $i < $countClientResponse; $i++){ ?>
                            <tr>
                                <?php foreach ($strukturData as $key => $struktur_data): ?>
                                <td>
                                    <?php if ($struktur_data->relasi == '1'): ?>
                                        <?php ActiveForm::begin(['method' => 'GET']) ?>
                                        <button type="submit" class="btn btn-link" name="<?= $struktur_data->parameter ?>" value="<?= $clientResponse[$i][$struktur_data->parameter] ?>" >
                                            <?= $clientResponse[$i][$struktur_data->parameter] ?>
                                        </button>
                                        <?php ActiveForm::end() ?>
                                    <?php else: ?>
                                        <?= $clientResponse[$i][$struktur_data->parameter] ?>                                
                                    <?php endif ?>
                                </td>
                                <?php 
                                    if ($struktur_data->grafik == 'x') {
                                        $x[] = $clientResponse[$i][$struktur_data->parameter];
                                    }elseif($struktur_data->grafik == 'y'){
                                        $y[] = (float) $clientResponse[$i][$struktur_data->parameter];
                                    }
                                ?>
                                <?php endforeach ?>
                            </tr>
                       <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php if ($strukturData[0]->grafik != 'tidak' && $strukturData[0]->grafik != null): ?>
        <?=
            Highcharts::widget([
               'options' => [
                    'title' => ['text' => $title],
                    'xAxis' => [
                     'categories' => $x
                    ],
                    'yAxis' => [
                     'title' => ['text' => $title]
                    ],
                    'series' => [
                     [
                        // 'type' => 'bar',
                        'name' => $title,
                        'data' => $y
                    ],
                     // ['name' => 'John', 'data' => [5, 7, 3]]
                    ],
               ],
            ]);
        ?>
    <?php endif ?>
    
<?php endif ?>

<?php 
// dd(settype($y, "integer"));
// echo "<pre>";
// print_r();die();
// settype($n, 'string');


// $toInt = array_map('intval', $y);
// if ($strukturData[0]->grafik != 'tidak') {
    
// }




?>
<!-- <div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div>
</div> -->
