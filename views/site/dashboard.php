<?php
/* @var $this yii\web\View */
use app\models\Sector;
use kartik\tree\Module;
use miloschuman\highcharts\Highcharts;

use kartik\tree\TreeView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\widgets\ActiveForm;

use hscstudio\mimin\components\Mimin;
use yii\grid\GridView;
use yii\widgets\Pjax;


$this->title = Yii::t('app', 'Dashboard Data');
$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="box box-primary">
    <div class="box-header with-border">
        Data
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-3">
                <?php $form = ActiveForm::begin(['method' => 'GET','action' => array(Yii::$app->requestedRoute)]); ?>
                <?php 
                    echo \kartik\tree\TreeViewInput::widget([
                        'name' => 'category',
                        'value' => 'false', // preselected values
                        'query' => $query,
                        'headingOptions' => ['label' => $this->title],
                        'rootOptions' => ['label'=>'<i class="fa fa-tree text-success"></i>'],
                        'fontAwesome' => true,
                        'asDropdown' => false,
                        'multiple' => false,
                        'options' => ['disabled' => false,'onchange'=>'this.form.submit()']
                    ]);
                ?>
            </div>
            <div class="col-md-9">
            	<?php if (Yii::$app->request->get('category')): ?>
            	    <div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
		        <div class="box-header with-border">
		          <h3 class="box-title"><?= Html::encode($title) ?></h3>
		          <div class="box-tools pull-right">
		            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		            </button>
		          </div>
		        </div>
		        <div class="box-body table-responsive">
		            <?= GridView::widget([
		                'dataProvider' => $dataProvider,
		                // 'filterModel' => $searchModel,
		                'columns' => [
		                    ['class' => 'yii\grid\SerialColumn'],

		                    // 'id',
		                    // 'title',
		                    [
		                        // 'label' => 'Indikator',
		                        'attribute' => 'title',
		                        // 'value' => 'organisasi.nama_organisasi',
		                        'format' => 'raw',
		                        'value' => function ($data){
		                        	
		                            return Html::a($data->title,[Yii::$app->controller->action->id,
		                            	'category' => Yii::$app->request->get('category'), 
		                            	'data' =>(null !== Yii::$app->request->get('data')) ? Yii::$app->request->get('data') : $data->id,
		                            	'resource' =>(null == Yii::$app->request->get('resource') && Yii::$app->request->get('data')) ? $data->id  : Yii::$app->request->get('resource') ,
		                            ]);
		                            // return $data->createdBy->nama_lengkap;
		                        },
		                    ],            
		                    // 'year',
		                    [
		                        // 'label' => 'Instansi',
		                        'attribute' => 'instansi_id',
		                        // 'value' => 'instansi.nama_instansi',
		                        'visible' => (null !== Yii::$app->request->get('data')) ? false : true,
		                        'format' => 'raw',
		                        'value' => function ($data){
		                        	return (null == Yii::$app->request->get('data')) ? $data->instansi->nama_instansi : '';
		                        },

		                    ],           
							[
		                        // 'label' => 'Tahun',
		                        'attribute' => 'year',
		                        // 'value' => 'instansi.nama_instansi',
		                        'visible' => (null == Yii::$app->request->get('data')) ? false : true,
		                        // 'format' => 'raw',
		                        // 'value' => function ($data){
		                        // 	return (null == Yii::$app->request->get('data')) ? $data->instansi->nama_instansi : '';
		                        // },

		                    ],  
		                    // 'instansi_id',
		                    // 'api_client_id',
		                    // 'uploaded_file_id',
		                    'created_at:datetime',
		                    //'updated_at',
		                    //'created_by',
		                    //'updated_by',
		                    //'deleted_by',
		                    //'lock',

		                    // ['class' => 'yii\grid\ActionColumn'],
		                    // [
		                    //   'class' => 'yii\grid\ActionColumn',
		                    //   'template' => Mimin::filterActionColumn([
		                    //       // 'view','update','delete'
		                    //     'update','delete'
		                    //   ],$this->context->route),
		                    //   'buttons'=>[
		                    //       'update'=>function ($url, $model) {
		                    //           return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update','id'=>$model->id], ['class' => 'btn btn-warning']);
		                    //       },
		                    //       'delete'=>function ($url, $model) {
		                    //           return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete','id'=>$model->id], ['class' => 'btn btn-danger','','data' => [
		                    //                 'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
		                    //                 'method' => 'post',
		                    //             ]]);
		                    //       },
		                    //   ],
		                    // ],
		                ],
		            ]); ?>
		        </div>
		    </div>
		<?php endif ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php if (Yii::$app->request->get('resource')): ?>
        <?php if (empty($model->uploaded_file_id)): ?>
		    <div class="sdi-api-grid">

		        <?= $this->render('_api-grid', [
		              'model' => $model,
		            ]); ?>

		    </div>
		<?php else: ?>
		    <div class="sdi-data-grid">

		        <?=  $this->render('_data-grid', [
		                'model' => $model,
		            ]); ?>

		    </div>
		<?php endif; ?>

		    <div class="sdi-data-detail">

		        <?= $this->render('_data-detail', [
		                'model' => $model,
		            ]); ?>

		    </div>
    </div>
<?php endif ?>
<?php 
// dd(settype($y, "integer"));
// echo "<pre>";
// print_r();die();
// settype($n, 'string');


// $toInt = array_map('intval', $y);
// if ($strukturData[0]->grafik != 'tidak') {
    
// }




?>
<!-- <div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div>
</div> -->
