<?php 

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
 ?>
<div class="box box-<?php $head = ['warning','info','danger','default','info']; shuffle($head); echo $head[0] ?>">
    <div class="box-header with-border">
      <h3 class="box-title"><?= Html::encode($model->title) ?></h3>
      <div class="box-tools pull-right">
          <?php
    try {
            // echo ExportMenu::widget([
            //     'dataProvider' => $model->getDataProvider(),
            //     'columns' => $model->getFileAttributeGridView(),
            //     'clearBuffers' => true,
            // ]);
          ?>
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
    </div> 
    <div class="box-body table-responsive">
    <?php 
      echo GridView::widget([
          'dataProvider' => $model->getDataProvider(),
          'columns' => $model->getFileAttributeGridView(),
      ]); 
    } catch (\Throwable $throwable) {
      echo("Data Tidak Dapat Ditampailkan... ");
    }

    ?>
    </div>
</div>