<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\AplikasiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Integrasi Data');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="integrasi-data-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <!-- <?= Html::a(Yii::t('app', 'Create Aplikasi'), ['create'], ['class' => 'btn btn-success']) ?> -->
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'Tema',
            // 'Instansi',
            // 'Jenis Data',
            // 'id',
            // 'tree_id',
            [
                'label' => 'Tema',
                'attribute' => 'name',
                // 'value' => 'root.name',
                'value' => function($model){
                    return $model->getRoot($model->root);
                },
            ],
            // 'name',
            [
                'label' => 'Jenis Data',
                'attribute' => 'name',
                'format'=>'raw',
                'value' => function($data){ 
                    return Html::a($data->name, ['/site/dashboard','kv-tree-search'=>'','data'=>$data->id]); 
                }
            ],
            [
                'label' => 'Nama Instansi',
                'attribute' => 'aplikasi_id',
                'value' => 'aplikasi.instansi.nama_instansi',
                
            ],
            [
                'label' => 'Aplikasi',
                'attribute' => 'aplikasi_id',
                'value' => 'aplikasi.nama_aplikasi',
            ],
            // [
            //     'attribute' => 'status',
            //     'value' => function($model){
            //         return $model->getStatus($model->status);
            //     }
            // ],
            'deskripsi',
            // [
            //     'label' => 'Nama Aplikasi',
            //     'attribute' => 'nama_aplikasi',
            //     'format' => 'raw',
            //     'value' => function ($data){
            //         return Html::a($data->nama_aplikasi,['view', 'id' => $data->id]);
            //     },
            // ],
            // 'url_aplikasi',
            // 'slug_nama_aplikasi',
            // 'username',
            //'password',
            //'token',
            // [
            //     'label' => 'Instansi',
            //     'attribute' => 'instansi_id',
            //     'value' => 'instansi.nama_instansi',
            // ],
            //'created_at',
            //'updated_at',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>