<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\AplikasiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="integrasi-data-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= // $form->field($model, 'tree_id') ?>

    <?= $form->field($model, 'nama_aplikasi') ?>

    <?= $form->field($model, 'url_aplikasi') ?>

    <?= // $form->field($model, 'slug_nama_aplikasi') ?>

    <?= // $form->field($model, 'username') ?>

    <?= $form->field($model, 'instansi') ?>

    <?php // echo $form->field($model, 'password') ?>

    <?php // echo $form->field($model, 'token') ?>

    <?php // echo $form->field($model, 'instansi_id') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
