<?php

namespace app\controllers;

use Yii;
use app\models\ApiBody;
use app\models\ApiClient;
use app\models\ApiData;
use app\models\ApiHeader;
use app\models\ApiQuery;
use app\models\FileModel;
use app\models\Model;
use app\models\SdiData;
use app\models\SdiDataSearch;
use app\models\SdiResource;
use app\models\SdiResourceSearch;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * SdiDataController implements the CRUD actions for SdiData model.
 */
class SdiDataController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ApiSdiData models.
     * @return mixed
     */
    public function actionApiIndex()
    {
        $searchModel = new SdiDataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider->query->select(['title','name','created_at']);

        return $this->asJson([
            'rows' => $dataProvider->getTotalCount(),
            'data' => $dataProvider->getModels(),
        ]);
        // return $this->render('index', [
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        // ]);
    }

    /**
     * Lists all SdiData models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SdiDataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SdiData model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionApiView($name)
    {
        $model = $this->findModel(['name'=>$name]);

        if (empty($model->uploaded_file_id)):
            $api = [
                'rows' => $model->getApiProvider()->getTotalCount(),
                'data' => $model->getApiProvider()->getModels(),
            ];
        else:
            $api = [
                'rows' => $model->getDataProvider()->getTotalCount(),
                'data' => $model->getDataProvider()->getModels(),
            ];
        endif;            

        return $this->asJson($api);
        // echo '<pre>';
        // print_r();
        // print_r($model->dataProvider->getModels());
        // exit();
        // echo '</pre>';

        // return $this->render('view', [
        //     'model' => $model,
        // ]);
    }

    /**
     * Displays a single SdiData model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $searchModelResource = new SdiResourceSearch();
        $dataProviderResource = $searchModelResource->search(Yii::$app->request->queryParams);
        $dataProviderResource->query->where(['sdi_data_id'=>$id]);

        // echo '<pre>';
        // // print_r();
        // print_r($model->resources);
        // exit();
        // echo '</pre>';

        return $this->render('view', [
            'model' => $model,
            'searchModelResource' => $searchModelResource,
            'dataProviderResource' => $dataProviderResource,
        ]);
    }

    /**
     * Creates a new SdiData model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SdiData();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
            // echo '<pre>';
            // print_r($file);
            // print_r($model->getErrors());
            // echo '</pre>';
            // // exit();
            // Yii::$app->session->setFlash('danger', 'Data Gagal Tersimpan...!');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SdiData model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
            // return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SdiData model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        // return $this->redirect(['index']);
        // return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));
        return $this->redirect(Yii::$app->request->referrer ? : ['index']);
    }

    /**
     * Finds the SdiData model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SdiData the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SdiData::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
