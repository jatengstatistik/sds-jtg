<?php

namespace app\controllers;

use Yii;
use app\models\DataDasar;
use app\models\DataDasarSearch;
use app\models\RpjmAnalisa;
use app\models\RpjmCapaian;
use app\models\RpjmCapaianSearch;
use app\models\RpjmIndikator;
use app\models\RpjmIndikatorSearch;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * RpjmIndikatorController implements the CRUD actions for RpjmIndikator model.
 */
class RpjmIndikatorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RpjmIndikator models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RpjmIndikatorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['created_at'=>SORT_DESC]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RpjmIndikator model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $modelAnalisa = RpjmAnalisa::find()->where(['indikator_id'=>$id])->one();
        $searchModelCapaian = new RpjmCapaianSearch();
        $dataProviderCapaian = $searchModelCapaian->search(Yii::$app->request->queryParams);
        $dataProviderCapaian->query->where(['indikator_id'=>$id]);
        $dataProviderCapaian->query->orderBy('tahun_data');

        return $this->render('view', [
            'model' => $this->findModel($id),
            'modelAnalisa' => $modelAnalisa,
            'searchModelCapaian' => $searchModelCapaian,
            'dataProviderCapaian' => $dataProviderCapaian,
        ]);
    }

    /**
     * Creates a new RpjmIndikator model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id='')
    {
        $searchModelDataDasar = new DataDasarSearch();
        $dataProviderDataDasar = $searchModelDataDasar->search(Yii::$app->request->queryParams);
        $dataProviderDataDasar->pagination = ['pageSize' => 5];

        $model = new RpjmIndikator();
        $modelCapaian = new RpjmCapaian();
        $modelAnalisa = new RpjmAnalisa();
        
        if (!empty($id)) {
            $detailProviderDataDasar = DataDasar::findOne($id);    
        }else{
            $detailProviderDataDasar = DataDasar::find()->indexBy('id')->one();
            // echo '<pre>';
            // print_r($detailProviderDataDasar);
            // echo '</pre>';
        }        
        
        if (Yii::$app->request->post()) {
            $indikator = RpjmIndikator::find()->where(['indikator'=>Yii::$app->request->post('indikator')])->one();
            // echo '<pre>';
            // print_r(empty($indikator));
            // echo '</pre>';
            if (empty($indikator)) {
                // Insert Data Indikator dan Capaian
                $model->organisasi_id = Yii::$app->request->post('organisasi_id');
                $model->urusan_id = Yii::$app->request->post('urusan_id');
                $model->indikator = Yii::$app->request->post('indikator');
                $model->satuan = Yii::$app->request->post('satuan');
                if ($model->save()) {
                    // Table Capaian && Table Analisa
                    $modelAnalisa->indikator_id = $model->id;
                    $modelCapaian->indikator_id = $model->id;
                    $modelCapaian->tahun_data = Yii::$app->request->post('tahun_data');
                    $modelCapaian->target_rkpd = Yii::$app->request->post('target_rkpd');
                    $modelCapaian->target_dppa = Yii::$app->request->post('target_dppa');
                    $modelCapaian->capaian = Yii::$app->request->post('capaian');
                    if ($modelCapaian->save() && $modelAnalisa->save()) {
                        return $this->redirect(['view', 'id' => $model->id]);    
                    }else{
                        // Error Capaian
                        foreach ($modelCapaian->getErrors() as $errorMsg) {
                            Yii::$app->session->setFlash('error',$errorMsg);   
                        }

                        // Error Analisa
                        foreach ($modelAnalisa->getErrors() as $errorMsg) {
                            Yii::$app->session->setFlash('error',$errorMsg);   
                        }
                        return $this->redirect(Yii::$app->request->referrer);
                    }
                }else{
                    foreach ($model->getErrors() as $errorMsg) {
                        Yii::$app->session->setFlash('error',$errorMsg);   
                    }
                    return $this->redirect(Yii::$app->request->referrer);
                }    
            }else{
                // Update Data Capaian
                // cek berdasar indikator dan tahun data
                $capaian = RpjmCapaian::find()->where([
                    'indikator_id'=> $indikator->id,
                    'tahun_data'=>Yii::$app->request->post('tahun_data')])->one();
                if (empty($capaian)) {
                    // Insert Baru Data Capaian
                    $modelCapaian->indikator_id = $indikator->id;
                    $modelCapaian->tahun_data = Yii::$app->request->post('tahun_data');
                    $modelCapaian->target_rkpd = Yii::$app->request->post('target_rkpd');
                    $modelCapaian->target_dppa = Yii::$app->request->post('target_dppa');
                    $modelCapaian->capaian = Yii::$app->request->post('capaian');
                    if ($modelCapaian->save()) {
                        return $this->redirect(['view', 'id' => $indikator->id]);    
                    }else{
                        foreach ($modelCapaian->getErrors() as $errorMsg) {
                            Yii::$app->session->setFlash('error',$errorMsg);   
                        }
                        return $this->redirect(Yii::$app->request->referrer);
                    }
                }else{
                    // Update data capaian
                    $capaian->tahun_data = Yii::$app->request->post('tahun_data');
                    $capaian->target_rkpd = Yii::$app->request->post('target_rkpd');
                    $capaian->target_dppa = Yii::$app->request->post('target_dppa');
                    $capaian->capaian = Yii::$app->request->post('capaian');
                    if ($capaian->save()) {
                        return $this->redirect(['view', 'id' => $indikator->id]);    
                    }else{
                        foreach ($capaian->getErrors() as $errorMsg) {
                            Yii::$app->session->setFlash('error',$errorMsg);   
                        }
                        return $this->redirect(Yii::$app->request->referrer);
                    }
                }
            }
            
        }

        return $this->render('create', [
            'model' => $model,
            'searchModelDataDasar' => $searchModelDataDasar,
            'dataProviderDataDasar' => $dataProviderDataDasar,
            'detailProviderDataDasar' => $detailProviderDataDasar,
        ]);
    }

    /**
     * Updates an existing RpjmIndikator model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionUpdate($id)
    // {
    //     $model = $this->findModel($id);

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     }

    //     return $this->render('update', [
    //         'model' => $model,
    //     ]);
    // }

    /**
     * Deletes an existing RpjmIndikator model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RpjmIndikator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RpjmIndikator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RpjmIndikator::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
