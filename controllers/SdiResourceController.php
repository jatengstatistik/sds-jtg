<?php

namespace app\controllers;

use Yii;
use app\models\Model;
use app\models\ApiBody;
use app\models\ApiClient;
use app\models\ApiData;
use app\models\ApiHeader;
use app\models\ApiQuery;
use app\models\ApiResource;
use app\models\ExcelResource;
use app\models\SdiResource;
use app\models\SdiResourceSearch;
use app\models\Setting;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

/**
 * SdiResourceController implements the CRUD actions for SdiResource model.
 */
class SdiResourceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SdiResource models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SdiResourceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SdiResource model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if (empty($model->uploaded_file_id)):
            return $this->render('view', [
                'model' => $this->findModelApi($id),
            ]);
        else:
            return $this->render('view', [
                'model' => $this->findModelFile($id),
            ]);
        endif;


    }

    /**
     * File Creates a new Sdiresource model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateFile($sdi_data_id)
    {
        $model = new ExcelResource();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $file = UploadedFile::getInstance($model, 'file');
            // $fileModel = FileModel::saveAs($file,['uploadPath' => '@runtime/upload/sdi-files']);
            if(!empty($file)){
                $model->sdi_data_id = $sdi_data_id;
                $model->save();
                return $this->redirect(['sdi-data/view', 'id' => $sdi_data_id]);
                // return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));
            }else{
                Yii::$app->session->setFlash('danger', 'Data Gagal Tersimpan...!');
                // echo '<pre>';
                // print_r($file);
                // print_r($model->getErrors());
                // echo '</pre>';
                // // exit();
            }
        }

        return $this->render('create-file', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SdiResource model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdateFile($id)
    {
        $model = $this->findModelFile($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // echo '<pre>';
            // print_r(Yii::$app->request->post());
            // exit();
            // echo '</pre>';
            return $this->redirect(['view', 'id' => $model->id]);
            // return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));
        }

        return $this->render('update-file', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SdiResource model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeleteFile($id)
    {
        $model = $this->findModelFile($id);
        $model->delete();
        return $this->redirect(['sdi-data/view','id'=>$model->sdi_data_id]);
        // return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));
        // return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Creates a new SdiData model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateApi($sdi_data_id)
    {
        $model = new SdiResource;
        $modelApiClient = new ApiClient;
        $modelApiHeader = [new ApiHeader];
        $modelApiQuery = [new ApiQuery];
        $modelApiBody = [new ApiBody];
        $modelApiData = [new ApiData];


        if ($model->load(Yii::$app->request->post())) {
            $model->sdi_data_id = $sdi_data_id;

            $modelApiClient->load(Yii::$app->request->post());
            $modelApiClient->title = $model->title;
            // $modelApiClient->instansi_id = $model->instansi_id;

            // $modelApiClient = Model::createMultiple(ApiClient::classname());
            $modelApiHeader = Model::createMultiple(ApiHeader::classname());
            $modelApiQuery = Model::createMultiple(ApiQuery::classname());
            $modelApiBody = Model::createMultiple(ApiBody::classname());
            $modelApiData = Model::createMultiple(ApiData::classname());
            // Model::loadMultiple($modelApiClient, Yii::$app->request->post());
            Model::loadMultiple($modelApiHeader, Yii::$app->request->post());
            Model::loadMultiple($modelApiQuery, Yii::$app->request->post());
            Model::loadMultiple($modelApiBody, Yii::$app->request->post());
            Model::loadMultiple($modelApiData, Yii::$app->request->post());
            // echo '<pre>';
            // print_r(Yii::$app->request->post());
            // print_r(Yii::$app->request->post()['SdiData']);
            // print_r(Yii::$app->request->post());
            // exit();
            // echo '</pre>';
            // die();
            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelApiHeader),
                    ActiveForm::validateMultiple($modelApiQuery),
                    ActiveForm::validateMultiple($modelApiBody),
                    ActiveForm::validateMultiple($modelApiData),
                    ActiveForm::validate($modelApiClient),
                    ActiveForm::validate($model),
                );
            }

            // validate all models
            // $validModel = $model->validate();
            $validModel = Model::validateMultiple($modelApiHeader) && Model::validateMultiple($modelApiQuery) && Model::validateMultiple($modelApiBody) && Model::validateMultiple($modelApiData) && $modelApiClient->validate() && $model->validate();

            
            
            if ($validModel) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($modelApiClient->save(false)) {

                            
                        foreach ($modelApiHeader as $keyHeader => $modelApiHeaders) {
                            $modelApiHeaders->api_client_id = $modelApiClient->id;
                            // $modelApiHeaders->label = $_POST['ApiHeader'][$keyHeader]['label'];
                            // $modelApiHeaders->attribute = $_POST['ApiHeader'][$keyHeader]['attribute'];
                            // $modelApiHeaders->value = $_POST['ApiHeader'][$keyHeader]['value'];
                            $modelApiHeaders->save(false);
                            if (! ($flag =  $modelApiHeaders->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }

                        foreach ($modelApiQuery as $keyQuery => $modelApiQuerys) {
                            $modelApiQuerys->api_client_id = $modelApiClient->id;
                            // $modelApiQuerys->label = $_POST['ApiQuery'][$keyQuery]['label'];
                            // $modelApiQuerys->attribute = $_POST['ApiQuery'][$keyQuery]['attribute'];
                            // $modelApiQuerys->value = $_POST['ApiQuery'][$keyQuery]['value'];
                            $modelApiQuerys->save(false);
                            if (! ($flag = $modelApiQuerys->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }

                        
                        foreach ($modelApiBody as $keyBody => $modelApiBodys) {

                            $modelApiBodys->api_client_id = $modelApiClient->id;
                            // $modelApiBodys->label = $_POST['ApiBody'][$keyBody]['label'];
                            // $modelApiBodys->attribute = $_POST['ApiBody'][$keyBody]['attribute'];
                            // $modelApiHeaders->value = $_POST['ApiBody'][$keyBody]['value'];
                            $modelApiBodys->save(false);
                            if (! ($flag = $modelApiBodys->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        foreach ($modelApiData as $keyData => $modelApiDatas) {
                            $modelApiDatas->api_client_id = $modelApiClient->id;
                            // $modelApiDatas->label = $_POST['ApiData'][$keyData]['label'];
                            // $modelApiDatas->attribute = $_POST['ApiData'][$keyData]['attribute'];
                            // $modelApiDatas->value = $_POST['ApiData'][$keyData]['value'];
                            $modelApiDatas->save(false);
                            if (! ($flag = $modelApiDatas->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        $model->api_client_id = $modelApiClient->id;
                        if (! ($flag = $model->save(false))) {
                            $transaction->rollBack();
                            // break;
                            exit();
                        }
                    }

                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (\Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('api-create', [
            'model' => $model,
            'modelApiClient' => $modelApiClient,
            'modelApiHeader' => (empty($modelApiHeader)) ? [new ApiHeader] : $modelApiHeader,
            'modelApiQuery' => (empty($modelApiQuery)) ? [new ApiQuery] : $modelApiQuery,
            'modelApiBody' => (empty($modelApiBody)) ? [new ApiBody] : $modelApiBody,
            'modelApiData' => (empty($modelApiData)) ? [new ApiData] : $modelApiData,
        ]);
    }

    /**
     * Updates an existing SdiData model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateApi($id)
    {
        $model = $this->findModelApi($id);
        $modelApiClient = ApiClient::findOne($model->api_client_id);
        $modelApiQuery = $modelApiClient->apiQueries;
        $modelApiHeader = $modelApiClient->apiHeaders;
        $modelApiBody = $modelApiClient->apiBodies;
        $modelApiData = $modelApiClient->apiDatas;

        if ($modelApiClient->load(Yii::$app->request->post())) {

            $model->load(Yii::$app->request->post());

            $oldHeaderIDs = ArrayHelper::map($modelApiHeader, 'id', 'id');
            $modelApiHeader = Model::createMultiple(ApiHeader::classname(), $modelApiHeader);
            Model::loadMultiple($modelApiHeader, Yii::$app->request->post());
            $deletedHeaderIDs = array_diff($oldHeaderIDs, array_filter(ArrayHelper::map($modelApiHeader, 'id', 'id')));

            $oldQueryIDs = ArrayHelper::map($modelApiQuery, 'id', 'id');
            $modelApiQuery = Model::createMultiple(ApiQuery::classname(), $modelApiQuery);
            Model::loadMultiple($modelApiQuery, Yii::$app->request->post());
            $deletedQueryIDs = array_diff($oldQueryIDs, array_filter(ArrayHelper::map($modelApiQuery, 'id', 'id')));

            $oldBodyIDs = ArrayHelper::map($modelApiBody, 'id', 'id');
            $modelApiBody = Model::createMultiple(ApiBody::classname(), $modelApiBody);
            Model::loadMultiple($modelApiBody, Yii::$app->request->post());
            $deletedBodyIDs = array_diff($oldBodyIDs, array_filter(ArrayHelper::map($modelApiBody, 'id', 'id')));

            $oldPageIDs = ArrayHelper::map($modelApiData, 'id', 'id');
            $modelApiData = Model::createMultiple(ApiData::classname(), $modelApiData);
            Model::loadMultiple($modelApiData, Yii::$app->request->post());
            $deletedPageIDs = array_diff($oldPageIDs, array_filter(ArrayHelper::map($modelApiData, 'id', 'id')));

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelApiData),
                    ActiveForm::validateMultiple($modelApiBody),
                    ActiveForm::validateMultiple($modelApiQuery),
                    ActiveForm::validateMultiple($modelApiHeader),
                    ActiveForm::validate($modelApiClient),
                    ActiveForm::validate($model)
                );
            }

            // validate all models
            $valid = $modelApiClient->validate();
            $valid = Model::validateMultiple($modelApiHeader) && Model::validateMultiple($modelApiQuery) && Model::validateMultiple($modelApiBody) && Model::validateMultiple($modelApiData) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $modelApiClient->save(false)) {

                        $model->save();

                        if (! empty($deletedHeaderIDs)) {
                            ApiHeader::deleteAll(['id' => $deletedHeaderIDs]);
                        }
                        if (! empty($deletedQueryIDs)) {
                            ApiQuery::deleteAll(['id' => $deletedQueryIDs]);
                        }
                        if (! empty($deletedBodyIDs)) {
                            ApiBody::deleteAll(['id' => $deletedBodyIDs]);
                        }
                        if (! empty($deletedPageIDs)) {
                            ApiData::deleteAll(['id' => $deletedPageIDs]);
                        }
                        foreach ($modelApiHeader as $modelApiHeaders) {
                            $modelApiHeaders->api_client_id = $modelApiClient->id;
                            if (! ($flag = $modelApiHeaders->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        foreach ($modelApiQuery as $modelApiQuerys) {
                            $modelApiQuerys->api_client_id = $modelApiClient->id;
                            if (! ($flag = $modelApiQuerys->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        foreach ($modelApiBody as $modelApiBodys) {
                            $modelApiBodys->api_client_id = $modelApiClient->id;
                            if (! ($flag = $modelApiBodys->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        foreach ($modelApiData as $modelApiDatas) {
                            $modelApiDatas->api_client_id = $modelApiClient->id;
                            if (! ($flag = $modelApiDatas->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }

                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('api-update', [
            'model' => $model,
            'modelApiClient' => $modelApiClient,
            'modelApiHeader' => (empty($modelApiHeader)) ? [new ApiHeader] : $modelApiHeader,
            'modelApiQuery' => (empty($modelApiQuery)) ? [new ApiQuery] : $modelApiQuery,
            'modelApiBody' => (empty($modelApiBody)) ? [new ApiBody] : $modelApiBody,
            'modelApiData' => (empty($modelApiData)) ? [new ApiData] : $modelApiData,
        ]);
    }

    /**
     * Deletes an existing SdiResource model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeleteApi($id)
    {
        $model = $this->findModelApi($id);
        $model->delete();
        return $this->redirect(['sdi-data/view','id'=>$model->sdi_data_id]);
        // return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));
        // return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPublish($id)
    {
        $setting = Setting::findOne(1);
        $model = $this->findModel($id);
        
        $client = new Client(['baseUrl' => $setting->url_open_data]);

        // Get API license Default Open Data
        $licenses = $client->createRequest()
            ->setMethod('GET')
            ->setUrl('api/3/action/license_list')
            ->send();
        if ($licenses->isOk) {
            $license = $licenses->getData()['result'][5]['id'];
        }else{
            $license = 'cc-by';
        }

        // Get API Organizations By User Open Data
        $organizations = $client->createRequest()
        ->setMethod('GET')
            ->setHeaders([
                'X-CKAN-API-Key' =>Yii::$app->user->identity->api_key_od
            ])
            ->setUrl('api/3/action/organization_list_for_user')
            ->send();
        if ($organizations->isOk) {
            $organization = $organizations->getData()['result'][0]['id'];
        }else{
            // Di Upload Atas Nama Kominfo
            $organization = '482355cb-70d0-415f-9988-dc15cab11791';
        }
        // dd($organization);
        
        // Cek API Package Open Data
        $package = $client->createRequest()
        ->setMethod('GET')
        ->setUrl('api/3/action/package_show')
            ->setHeaders([
                'X-CKAN-API-Key' =>Yii::$app->user->identity->api_key_od
            ])
            ->setData([
                    'id' => $model->sdiData->name,
                    ])
            ->send();
        if ($package->isOk) {
            $package_status['pakage'] = $package->getData()['result']['id'];
            $package_status['resource'] = array_column($package->data['result']['resources'], 'name');
        }else{
            $package_status = false;
        }
        // dd($model->uploadFile);
        try {     
            // Buat Package Create
            // Jika Tidak ada package
            if(!$package_status){
                $package = $client->createRequest()
                ->setMethod('POST')
                ->setFormat(Client::FORMAT_JSON)
                ->setUrl('api/3/action/package_create')
                ->setHeaders([
                    'X-CKAN-API-Key' => Yii::$app->user->identity->api_key_od,
                ])
                ->setData([
                    'title' => $model->sdiData->title,
                    'name' => $model->sdiData->name,
                    'private' => ($model->sdiData->public===1) ? False : True,
                    'author' => Yii::$app->user->identity->nama_lengkap,
                    'license_id' => $license,
                    // 'url' => $url,
                    'owner_org' => $organization,
                ])
                ->send();
            }
            if ($package->isOk) {
                // Buat Resoure dari ID Package yang sudah dibuat sebelumnya
                // echo 'Okee Resoure\n';
                // dd($package->getData()['result']['id']);
                // Yii::$app->session->setFlash('success', 'Data Berhasil di simpan ...');
                $client_resource = new Client([
                    'baseUrl' => $setting->url_open_data,
                    'transport' => 'yii\httpclient\CurlTransport',     
                ]);
                if(!in_array($model->title, $package_status['resource'])){
                    $resource_create = $client_resource->createRequest()
                    ->setFormat(Client::FORMAT_CURL)
                    ->setMethod('POST')
                    ->setUrl('api/3/action/resource_create')
                    ->setHeaders([
                        'X-CKAN-API-Key' => Yii::$app->user->identity->api_key_od,
                    ])
                    ->setData([
                        'package_id' => $package->getData()['result']['id'],
                        'name' => $model->title,
                        'upload' => new \CURLFile($model->uploadFile->filename, $model->uploadFile->type, $model->uploadFile->filename),
                    ])
                    ->send();
                    if ($resource_create->isOk) {
    
                        Yii::$app->session->setFlash('success', 'Data Berhasil Tersimpan...!');
                    }else{
                        Yii::$app->session->setFlash('danger', 'Data Gagal Dibuat...!');
                    }
                }else{
                    Yii::$app->session->setFlash('warning', 'Data Sudah Ada...!');
                }
                
            } else {
                Yii::$app->session->setFlash('danger', 'Data Gagal Dibuat...!');
            }
        } catch (\Throwable $th) {
            Yii::$app->session->setFlash('danger', 'Data Gagal Tersimpan...!');
            // echo $th;
        }
        return $this->redirect(['sdi-data/view','id'=>$model->sdi_data_id]);
        // dd($open_data);

    }

    /**
     * Finds the SdiResource model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SdiResource the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SdiResource::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    protected function findModelFile($id)
    {
        if (($model = ExcelResource::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    protected function findModelApi($id)
    {
        if (($model = ApiResource::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}
