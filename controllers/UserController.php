<?php

namespace app\controllers;

use Yii;
use app\models\AuthInstansiUser;
use app\models\Instansi;
use app\models\User;
use app\models\UserSearch;
use hscstudio\mimin\models\AuthAssignment;
use hscstudio\mimin\models\AuthItem;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	/**
	 * Lists all User models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new UserSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single User model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		$model = $this->findModel($id);
   		$pegawai = $this->findPegawai($id);
		$authAssignments = AuthAssignment::find()->where([
			'user_id' => $model->id,
		])->column();

		$authUserInstansis = AuthInstansiUser::find()->where([
			'user_id' => $model->id,
		])->column();

		$authItems = ArrayHelper::map(
			AuthItem::find()->where([
				'type' => 1,
			])->asArray()->all(),
			'name', 'name');

		$authAssignment = new AuthAssignment([
			'user_id' => $model->id,
		]);

		$authInstansiUser = new AuthInstansiUser([
			'user_id' => $model->id,
		]);

        $instansi = ArrayHelper::map(Instansi::find()->asArray()->all(),'id','nama_instansi');

       
		return $this->render('view', [
			'model' => $model,
			'authAssignment' => $authAssignment,
			'authItems' => $authItems,
			'pegawai' => $pegawai,
			'instansi' => $instansi,
			'authInstansiUser' => $authInstansiUser,
		]);
	}

	public function actionRuleData($id)
	{
		$model = $this->findModel($id);
   		$pegawai = $this->findPegawai($id);
		$authAssignments = AuthAssignment::find()->where([
			'user_id' => $model->id,
		])->column();

		$authUserInstansis = AuthInstansiUser::find()->where([
			'user_id' => $model->id,
		])->column();

		$authItems = ArrayHelper::map(
			AuthItem::find()->where([
				'type' => 1,
			])->asArray()->all(),
			'name', 'name');

		$authAssignment = new AuthAssignment([
			'user_id' => $model->id,
		]);

		$authInstansiUser = new AuthInstansiUser([
			'user_id' => $model->id,
		]);

        $instansi = ArrayHelper::map(Instansi::find()->asArray()->all(),'id','nama_instansi');

        $post = Yii::$app->request->post();
		if ($authAssignment->load($post)&&$authInstansiUser->load($post)) {
			$user_id = $model->id;
			// delete all role
			AuthAssignment::deleteAll(['user_id' => $user_id]);
			if (is_array($authAssignment->item_name)) {
				foreach ($authAssignment->item_name as $item) {
					// if (!in_array($item, $authAssignments)) {
					// if (!in_array($item, $authAssignments)) {
						$authAssignment2 = new AuthAssignment(['user_id' => $user_id]);
						$authAssignment2->item_name = $item;
						$authAssignment2->created_at = time();
						$authAssignment2->save();
					// }
				}
			}
			// End AuthAssignment

			
			// delete all Instasi By User
			AuthInstansiUser::deleteAll(['user_id'=>$user_id]);
			$authInstansiUser3 = new AuthInstansiUser(['user_id' => $user_id]);
			$authInstansiUser3->instansi_id = $model->instansi_id;
			$authInstansiUser3->created_at = time();
			$authInstansiUser3->save();
			if (is_array($authInstansiUser->instansi_id)) {
				foreach ($authInstansiUser->instansi_id as $item2) {
					if (!in_array($item2, $authUserInstansis)) {
						$authInstansiUser2 = new AuthInstansiUser(['user_id' => $user_id]);
						$authInstansiUser2->user_id = $model->id;
						$authInstansiUser2->instansi_id = $item2;
						$authInstansiUser2->created_at = time();
						$authInstansiUser2->save();					
					}
				}
			}
			// End AuthInstansiUser
			Yii::$app->session->setFlash('success', 'Data tersimpan');
		}

		// $authAssignments = AuthAssignment::find()->where(['user_id' => $model->id])->column();
		// $authAssignment->item_name = $authAssignments;
		$authAssignment->item_name = ArrayHelper::map(
			AuthAssignment::find()->where([
				'user_id' => $model->id,
			])->asArray()->all(),
			'item_name', 'item_name');
		// $authInstansiUser->instansi_id = $authUserInstansis;
		$authInstansiUser->instansi_id = ArrayHelper::map(
			AuthInstansiUser::find()->where([
				'user_id' => $model->id,
			])->asArray()->all(),
			'instansi_id', 'instansi_id');

		// echo "<pre>";
		// print_r($pegawai);
		// echo "<pre>";
		// print_r(!empty($pegawaiResponse->content));
		// echo "</pre>";

		// echo "<pre>";
		// print_r($authInstansiUser->instansi_id);
		// print_r(Yii::$app->request->post());
		// echo "</pre>";
		return $this->render('rule-data', [
			'model' => $model,
			'authAssignment' => $authAssignment,
			'authItems' => $authItems,
			'pegawai' => $pegawai,
			'instansi' => $instansi,
			'authInstansiUser' => $authInstansiUser,
		]);
	}

	/**
	 * Creates a new User model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new User();

		if ($model->load(Yii::$app->request->post())) {
			

			$model->setPassword(substr($model->nip, -10));
			$model->status = $model->status==1?10:0;
			// $model->nama_lengkap = $model->getPns($model->nip)->nama;
			// $model->jabatan = $model->getPns($model->nip)->jabatan;
			// preg_replace('/START[\s\S]+?END/', '', $string);
			
			if ($model->save()) {
				Yii::$app->session->setFlash('success', 'User berhasil dibuat dengan password 10 Digit NIP Terakhir');
				return $this->redirect(['view', 'id' => $model->id]);
			} else {
				Yii::$app->session->setFlash('error',$model->getErrors()['nip'][0]);
				return $this->redirect(['index']);
			}

			// echo "<pre>";
			// print_r($model->getPns($model->nip)->nama);
			// print_r(Yii::$app->request->post());
			// echo "</pre>";

			
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing User model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post())) {
			if (!empty($model->new_password)) {
			    $model->setPassword($model->new_password);
			}
			
			$model->status = $model->status==1?10:0;
			// $model->nama_lengkap = $model->getPns($model->nip)->nama;
			// $model->jabatan = $model->getPns($model->nip)->jabatan;

			if ($model->save()) {
			    Yii::$app->session->setFlash('success', 'User berhasil diupdate');
			} else {
			    Yii::$app->session->setFlash('error', 'User gagal diupdate');
			}
			// if(Yii::$app->user->id == $model->id){
			// 	return $this->redirect(['profile']);
			// }else{
			// 	return $this->redirect(['view', 'id' => $model->id]);
			// }
			// return $this->redirect(Yii::$app->request->referrer);
			return $this->goBack(Yii::$app->request->referrer);
		} else {
			$model->status = $model->status==10?1:0;
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing User model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		$authAssignments = AuthAssignment::find()->where([
			'user_id' => $model->id,
		])->all();
		foreach ($authAssignments as $authAssignment) {
			$authAssignment->delete();
		}

		Yii::$app->session->setFlash('success', 'Delete success');
		$model->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the User model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return User the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = User::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	/**
	 * Finds the UserPegawai model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $nip
	 * @return User the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findPegawai($id)
	{
		$user = new User;
		if (($pegawai = $user->getPegawai($id)) !== null) {
			return $pegawai;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
	
	public function actionProfile()
	{
		$id = Yii::$app->user->id;
		$model = $this->findModel($id);
   		$pegawai = $this->findPegawai($id);
		   $authAssignments = AuthAssignment::find()->where([
			   'user_id' => $model->id,
		])->column();

		$authUserInstansis = AuthInstansiUser::find()->where([
			'user_id' => $model->id,
		])->column();

		$authItems = ArrayHelper::map(
			AuthItem::find()->where([
				'type' => 1,
			])->asArray()->all(),
			'name', 'name');

		$authAssignment = new AuthAssignment([
			'user_id' => $model->id,
		]);

		$authInstansiUser = new AuthInstansiUser([
			'user_id' => $model->id,
		]);

        $instansi = ArrayHelper::map(Instansi::find()->asArray()->all(),'id','nama_instansi');

       
		return $this->render('profile', [
			'model' => $model,
			'authAssignment' => $authAssignment,
			'authItems' => $authItems,
			'pegawai' => $pegawai,
			'instansi' => $instansi,
			'authInstansiUser' => $authInstansiUser,
		]);
	}
}
