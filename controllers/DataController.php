<?php

namespace app\controllers;

use Yii;
use app\models\Aplikasi;
use app\models\Category;
use app\models\DynamicForms;
use app\models\Instansi;
use app\models\NewTree;
use app\models\ParameterApi;
use app\models\StrukturData;
use app\models\search\DataSearch;
use yii\base\Model;
use yii\db\Exception;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * DataController implements the CRUD actions for Data model.
 */
class DataController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Data models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $searchModel = new DataSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $query = Category::find()->addOrderBy('root, lft'); 

        // if (Yii::$app->request->post()) {
        //     $category = new Category();

        //     $category->name = $_POST['Category']['name'];    
        //     $category->aplikasi_id = $_POST['Category']['aplikasi_id'];

        //     $category->save();    
        // }

        
        // echo "<pre>";
        // print_r($query->all());die();
        return $this->render('index', [
            'query' => $query,
        ]);
    }

    public function actionNewTree()
    {
        $tree = Category::findOne(['slug' => Yii::$app->session->get('slug_tree')]);

        Yii::$app->session->set('new_tree_id', $tree->id);

        // Yii::$app->session->set('slug_new_tree', $tree->slug_new_tree);
        
        $getTreeId = Yii::$app->session->get('new_tree_id');

        $query = NewTree::find()
        ->where(['tree_id' => $getTreeId])
        ->addOrderBy('root, lft');

        return $this->render('new_tree', [
            'tree' => $tree,
            'query' => $query,
        ]);
    }

    public function actionCreate()
    {
        $new_tree = NewTree::findOne(Yii::$app->session->get('getNewTreeId'));

        $findParameterApi = ParameterApi::findOne(['new_tree_id' => $new_tree->id]);

        $parameter_api = (empty($findParameterApi)) ? new ParameterApi() : $findParameterApi;

        if ($findParameterApi != null) {
            $findStrukturData = StrukturData::find()
            ->where(['parameter_api_id' => $findParameterApi->id])
            ->all();

            $strukturData = $findStrukturData;
        }else{
            $strukturData = [new StrukturData];
        }
        

        // dd($parameter_api);

        if ($parameter_api->load(Yii::$app->request->post())) {

            $oldIDs = ArrayHelper::map($strukturData, 'id', 'id');
            $strukturData = DynamicForms::createMultiple(StrukturData::classname(), $strukturData);
            DynamicForms::loadMultiple($strukturData, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($strukturData, 'id', 'id')));

            // validate all models
            $valid = $parameter_api->validate();
            $valid = Model::validateMultiple($strukturData) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {

                    $parameter_api->new_tree_id = $new_tree->id;
                    $parameter_api->save(false);

                    if ($flag = $parameter_api->save(false)) {

                        if (!empty($deletedIDs)) {
                            StrukturData::deleteAll(['id' => $deletedIDs]);
                        }

                        foreach ($strukturData as $key => $strukturDatas) {
                            $strukturDatas->parameter_api_id = $parameter_api->id;
                            if (! ($flag = $strukturDatas->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(Yii::$app->request->referrer);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }

        }

        return $this->renderAjax('formNewTree',[
            'new_tree' => $new_tree,
            'parameter_api' => $parameter_api,
            'strukturData' => $strukturData,
        ]);
    }

    /**
     * Displays a single Data model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionView($id)
    // {
    //     return $this->render('view', [
    //         'model' => $this->findModel($id),
    //     ]);
    // }

    /**
     * Creates a new Data model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
        // $model = new Data();

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'id' => $model->id]);
        // }

        // return $this->render('create', [
        //     'model' => $model,
        // ]);

        // $query = Category::find()->addOrderBy('root, lft');
        
        // return $this->render('create', [
        //     'query' => $query,
        // ]);
    // }

    /**
     * Updates an existing Data model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionUpdate()
    // {
    //     // retrieve existing Deposit data
    //     $tree_id = Yii::$app->session->get('tree_id');
    //     $modelCategory = Category::findOne($tree_id);


    //     $modelData = Data::findOne(['tree_id' => $tree_id]);
        
    //     if (empty($modelData)) {
    //         $modelData = new Data();
    //     }

    //     // echo "<pre>";
    //     // print_r($modelCategory);die();
    //     // retrieve existing data
    //     $oldDataApiIds = DataApi::find()->select('id')->where(['data_id' => $modelData->id])->asArray()->all();
    //     $oldDataApiIds = ArrayHelper::getColumn($oldDataApiIds,'id');
    //     $modelsDataApi = DataApi::findAll(['id' => $oldDataApiIds]);
    //     $modelsDataApi = (empty($modelsDataApi)) ? [new DataApi] : $modelsDataApi;

    //     // retrieve existing detail data
    //     $oldStrukturDataIds = [];
    //     foreach ($modelsDataApi as $i => $modelDataApi) {
    //         $strukturData = StrukturData::findAll(['data_api_id' => $modelDataApi->id]);
    //         $modelsStrukturData[$i] = $strukturData;
    //         $oldStrukturDataIds = array_merge($oldStrukturDataIds,ArrayHelper::getColumn($strukturData,'id'));
    //         $modelsStrukturData[$i] = (empty($modelsStrukturData[$i])) ? [new StrukturData] : $modelsStrukturData[$i];
    //     }




    //     // handle POST
    //     if ($modelCategory->load(Yii::$app->request->post())) {

    //         // VarDumper::dump($modelCategory);


    //         // get data from POST
    //         $modelsDataApi = DynamicForms::createMultiple(DataApi::classname(), $modelsDataApi);
    //         DynamicForms::loadMultiple($modelsDataApi, Yii::$app->request->post());
    //         $newDataApiIds = ArrayHelper::getColumn($modelsDataApi,'id');

    //         // get detail data from POST
    //         $newStrukturDataIds = [];
    //         $loadsData['_csrf'] =  Yii::$app->request->post()['_csrf'];

    //         $i = 0;
    //         foreach ($modelsDataApi as $id => $value) {
    //             $loadsData['StrukturData'] =  Yii::$app->request->post()['StrukturData'][$i];
    //             if (!isset($modelsStrukturData[$id]))
    //                 $modelsStrukturData[$id] = [new StrukturData()];
    //             $modelsStrukturData[$id] = DynamicForms::createMultiple(StrukturData::classname(), $modelsStrukturData[$id], $loadsData);
    //             Model::loadMultiple($modelsStrukturData[$id], $loadsData);
    //             $newStrukturDataIds = array_merge($newStrukturDataIds, ArrayHelper::getColumn($loadsData['StrukturData'], 'id'));
    //             $i++;
    //         }

    //         // delete removed data
    //         $oldStrukturDataIds = array_diff($oldStrukturDataIds,$newStrukturDataIds);
    //         if (! empty($oldStrukturDataIds)) StrukturData::deleteAll(['id' => $oldStrukturDataIds]);

    //         $delDataApiIds = array_diff($oldDataApiIds,$newDataApiIds);
    //         if (! empty($delDataApiIds)) DataApi::deleteAll(['id' => $delDataApiIds]);

    //         // validate all models
    //         $valid = $modelCategory->validate();
    //         // $valid = DataApi::validateData($modelsDataApi,$modelsStrukturData) && $valid;

             
    //         // save deposit data
    //         if ($valid) {
    //             if ($this->saveData($modelCategory, $modelData, $modelsDataApi,$modelsStrukturData)) {
    //                 return $this->redirect(Yii::$app->request->referrer);
    //             }
    //         }
    //     }

    //     // show VIEW
    //     return $this->renderAjax('update', [
    //         'tree_id' => $tree_id,
    //         'modelData' => $modelData,
    //         'modelCategory' => $modelCategory,
    //         'modelsDataApi'  => $modelsDataApi,
    //         'modelsStrukturData' => $modelsStrukturData,
    //     ]);
    // }

    // public function saveData($modelCategory,$modelData, $modelsDataApi,$modelsStrukturData ) {
    //     $transaction = Yii::$app->db->beginTransaction();
    //     try {
    //         if ($go = $modelCategory->save(false)) {
    //             $modelData->tree_id = $modelCategory->id;
    //             $modelData->deskripsi = $_POST['Data']['deskripsi'];
    //             $modelData->status = $_POST['Data']['status'];
    //             $modelData->save(false);
    //             // loop through each data
    //             foreach ($modelsDataApi as $indexDataApi => $modelDataApi) {
    //                 // save the data record
    //                 $modelDataApi->data_id = $modelData->id;
    //                 $modelDataApi->ordering = $indexDataApi +1;
    //                 if ($go = $modelDataApi->save(false)) {
    //                     // loop through each struktur data
    //                     foreach ($modelsStrukturData[$indexDataApi] as $indexStrukturData => $modelStrukturData) {
    //                         // save the struktur data record
    //                         $modelStrukturData->data_api_id = $modelDataApi->id;
    //                         if (! ($go = $modelStrukturData->save(false))) {
    //                             $transaction->rollBack();
    //                             break;
    //                         }

    //                     }
    //                 }
    //             }
    //         }
    //         // die(print_r($go));
    //         if ($go) {
    //             $transaction->commit();
    //         }
    //     } catch (Exception $e) {
    //         $transaction->rollBack();
    //     }
    //     return $go;
    // }

    // public function actionGetDataApi()
    // {
    //     $client = new Client();
    //     $response = $client->createRequest()
    //     ->setMethod('GET')
    //     // ->setFormat(Client::FORMAT_JSON)
    //     ->addHeaders(['content-type' => 'application/json'])
    //     ->setUrl('http://data.jatengprov.go.id/apps/api/example/sds', ['gol' => 'I/a'])
    //     // ->setData(['name' => 'John Doe', 'email' => 'johndoe@example.com'])
    //     ->send();

    //     // foreach ($response as $key => $value) {
    //     //     $data[] = [
    //     //         'gol' => $value->gol,
    //     //         'jumlah' => $value->jumlah
    //     //     ];
    //     // }

    //     return $response->content;
    // }

    /**
     * Deletes an existing Data model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionDelete($id)
    // {
    //     $this->findModel($id)->delete();

    //     return $this->redirect(['index']);
    // }

    /**
     * Finds the Data model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Data the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    // protected function findModel($id)
    // {
    //     if (($model = Data::findOne($id)) !== null) {
    //         return $model;
    //     }

    //     throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    // }
}
