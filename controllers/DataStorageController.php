<?php

namespace app\controllers;

use PhpOffice\PhpSpreadsheet\IOFactory;
use Yii;
use app\models\Aplikasi;
use app\models\DataStorage;
use app\models\DataStorageSearch;
use app\models\Instansi;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * DataStorageController implements the CRUD actions for DataStorage model.
 */
class DataStorageController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DataStorage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DataStorageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DataStorage model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        // echo '<pre>';
        // var_dump(file_exists(Yii::getAlias("@webroot/uploads/storage_data/{$model->file}")));
        // echo '<br>';
        // print_r(Yii::getAlias("@uploads/storage_data/{$model->file}"));
        // echo '</pre>';
        if (!empty($model->file) && file_exists(Yii::getAlias("@uploads/storage_data/{$model->file}"))) {
            $dataset = $model->loadFile(Yii::getAlias("@web/web/uploads/storage_data/{$model->file}"));
            return $this->render('view', [
                'model' => $this->findModel($id),
                'dataProvider' => $model->getDataProvider($dataset),
                'attribute' => $model->getFileAttributeGridView($dataset),
            ]);
        }else{
            Yii::$app->session->setFlash('warning','File Belum di Upload, Silahkan Upload File Indikator Terlebihdahulu...');
            return $this->redirect(['update', 'id' => $model->id]);
        }
    }

    /**
     * Creates a new DataStorage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DataStorage();

        $list_aplikasi = ArrayHelper::map(Aplikasi::find()->asArray()->all(), 'id', 'nama_aplikasi');
        $list_instansi = ArrayHelper::map(Instansi::find()->asArray()->all(), 'id', 'nama_instansi');

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'file');
            if (!empty($file) && $file->size !== 0) {   
                $file->saveAs(Yii::getAlias("@uploads/storage_data/{$file->name}"));
                $model->file = $file->name;
            }

            if ($model->validate() && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);   
            }else{
                Yii::$app->session->setFlash('error',$model->getErrors());
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'list_aplikasi' => $list_aplikasi,
            'list_instansi' => $list_instansi,
        ]);
    }

    /**
     * Updates an existing DataStorage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_file = $model->file;

        $list_aplikasi = ArrayHelper::map(Aplikasi::find()->asArray()->all(), 'id', 'nama_aplikasi');
        $list_instansi = ArrayHelper::map(Instansi::find()->asArray()->all(), 'id', 'nama_instansi');
        
        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'file');
            if (!empty($file) && $file->size !== 0) {
                // File Diganti
                if (!empty($old_file) && file_exists(Yii::getAlias("@uploads/storage_data/{$old_file}"))) {
                    unlink(Yii::getAlias("@uploads/storage_data/{$old_file}"));    
                }
                // Upload Ulang File
                $file->saveAs(Yii::getAlias("@uploads/storage_data/{$file->name}"));
                $model->file = $file->name;
            }else{
                $model->file = $old_file;
            }
            
            if ($model->validate() && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);   
            }else{
                Yii::$app->session->setFlash('error',$model->getErrors());
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'list_aplikasi' => $list_aplikasi,
            'list_instansi' => $list_instansi,
        ]);
    }

    /**
     * Deletes an existing DataStorage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DataStorage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DataStorage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DataStorage::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /*public function loadFile($pathCsv='')
    {
        $inputFileName = Yii::$app->basePath.$pathCsv;
        $inputFileType = IOFactory::identify($inputFileName);

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
        $spreadsheet = $reader->load($inputFileName);     
        return $sheetData = $spreadsheet->getActiveSheet()->toArray();
    }*/
}
