<?php

namespace app\controllers;

use Yii;
use app\models\ApiBody;
use app\models\ApiClient;
use app\models\ApiClientSearch;
use app\models\ApiHeader;
use app\models\ApiData;
use app\models\ApiQuery;
use app\models\Model;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ApiClientController implements the CRUD actions for ApiClient model.
 */
class ApiClientController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ApiClient models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ApiClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ApiClient model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerApiBody = new \yii\data\ArrayDataProvider([
            'allModels' => $model->apiBodies,
        ]);
        $providerApiHeader = new \yii\data\ArrayDataProvider([
            'allModels' => $model->apiHeaders,
        ]);
        $providerApiQuery = new \yii\data\ArrayDataProvider([
            'allModels' => $model->apiQueries,
        ]);
        $providerApiData = new \yii\data\ArrayDataProvider([
            'allModels' => $model->apiDatas,
        ]);

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod($model->method)
            ->setUrl($model->url)
            // ->setData(['name' => 'John Doe', 'email' => 'johndoe@example.com'])
            ->send();
        if ($response->isOk) {
            // $segment = preg_split('@,@', $model->segment, NULL, PREG_SPLIT_NO_EMPTY);

            $result = $response->data[$model->segment];
        }else{
            $result = json_encode([]);
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'apiResult' => $result,
            'providerApiBody' => $providerApiBody,
            'providerApiHeader' => $providerApiHeader,
            'providerApiQuery' => $providerApiQuery,
            'providerApiData' => $providerApiData,
        ]);
    }

    /**
     * Creates a new ApiClient model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new ApiClient();

    //     if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     } else {
    //         return $this->render('create', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    /**
     * Creates a new Customer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $modelApiClient = new ApiClient;
        $modelApiHeader = [new ApiHeader];
        $modelApiQuery = [new ApiQuery];
        $modelApiBody = [new ApiBody];
        $modelApiData = [new ApiData];

        if ($modelApiClient->load(Yii::$app->request->post())) {

            $modelApiHeader = Model::createMultiple(ApiHeader::classname());
            $modelApiQuery = Model::createMultiple(ApiQuery::classname());
            $modelApiBody = Model::createMultiple(ApiBody::classname());
            $modelApiData = Model::createMultiple(ApiData::classname());
            Model::loadMultiple($modelApiHeader, Yii::$app->request->post());
            Model::loadMultiple($modelApiQuery, Yii::$app->request->post());
            Model::loadMultiple($modelApiBody, Yii::$app->request->post());
            Model::loadMultiple($modelApiData, Yii::$app->request->post());

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelApiHeader),
                    ActiveForm::validateMultiple($modelApiQuery),
                    ActiveForm::validateMultiple($modelApiBody),
                    ActiveForm::validateMultiple($modelApiData),
                    ActiveForm::validate($modelApiClient)
                );
            }

            // validate all models
            $valid = $modelApiClient->validate();
            $valid = Model::validateMultiple($modelApiHeader) && Model::validateMultiple($modelApiQuery) && Model::validateMultiple($modelApiBody) && Model::validateMultiple($modelApiData) && $valid;
            
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $modelApiClient->save(false)) {
                        foreach ($modelApiHeader as $modelApiHeaders) {
                            $modelApiHeaders->api_client_id = $modelApiClient->id;
                            if (! ($flag = $modelApiHeaders->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        foreach ($modelApiQuery as $modelApiQuerys) {
                            $modelApiQuerys->api_client_id = $modelApiClient->id;
                            if (! ($flag = $modelApiQuerys->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        foreach ($modelApiBody as $modelApiBodys) {
                            $modelApiBodys->api_client_id = $modelApiClient->id;
                            if (! ($flag = $modelApiBodys->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        foreach ($modelApiData as $modelApiDatas) {
                            $modelApiDatas->api_client_id = $modelApiClient->id;
                            if (! ($flag = $modelApiDatas->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $modelApiClient->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
            'modelApiClient' => $modelApiClient,
            'modelApiHeader' => (empty($modelApiHeader)) ? [new ApiHeader] : $modelApiHeader,
            'modelApiQuery' => (empty($modelApiQuery)) ? [new ApiQuery] : $modelApiQuery,
            'modelApiBody' => (empty($modelApiBody)) ? [new ApiBody] : $modelApiBody,
            'modelApiData' => (empty($modelApiData)) ? [new ApiData] : $modelApiData,
        ]);
    }

    /**
     * Updates an existing ApiClient model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    // public function actionUpdate($id)
    // {
    //     $model = $this->findModel($id);

    //     if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     } else {
    //         return $this->render('update', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    public function actionUpdate($id)
    {
        $modelApiClient = $this->findModel($id);
        $modelApiQuery = $modelApiClient->apiQueries;
        $modelApiHeader = $modelApiClient->apiHeaders;
        $modelApiBody = $modelApiClient->apiBodies;
        $modelApiData = $modelApiClient->apiDatas;

        if ($modelApiClient->load(Yii::$app->request->post())) {

            $oldHeaderIDs = ArrayHelper::map($modelApiHeader, 'id', 'id');
            $modelApiHeader = Model::createMultiple(ApiHeader::classname(), $modelApiHeader);
            Model::loadMultiple($modelApiHeader, Yii::$app->request->post());
            $deletedHeaderIDs = array_diff($oldHeaderIDs, array_filter(ArrayHelper::map($modelApiHeader, 'id', 'id')));

            $oldQueryIDs = ArrayHelper::map($modelApiQuery, 'id', 'id');
            $modelApiQuery = Model::createMultiple(ApiQuery::classname(), $modelApiQuery);
            Model::loadMultiple($modelApiQuery, Yii::$app->request->post());
            $deletedQueryIDs = array_diff($oldQueryIDs, array_filter(ArrayHelper::map($modelApiQuery, 'id', 'id')));

            $oldBodyIDs = ArrayHelper::map($modelApiBody, 'id', 'id');
            $modelApiBody = Model::createMultiple(ApiBody::classname(), $modelApiBody);
            Model::loadMultiple($modelApiBody, Yii::$app->request->post());
            $deletedBodyIDs = array_diff($oldBodyIDs, array_filter(ArrayHelper::map($modelApiBody, 'id', 'id')));

            $oldPageIDs = ArrayHelper::map($modelApiData, 'id', 'id');
            $modelApiData = Model::createMultiple(ApiData::classname(), $modelApiData);
            Model::loadMultiple($modelApiData, Yii::$app->request->post());
            $deletedPageIDs = array_diff($oldPageIDs, array_filter(ArrayHelper::map($modelApiData, 'id', 'id')));

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelApiData),
                    ActiveForm::validateMultiple($modelApiBody),
                    ActiveForm::validateMultiple($modelApiQuery),
                    ActiveForm::validateMultiple($modelApiHeader),
                    ActiveForm::validate($modelApiClient)
                );
            }

            // validate all models
            $valid = $modelApiClient->validate();
            $valid = Model::validateMultiple($modelApiHeader) && Model::validateMultiple($modelApiQuery) && Model::validateMultiple($modelApiBody) && Model::validateMultiple($modelApiData) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $modelApiClient->save(false)) {
                        if (! empty($deletedHeaderIDs)) {
                            ApiHeader::deleteAll(['id' => $deletedHeaderIDs]);
                        }
                        if (! empty($deletedQueryIDs)) {
                            ApiQuery::deleteAll(['id' => $deletedQueryIDs]);
                        }
                        if (! empty($deletedBodyIDs)) {
                            ApiBody::deleteAll(['id' => $deletedBodyIDs]);
                        }
                        if (! empty($deletedPageIDs)) {
                            ApiData::deleteAll(['id' => $deletedPageIDs]);
                        }
                        foreach ($modelApiHeader as $modelApiHeaders) {
                            $modelApiHeaders->api_client_id = $modelApiClient->id;
                            if (! ($flag = $modelApiHeaders->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        foreach ($modelApiQuery as $modelApiQuerys) {
                            $modelApiQuerys->api_client_id = $modelApiClient->id;
                            if (! ($flag = $modelApiQuerys->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        foreach ($modelApiBody as $modelApiBodys) {
                            $modelApiBodys->api_client_id = $modelApiClient->id;
                            if (! ($flag = $modelApiBodys->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        foreach ($modelApiData as $modelApiDatas) {
                            $modelApiDatas->api_client_id = $modelApiClient->id;
                            if (! ($flag = $modelApiDatas->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }

                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $modelApiClient->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
            'modelApiClient' => $modelApiClient,
            'modelApiHeader' => (empty($modelApiHeader)) ? [new ApiHeader] : $modelApiHeader,
            'modelApiQuery' => (empty($modelApiQuery)) ? [new ApiQuery] : $modelApiQuery,
            'modelApiBody' => (empty($modelApiBody)) ? [new ApiBody] : $modelApiBody,
            'modelApiData' => (empty($modelApiData)) ? [new ApiData] : $modelApiData,
        ]);
    }

    /**
     * Deletes an existing ApiClient model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the ApiClient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApiClient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ApiClient::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
