<?php

namespace app\controllers;

use Yii;
use app\models\Instansi;
use app\models\StatusSds;
use app\models\StatusSdsSearch;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * StatusSdsController implements the CRUD actions for StatusSds model.
 */
class StatusSdsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StatusSds models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StatusSdsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single StatusSds model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StatusSds model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $existStatus = StatusSds::find()->select('instansi_id')->asArray()->all();
        $onlyNotExistStatus = Instansi::find()->where(['not in', 'id', array_column($existStatus,'instansi_id')])->asArray()->all();
        $list_instansi = ArrayHelper::map($onlyNotExistStatus, 'id', 'nama_instansi');

        $model = new StatusSds();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'list_instansi' => $list_instansi,
        ]);
    }

    /**
     * Updates an existing StatusSds model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        // $existStatus = StatusSds::find()->select('instansi_id')->asArray()->all();
        // $onlyNotExistStatus = Instansi::find()->where(['not in', 'id', array_column($existStatus,'instansi_id')])->asArray()->all();
        // $list_instansi = ArrayHelper::map($onlyNotExistStatus, 'id', 'nama_instansi');
        
        $list_instansi = ArrayHelper::map(Instansi::find()->asArray()->all(), 'id', 'nama_instansi');
        
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'list_instansi' => $list_instansi,
        ]);
    }

    /**
     * Deletes an existing StatusSds model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the StatusSds model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StatusSds the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StatusSds::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
