<?php

namespace app\controllers;

use Yii;
use app\models\DataDasar;
use app\models\DataDasarSearch;
use app\models\RpjmOrganisasi;
use app\models\RpjmUrusan;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * DataDasarController implements the CRUD actions for DataDasar model.
 */
class DataDasarController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DataDasar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DataDasarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['created_at'=>SORT_DESC]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DataDasar model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        // $data = $this->findModel($id)->getDataRpjm();

        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';
        $model = $this->findModel($id);
        $data_rpjm = $model->getDataRpjm();

        return $this->render('view', [
            'model' => $model,
            'dataProviderData' => $model->getDataProvider($data_rpjm),
        ]);
    }

    /**
     * Creates a new DataDasar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DataDasar();

        // Tahun RPJM
        for ($i = 2019; $i < 2024; $i++) {
            $year_rpjm[] = [
                'year' => $i,
            ];
        }

        $list_instansi = ArrayHelper::map(RpjmOrganisasi::find()->asArray()->all(), 'id', 'nama_organisasi');
        $list_urusan = ArrayHelper::map(RpjmUrusan::find()->asArray()->all(), 'id', 'nama_urusan');
        $list_tahun = ArrayHelper::map($year_rpjm, 'year', 'year');

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'files');
            if (!empty($image) && $image->size !== 0) {
                $path = 'uploads/data_dasar';
                FileHelper::createDirectory($path);
                $image->saveAs($path.'/'.$image->name);
                $model->files = $path.'/'.$image->name;
                $model->file_name = $image->name;
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);   
            }else{
                Yii::$app->session->setFlash('error',$model->getErrors());
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'list_instansi' => $list_instansi,
            'list_urusan' => $list_urusan,
            'list_tahun' => $list_tahun,
        ]);
    }

    /**
     * Updates an existing DataDasar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        // Tahun RPJM
        for ($i = 2019; $i < 2024; $i++) {
            $year_rpjm[] = [
                'year' => $i,
            ];
        }

        $list_instansi = ArrayHelper::map(RpjmOrganisasi::find()->asArray()->all(), 'id', 'nama_organisasi');
        $list_urusan = ArrayHelper::map(RpjmUrusan::find()->asArray()->all(), 'id', 'nama_urusan');
        $list_tahun = ArrayHelper::map($year_rpjm, 'year', 'year');

        $model = $this->findModel($id);
        $current_file = $model->files;

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'files');
            if (!empty($image) && $image->size !== 0) {
                $path = 'uploads/data_dasar';
                FileHelper::createDirectory($path);
                $image->saveAs($path.'/'.$image->name);
                $model->files = $path.'/'.$image->name;
                $model->file_name = $image->name;
            }else{
                $model->files = $current_file;
            }
            
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                Yii::$app->session->setFlash('error',$model->getErrors());
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'list_instansi' => $list_instansi,
            'list_urusan' => $list_urusan,
            'list_tahun' => $list_tahun,
        ]);
    }

    /**
     * Deletes an existing DataDasar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (file_exists($this->findModel($id)->files)) {
            FileHelper::unlink($this->findModel($id)->files);    
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DataDasar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DataDasar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DataDasar::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
