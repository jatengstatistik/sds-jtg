<?php

namespace app\controllers;

use Yii;
use app\models\AuthInstansiUser;
use app\models\Category;
use app\models\SdiCategory;
use app\models\SdiData;
use app\models\SdiDataSearch;
use app\models\ApiResource;
use app\models\ExcelResource;
use app\models\SdiResource;
use app\models\SdiResourceSearch;
use app\models\ContactForm;
use app\models\LoginForm;
use app\models\NewTree;
use app\models\ParameterApi;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionDashboard()
    {

        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }


        // Show Data Tree
        $query = Category::find()->addOrderBy('root, lft');
        // get Data Tree By Parameter data
        if (Yii::$app->request->get('data')) {
            
            $category = Category::findOne(Yii::$app->request->get('data'));
            // echo "<pre>";
            // print_r($category->aplikasi->instansi_id);
            // print_r(AuthInstansiUser::find()->select('instansi_id')->where(['user_id' => Yii::$app->user->id])->asArray()->column());
            // echo "</pre>";
            
            // if (!array_search((!empty($category->aplikasi_id))? $category->aplikasi->instansi_id : '', AuthInstansiUser::find()->select('instansi_id')->where(['user_id' => Yii::$app->user->id])->asArray()->column())) {
            //     // Kondisi jika bukan data instansi yang bersangkutan di akses
            //     Yii::$app->session->setFlash('info', "Anda tidak di perbolehkan Mengakses Data Ini",true);
            //     return $this->redirect(Yii::$app->request->referrer);
            // }

            // if (empty($category->aplikasi_id)) {
            //     // Cek jika data tersebut ada aplikasi atau Tidak
            //     Yii::$app->session->setFlash('info', "Mohon Maaf, Silahkan Pilih Menu Data yang Tersedia ",true);
            //     return $this->redirect(Yii::$app->request->referrer);
            // }

            if ($category->status == 0 && empty($category->aplikasi_id)) {
                // Kondisi
                // Status Tidak Aktif dan Tidak ada Aplikasi
                Yii::$app->session->setFlash('info', "Data Belum Tersedia, Silahkan Pilih Data yang Tersedia",true);
                return $this->redirect(Yii::$app->request->referrer);
            }else{
                // if (!array_search($category->aplikasi->instansi_id, AuthInstansiUser::find()->select('instansi_id')->where(['user_id' => Yii::$app->user->id])->asArray()->column())) {
                if (!in_array($category->aplikasi->instansi_id, AuthInstansiUser::find()->select('instansi_id')->where(['user_id' => Yii::$app->user->id])->asArray()->column())) {
                    // Kondisi jika bukan data instansi yang bersangkutan di akses
                    Yii::$app->session->setFlash('info', "Anda tidak di perbolehkan Mengakses Data Ini",true);
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }

            $parameter = key(array_slice(Yii::$app->request->get(), -1, true));

            $instansi =(!empty($category->aplikasi)) ? $category->aplikasi->instansi->nama_instansi : '';
                       
            if ($parameter == 'data') {
                $strukturData = (!empty($category->newTree)) ? (!empty($category->newTree->parameterApi->strukturData)) ? $category->newTree->parameterApi->strukturData : '' : '';
                $title = (!empty($category->newTree)) ? $category->newTree->getBreadcrumbs(null) : '';
                $deskripsi = (!empty($category->newTree)) ? $category->newTree->deskripsi : '';
            }else{
                $getParameterApi = ParameterApi::findOne(['parameter_api' => $parameter]);
                $strukturData = $getParameterApi->strukturData;
                $title = (!empty($getParameterApi->newTree)) ? $getParameterApi->newTree->getBreadcrumbs(null) : '';
                $deskripsi = (!empty($getParameterApi->newTree)) ? $getParameterApi->newTree->deskripsi : '';
                
            }

            $parameterArray = Yii::$app->request->get();
            $parameterArrayFilter = array_splice($parameterArray, 2);

            // Catatan
            // $parameterArrayUnsetData = array_splice($parameterArrayFilter,1,1);

            // Ambil Data dari relasi data=xxxx
            // $parameterArrayData = $category->newTree->slug_new_tree];

            // Untuk PHP 7.0
            // Ambil Data dari relasi data=xxxx Jika ada data sama otomatis -2xx , di replace agar dat yang di ambil tetap
            // $parameterArrayData = ['data'=> ereg_replace("(-\d)+", "", $category->newTree->slug_new_tree)];

            // Untuk PHP 5.6
            // Ambil Data dari relasi data=xxxx Jika ada data sama otomatis -2xx , di replace agar dat yang di ambil tetap
            
            
            if (!empty($category->aplikasi)) {

                if (!empty($category->newTree)) {
                    // $parameterArrayData = ['data'=> preg_replace("(-\d)", "", $category->newTree->slug_new_tree)];
                    // $parameterArrayData = ['data'=> preg_replace("(-\d)", "", $category->newTree->parameterApi->parameter_api)];
                    $parameterArrayData = ['data'=> $category->newTree->parameterApi->parameter_api];
                    $newParamArray = array_merge($parameterArrayData,$parameterArrayFilter);
                }

                // Get API Data
                $client = new Client();
                $clientResponse = $client->createRequest()
                    ->setMethod('GET')
                    ->setUrl($category->aplikasi->url_aplikasi)
                    ->setData($newParamArray)
                    // ->setOptions([
                        // 'proxy' => 'tcp://proxy.example.com:5100', // use a Proxy
                        // 'timeout' => 5, // set timeout to 5 seconds for the case server is not responding
                    // ])
                    ->send();

                try {
                    $clientResponseData = (!empty($clientResponse->data)) ? $clientResponse->data : null;
                } catch (\Exception $e) {
                    // \Yii::error("Json api response error: " . $e->getMessage() . ". Response: \n{}", self::className());
                    // $clientResponseData = [];
                    Yii::$app->session->setFlash('error', "Data Tidak Tersedia, Silahkan Pilih Sub Menu Lainya...",true);
                    return $this->redirect(Yii::$app->request->referrer);
                }

                if (!empty($strukturData) && $clientResponse->isOk) {
                    return $this->render('index',[
                        'title' => (!empty($title)) ? $title : null,
                        'deskripsi' => (!empty($deskripsi)) ? $deskripsi : null,
                        'query' => $query,
                        'instansi' => $instansi,
                        'strukturData' => $strukturData,
                        'clientResponse' => $clientResponseData,
                        'countClientResponse' => (!empty(count($clientResponseData))) ? count($clientResponseData) : null,
                        // 'countClientResponse' => (!empty(count($clientResponse->data))) ? count($clientResponse->data) : null,
                    ]);

                }else{
                    Yii::$app->session->setFlash('error', "Data Tidak Tersedia, Silahkan Pilih Sub Menu Lainya...",true);
                    return $this->redirect(Yii::$app->request->referrer);
                    // return $this->render('index', [
                    //     'query' => $query,
                    // ]);
                }
            // Close Condittion Category Aplikasi
            }

        // Close Get Data From Url
        }
        
        return $this->render('index', [
            'query' => $query,
        ]);
    }

    /**
     * Displays Dashboard Data.
     *
     * @return string
     */
    public function actionIndex()
    {

        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        // Show Data Tree
        $query = SdiCategory::find()->addOrderBy('root, lft');
        // get Dafatr Data
        // $request = Yii::$app->request->get();
        if (Yii::$app->request->get('category')) {

            $searchModel = new SdiDataSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->where(['sdi_category_id'=>Yii::$app->request->get('category')]);

            // Get Resources 
            if (Yii::$app->request->get('category') && Yii::$app->request->get('data')) {
                $searchModelResource = new SdiResourceSearch();
                $dataProviderResource = $searchModelResource->search(Yii::$app->request->queryParams);
                $dataProviderResource->query->where(['sdi_data_id'=>Yii::$app->request->get('data')]);

                // Get Resources Data
                if (Yii::$app->request->get('category') && Yii::$app->request->get('data') && Yii::$app->request->get('resource')) {
                    $model = $this->findModelResource(Yii::$app->request->get('resource'));

                    if (empty($model->uploaded_file_id)):
                        return $this->render('dashboard', [
                            'model' => $this->findModelApi(Yii::$app->request->get('resource')),
                            'title' => 'Seris Data',
                            'query' => $query,
                            'searchModel' => $searchModelResource,
                            'dataProvider' => $dataProviderResource,
                        ]);
                    else:
                        return $this->render('dashboard', [
                            'model' => $this->findModelFile(Yii::$app->request->get('resource')),
                            'title' => 'Seris Data',
                            'query' => $query,
                            'searchModel' => $searchModelResource,
                            'dataProvider' => $dataProviderResource,
                        ]);
                    endif;
                }

                return $this->render('dashboard', [
                    'title' => 'Series Data',
                    'query' => $query,
                    'searchModel' => $searchModelResource,
                    'dataProvider' => $dataProviderResource,
                ]);
            }

            return $this->render('dashboard', [
                'title' => 'Daftar Data',
                'query' => $query,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);

        }
        
        return $this->render('dashboard', [
            'query' => $query,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    protected function findModelResource($id)
    {
        if (($model = SdiResource::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    protected function findModelFile($id)
    {
        if (($model = ExcelResource::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    protected function findModelApi($id)
    {
        if (($model = ApiResource::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
