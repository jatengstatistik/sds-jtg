<?php

namespace app\controllers;

use Yii;
use app\models\CategorySearch;
use yii\helpers\VarDumper;

class StatistikController extends \yii\web\Controller
{
    public function actionIndex()
    {
    	$searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider->query->where(['status' => 1]);
        $dataProvider->query->having('aplikasi_id >:aplikasi_id')->addParams([':aplikasi_id'=> 0]);
        // var_dump($dataProvider->getTotalCount());
        return $this->render('index',[
        	'jumlah_jenis_data' => $dataProvider->getTotalCount(),
        	'jumlah_instansi' => '',
        ]);
    }

}
