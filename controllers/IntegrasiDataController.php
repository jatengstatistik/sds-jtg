<?php

namespace app\controllers;

use Yii;
use app\models\Category;
use app\models\CategorySearch;
// use app\models\ContactForm;
// use app\models\LoginForm;
// use app\models\NewTree;
// use app\models\ParameterApi;
// use yii\filters\AccessControl;
use yii\filters\VerbFilter;
// use yii\httpclient\Client;
use yii\web\Controller;
// use yii\web\Response;

class IntegrasiDataController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        // $query = Category::find()->where(['status'=>'aktif'])->all();
        // echo "<pre>";
        // print_r($query);
       // echo "</pre>";
       // 
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider->query->where(['status' => 1]);
        $dataProvider->query->having('aplikasi_id >:aplikasi_id')->addParams([':aplikasi_id'=> 0]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

        
    }

   
}
