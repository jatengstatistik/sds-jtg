<?php

namespace app\components;
 
use Yii;
use yii\base\Component;
 
class ArrayDataProvider extends \yii\data\ArrayDataProvider
{
 
    /*
     *  @inheritdoc
     */
    protected function prepareModels()
    {
        if (($models = $this->allModels) === null) {
            return [];
        }
 
        if (($sort = $this->getSort()) !== false) {
            $models = $this->sortModels($models, $sort);
        }
 
        if (($pagination = $this->getPagination()) !== false) {
            $pagination->totalCount = $this->getTotalCount();
        }
 
        return $models;
    }
 
    /*
     *       @inheritdoc
     */
    protected function prepareTotalCount()
    {
        return $this->getTotalCount();
    }
 
}