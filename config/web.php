<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$helpers = require __DIR__ . '/helpers.php';

$config = [
    'id' => 'single-data-system',
    'name' => 'Single Data System',
    'timeZone' => 'Asia/Jakarta',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower'    => '@vendor/bower-asset',
        '@npm'      => '@vendor/npm-asset',
        '@uploads'  => '@app/web/uploads',
    ],
    'as access' => [
         'class' => '\hscstudio\mimin\components\AccessControl',
         'allowActions' => [
            // add wildcard allowed action here!
            // only in dev mode
            // 'site/*',
            // 'debug/*',
            // 'mimin/*', // only in dev mode
            // 'gii/*', // only in dev mode
            // 'user/*', // only in dev mode
            // 'status/*', // only in dev mode
            // 'site/login', 
            // 'site/error',
        ],

    ],
    'modules' => [
        'controllerMap' => [
            'file' => 'mdm\upload\FileController', // use to show or download file
        ],
        'mimin' => [
            'class' => '\hscstudio\mimin\Module',
        ],
        'gridview' => [
          'class' => '\kartik\grid\Module',
          // see settings on http://demos.krajee.com/grid#module
        ],
        'datecontrol' => [
          'class' => '\kartik\datecontrol\Module',
          // see settings on http://demos.krajee.com/datecontrol#module
        ],
        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
            // 'unsetAjaxBundles' => [
            //     'yii\web\YiiAsset',
            //     'yii\web\JqueryAsset',
            //     'yii\widgets\ActiveFormAsset',
            //     'yii\validators\ValidationAsset'
            // ]
            // other module settings, refer detailed documentation
        ]
    ],
    'components' => [
        'formatter' => [
            'dateFormat' => 'd-M-Y H:i:s',
            'timeFormat' => 'H:i:s',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => 'IDR',
            'locale' => 'id-ID',
            'defaultTimeZone' => 'Asia/Jakarta',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager', // only support DbManager
        ],
        'assetManager' => [
            // Enable in Production..
            'linkAssets' => true,
            'appendTimestamp' => true,
        ], 
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'NfbfQVOisttygLHe-CVzsJgTilxLSBMK',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => false,
            'authTimeout' => 3600, // auth expire 
            // 'calss' => 'WebUser',
        ],
        'session' => [
            'class' => 'yii\web\Session',
            'cookieParams' => ['httponly' => true, 'lifetime' => 3600 * 4],
            'timeout' => 3600*4, //session expire
            'useCookies' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,        
        'urlManager' => [
            'baseUrl' => '/', // localhost/yii2advance/admin
            'enablePrettyUrl' => true,
            // 'showScriptName' => true,
            // Disable index.php
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    // '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/testing/app'
                    '@app/views' => '@app/views/layouts-adminlte'
                ],
            ],
        ],
        
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not confnecting from localhost.
        // 'allowedIPs' => ['127.0.0.1', '::1'],
        // 'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // 'password'=>'',
        // uncomment the following to add your IP if you are not connecting from localhost.
        // 'allowedIPs' => ['*'],
        // 'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
